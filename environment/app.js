var app = angular.module("myApp", []); 
app.controller("departureCntroller", function($scope,$http) {
	
	$scope.test="prueba";
    $scope.price=2;
    $scope.updDepartureCity = function(city,path,tourId)
    {
    	
    	$http.get(path +'services/tourDetailService.php?action=updateDepartureCity&newDepCity='+ city +'&tourId='+tourId).success(function(estudios){

           window.location.reload();
        })
    };
});

app.controller("passengerController",function($scope,$http){
	

 	
	$scope.updStates = function(country,path){

		
			
			$http.get(path + 'services/optionService.php?action=getStatesbyCountry&country=' +country).success(function(data){
			var $el = $("#state");
			$el.empty();
				$.each(data, function(i, item) {
  						 
  						$el.append($("<option></option>").attr("value", i).text(item));
				})
				
			})
			

		}
	$scope.updStatesOrganization = function(country,path){

		$http.get(path + 'services/optionService.php?action=getStatesbyCountry&country=' +country).success(function(data){
			var $el = $("#state.organizationAddress");
			$el.empty();
				$.each(data, function(i, item) {
  						 
  						$el.append($("<option></option>").attr("value", i).text(item));
				})
				
			})
	}
	
	
	$scope.submit = function(valid,clientId,path)
	{		
		var title = $('#selectTitles').val();
		var firstName = $('#inputFirstName').val();
		var lastName = $('#inputLastName').val();
		var role = $('#selectRole').val();
		var status = $('#selectStatus').val();	
		var phones = $('.phone');
		var emails =  $('.email');
		var passport = $('.passport');
		var address = $('.address');

		var addressUrl = path +"services/clientDetailService.php?action=updateAddress&clientId="+clientId;
		$.each(address, function( index, value ) {
				addressUrl=addressUrl + "&"+value.id + "=" +value.value;
 			 
			});

		var addressUrl = encodeURI(addressUrl);
		


		$http.get(path +'services/clientDetailService.php?action=updatePassenger&title='+ title +'&firstName='+firstName+'&lastName='+lastName+'&role='+role+'&status='+status+'&clientId='+clientId).success(function(data){

			$.each(phones, function( index, value ) {
				//console.log(path +'services/clientDetailService.php?action=updateContact&contactId=' +value.id+'&contactValue='+value.value+'&clientId='+clientId);
 			 $http.get(path +'services/clientDetailService.php?action=updateContact&contactId=' +value.id+'&contactValue='+value.value+'&clientId='+clientId).success(function(dataContact){})
			});

			$.each(emails, function( index, value ) {
				//console.log(path +'services/clientDetailService.php?action=updateContact&contactId=' +value.id+'&contactValue='+value.value+'&clientId='+clientId);
 			 $http.get(path +'services/clientDetailService.php?action=updateContact&contactId=' +value.id+'&contactValue='+value.value+'&clientId='+clientId).success(function(dataContact){})
			});

			$.each(passport, function( index, value ) {
				//console.log(path +'services/clientDetailService.php?action=updateInformation&informationId=' +value.id+'&contactValue='+value.value+'&clientId='+clientId);
 			 $http.get(path +'services/clientDetailService.php?action=updateInformation&informationId=' +value.id+'&informationValue='+value.value+'&clientId='+clientId).success(function(dataContact){})
			});

			$http.get(addressUrl).success(function(dataAddress)
				{
					window.location.reload();
				});
			
        	

       	})


			

	}


		$scope.addPassenger = function(tourId,path,tourName,leaderFullName)
    {


    	if(
			($scope.firstName != undefined) && 
			($scope.firstName !="") &&
			($scope.lastName != undefined) &&
			($scope.lastName !="") &&
			($scope.phone != undefined) &&
			($scope.phone !="") &&
			($scope.email != undefined) && 
			($scope.email !="")
		){

    	$http.get(
    		path +
    		'services/clientDetailService.php?action=addPassenger'+
    		'&email='+$scope.email+
    		'&firstName='+$scope.firstName+
    		'&lastName='+$scope.lastName+
    		'&tourId='+tourId+
    		'&phone='+$scope.phone+
    		'&tourName='+tourName+
    		'&leaderFullName='+leaderFullName
    		)
    	.success(function(response){

    		if(response.message == ""){

    			window.location.reload();
    		}
    		else{

    			alert(response.message);
    			
    		}

           
        })

		
		}
		else
		{

			alert("All fields required.");
		}
	
    };
 
});


app.controller("priceController", function($scope,$http) {
	
	$scope.init=function(value){
    
	var priceFormat = value.replace(",","");
	$scope.price=priceFormat;
	

	}
	
    $scope.updateTourPrice=function(tourId,path){
    	var price= $('#tourPriceId').val();
		if (price!="")
		 {

		 	$http.get(path +'services/tourDetailService.php?action=updateTourPrice&price='+ price +'&tourId='+tourId).success(function(estudios){

           window.location.reload();
        })

		 }
		 else
		 {
		 	alert("Field required.");
		 }


    }
   
});

app.controller("tourPriceController",function($scope,$http){

	$scope.updatePrice=function(path,tourPriceId,price){

		alert("Hola");
	}
	$scope.submit = function(valid,path){

		var tourName = $('#selectTourName').val();
		var departureCity = $('#selectDepartureCity').val();
		var departureDate = $('#departureDate').val();

		$http.get(path +"services/tourPriceService.php?action=searchTourPrice&tourName="+tourName+"&departureCity="+departureCity+"&departureDate="+departureDate).success(function(data){
			
			var el =$('#tourPriceResult tbody');
			el.empty();
			$.each(data, function(i, item) {
				 		var row = $('<tr></tr>')
  						$('<td></td>').text(item.tourName).appendTo(row);
  						$('<td></td>').text(item.tourDepartureCity).appendTo(row);
  						$('<td></td>').text(item.tourDepartureDate).appendTo(row);
  						$('<td><input type="number" value="'+item.tourPrice+'" id=tourPriceId'+item.tourPriceId+' required/></td>').appendTo(row);
  						$('<td><button onclick="updatePrice('+item.tourPriceId+')">edit</button></td>').appendTo(row);
  						$('#tourPriceResult tbody').append(row);
				})

		})
	}

});

app.controller("tourController", function($scope,$http) {


	$scope.deleteTour=function(tourId,path){

		$http.get(
    		path +
    		'services/tourDetailService.php?action=deleteTour'+
    		'&tourId='+tourId
    		
    		)
    	.success(function(response){

    		if(response.message == ""){

    			window.location.reload();
    		}
    		else{

    			alert(response.message);
    		}
	
           
        })

		
		
	}
	
	
});

