<?php

require_once("../classes/system/CMySql.php");
require_once("../classes/system/CLibrary.php");
require_once("../classes/system/CConfig.php");
require_once("../classes/system/CSql.php");
require_once("../classes/system/CLanguage.php");
require_once("../classes/system/CUser.php");
require_once("../classes/CCommon.php");
require_once("../classes/CApi.php");

$Library = new CLibrary();
$Common = new CCommon();

$connection = array(
	"host"=>"localhost",
	"login"=>"root",
	"pass"=>"",
	"db"=>"gst-backoffice"
);

$Sql = new CSql("DEFAULT", $connection["host"], $connection["login"], $connection["pass"], $connection["db"]);

$Config = new CConfig();


function updatePassenger(){

	$db = new DB_Sql;  
	
	$title = $_REQUEST['title'];
	$firstName = $_REQUEST['firstName'];
	$lastName = $_REQUEST['lastName'];
	$role = $_REQUEST['role'];
	$status = $_REQUEST['status'];
	$clientId = $_REQUEST['clientId'];

	$query = "Update clients set title='".$title."', firstname='".$firstName."',lastname='".$lastName."', role='".$role."', status ='".$status."' where client_id=".$clientId;	
	$result = $db->query($query);

	echo json_encode($result);


}
function updateContact(){

$db = new DB_Sql;  
$contactId = $_REQUEST['contactId'];
$contactValue = $_REQUEST['contactValue'];
$contactValue = str_replace("-", "", $contactValue);
$contactValue = ltrim($contactValue);
$clientId = $_REQUEST['clientId'];
$query = "Update `clients-contacts` set contact='".$contactValue."' where contact_id='".$contactId."' and client_id='".$clientId."';";
$result = $db->query($query);
echo json_encode($result);
}

function updateInformation() {
$db = new DB_Sql;  
$informationId = $_REQUEST['informationId'];
$informationValue = $_REQUEST['informationValue'];
$contactValue = str_replace("-", "", $contactValue);
$contactValue = ltrim($contactValue);
$clientId = $_REQUEST['clientId'];
$query = "Update `clients-informations` set information='".$informationValue."' where information_id='".$informationId."' and client_id='".$clientId."';";
$result = $db->query($query);
echo json_encode($result);
}

function updateAddress(){
$db = new DB_Sql;  
$clientId = $_REQUEST['clientId'];
$query = "Update `clients-addresses` set  ";
$canUpdate = false;
if (isset($_REQUEST['country'])) {
	
	$query.= " country = '".$_REQUEST['country']."' ,";
	$canUpdate = true;
}
if (isset($_REQUEST['state'])) {
	
	$query.= " state = '".$_REQUEST['state']."' ,";
}

if (isset($_REQUEST['city'])) {
	
	$query.= " city = '".$_REQUEST['city']."' ,";
}

if (isset($_REQUEST['zip'])) {
	
	$query.= " zip = '".$_REQUEST['zip']."' ,";
}
if (isset($_REQUEST['address1'])) {
	
	$query.= " address1 = '".$_REQUEST['address1']."' ,";
}
if (isset($_REQUEST['address2'])) {
	
	$query.= " address2 = '".$_REQUEST['address2']."' ,";
}
if (isset($_REQUEST['address3'])) {
	
	$query.= " address3 = '".$_REQUEST['address3']."' ,";
}

if ($canUpdate) {	
	$query = rtrim($query, ",") ;
	$query.= "where client_id='".$clientId."';";
	$result = $db->query($query);
	echo $result;
}

}


function addPassenger(){


	$email = $_REQUEST['email'];
	$firstName = $_REQUEST['firstName'];
	$lastName = $_REQUEST['lastName'];
	$phone = $_REQUEST['phone'];

	$tourId = $_REQUEST['tourId'];
	$tourName = $_REQUEST['tourName'];
	$leaderFullName = $_REQUEST['leaderFullName'];


	$api = new SoapClient(null, array("location" => __PATH_API__, "uri" => __PATH_API__, "login" => $GLOBALS["Library"]->Decrypt(__API_USERNAME__), "password" => $GLOBALS["Library"]->Decrypt(__API_PASSWORD__), "trace" => 1, "exceptions" => 0));


	if($api->GetClient($email,"email")){

		echo'{ "message": "USER_ALREADY_REGISTERED"}';
		return false;
	}

	$pass = $GLOBALS["Library"]->GenPass();
	$add = array(
 				"firstname"=>$firstName,
				"lastname"=>$lastName,
				"email"=>$email,
 				"phone"=>$phone,
 				"password"=>$pass,
 				"role"=>"passenger");


	if ($client_id = $api->Register(array("tour_id"=>$tourId),$add) ){

		$data = array(
 				"LEADER_TITLE"=>"",
				"TOUR_NAME"=>$tourName,
				"PASSWORD"=>$pass,
 				"LEADER_FIRSTNAME"=>$leaderFullName,
 				"LEADER_LASTNAME"=>"");

		$api->SendEmail($client_id, "join-passenger", __LANGUAGE__, $data, false);

		echo'{ "message": ""}';

	}
	else{

		echo'{ "message": "CANT_JOIN_PASSENGER"}';
		return false;
	}

}



if ($_REQUEST['action'])
{
    switch($_REQUEST['action']){
        CASE "updatePassenger":updatePassenger(); 
            break;
        CASE "updateContact":updateContact();
            break;
        CASE "updateInformation":updateInformation();
            break;
        CASE "updateAddress":updateAddress();
            break;
        CASE "addPassenger":addPassenger();
            break;


    }

}