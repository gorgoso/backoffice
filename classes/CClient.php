<?php

/*

	CClient.php
	------------------------------------------

	Version: 			2.00
	Last change: 	26.6.2011

*/

class CClient {

	public function Get($id, $type = "client_id") {

		//$sqldata = array("hash"=>$id, "key"=>__CRYPT_KEY__);
		//$results = $GLOBALS["Sql"]->Fetch("SELECT * FROM `clients` WHERE SHA2(CONCAT(:key, `email`), 256)=:hash", $sqldata);

		if (!$data["client"] = $GLOBALS["Sql"]->Fetch("SELECT * FROM `clients` WHERE `status` NOT IN ('deleted') AND `$type`=:id", array("id"=>$id), "client_id")) return false;
		$data["address"] = $GLOBALS["Sql"]->Fetch("SELECT * FROM `clients-addresses` WHERE `status`='active' AND `client_id`=:client_id", array("client_id"=>$data["client"]["client_id"]), "address_id");
		$data["contact"] = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `clients-contacts` WHERE `status`='active' AND `client_id`=:client_id", array("client_id"=>$data["client"]["client_id"]), "type");
		$data["information"] = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `clients-informations` WHERE `status`='active' AND `client_id`=:client_id", array("client_id"=>$data["client"]["client_id"]), "type");
		$data["request"] = $GLOBALS["Sql"]->Fetch("SELECT * FROM `clients-requests` WHERE `status`='active' AND `client_id`=:client_id", array("client_id"=>$data["client"]["client_id"]), "request_id");
		$data["organization"] = $GLOBALS["Sql"]->Fetch("SELECT * FROM `clients-organizations` WHERE `status`='active' AND `client_id`=:client_id", array("client_id"=>$data["client"]["client_id"]), "organization_id");
		$data["payments"] = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `clients-payments` WHERE `status` NOT IN('deleted') AND `client_id`=:client_id", array("client_id"=>$data["client"]["client_id"]), "payment_id");

		return $data;

	}

	public function Add($tour_id, $client) {

		if (!is_array($client)) return false;

		$save = array(
			"add_date"=>time(),
			"tour_id"=>$tour_id,
			"role"=>$client["role"],
			"email"=>$GLOBALS["Format"]->Lowercase($client["email"]),
			"password"=>$GLOBALS["Common"]->Password($client["password"]),
			"firstname"=>ucwords($GLOBALS["Format"]->Lowercase($client["firstname"])),
			"lastname"=>ucwords($GLOBALS["Format"]->Lowercase($client["lastname"])),
			"credit"=>"0",
			"status"=>"pending"
		);

		if (!$client_id = $GLOBALS["Sql"]->Insert("clients", $save, "client_id")) return false;

		$GLOBALS["Common"]->AddHistory("Client added successfuly. (ID: ${client_id})", "clients", $client_id);

		return $client_id;

	}

	public function Update($client_id, $client) {

		if (!is_array($client)) return false;

		$save = array(
			"title"=>$client["title"],
			"firstname"=>ucwords($GLOBALS["Format"]->Lowercase($client["firstname"])),
			"lastname"=>ucwords($GLOBALS["Format"]->Lowercase($client["lastname"])),
		);

		if (!$GLOBALS["Sql"]->Update("clients", $save, "client_id", $client_id)) return false;

		$GLOBALS["Common"]->AddHistory("Client update successfuly. (ID: ${client_id})", "clients", $client_id);

		return true;

	}

	public function UpdatePassword($client_id, $password) {

		if (!$GLOBALS["Sql"]->Update("clients", array("password"=>$GLOBALS["Common"]->Password($password)), "client_id", $client_id)) return false;

		$GLOBALS["Common"]->AddHistory("Client password changed successfuly. (ID: ${client_id})", "clients", $client_id);

		return true;

	}

	public function SetStatus($client_id, $status) {

		if (!$client = $this->Get($client_id)) return false;

		$types = $GLOBALS["Library"]->GetStatuses("clients");

		if (!in_array($status, array_keys($types))) return false;

		if ($client_id = $client["client_id"]) {

			if (!$GLOBALS["Sql"]->Update("clients", array("status"=>$status), "client_id", $client_id)) return false;

			$GLOBALS["Common"]->AddHistory("Client status set successfuly. (ID: $client_id, status: ".$types[$status].")", "statuses", $client_id);

		}

		return true;

	}

	public function Contact($client_id, $type, $contact, $notice = NULL) {

		$save = array(
			"client_id"=>$client_id,
			"add_date"=>time(),
			"type"=>$type,
			"contact"=>$contact,
			"notice"=>$notice,
			"status"=>"active"
		);

		if ($contact_id = $GLOBALS["Sql"]->Value("clients-contacts", "contact_id", "client_id", $client_id, "AND `type`=:type AND `status`='active'", array("type"=>$type))) {

			if (!$GLOBALS["Sql"]->Update("clients-contacts", $save, "contact_id", $contact_id)) return false;

			$GLOBALS["Common"]->AddHistory("Contact changed successfuly. (ID: ${contact_id})", "contacts", $client_id);

		} else {

			if (!$contact_id = $GLOBALS["Sql"]->Insert("clients-contacts", $save, "contact_id")) return false;

			$GLOBALS["Common"]->AddHistory("Contact added successfuly. (ID: ${contact_id})", "addresses", $client_id);

		}

		return true;

	}

	public function AddContact($client_id, $type, $contact, $notice = NULL) {

		$save = array(
			"client_id"=>$client_id,
			"add_date"=>time(),
			"type"=>$type,
			"contact"=>$contact,
			"notice"=>$notice ? $notice : NULL,
			"status"=>"active"
		);

		if (!$contact_id = $GLOBALS["Sql"]->Insert("clients-contacts", $save, "contact_id")) return false;

		$GLOBALS["Common"]->AddHistory("Contact added successfuly. (ID: ${contact_id})", "contacts", NULL, $client_id);

		return true;

	}

	public function DeleteContact($contact_id) {

		if (!$GLOBALS["Sql"]->Update("loans-contacts", array("status"=>"deleted", "del_date"=>time()), "contact_id", $contact_id)) return false;

		$types = $GLOBALS["Library"]->GetOptions("clients_contacts_types");
		$contact = $GLOBALS["Sql"]->Fetch("SELECT * FROM `loans-contacts` WHERE `contact_id`=:contact_id", array("contact_id"=>$contact_id));

		$GLOBALS["Common"]->AddHistory("Contacto borrado exitosamente. (ID: ${contact_id}, tipo: ".$types[$contact["type"]].", contacto: ".$contact["contact"].")", "contacts", NULL, $contact["loan_id"]);

		return true;

	}

	public function Address($client_id, $address) {

		$save = array(
			"client_id"=>$client_id,
			"add_date"=>time(),
			"address1"=>$address["address1"],
			"address2"=>$address["address2"],
			"address3"=>$address["address3"],
			"city"=>$address["city"],
			"zip"=>$address["zip"],
			"state"=>$address["state"],
			"country"=>$address["country"],
			"status"=>"active"
		);

		if ($address_id = $GLOBALS["Sql"]->Value("clients-addresses", "address_id", "client_id", $client_id, "AND `status`='active'")) {

			if (!$GLOBALS["Sql"]->Update("clients-addresses", $save, "address_id", $address_id)) return false;

			$GLOBALS["Common"]->AddHistory("Address changed successfuly. (ID: ${address_id})", "addresses", $client_id);

		} else {

			if (!$address_id = $GLOBALS["Sql"]->Insert("clients-addresses", $save, "address_id")) return false;

			$GLOBALS["Common"]->AddHistory("Address added successfuly. (ID: ${address_id})", "addresses", $client_id);

		}

		return true;

	}

	public function DeleteAddress($address_id) {

		if (!$GLOBALS["Sql"]->Update("clients-addresses", array("status"=>"deleted", "del_date"=>time()), "address_id", $address_id)) return false;

		$client_id = $GLOBALS["Sql"]->Value("clients-addresses", "client_id", "address_id", $address_id);

		$GLOBALS["Common"]->AddHistory("Address deleted successfuly. (ID: ${address_id})", "addresses", $client_id);

		return true;

	}

	public function Information($client_id, $type, $information, $notice = NULL) {

		$save = array(
			"client_id"=>$client_id,
			"add_date"=>time(),
			"type"=>$type,
			"information"=>$information,
			"notice"=>$notice,
			"status"=>"active"
		);

		if ($information_id = $GLOBALS["Sql"]->Value("clients-informations", "information_id", "client_id", $client_id, "AND `type`=:type AND `status`='active'", array("type"=>$type))) {

			if (!$GLOBALS["Sql"]->Update("clients-informations", $save, "information_id", $information_id)) return false;

			$GLOBALS["Common"]->AddHistory("Information changed successfuly. (ID: ${information_id})", "informations", $client_id);

		} else {

			if (!$information_id = $GLOBALS["Sql"]->Insert("clients-informations", $save, "information_id")) return false;

			$GLOBALS["Common"]->AddHistory("Information added successfuly. (ID: ${information_id})", "addresses", $client_id);

		}

		return true;

	}

	public function AddInformation($client_id, $type, $information, $notice = NULL) {

		$save = array(
			"client_id"=>$client_id,
			"add_date"=>time(),
			"type"=>$type,
			"information"=>$information,
			"status"=>"active"
		);

		if ($notice) $save["notice"] = $notice;

		if (!$information_id = $GLOBALS["Sql"]->Insert("clients-informations", $save, "information_id")) return false;

		$GLOBALS["Common"]->AddHistory("Information added successfuly. (ID: ${information_id})", "information", $client_id);

		return true;

	}

	public function DeleteInformation($information_id) {

		if (!$GLOBALS["Sql"]->Update("clients-informations", array("status"=>"deleted", "del_date"=>time()), "information_id", $information_id)) return false;

		$client_id = $GLOBALS["Sql"]->Value("clients-informations", "client_id", "information_id", $information_id);

		$GLOBALS["Common"]->AddHistory("Information deleted successfuly. (ID: ${information_id})", "information", $client_id);

		return true;

	}

	public function Request($client_id, $request) {

		$save = array(
			"client_id"=>$client_id,
			"add_date"=>time(),
			"text"=>$request,
			"status"=>"active"
		);

		if ($request_id = $GLOBALS["Sql"]->Value("clients-requests", "request_id", "client_id", $client_id, "AND `status`='active'")) {

			if (!$GLOBALS["Sql"]->Update("clients-requests", $save, "request_id", $request_id)) return false;

			$GLOBALS["Common"]->AddHistory("Special request changed successfuly. (ID: ${request_id})", "requests", $client_id);

		} else {

			if (!$request_id = $GLOBALS["Sql"]->Insert("clients-requests", $save, "request_id")) return false;

			$GLOBALS["Common"]->AddHistory("Special request added successfuly. (ID: ${request_id})", "requests", $client_id);

		}

		return true;

	}

	public function Organization($client_id, $organization) {

		$save = array(
			"add_date"=>time(),
			"client_id"=>$client_id,
			"name"=>ucwords($GLOBALS["Format"]->Lowercase($organization["name"])),
			"type"=>$organization["type"],
			"denomination"=>$organization["denomination"],
			"members"=>$organization["members"],
			"supporters"=>$organization["supporters"],
			"participants"=>$organization["participants"],
			"address1"=>$organization["address1"],
			"address2"=>$organization["address2"],
			"address3"=>$organization["address3"],
			"city"=>$organization["city"],
			"zip"=>$organization["zip"],
			"state"=>$organization["state"],
			"country"=>$organization["country"],
			"phone"=>$organization["phone"],
			"email"=>$organization["email"],
			"www"=>$organization["www"]
		);

		if ($organization_id = $GLOBALS["Sql"]->Value("clients-organizations", "organization_id", "client_id", $client_id, "AND `status`='active'")) {

			if (!$GLOBALS["Sql"]->Update("clients-organizations", $save, "organization_id", $organization_id)) return false;

			$GLOBALS["Common"]->AddHistory("Organization changed successfuly. (ID: ${organization_id})", "organizations", $client_id);

		} else {

			if (!$organization_id = $GLOBALS["Sql"]->Insert("clients-organizations", $save, "organization_id")) return false;

			$GLOBALS["Common"]->AddHistory("Organization added successfuly. (ID: ${organization_id})", "organizations", $client_id);

		}

		return true;

	}

	public function AddOrganization($client_id, $name, $denomination = NULL, $members = NULL, $address1 = NULL, $address2 = NULL, $address3 = NULL, $city = NULL, $zip = NULL, $state = NULL, $country = NULL) {

		$save = array(
			"add_date"=>time(),
			"client_id"=>$client_id,
			"name"=>ucwords($GLOBALS["Format"]->Lowercase($name)),
			"denomination"=>$denomination,
			"members"=>$members,
			"address1"=>$address1,
			"address2"=>$address2,
			"address3"=>$address3,
			"city"=>$city,
			"zip"=>$zip,
			"state"=>$state,
			"country"=>$country
		);

		if (!$organization_id = $GLOBALS["Sql"]->Insert("clients-organizations", $save, "organization_id")) return false;

		$GLOBALS["Common"]->AddHistory("Organization added successfuly. (ID: ${organization_id})", "organizations", NULL, $client_id);

		return true;

	}

	public function DeleteOrganization($job_id) {

		$client_id = $GLOBALS["Sql"]->Value("job2cli", "client_id", "job_id", $job_id);

		if (!$client = $this->Get($client_id)) return false;

		if ($job = $client["jobs"][$job_id]) {

			if (!$GLOBALS["Sql"]->Update("job2cli", array("status"=>"deleted", "del_date"=>time()), "job_id", $job_id)) return false;

			$GLOBALS["Common"]->AddHistory("Job deleted successfuly. (ID: $job_id, company: ".$job["company"].", start: ".$job["start_date"].", position: ".$job["position"].", salary: ".$job["salary"].")", "jobs", $client_id);

		}

		return true;

	}

	public function GetPayment($payment_id) {

		if (!$payment = $GLOBALS["Sql"]->Fetch("SELECT CP.*, `tour`, `code` FROM `clients-payments` CP INNER JOIN `clients` USING(`client_id`) INNER JOIN `tours` USING(`tour_id`) WHERE CP.`payment_id`=:payment_id", array("payment_id"=>$payment_id))) return false;

		return $payment;

	}

	public function AddPayment($client_id, $payment) {

		if (!is_array($payment)) return false;

		//if ($payment["method"]=="credit_card") if (!$paypal_cc = $GLOBALS["Paypal"]->StoreCreditCard($client_id, $payment["card_type"], $payment["card_number"], $payment["expiration_month"], $payment["expiration_year"], $payment["cvc"], $payment["firstname"], $payment["lastname"])) return false;

		$save = array(
			"add_date"=>time(),
			"client_id"=>$client_id,
			//"paypal_cc"=>$paypal_cc,
			//"method"=>$payment["method"],
			"type"=>$payment["type"],
			"amount"=>$payment["amount"],
			"status"=>"pending"
		);

		if (!$payment_id = $GLOBALS["Sql"]->Insert("clients-payments", $save, "payment_id")) return false;

		$GLOBALS["Common"]->AddHistory("Payment added successfuly. (ID: ${payment_id})", "payments", $client_id);

		return $payment_id;

	}

	public function ProcessPayment($payment_id) {

		if (!$payment = $this->GetPayment($payment_id)) return false;

		switch($payment["method"]) {

			case "paypal":
				$results = $GLOBALS["Paypal"]->Paypal($payment_id);
			break;

			case "credit_card":
				$results = $GLOBALS["Paypal"]->CreditCard($payment_id);
			break;

		}

		$GLOBALS["Common"]->AddHistory("Payment processed successfuly. (ID: ${payment_id})", "payments", $payment["client_id"]);

		return $results;

	}

	public function GetEmail($client_id) {

		return $this->GetContact($client_id, "email");

	}

	public function GetCellular($client_id) {

		return $this->GetContact($client_id, "cellular");

	}

}

?>