<?php

/*

	CPaypal.php
	------------------------------------------

	Version: 			1.00
	Last change: 	8.12.2014

*/

class CPaypal {

	private $url, $token, $client_id, $secret;

	public function __construct() {

		$this->url = __PAYPAL_API__;
		$this->client_id = $GLOBALS["Library"]->Decrypt(__PAYPAL_CLIENT_ID__);
		$this->secret = $GLOBALS["Library"]->Decrypt(__PAYPAL_SECRET__);
		$this->token = (time() < __PAYPAL_TOKEN_EXPIRE__) ? $GLOBALS["Library"]->Decrypt(__PAYPAL_TOKEN__) : $this->GetToken();

		return true;

	}

	private function GetToken() {

		$headers = array(
			"Authorization: Basic ".base64_encode($this->client_id.":".$this->secret),
			"Content-Type: application/x-www-form-urlencoded"
		);

		$results = json_decode($GLOBALS["Library"]->PostRequest($this->url."v1/oauth2/token", "grant_type=client_credentials", false, false, false, true, $headers));

		$GLOBALS["Sql"]->Update("config", array("value"=>$GLOBALS["Library"]->Crypt($results->access_token)), "config_id", "PAYPAL_TOKEN");
		$GLOBALS["Sql"]->Update("config", array("value"=>time()+$results->expires_in), "config_id", "PAYPAL_TOKEN_EXPIRE");

		return true;

	}

	public function StoreCreditCard($client_id, $type, $number, $month, $year, $cvc, $firstname, $lastname) {

		$headers = array(
			"Content-Type:application/json",
			"Authorization: Bearer ".$this->token
		);

		$postdata = array(
			"payer_id"=>$client_id,
			"type"=>$type,
			"number"=>$number,
			"expire_month"=>$month,
			"expire_year"=>$year,
			"cvv2"=>$cvc,
			"first_name"=>$firstname,
			"last_name"=>$lastname
		);

		$results = json_decode($GLOBALS["Library"]->PostRequest($this->url."v1/vault/credit-card", json_encode($postdata), false, false, false, true, $headers));

		if ($results->state =="ok") return $results->id;

		return false;

	}

	public function CreditCard($payment_id) {

		if (!$payment = $GLOBALS["Client"]->GetPayment($payment_id)) return false;

		if ($payment["status"]!="pending") return false;
		if ($payment["method"]!="credit_card") return false;
		if (!$payment["paypal_cc"]) return false;

		$tours = $GLOBALS["Library"]->GetOptions("tours");

		$headers = array(
			"Content-Type:application/json",
			"Authorization: Bearer ".$this->token
		);

		$postdata = array(
			"intent"=>"sale",
			"payer"=>array("payment_method"=>"credit_card")
		);

		$postdata["payer"]["funding_instruments"][] = array("credit_card_token"=>array(
			"credit_card_id"=>$payment["paypal_cc"],
			"payer_id"=>$payment["client_id"],
		));

		$postdata["transactions"][]= array(
			"amount"=>array(
				"total"=>$payment["amount"],
				"currency"=>"USD"
			),
			"description"=>"Tour: ".$tours[$payment["tour"]].", code: ".$payment["code"]
		);

		$results = json_decode($GLOBALS["Library"]->PostRequest($this->url."v1/payments/payment", json_encode($postdata), false, false, false, true, $headers));

		return $results;

		if ($results->state =="approved") if ($GLOBALS["Sql"]->Update("clients-payments", array("paypal_id"=>$results->id, "status"=>"payed"), "payment_id", $payment_id)) return true;

		return false;

	}

	public function PayPal($payment_id) {

		$payment = $GLOBALS["Client"]->GetPayment($payment_id);

		if ($payment["status"]!="pending") return false;
		if ($payment["method"]!="paypal") return false;

		$tours = $GLOBALS["Library"]->GetOptions("tours");

		$headers = array(
			"Content-Type:application/json",
			"Authorization: Bearer ".$this->token
		);

		$postdata = array(
			"intent"=>"sale",
			"redirect_urls"=>array(
				"return_url"=>"https://www.goodshepherdtour.com/".__LANGUAGE__."/".(__LANGUAGE__=="en" ? "account/payment-detail" : "cuenta/pago-detalle")."/${payment_id}/",
				"cancel_url"=>"https://www.goodshepherdtour.com/".__LANGUAGE__."/".(__LANGUAGE__=="en" ? "account/payment-cancel" : "cuenta/pago-cancelar")."/${payment_id}/",
			),
			"payer"=>array("payment_method"=>"paypal")
		);

		$postdata["transactions"][]= array(
			"amount"=>array(
				"total"=>$payment["amount"],
				"currency"=>"USD"
			),
			"description"=>"Tour: ".$tours[$payment["tour"]].", code: ".$payment["code"]
		);

		$results = json_decode($GLOBALS["Library"]->PostRequest($this->url."v1/payments/payment", json_encode($postdata), false, false, false, true, $headers));

		if (!$results->id) return false;

		$GLOBALS["Sql"]->Update("clients-payments", array("paypal_id"=>$results->id), "payment_id", $payment_id);

		foreach($results->links as $l) if ($l->rel=="approval_url") return $l->href;

		return false;

	}


}

?>