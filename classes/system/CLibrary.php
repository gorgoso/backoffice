<?php

/*

	CLibrary.php
	------------------------------------------

	Version: 			2.00
	Last change: 	4.8.2010

*/

class CLibrary {

	private $timeout = 90;

	/* generate random password */
	function GenPass($length=8, $list="23456789ABCDEFGHJKMNPQRSTUVWXYZ")
	{

		$newstring="";

		mt_srand((double)microtime()*1000000);
		while(strlen($newstring)<$length) $newstring.=$list[mt_rand(0, strlen($list)-1)];

		return $newstring;

	}

	function GetClientIP() {

		$check = array("X_FORWARDED_FOR", "X_HTTP_FORWARDED_FOR", "HTTP_CLIENT_IP", "HTTP_FORWARDED", "HTTP_X_FORWARDED_FOR");

		foreach($check as $ch) if (isset($_SERVER[$ch])) return $_SERVER[$ch];

		return isset($_SERVER["REMOTE_ADDR"])?$_SERVER["REMOTE_ADDR"]:"0.0.0.0";

	}

	function GetPeriod($period = NULL, $day = NULL, $week = NULL, $month = NULL, $year = NULL) {

		$day = $day ? $day : date("d");
		$week = $week ? $week : date("W");
		$month = $month ? $month : date("m");
		$year = $year ? $year : date("Y");

		switch ($period) {

			default:
				return false;
			break;

			case "days":

				$return[0] = mktime(0,0,0,$month,$day,$year);
				$return[1] = mktime(23,59,59,$month,$day,$year);

			break;

			case "weeks":

				$return[0] = strtotime("{$year}-W{$week}-1");
				$return[1] = strtotime("{$year}-W{$week}-7");

			break;

			case "months":

				$days = date("t", mktime(0,0,0,$month,1,$year));

				$return[0] = mktime(0,0,0,$month,1,$year);
				$return[1] = mktime(23,59,59,$month,$days,$year);

			break;

			case "years":

				$return[0] = mktime(0,0,0,1,1,$year);
				$return[1] = mktime(23,59,59,12,31,$year);

			break;

		}

		return $return;

	}

	public function FormatFile($template, $values, $type, $language, $add_default_values = false, $header = true, $footer = true) {

		$header_txt = ($header) ? file_get_contents(__TEMPLATES_DIR__."/system/${type}/_header.tpl") : "";
		$footer_txt = ($footer) ? file_get_contents(__TEMPLATES_DIR__."/system/${type}/_footer.tpl") : "";
		$body = file_get_contents(__TEMPLATES_DIR__."/system/${type}/${language}/${template}.tpl");

		$File = new CTemplate();
		$File->StringTemplate($header_txt.$body.$footer_txt);

		if ($add_default_values) {

			$File->Add("CURRENT_TIME", $GLOBALS["Format"]->Date(NULL, __FORMAT_DATE_FULL__));

		}

		if ($footer) $File->Add("CURRENT_YEAR", date("Y"));

		$File->AddArray($values);
		if (isset($values["data_html"])) $File->AddHTMLArray($values["data_html"]);

		$File->Translate();

		return $File->result;

	}

	public function FormatEmail($template, $values, $language, $add_default_values = false, $header = true, $footer = true) {

		return $this->FormatFile($template, $values, "emails", $language, $add_default_values, $header, $footer);

	}

	public function FormatDocument($template, $values, $language, $add_default_values = false, $header = true, $footer = true) {

		return $this->FormatFile($template, $values, "documents", $language, $add_default_values, $header, $footer);

	}

	/* function to send e-mail in plaintext or html */
	public function SendMail($to, $subject, $text, $attachement = false, $from_email = __SENDMAIL_EMAIL__, $from_name = __SENDMAIL_NAME__)	{

		$headers = array(
			"Accept: application/json",
  		"Content-Type: application/json",
  		"X-Postmark-Server-Token: b8568d8c-a366-4be1-ae5b-96a31648d8a4"
		);

		$data = array(
			"From"=>'"'.$from_name.'"'.' <'.$from_email.'>',
			"To"=>$to,
			"Subject"=>$subject ? $subject : "Good Shepherd Tours",
			"Tag"=>"WWW",
			"HtmlBody"=>$text,
			"TextBody"=>strip_tags($text),
			"Attachments"=>array(0=>array(
				"Name"=>"gst-logo.gif",
				"Content"=>"R0lGODlhtgBGALMAAMzLxwICA1paWZytJrW0scXFwI6OjKmppr/DmicnKL69umxsbJugcZ2dm4CAfkRERCH5BAAAAAAALAAAAAC2AEYAAAT/EMhJq7046827/6B2HIYSnmiqrmzbEc4iHG5t33i+FU/gLwWdcEgsahy+QOJhMjqf0JbB10s0o9isFlMNMDMNRyO4LZtrB2VCScAcxA4D+UyvexQ9wUMAvBQabQ0CV3aFhhQECUsBAnMWBEEkjoeUdXgPawF9jyYFBjSVoXV6mQ+bFAUHbTBtoq5lmT4JchoKoK+4WD1JPxoluagFk1HCZ7tKVMMSBQKtuQqCe2NYBzKmhLo+elQZBA3AAMxJS99QDZmKC7A+BgJUMZ/h4QUL2KIEuwliDfZDAmsJZjS4RQ0dKXRLBLTTQ1AUPSUNHhAwoO+JAh8xFuB7oMzIAl56/zDxkqVoFi4YPxYEdNCjYY4/Vc7p+/hAi4JYu0SOGxGnY6ED6BqoSaJuCEqMNHdqocgrwUclPQyEg2RuwYOrV0Mu2CpjiVdFO4cSdZbBasg9evhsJYUJU6wADtwdg+hTiFWcFANiamMLSpqRgAMDloFspFQN7gQrBjwL4EiOusDyavAUcoPDRs4t3jwOAMWIhrs95bxZjtMpvCBD4eElnxemUhXUc8KUNGe9WQ0YwFTuwlzbgpdgfRDHQcC6dwD0862tS4ACBBbQiPEENXDgNcMp6Fjg+vUE36AvB2E8YIebSY4loBGEh0sdeLwDT4Dcs3zgmFt8PF0fvaxjvRXgwP8Tf913W31vGahYfitMYUo7RWlQQIJJNLIMFIkouNkXG4ymYWAmSSjMiCRuZ8t+KmXyXgUUymKCAg6Mh0OBH4KIHGs1CsZgBbuV1NZBknmRmCwrSgCUWLy0ccAgTwi12G/f3djiOFOStuMESFwX1z+ZXIllErVVCMAIUUA52ZDeqaaBk4IJwKZ3IT4in48GhFGkgCT9ZoUDZBURn2J7jMQHoLtYuEGWiqnUlHHB/QORH2jKx96hGDkG0i9QIGobATSOpM8aA3LQ3XWNVEmYFX5UeVtviFGRx1v0YfGmlQSQFuEGZgI64YGpasgqBmEq9qsRwQL2SVORBsbhBn8GpwD/mo5uNqwE/jWVq6fTIhJkrmo60SlggyL7pIyoJLtogooKm4E4ng4ZJJUBFCnBfmC2uF4UsqkKlo/4BNenhKApVpIXisShWKgY4LmYm4C5Je9fsQhgrhdZTAweATAAwukfIMqLQa2CaUTCJyMo0CwvCHejbD4Td2vBfrEEpBi5ORzpoyLZ2jeSAx4n7M4ibhlqwa6XvkChKeNAmV0GRxZGRVOp1VcDGT/rIYdPu/KxxtIhRNLcbD4h4VRiPS+jqSxcLdDArKhm0KxOBKe3M74+hMHOmppQu5tEKdB0pMuoKNJKNF4OXUVAGlnAC6ZuD2lVcJ5KXYNK6jwUwLp7VIBE/+EaJPIFaClrfi8FSnzwzz/vWRXG6BukxUjMb0WLEYEVLZMG3xXQwzq1vElOrcTOEOAOLRUAJYAFxjG5w8WYYHMAh0DlPOZEJuiG01xwg+fEOUJLMMXuz9ZtQXycWyB4BeidAoCT3XtPcS1uUvvRFRtRcE629EDTmwELxBCDKaZwgP92E6MisCZ1AcgPDNSmjFol4AMOusBF2ua9/i2nJRrARAX20ITd/EolyoDRTRLwrwkQABsRUUkBhzAF9S3DLE0QkABWmDtBuBADIyxhROJnJD5kSza4u4BxCLKR6hEndxJrYI/4oAyuScAUGdqK70KQhgQg4AIZwgxqVvSQ4/91YA1XesgDJ6ANpinhin4g3gTeoLdp0bCGDOiJBRDgkgOsjQEGwCMKhIEAPhYAAYAMJCAZcABBGjKODBAkAxJpSEUuspGQRMAjIylJRg5ykpGMYyEj+UdDdhKQn/QkJfv4x1AK0pSBLGUfR9nIICBgAANgACxnScta2vKWuMylLnfJy1768pfADKYwbznIWb5ymMhMpjKXycxmInOOfGSlNKdJzWpa85rYzKY2rTlFcHjzm+C0QzdfMgKqOIJTB2gPBWyhinkoRwEnPOGF/IDO9hTAZBO4ZyfiibETCgOe8DSnPvOJT/FgDKD/5GcnphLQGI4voP3cTns4BYliQGf/Dhd9Jz/VKYF6ygMDmqFCRZUzpAeksz0h1UQn6KWEq0nwGM0Yk1MsmrcJykIJ0LCUSWW6ie4AQSg69UyQZhEEsckiRsPIh2O+8bz0UCUNUgkCRaTCUi+clF1WLYYF2McoVKFHH2QLgpO2soZGsAYtGNEqBXpAHLamwgvFqNUgqiixq9Tje3XF6e3iyggYKSEtptBZXgPwDZoEShNqlQBLEpMVjL2jCoGASzGm4ABxoKUK29lPXNjw0QmgpwT37ME3HHRC2cA1iyY7UjrdcQCTOWmkJsRpKtZgi74GQa5v1YjJTOYJlcKzJWkYxG37ujnxSLSFvEUCED7Cj9rGS60j/wKAaHnrjnrAs7ocq6wwKCsOfmBMtADwQTrjM4ZJ3G63qZhBfMZ7T9oKZRC8RS5rd/uRqCJCCf8MQ22FC52+CgUIEt2ub/FUJ9v2dxCbUw6JWnjP3jaCucmV7CSE0ZLtOLC0b7WCUJDa28q6YwzbiUp31nPPasD2SyXgLW+riOGH2C3FFvaC8J67nf9uZ51V4JlEhUIc3SChETw2QHHSiVyTifZ2cRAycRkhZN0QmREj0IwcPrxi2+ZOuoTdTnDRG5+JwOW4X3YHz0jgA0j8TG0vSqyLt6uWgXiBt1JNIHMbDIBEkJC196yzSqHbL5FyDFzQAFdvnfI4Es5KG37dWf9vR6IRF0s0uCeewHxzG2BxeOPLAvawp2Bc0vI6Ys0AcNztYLu5OfP1zs8VRhrqkdhwUAYgtZ1FHD4C5NcMedHpGQNQm0xrvyqkyU92ytbom2Wv8dcCLVH1ntvjDi9zmLLhC1eMbpwKRqWaR5jm1BRmQFsSsRbaxTgvnjMdXQlQ5qT9cjN8Dxxo6y4YygPx55ZjjOAENnhELaxzVDJbbI6xehiTPm8x0OPsBnMXz1VsbTVA/BAgmFe2xg6fSy9cRYkC4CMxavY/RZtYWo9oc++VNyP8fW8BAzjAkNbykuVQ6SIDFRKmDl95mXNS9Jx0fW9+r0QxLnN+N+J7ed72jVH/kQeMNXUQTuJHU61b9ESbmbCc2o/F11g3W6xhBIyoqFz3W86DFjm682Y3EvikCoz11rrxGVB9VSG2SEs6XmBeT07t/Relr2EMH74nyE7Ijt/ae8L+QUdrE10Kf2aoMCDW01VRkaVM/PTNwqgVRw4dr/eWe0yQ7y8TzuYDBTBY5QEIX1M8zRxPs8vxu238D4xM2AZ73Eml4O2VPY+JGUJix/8gjkQtzBKnDN7za1nAJ2QfDEEswQCQiM7V7il86HBFLSJjoCNks3zq/yH4XEkFmj/eDMrIoH+Db3UFKxoE2gckxf9sx0p2yz/yR4dP118Cn6YejMhXekTopfM/uYx/gX2SyA96t3skYlH3pmIq9n/5NIDz8E/6JIAKyIAHiIC5o4ALCGfR1YADeIH9F4ABljAZSAYfiFEfuIAUCIAMKIHLEIIlOIH1p4IcZU8huAOdRYLlpoLyIII2uAOXdyHix4NDM4MJ404eYE9A+AHqZFEyiIQpwFEs6DtKGE5QWAcRAAA7",
				"ContentType"=>"image/gif",
				"ContentID"=>"cid:gstlogo"
			))
		);

		if ($attachement) {

			$data["Attachments"][] = array(
				"Name"=>$attachement["name"],
				"Content"=>base64_encode($attachement["data"]),
				"ContentType"=>$attachement["type"],
			);

		}

		$results = $this->PostRequest("https://api.postmarkapp.com/email", json_encode($data), false, false, false, true, $headers);

		if ($results->ErrorCode != 0) return false;

		return true;

		$boundary = uniqid("----=");

		$headers = "From: =?UTF-8?B?".base64_encode($from_name)."?= <".$from_email.">\n";
		$headers.= "MIME-Version: 1.0\n";
		$headers.= "Content-Type: multipart/alternative;\n\tboundary=\"$boundary\"";

		$body= "This is a MIME encoded message.\n\n";

		// Plain text version of message
		$body.= "--$boundary\n".
			"Content-Type: text/plain; charset=utf-8\n".
			"Content-Transfer-Encoding: base64\n\n";
		$body.= chunk_split(base64_encode(strip_tags($text)));

		// HTML version of message
		$body.= "\n--$boundary\n".
			"Content-Type: multipart/related;\n\tboundary=\"$boundary-2\"; type=\"text/html\"\n\n".
			"--$boundary-2\n".
			"Content-Type: text/html; charset=utf-8\n".
			"Content-Transfer-Encoding: base64\n\n";

		$body.= chunk_split(base64_encode($text));

		// LOGO GST
		$body.= "\n--$boundary-2\n".
			"Content-Type: application/octet-stream\n".
			"Content-ID: <gstlogo>\n".
			"Content-Transfer-Encoding: base64\n\n";

		$body.= chunk_split("R0lGODlhtgBGALMAAMzLxwICA1paWZytJrW0scXFwI6OjKmppr/DmicnKL69umxsbJugcZ2dm4CAfkRERCH5BAAAAAAALAAAAAC2AEYAAAT/EMhJq7046827/6B2HIYSnmiqrmzbEc4iHG5t33i+FU/gLwWdcEgsahy+QOJhMjqf0JbB10s0o9isFlMNMDMNRyO4LZtrB2VCScAcxA4D+UyvexQ9wUMAvBQabQ0CV3aFhhQECUsBAnMWBEEkjoeUdXgPawF9jyYFBjSVoXV6mQ+bFAUHbTBtoq5lmT4JchoKoK+4WD1JPxoluagFk1HCZ7tKVMMSBQKtuQqCe2NYBzKmhLo+elQZBA3AAMxJS99QDZmKC7A+BgJUMZ/h4QUL2KIEuwliDfZDAmsJZjS4RQ0dKXRLBLTTQ1AUPSUNHhAwoO+JAh8xFuB7oMzIAl56/zDxkqVoFi4YPxYEdNCjYY4/Vc7p+/hAi4JYu0SOGxGnY6ED6BqoSaJuCEqMNHdqocgrwUclPQyEg2RuwYOrV0Mu2CpjiVdFO4cSdZbBasg9evhsJYUJU6wADtwdg+hTiFWcFANiamMLSpqRgAMDloFspFQN7gQrBjwL4EiOusDyavAUcoPDRs4t3jwOAMWIhrs95bxZjtMpvCBD4eElnxemUhXUc8KUNGe9WQ0YwFTuwlzbgpdgfRDHQcC6dwD0862tS4ACBBbQiPEENXDgNcMp6Fjg+vUE36AvB2E8YIebSY4loBGEh0sdeLwDT4Dcs3zgmFt8PF0fvaxjvRXgwP8Tf913W31vGahYfitMYUo7RWlQQIJJNLIMFIkouNkXG4ymYWAmSSjMiCRuZ8t+KmXyXgUUymKCAg6Mh0OBH4KIHGs1CsZgBbuV1NZBknmRmCwrSgCUWLy0ccAgTwi12G/f3djiOFOStuMESFwX1z+ZXIllErVVCMAIUUA52ZDeqaaBk4IJwKZ3IT4in48GhFGkgCT9ZoUDZBURn2J7jMQHoLtYuEGWiqnUlHHB/QORH2jKx96hGDkG0i9QIGobATSOpM8aA3LQ3XWNVEmYFX5UeVtviFGRx1v0YfGmlQSQFuEGZgI64YGpasgqBmEq9qsRwQL2SVORBsbhBn8GpwD/mo5uNqwE/jWVq6fTIhJkrmo60SlggyL7pIyoJLtogooKm4E4ng4ZJJUBFCnBfmC2uF4UsqkKlo/4BNenhKApVpIXisShWKgY4LmYm4C5Je9fsQhgrhdZTAweATAAwukfIMqLQa2CaUTCJyMo0CwvCHejbD4Td2vBfrEEpBi5ORzpoyLZ2jeSAx4n7M4ibhlqwa6XvkChKeNAmV0GRxZGRVOp1VcDGT/rIYdPu/KxxtIhRNLcbD4h4VRiPS+jqSxcLdDArKhm0KxOBKe3M74+hMHOmppQu5tEKdB0pMuoKNJKNF4OXUVAGlnAC6ZuD2lVcJ5KXYNK6jwUwLp7VIBE/+EaJPIFaClrfi8FSnzwzz/vWRXG6BukxUjMb0WLEYEVLZMG3xXQwzq1vElOrcTOEOAOLRUAJYAFxjG5w8WYYHMAh0DlPOZEJuiG01xwg+fEOUJLMMXuz9ZtQXycWyB4BeidAoCT3XtPcS1uUvvRFRtRcE629EDTmwELxBCDKaZwgP92E6MisCZ1AcgPDNSmjFol4AMOusBF2ua9/i2nJRrARAX20ITd/EolyoDRTRLwrwkQABsRUUkBhzAF9S3DLE0QkABWmDtBuBADIyxhROJnJD5kSza4u4BxCLKR6hEndxJrYI/4oAyuScAUGdqK70KQhgQg4AIZwgxqVvSQ4/91YA1XesgDJ6ANpinhin4g3gTeoLdp0bCGDOiJBRDgkgOsjQEGwCMKhIEAPhYAAYAMJCAZcABBGjKODBAkAxJpSEUuspGQRMAjIylJRg5ykpGMYyEj+UdDdhKQn/QkJfv4x1AK0pSBLGUfR9nIICBgAANgACxnScta2vKWuMylLnfJy1768pfADKYwbznIWb5ymMhMpjKXycxmInOOfGSlNKdJzWpa85rYzKY2rTlFcHjzm+C0QzdfMgKqOIJTB2gPBWyhinkoRwEnPOGF/IDO9hTAZBO4ZyfiibETCgOe8DSnPvOJT/FgDKD/5GcnphLQGI4voP3cTns4BYliQGf/Dhd9Jz/VKYF6ygMDmqFCRZUzpAeksz0h1UQn6KWEq0nwGM0Yk1MsmrcJykIJ0LCUSWW6ie4AQSg69UyQZhEEsckiRsPIh2O+8bz0UCUNUgkCRaTCUi+clF1WLYYF2McoVKFHH2QLgpO2soZGsAYtGNEqBXpAHLamwgvFqNUgqiixq9Tje3XF6e3iyggYKSEtptBZXgPwDZoEShNqlQBLEpMVjL2jCoGASzGm4ABxoKUK29lPXNjw0QmgpwT37ME3HHRC2cA1iyY7UjrdcQCTOWmkJsRpKtZgi74GQa5v1YjJTOYJlcKzJWkYxG37ujnxSLSFvEUCED7Cj9rGS60j/wKAaHnrjnrAs7ocq6wwKCsOfmBMtADwQTrjM4ZJ3G63qZhBfMZ7T9oKZRC8RS5rd/uRqCJCCf8MQ22FC52+CgUIEt2ub/FUJ9v2dxCbUw6JWnjP3jaCucmV7CSE0ZLtOLC0b7WCUJDa28q6YwzbiUp31nPPasD2SyXgLW+riOGH2C3FFvaC8J67nf9uZ51V4JlEhUIc3SChETw2QHHSiVyTifZ2cRAycRkhZN0QmREj0IwcPrxi2+ZOuoTdTnDRG5+JwOW4X3YHz0jgA0j8TG0vSqyLt6uWgXiBt1JNIHMbDIBEkJC196yzSqHbL5FyDFzQAFdvnfI4Es5KG37dWf9vR6IRF0s0uCeewHxzG2BxeOPLAvawp2Bc0vI6Ys0AcNztYLu5OfP1zs8VRhrqkdhwUAYgtZ1FHD4C5NcMedHpGQNQm0xrvyqkyU92ytbom2Wv8dcCLVH1ntvjDi9zmLLhC1eMbpwKRqWaR5jm1BRmQFsSsRbaxTgvnjMdXQlQ5qT9cjN8Dxxo6y4YygPx55ZjjOAENnhELaxzVDJbbI6xehiTPm8x0OPsBnMXz1VsbTVA/BAgmFe2xg6fSy9cRYkC4CMxavY/RZtYWo9oc++VNyP8fW8BAzjAkNbykuVQ6SIDFRKmDl95mXNS9Jx0fW9+r0QxLnN+N+J7ed72jVH/kQeMNXUQTuJHU61b9ESbmbCc2o/F11g3W6xhBIyoqFz3W86DFjm682Y3EvikCoz11rrxGVB9VSG2SEs6XmBeT07t/Relr2EMH74nyE7Ijt/ae8L+QUdrE10Kf2aoMCDW01VRkaVM/PTNwqgVRw4dr/eWe0yQ7y8TzuYDBTBY5QEIX1M8zRxPs8vxu238D4xM2AZ73Eml4O2VPY+JGUJix/8gjkQtzBKnDN7za1nAJ2QfDEEswQCQiM7V7il86HBFLSJjoCNks3zq/yH4XEkFmj/eDMrIoH+Db3UFKxoE2gckxf9sx0p2yz/yR4dP118Cn6YejMhXekTopfM/uYx/gX2SyA96t3skYlH3pmIq9n/5NIDz8E/6JIAKyIAHiIC5o4ALCGfR1YADeIH9F4ABljAZSAYfiFEfuIAUCIAMKIHLEIIlOIH1p4IcZU8huAOdRYLlpoLyIII2uAOXdyHix4NDM4MJ404eYE9A+AHqZFEyiIQpwFEs6DtKGE5QWAcRAAA7");

		// Attachement
		if ($attachement) {

			$body.= "\n--$boundary-2\n".
				"Content-Type: ".$attachement["type"]."; name=\"".$attachement["name"]."\"\n".
				"Content-Transfer-Encoding: base64\n".
				"Content-Disposition: attachment\n\n";

			$body.= chunk_split(base64_encode($attachement["data"]));

		}

		$body.= "\n--$boundary-2--\n".
			"--$boundary--";

		if (!@mail($to, "=?UTF-8?B?".base64_encode($subject)."?=", $body, $headers)) return false;

		return true;

	}

	function ShowSelect($select, $selected=NULL, $convert=false) {

		if (!$select) return "";

		$return = "";
		$is_array = (is_array($selected));

		if ($convert===true) {

			$select = array_values($select);
			$select = array_combine($select, $select);

		}

		foreach ($select as $key=>$value) {

			$is_selected = ($is_array)?in_array($key, $selected):($key==$selected && !is_null($selected));
			$return.="<option value=\"$key\"".(($is_selected)?" selected=\"selected\"":"").">".htmlspecialchars($value, ENT_QUOTES)."</option>".chr(10);

		}

		return $return;

	}

	function Register($var, $default = false){

		return isset($_POST[$var])?$_POST[$var]:(isset($_GET[$var])?$_GET[$var]:$default);

	}

	function Redirect($location=NULL) {

		header("location: ".(($location)?$location:__PATH_ADMIN__));
		exit;

	}

	function StartOfDay($stamp, $end = false){

		$day_array = getdate($stamp);
		$h = $end ? 23 : 0;
		$m_s = $end ? 59 : 0;

		return mktime($h, $m_s, $m_s, $day_array['mon'], $day_array['mday'], $day_array['year']);

	}

	function GetTimestamp($date) {

		if (is_int($date)) return $date;

		$date = str_replace(array("[", "]") , "", $date);

		return strtotime($date);

	}

	function MakeList($items_count, $per_page, $current_start = 0, $link = false, $page_count = __FILTER_PAGE_COUNT__, $list_count = __FILTER_LIST_COUNT__, $list_per_page = __FILTER_LIST_PER_PAGE__, $fastlist_per_page = __FILTER_FASTLIST_PER_PAGE__){

		$link = __PATH_ADMIN__.__MODULE__."/".__ACTION__."/".$link;

		$textout = '';
		$pages = ceil($items_count/$per_page);
		$current_page = $current_start?ceil($current_start/$per_page)+1:1;

		$lists = ceil($pages/$page_count);
		$current_list = ceil($current_page/$page_count);

		$fastlists = ceil($pages/$list_count);
		$current_fastlist = ceil($current_page/$list_count);

		$start = (($current_list-1)*$page_count)+1;
		$end = (($current_list*$page_count)+1)<=$pages?($current_list*$page_count)+1:$pages+1;

		$page_start = $current_list-$list_per_page-1>=0?$current_list-$list_per_page-1:0;
		$page_end = $current_list+$list_per_page<=$lists?$current_list+$list_per_page:$lists;

		$list_start = $current_fastlist-$fastlist_per_page-1>=0?$current_fastlist-$fastlist_per_page-1:0;
		$list_end = $current_fastlist+$fastlist_per_page<=$fastlists?$current_fastlist+$fastlist_per_page:$fastlists;

		$link .= strpos($link, "?")?"&amp;":"?";

		for($i = $list_start; $i<($current_fastlist-1); $i++) {
			$textout .= ("<a class=\"page_list\" href=\"".$link."start=".($i*$per_page*$list_count+$per_page)."\">".($i*$list_count+1)."-".(($i*$list_count)+$list_count)."</a>&nbsp;");
			//else $textout .= $i."&nbsp;";

		}

		for($i = $page_start; $i<($current_list-1); $i++) {
			$textout .= ("<a class=\"page_list\" href=\"".$link."start=".($i*$per_page*$page_count)."\">".($i*$page_count+1)."-".(($i*$page_count)+$page_count)."</a>&nbsp;");
			//else $textout .= $i."&nbsp;";

		}

		for($i = $start; $i<$end; $i++) {
			if(($current_page) != $i) $textout .= ("<a class=\"single_list\" href=\"".$link."start=".((($i-1)*$per_page))."\">".($i)."</a>&nbsp;");
			else $textout .= ($i)."&nbsp;";

		}

		for($i = $current_list; $i<$page_end; $i++) {
			$textout .= ("<a class=\"page_list\" href=\"".$link."start=".($i*$per_page*$page_count)."\">".($i*$page_count+1)."-".(($i*$page_count)+$page_count)."</a>&nbsp;");
			//else $textout .= $i."&nbsp;";

		}

		for($i = $current_fastlist; $i<$list_end; $i++) {

			$textout .= ("<a class=\"page_list\" href=\"".$link."start=".($i*$per_page*$list_count+$per_page)."\">".($i*$list_count+1)."-".(($i*$list_count)+$list_count)."</a>&nbsp;");
			//else $textout .= $i."&nbsp;";

		}

		return $textout;

	}

	function PostRequest($destination, $postdata = array(), $outheader=false, $cookies=false, $redirect=false, $raw=false, $ownheaders = array()) {

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $destination);
		curl_setopt($curl, CURLOPT_USERAGENT, 'Good Shepherd Tours (Backoffice System / V1)');
		curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->timeout);
		curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
		curl_setopt($curl, CURLOPT_HEADER, $redirect ? true : $outheader);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $ownheaders);

		if ($cookies) {

			curl_setopt($curl, CURLOPT_COOKIEJAR, $cookies);
			curl_setopt($curl, CURLOPT_COOKIEFILE, $cookies);

		}

		if (!empty($postdata) || $raw===true) {

			$poststring = '';
			if ($raw===true) $poststring = $postdata." ";
			else {

				if (!empty($postdata)) foreach($postdata as $key=>$value) $poststring.= $key.'='.urlencode($value).'&';

			}

			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, substr($poststring, 0, -1));

		} else curl_setopt($curl, CURLOPT_HTTPGET, 1);

		$parse = curl_exec($curl);
		$status = curl_errno($curl);
		$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($http_code == 301 || $http_code == 302) {

        list($header, $data) = explode("\r\n\r\n", $parse, 2);

        $matches = array();
        preg_match('/[Ll][Oo][Cc][Aa][Tt][Ii][Oo][Nn]:(.*?)\n/', $header, $matches);
        $url = @parse_url(trim(array_pop($matches)));

        if (!$url) {

            //couldn't process the url to redirect to
            $curl_loops = 0;
            return $outheader?$parse:$data;

        }

        $last_url = parse_url(curl_getinfo($curl, CURLINFO_EFFECTIVE_URL));

        if (!isset($url['scheme'])) $url['scheme'] = $last_url['scheme'];
        if (!isset($url['host'])) $url['host'] = $last_url['host'];
        if (!isset($url['path'])) $url['path'] = $last_url['path'];

        $new_url = $url['scheme'] . '://' . $url['host'] . "/". $url['path'] . (isset($url['query'])?'?'.$url['query']:'');

				preg_match_all('|Set-Cookie: (.*);|U', $parse, $results);
				$cookies = implode(';', $results[1]);

        return $this->PostRequest($new_url, $outheader, $cookies, $redirect);

    } else {

        $curl_loops=0;

    }

		curl_close($curl);

		if ($status == 7) return "ERROR: HTTP_CANT_CONNECT";
		if ($status == 28) return "ERROR: HTTP_TIMEOUT";
		if ($status == 56) return "ERROR: HTTP_FAILURE";
		if (!$parse) return "ERROR: HTTP_BLANK_RESPONSE";

		return $parse;

	}

	function GetOptions($group, $language = __LANGUAGE__) {

		$groups = (is_array($group)) ? implode("','", $groups) : $group;

		$sqldata = array("groups"=>$groups, "language"=>$language);

		$options = $GLOBALS["Sql"]->Values("options", "option", "description", "`language`=:language AND `group` IN (:groups) ORDER BY `group`, `description`", $sqldata);

		return $options;

	}

	public function HTML2PDF($text, $options = "-s Letter -B 15mm -L 15mm -T 15mm -R 15mm") {

		$htmlfile = md5(time().rand());
		file_put_contents("/tmp/".$htmlfile.".html", $text);

		system("/usr/bin/wkhtmltopdf $options /tmp/".$htmlfile.".html /tmp/".$htmlfile.".pdf");

		$pdffile = file_get_contents("/tmp/".$htmlfile.".pdf");

		unlink("/tmp/".$htmlfile.".html");
		unlink("/tmp/".$htmlfile.".pdf");

		return $pdffile;

	}

	function Crypt($code, $key=__CRYPT_KEY__) {

		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		return(bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $code, MCRYPT_MODE_ECB, $iv)));

	}

	function Decrypt($code, $key=__CRYPT_KEY__) {

		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

		 return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), pack("H*", $code), MCRYPT_MODE_ECB, $iv));

	}

}

?>