<?php

/*

	CInit.php
	------------------------------------------

	Version: 			2.00
	Last change: 	4.8.2010

*/

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once("classes/system/CLibrary.php");
require_once("classes/system/CConfig.php");
require_once("classes/system/CSql.php");
require_once("classes/system/CError.php");
require_once("classes/system/CFilter.php");

require_once("classes/system/CTemplate.php");
require_once("classes/system/CPage.php");
require_once("classes/system/CLanguage.php");
require_once("classes/system/CMySql.php");
require_once("classes/system/CUser.php");
require_once("classes/system/CFormat.php");

require_once("classes/CCommon.php");
require_once("classes/CClient.php");
require_once("classes/CTour.php");
require_once("classes/CPaypal.php");

date_default_timezone_set('America/Santo_Domingo');
session_cache_limiter('nocache');

if(empty(session_id()) && strpos($_SERVER["REQUEST_URI"], "/system/api/")===false) session_start();

/* create all default objects now */
$Library = new CLibrary();
$Error = new CError();

$connection = array(
	"host"=>"localhost",
	"login"=>"root",
	"pass"=>"",
	"db"=>"gst-backoffice"
);

$Sql = new CSql("DEFAULT", $connection["host"], $connection["login"], $connection["pass"], $connection["db"]);


$Config = new CConfig();

$Common = new CCommon();
$Client = new CClient();
$Tour = new CTour();
$Paypal = new CPaypal();
$Format = new CFormat();

?>