<?php

/*

	CSql.php
	------------------------------------------

	Version: 			2.0
	Last change: 	15.11.2007

*/

class CSql {

	public $sqlids, $sqlid, $connections, $connection;
	public $rows;

	function __construct($connection = false, $db_hostname=NULL, $db_username=NULL, $db_password=NULL, $db_name=NULL) {

		if($connection) {

			if($this->AddConnection($connection, $db_hostname, $db_username, $db_password, $db_name)) {

				if (!$this->Connect($connection)) die("SQL_${connection}_CONNECTION_FAILED");

			}

		}

		return true;

	}

	function Connect($connection) {

		set_exception_handler(array(__CLASS__, "Error"));
						
  	$this->sqlids[$connection] = new PDO("mysql:host=".$this->connections[$connection]["host"].";dbname=".$this->connections[$connection]["db"], $this->connections[$connection]["login"], $this->connections[$connection]["pass"], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
  	  	  	  	
  	restore_exception_handler();
  					
		if (!$this->sqlids[$connection]) return false;

  	if(!$this->ChangeConnection($connection)) return false;
  	
		return true;

	}

	function ChangeConnection($connection = 0) {

		if(is_object($this->sqlids[$connection])) {

			$this->sqlid = $this->sqlids[$connection];

			$this->rows = 0;

			$this->connection = $connection;

			return true;

		}

		return false;

	}

	function AddConnection($connection, $db_hostname, $db_username, $db_password, $db_name) {

		if(isset($this->connections[$connection])) $this->Close($connection);

		$this->sqlids[$connection] = NULL;
		$this->connections[$connection]["host"] = $db_hostname;
		$this->connections[$connection]["login"] = $db_username;
		$this->connections[$connection]["pass"] = $db_password;
		$this->connections[$connection]["db"] = $db_name;
		
		return true;

	}

	function Close($connection = false) {

		$resource = $connection?$this->sqlids[$connection]:$this->sqlid;

		if(is_object($resource)) $resource=NULL;
		
		return true;

	}

	function AutoId() {

		return $this->sqlid->lastInsertId();

	}

	function Affected() {
		
		return $this->Sql->rowCount();

	}

	function Query($sql, $binds) {
				
		$this->Sql = $this->sqlid->prepare($sql);
								
		if (!$this->Sql) {
			
			$error = $this->sqlid->errorInfo();
			$this->Error($error[0], $sql);
			
			return false;

		}
				
		$this->Sql->execute($binds);
		
		$error = $this->Sql->errorInfo();
					
		if ($error[0]!="00000") {

			$this->Error($error[0], $sql);
			
			return false;

		}
				
		$this->rows = $this->RowCount($sql, $binds);
				
		return $this->Sql;

	}

	function Select($sql, $sqldata) {

		if (!$result = $this->Query($sql, $sqldata)) return false;
						
		//if ($this->rows == 0) return false;

		return $result;

	}

	function SelectArray($sql, $sqldata = array(), $id_column = false, $unset = true) {
		
		if (!$result = $this->Select($sql, $sqldata)) return array();
				
		$array = array();
								
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			
			if($id_column) {

				$array[$row[$id_column]] = $row;
				if (!$unset) unset($array[$row[$id_column]][$id_column]);

			}

			else $array[] = $row;

		}
		
		$result->closeCursor();
		
		return $array;

	}
	
	function NextId($table, $id_column) {
		
		$last = $this->Fetch("SELECT MAX(`$id_column`) AS `last_id` FROM `$table`");
		
		return $last?($last["last_id"]+1):1;

	}

	function Insert($table, $sqldata, $id_column=false, $id=NULL) {
		
		$update = "";

		$id = ($id_column&&!$id)?$this->NextId($table, $id_column):$id;
		
		if ($id_column) $sqldata[$id_column]=$id;

		foreach ($sqldata as $key=>$value) {
			
			$update.= "`$key`=".((strlen($value) && $value!="NULL")?(":".strtolower($key)):"NULL").", ";
			if (!strlen($value) || $value=="NULL") unset($sqldata[$key]);
			
		}
				
		$result = $this->Query("INSERT INTO `$table` SET ".substr($update,0,-2), $sqldata);
		
		$result_id = (is_null($id))?$this->AutoId():$id;

		if (!$result_id && !$result) return false;

		return ($result_id)?$result_id:true;

	}

	function Update($sql_table, $sqldata, $sql_id_column, $sql_result_id, $limit1=false) {

		$update = "";
		
		foreach ($sqldata as $key=>$value) {
			
			$update.= '`'.$key.'`='.((strlen($value) && $value!="NULL") ? ':'.strtolower($key) : "NULL").', ';
			if (!strlen($value) || $value=="NULL") unset($sqldata[$key]);
			
		}
				
		$sqldata["sql_result_id"] = $sql_result_id;
		
										
		$this->Query("UPDATE `$sql_table` SET ".substr($update,0,-2)." WHERE `$sql_id_column`=:sql_result_id".(($limit1)?" LIMIT 1":""), $sqldata);
		
		if (!$this->Affected()) return false;

		return true;

	}

	function Replace($table, $data, $limit1=false) {

		$update = NULL;
		$keys = array_keys($data);

		foreach ($keys as $k) $update.= "`$k`=".((strlen($data[$k]) && $data[$k]!="NULL")?"'".addslashes($data[$k])."'":"NULL").", ";

		$this->Query("REPLACE INTO `$table` SET ".substr($update,0,-2).(($limit1)?" LIMIT 1":""));

		if (!$this->Affected()) return false;

		return true;

	}

	function Delete($table, $id_column, $result_id, $where=NULL) {

		$where = (!is_null($where))?$where:" `$id_column`=:result_id";

		$this->Query("DELETE FROM `$table` WHERE $where", array("result_id"=>$result_id));

		if (!$this->Affected()) return false;

		return true;

	}

	function Fetch($sql, $sqldata = array()) {
		
		if (!$result = $this->Select($sql." LIMIT 1", $sqldata)) return array();
		
		if ($this->rows==1) {

			$value = $result->fetch(PDO::FETCH_ASSOC);

			$result->closeCursor();

		 	return $value;

		}

		return false;

	}

	function Value($table, $column, $id_column, $result_id, $where = NULL, $sqldata = array()) {
		
		$sqldata["result_id"] = $result_id;
				die(json_encode($sqldata));
		$result = $this->Select("SELECT `$column` FROM `$table` WHERE `$id_column`=:result_id $where", $sqldata);

		if ($this->rows==1){

			$value = $result->fetch(PDO::FETCH_ASSOC);

			$result->closeCursor();
			
			return $value[$column];

		}

		return false;

	}

	function Values($table, $key, $value, $where = 1, $sqldata = array()) {
				
		$result = $this->Select("SELECT `$key`,`$value` FROM `$table` WHERE $where", $sqldata);
				
		if ($this->rows==0) return false;

		$values = array();

		while ($c = $result->fetch(PDO::FETCH_ASSOC)) $values[$c[$key]]=$c[$value];
		
		$result->closeCursor();

		if ($values) return $values;

		return false;

	}

	function Rollback() {

		$GLOBALS["Sql"]->Query("ROLLBACK", array());

	}

	function Commit() {

		$GLOBALS["Sql"]->Query("COMMIT", array());

	}

	function StartTransaction() {

		$GLOBALS["Sql"]->Query("START TRANSACTION", array());

	}

	function Error($error, $sql = false) {
		
		$error = (is_object($error) ? $error->getMessage() : array("code"=>0, "message"=>$error));
										
		$GLOBALS["Error"]->Log($error["code"].": ".$error["message"]." (SQL: ".$sql.")", "sql");

		$GLOBALS["Error"]->Add("SQL ERROR: ".$error["message"]);

		return false;

	}

	function RowCount($sql, $binds) {
		
		if (strpos($sql, "LIMIT 1") !== false) return 1;
		
		$regex = '/^SELECT\s+(?:ALL\s+|DISTINCT\s+)?(?:.*?)\s+FROM\s+(.*)$/i';
		
		if (preg_match($regex, $sql, $output) > 0) {
			
			$new_query = "SELECT COUNT(*) FROM {$output[1]}";
			
			$result = $this->sqlid->prepare($new_query);
			
			if (!$result) {
	
				$this->Error($sql);
				return false;
	
			}
			
			$result->execute($binds);
				
			return $result->fetchColumn();
		
		}
	
		return false;
		
	}

	function Validate($validates, $notest = array(), $error=NULL){

		$noerror = true;

		foreach($validates as $key=>$value) {

			if ((in_array($key, $notest) && $value) || !in_array($key, $notest)) if ($condition = $this->Fetch("SELECT `error_id`, `condition` FROM `validate` WHERE `validate_id`=:validate_id", array("validate_id"=>$key))) {

				if (!preg_match("/".$condition["condition"]."/i", trim($value))) {

					$GLOBALS["Error"]->AddError(($error)?$error:$condition["error_id"]);
					$noerror = false;

				}

			}

		}

		return $noerror;

	}

}

?>