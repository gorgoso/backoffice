<?php

/*

	CPage.php
	------------------------------------------

	Version: 			2.00
	Last change: 	4.8.2010

*/

class CPage extends CTemplate {

	private $page_id, $page, $blocks;

	function __construct($file=NULL) {
		
		list($module, $action) = explode("/", $file);
		
		$this->page_id = $this->GetPage($module, $action);
		//die(json_encode($this->CheckPrivileges()));
		if ($this->CheckPrivileges()==1) {


			if (!$_SESSION["User"]->IsAuth()) {
								
				$file = "main/info";
				$this->page_id = $this->GetPage("main", "info");
				
			} else {
				
				$file = "system/denied";
				$this->page_id = $this->GetPage("system", "denied");

			}				

		}

		$this->blocks = $this->GetBlocks();
		
		$Page = new CTemplate(__TEMPLATES_DIR__."/$file.tpl");
		
		$Page->Add("PATH_ADMIN", __PATH_ADMIN__);
		$Page->AddArray($_SESSION["Language"]->PageDictionary($this->page_id), "LANG_TXT_");
		$Page->AddHTMLArray($this->blocks);
		
		if (file_exists(__MODULES_DIR__."/$file.php")) require_once(__MODULES_DIR__."/$file.php");

		$Page->Translate();

		$this->page = $Page->result;

	}

	public function Show() {

		if (!$this->page) return false;

		echo $this->page;
		unset($this->page);

		return true;

	}

	private function GetPage($module, $action) {
		
		$sqldata = array("module"=>$module, "action"=>$action);
				
		if (!$page = $GLOBALS["Sql"]->Fetch("SELECT * FROM `pages` WHERE `module`=:module AND `action`=:action", $sqldata)) return false;

		return $page["page_id"];
		
		return true;
		
	}
	
	private function GetBlocks() {
		
		$sqldata = array("page_id"=>$this->page_id);
		
		if (!$blocks = $GLOBALS["Sql"]->Values("blocks", "block_id", "name", "`page_id`=:page_id", $sqldata)) return false;
		
		$blocks_tmp = array();
		
		foreach($blocks as $block_id=>$name) $blocks_tmp[$name] = $this->GetBlockPrivileges($block_id);
		
		return $blocks_tmp;
				
	}
	
	private function CheckPrivileges() {
		
		if(!$this->page_id) return false;
		
		$sqldata = array("page_id"=>$this->page_id, "user_id"=>$_SESSION["User"]->user_id, "group_id"=>$_SESSION["User"]->group_id);
		
		$privileges = $GLOBALS["Sql"]->Fetch("SELECT `privileges` FROM `privileges` WHERE `page_id`=:page_id AND `block_id`=0 AND (`user_id`=:user_id OR `user_id`=0) AND (`group_id`=:group_id OR `group_id`=0)", $sqldata);
		$db = new DB_Sql;
		if($sqldata["user_id"]==null)
			$sqldata["user_id"]=0;
		$query = "SELECT privileges FROM privileges WHERE page_id=".$sqldata["page_id"]." AND block_id=0 AND (user_id=".$sqldata["user_id"]." OR user_id=0) AND (group_id=".$sqldata["group_id"]." OR group_id=0)";
		
		$db->query($query);
		if ($db->affected_rows()) {

			$db->next_record();
			return $db->f("privileges")=="true";
		}
		return 0;

		

	}

	private function GetBlockPrivileges($block_id) {
		
		$sqldata = array("page_id"=>$this->page_id, "block_id"=>$block_id, "user_id"=>$_SESSION["User"]->user_id, "group_id"=>$_SESSION["User"]->group_id);
		
		$privileges = $GLOBALS["Sql"]->Fetch("SELECT `privileges` FROM `privileges` WHERE `page_id`=:page_id AND `block_id`=:block_id AND (`user_id`=:user_id OR `user_id`=0) AND (`group_id`=:group_id OR `group_id`=0)", $sqldata);

		if(!$privileges) return false;
		
		return ($privileges["privileges"]=="true");

	}
	
	public function GetPageID() {
		
		return $this->page_id;
		
	}
		
}

?>