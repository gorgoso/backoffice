<?php

/*

	CUser.php
	------------------------------------------

	Version: 			2.00
	Last change: 	4.8.2010

*/

class CUser {

	public $group_id, $lastlog, $regdate;
	public $user_id, $username, $password;
	public $ip, $groups;
	private $isauth;

	function __construct() {

		$this->Groups();

		$this->ChangeGroup(array_search("visitor", $this->groups));

		$this->ip = $GLOBALS["Library"]->GetClientIP();

		return true;

	}

	function Groups() {

		$this->groups = $GLOBALS["Sql"]->Values("groups", "group_id", "name");

		return true;

	}

	function Login($username, $password, $captcha) {

		$captcha = strtoupper($captcha);

		$this->isauth = $this->Auth($username, $password, $captcha);

		return $this->isauth;

	}

	function Auth($username, $password, $captcha) {

		if (!$this->CheckCaptcha($captcha)) return false;

		$this->username = $username;
		$this->password = $password;

		if ($this->CheckPasswordAttempt()) {

			$this->DisableUser();
			$GLOBALS["Error"]->AddSession("MAX_PASSWORD_ATTEMPT");

		}

		if ($this->GetUserdata())
		{

			if (!$this->IpClearance()) {

				$GLOBALS["Error"]->AddSession("IP_NOT_ALLOWED");
				$this->Logout();

				return false;

			}

			$this->Logged();

			$GLOBALS["Error"]->Log($this->username." was logged from IP: ".$this->ip, "info");

			unset($_SESSION["Captcha"], $_SESSION["PasswordAttempt"]);

			return true;

		} else {

			$GLOBALS["Error"]->AddSession("BAD_LOGIN_INFORMATION");
			return false;

		}
	}

	function CheckCaptcha($captcha) {

		if (!isset($_SESSION["Captcha"])) return false;

		if ($captcha!=$_SESSION["Captcha"]) {

			$GLOBALS["Error"]->AddSession("BAD_CAPTCHA");
			return false;

		}

		return true;

	}

	function CheckPasswordAttempt() {

		if (!isset($_SESSION["PasswordAttempt"])) $_SESSION["PasswordAttempt"]=0;

		if ($_SESSION["PasswordAttempt"]<__MAX_PASSWORD_ATTEMPTS__) {

			$_SESSION["PasswordAttempt"]=$_SESSION["PasswordAttempt"]+1;
			$GLOBALS["Error"]->Log("User ".$this->username." fill bad password - attempt: ".$_SESSION["PasswordAttempt"].".", "warning");

			return false;

		}

		return true;

	}

	function IpClearance($allowed_ips = NULL) {

		$allowed_ips = ($allowed_ips) ? $allowed_ips : $this->allowed_ip;

		if(!$allowed_ips) return true;

		$ips = array_map("trim", explode(",", $allowed_ips));

		return in_array($this->ip, $ips);

	}

	function GetUserdata()
	{

		$sqldata = array("username"=>$this->username);

		if (!$userdata = $GLOBALS["Sql"]->Fetch("SELECT * FROM `users` WHERE `username`=:username", $sqldata)) return false;
	/* if (!password_verify($this->password, $userdata["password"])) return false; */

		switch($userdata["status"]) {

			case "disabled":

				$GLOBALS["Error"]->AddSession("USER_DISABLED");
				return false;

			break;

			case "deleted":

				$GLOBALS["Error"]->AddSession("USER_DELETED");
				return false;

			break;

			case "pending":

				$GLOBALS["Error"]->AddSession("USER_PENDING");
				return false;

			break;

			case "active":

				if (!$this->SetUserdata($userdata)) return false;
				return true;

			break;

			default:

				$GLOBALS["Error"]->AddSession("BAD_USER_STATUS");
				return false;

			break;

		}

	}

	function Logged() {

		$GLOBALS["Sql"]->Delete("logged", NULL, NULL, "(`time`)+86400<UNIX_TIMESTAMP(NOW())");

		if ($loginfo = $GLOBALS["Sql"]->Fetch("SELECT * FROM `logged` WHERE `user_id`=:user_id", array("user_id"=>$this->user_id))) {

			if ($loginfo["session"]!=session_id()) {

				$GLOBALS["Error"]->Log("User ".$this->username." was logout due second access from IP: ".$this->ip, "warning");
				if (file_exists(__SESSION_PATH__."/sess_".$loginfo["session"])) unlink(__SESSION_PATH__."/sess_".$loginfo["session"]);

			}
		}

		$sqldata = array("user_id"=>$this->user_id, "session"=>session_id(), "ip"=>$this->ip);
		$GLOBALS["Sql"]->Query("REPLACE `logged` SET `user_id`=:user_id, `session`=:session, `time`=UNIX_TIMESTAMP(), `ip`=:ip", $sqldata);

		$sqldata = array("user_id"=>$this->user_id);
		$GLOBALS["Sql"]->Query("UPDATE `users` SET `lastlog`=UNIX_TIMESTAMP() WHERE `user_id`=:user_id", $sqldata);

		return true;

	}

	function DisableUser() {

		$GLOBALS["Sql"]->Update("users", array("status"=>"disabled"), "username", $this->username, true);

		$GLOBALS["Error"]->Log("User ".$this->username." account was disabled.", "warning");

		$this->Logout();

		return true;
	}

	function SetUserdata($userdata) {

		$keys = array_keys($GLOBALS["Library"]->GetOptions("userdata"));

		foreach($keys as $k) if (array_key_exists($k, $userdata)) $this->$k = $userdata[$k];

		return true;

	}

	function ChangeGroup($group_id){

		$this->group_id = $group_id;

		return true;

	}

	function Logout() {

		$GLOBALS["Sql"]->Delete("logged", "user_id", $this->user_id);
		$GLOBALS["Error"]->Log("User ".$this->username." logout.", "info");

		if (file_exists(__SESSION_PATH__."/sess_".session_id())) unlink(__SESSION_PATH__."/sess_".session_id());

		$_SESSION["User"] = new CUser();

		return true;

	}

	function UserExists($username) {

		$confirm = $GLOBALS["Sql"]->Value("users", "username", "username", $username);

		return $confirm ? true:false;

	}

	function InGroup($groups) {

		if (!is_array($groups)) $groups = array($groups);

		return in_array($_SESSION["User"]->group_id, $groups);

	}

	public function IsAuth() {

		return ($this->isauth===true) ? true : false;

	}

}

?>