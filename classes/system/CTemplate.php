<?php

/*

	CTemplate.php
	------------------------------------------

	Version: 			2.00
	Last change: 	4.8.2010

*/

class CTemplate {

	public $result;
	private $source;
	public $replace, $replaceEx;

	function __construct($file=NULL) {

		$this->replace = array();

		if ($file) if (!$this->result = $this->OpenTemplate($file)) return false;

		return true;
		
	}

	private function OpenTemplate($file, $isstring=false) {

		$this->source = ($isstring)?$file:@file_get_contents($file);
		return $this->source;

	}

	public function StringTemplate($string) {

		if ($string) if (!$this->result = $this->OpenTemplate($string, true));
		return true;

	}
	
	public function Reset() {

		$this->result = $this->source;

	}

	public function Add($key, $value) {

		$this->replace[$key] = $value;
		return true;

	}

	public function AddArray($array, $prefix = ""){

		if(is_array($array)) foreach($array as $key=>$value) $this->Add(strtoupper($prefix.$key), $value);

	}

	public function AddHTML($key, $value) {

		$this->replaceEx[$key] = $value;
		return true;

	}

	public function AddHTMLArray($array, $prefix = "") {

		if(is_array($array)) foreach($array as $key=>$value) $this->AddHTML($prefix.$key, $value);

	}

	public function TemplateEmpty() {

		$this->replace = array();
		$this->replaceEx = array();

		return true;

	}

	public function Translate() {

		$ok = false;
		
		if (count($this->replace)) {
		
			foreach ($this->replace as $key => $value) $this->result = str_replace(array("{".$key."}", "{-".$key."}"),$value,$this->result);
			$ok = true;
		
		}

		if (count($this->replaceEx)) {

			foreach ($this->replaceEx as $key => $value) {

				if ($value) {

	  			$this->result = str_replace(array("{".$key."}", "{-".$key."}"),"",$this->result);

				} else {

	  			$this->result = preg_replace("/\{$key\}(.*?)\{-$key\}/ms","",$this->result);
	  				
				}

				$ok = true;

			}

			return $ok;

		}
	}

	public function RepeatHTML($add_array, $addhtml_array=NULL) {

		$tmpresult = "";
		
		if ($add_array) foreach ($add_array as $key => $array) {

			$this->Reset();

			$this->AddArray($array);
			$this->AddArray($_SESSION["Language"]->PageDictionary(), "LANG_TXT_");

			if(is_array($addhtml_array[$key])) $this->AddHTMLArray($addhtml_array[$key]);
			
			$this->Translate();

			$tmpresult .= $this->result;

		}

		return $tmpresult;

	}

}
?>