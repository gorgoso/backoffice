<?php

/*

	CLanguage.php
	------------------------------------------

	Version: 			1.0
	Last change: 	15.11.2007

*/


class CLanguage {

	private $dictionary;
	public $language;

	function __construct($lang = false) {

		if(!$lang) $lang = __DEFAULT_LANGUAGE__;
		
		$this->ChangeLanguage($lang);

	}
	
	function GetLanguages($lang = __LANGUAGE__) {
				
		return $GLOBALS["Library"]->GetOptions("languages", $lang);
		
	}

	function ChangeLanguage($lang) {
		
		if(!in_array($lang, array_keys($this->GetLanguages($lang)))) {

			$lang = __DEFAULT_LANGUAGE__;
			$GLOBALS["Error"]->AddError("UNKNOWN_LANGUAGE - DEFAULT_LANGUAGE_LOADED");

		}
		
		$this->language = $lang;
		$this->ResetDictionary();
		$this->LoadDictionary(0);
		
		setlocale(LC_ALL, $lang);
		
		return true;
		
	}

	function LoadDictionary($page_id) {

		$dictionary = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `languages` WHERE `language`=:language AND `page_id`=:page_id", array("language"=>$this->language, "page_id"=>$page_id));
				
		foreach($dictionary as $word) $this->dictionary[$word["key"]] = $word["text"];
				
		return true;

	}

	function Translate($key) {

		if(isset($this->dictionary[$key])) return $this->dictionary[$key];

		return false;

	}

	function PageDictionary($page_id = NULL) {

		if ($page_id) $this->LoadDictionary($page_id);
					
		return $this->dictionary;

	}
	
	public function ResetDictionary() {
		
		$this->dictionary = array();
		
		return true;
		
	}
	
}

?>