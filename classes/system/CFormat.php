<?php

/*

	CFormat.php
	------------------------------------------

	Version: 			1.00
	Last change: 	9.1.2012

*/

class CFormat {

	public function Text($txt) {

		return preg_replace("/\s\s+/"," ", trim(strip_tags($txt)));

	}
	
	public function TextArray($texts, $update = array()) {
		
		foreach($update as $u) {
			
			if (array_key_exists($u, $texts)) $texts[$u] = $this->Text($texts[$u]);
		
		}
		
		return $texts;
		
	}

	public function Number($number, $decimal = 2, $point = ".", $thousands=",") {
		
		return number_format(preg_replace("/[^0-9\.-]/", "", $number), $decimal, $point, $thousands);

	}
	
	public function Date($timestamp = NULL, $format = NULL) {
		
		if (!$timestamp) $timestamp = time();
												
		$days = $GLOBALS["Library"]->GetOptions("days");
		$months = $GLOBALS["Library"]->GetOptions("months");
										
		return str_replace(array("{DAY}", "{MONTH}"), array($days[date("N", $timestamp)], $months[date("n", $timestamp)]), date($format ? $format : __FORMAT_DATE__, $timestamp));
		
	}
	
	public function Id($id, $zeros = 6) {
		
		if (!$id) return false;
		
		return sprintf("%0".$zeros."d", $id);
		
	}
	
	public function Phone($number) {
				
		switch(strlen($number)) {

			case 6:
				return preg_replace("/([0-9]{3})([0-9]{3})/", "$1-$2", $number);
			break;
			
			case 7:
				return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $number);
			break;

			case 9:
				return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{3})/", "$1-$2-$3", $number);
			break;

			case 10:
				return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $number);
			break;

			case 11:
				return preg_replace("/([0-9]{4})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $number);
			break;
			
		}
		
		return $number;
		
	}
	
	public function Uppercase($text) {
		
		return mb_strtoupper($text, 'UTF-8');
		
	}

	public function Lowercase($text) {
		
		return mb_strtolower($text, 'UTF-8');
		
	}

}

?>