<?php

/*

	CConfig.php
	------------------------------------------

	Version: 			2.00
	Last change: 	4.8.2010

*/

class CConfig {

	function __construct() {

		$this->GetConstants();
		$this->SetSessionData();

		return true;

	}

	function GetConstants() {

		$constants = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `config`");
		foreach($constants as $constant) define("__".$constant["config_id"]."__", $constant["value"]);

		return true;

	}

	function SetSessionData() {

		$language = $GLOBALS["Library"]->Register("language");

		if(!isset($_SESSION["Language"])) $_SESSION["Language"] = new CLanguage($language);
		elseif($language) $_SESSION["Language"]->ChangeLanguage($language);

		define("__MODULE__", (isset($GLOBALS["_GET"]["module"]))?$GLOBALS["_GET"]["module"]:"main");
		define("__ACTION__", (isset($GLOBALS["_GET"]["action"]))?$GLOBALS["_GET"]["action"]:"info");
		define("__LANGUAGE__", $_SESSION["Language"]->language);

		if (!isset($_SESSION["User"])) $_SESSION["User"]=new CUser();

		if (isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["captcha"]) && !$_SESSION["User"]->IsAuth()) {

			if ($_SESSION["User"]->Login($_POST["username"], $_POST["password"], $_POST["captcha"])) {

				// set special USER options
				$_SESSION["Filter"] = new CFilter();
				if ($redirect = $GLOBALS["Library"]->Register("redirect")) $GLOBALS["Library"]->Redirect($redirect);

			}
		}

	}

}

?>