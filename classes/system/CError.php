<?php

/*

	CError.php
	------------------------------------------

	Version: 			2.00
	Last change: 	4.8.2010

*/

class CError {

	public $errors = array("error"=>array(), "warning"=>array(), "notice"=>array());

	function Add($id_error = "UNDEFINED", $type = "error", $add = false) {
		
		$error_txt = (isset($GLOBALS["Sql"])) ? $GLOBALS["Sql"]->Fetch("SELECT `text` FROM `errors` WHERE `language`=:language AND `error_id`=:id_error", array("language"=>__LANGUAGE__, "id_error"=>$id_error)) : $id_error;

		if(!$error_txt && $add) $GLOBALS["Sql"]->Insert("errors", array("error_id"=>$id_error, "language"=>__LANGUAGE__, "text"=>__MODULE__."/".__ACTION__));

		if(isset($this->errors[$type])) $this->errors[$type][] = array("error_id"=>$id_error, "error_txt"=>($error_txt)?($error_txt["text"]?$error_txt["text"]:$id_error):$id_error);

	}

	function AddSession($id_error = "UNDEFINED", $type = "error", $add = true) {
				
		$error_txt = (isset($GLOBALS["Sql"])) ? $GLOBALS["Sql"]->Fetch("SELECT `text` FROM `errors` WHERE `language`=:language AND `error_id`=:id_error", array("language"=>__LANGUAGE__, "id_error"=>$id_error)) : $id_error;

		if(!$error_txt && $add) $GLOBALS["Sql"]->Insert("errors", array("error_id"=>$id_error, "language"=>__LANGUAGE__, "text"=>__MODULE__."/".__ACTION__));

		if(isset($this->errors[$type])) $_SESSION["errors"][$type][] = array("error_id"=>$id_error, "error_txt"=>($error_txt)?($error_txt["text"]?$error_txt["text"]:$id_error):$id_error);

	}
	
	function Log($text, $importance="info") {
		
		file_put_contents("error.log", date("Y-m-d H:i:s")."\t".$text."\n");

		if (isset($GLOBALS["Sql"])) $GLOBALS["Sql"]->Insert("logs", array("date"=>date("Y-m-d H:i:s"), "importance"=>$importance, "logdata"=>$text), "log_id");
		
		return true;

	}

	function Exists($type = false){

		if(isset($_SESSION["errors"])) {

			if(!$type) {

				foreach($this->errors as $type_err=>$errors) if(isset($_SESSION["errors"][$type_err])) return true;

			} elseif(isset($_SESSION["errors"][$type])) return true;

		}

		$array = $this->errors;

		if(!$type) {

			foreach($array as $type_err=>$errors) if(!empty($errors)) return true;

		} else return !empty($array[$type]);

		return false;

	}

	function Get($type = false) {

		$array = $this->errors;

		if(!$type) return $array;
		else return $array[$type];

	}

	function Show() {

		$error_txt = "";

		foreach($this->errors as $type=>$errors) {

			$ErrorTemplate = new CTemplate(__TEMPLATES_DIR__."/system/err-$type.tpl");
			$error_txt .= $ErrorTemplate->RepeatHTML($errors);

		}

		if(isset($_SESSION["errors"])) {

			foreach($_SESSION["errors"] as $type=>$errors) {

				$ErrorTemplate = new CTemplate(__TEMPLATES_DIR__."/system/err-$type.tpl");
				$error_txt .= $ErrorTemplate->RepeatHTML($errors);

			}
			
			unset($_SESSION["errors"]);

		}

		return $error_txt;

	}

}

?>