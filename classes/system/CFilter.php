<?php

/*

	CFilter.php
	------------------------------------------

	Version: 			2.00
	Last change: 	4.8.2010

*/

class CFilter {

	private $filters;

	function __construct() {

		$this->filters = array();

	}

	function GetFilter($filter=false, $orderby=NULL, $sort="", $start=0, $module=__MODULE__, $action=__ACTION__) {

		$reset = $GLOBALS["Library"]->Register("reset");
		if($reset) $this->ResetFilter();

		if (!isset($this->filters["${module}_${action}"])) {

			$this->SetFilter($filter, $orderby, $sort, $start, $module, $action);

		} else {

			$setFilter = $GLOBALS["Library"]->Register("filter");
			$setOrder = $GLOBALS["Library"]->Register("order");
			$setSort = $GLOBALS["Library"]->Register("sort");
			$setStart = $GLOBALS["Library"]->Register("start");

			if(($setFilter || $setOrder || $setSort || $setStart!==false)) {

				$this->SetFilter($setFilter, $setOrder, $setSort, $setStart, $module, $action);

			}

		}

		return $this->filters["${module}_${action}"];

	}

	function SetFilter($filter=false, $orderby=false, $sort=false, $start=0, $module=__MODULE__, $action=__ACTION__) {

		if(isset($filter["dates"])) foreach($filter["dates"] as $key=>$value) {

			if (isset($filter["dates"][$key][0])) if ($filter["dates"][$key][0]) $filter["dates"][$key][0]=$GLOBALS["Library"]->GetTimestamp($value[0]); else unset($filter["dates"][$key][0]);
			if (isset($filter["dates"][$key][1])) if ($filter["dates"][$key][1]) $filter["dates"][$key][1]=$GLOBALS["Library"]->GetTimestamp($value[1])+59; else unset($filter["dates"][$key][1]);

		}

		if ($filter) $this->filters["${module}_${action}"]["in"]["conditions"] = $filter;
		if ($orderby) $this->filters["${module}_${action}"]["in"]["order"] = $orderby;
		if ($sort) $this->filters["${module}_${action}"]["in"]["sort"] = $sort;
		$this->filters["${module}_${action}"]["in"]["start"] = ($start!==false)?$start:0;

		$filter = ($filter)?$filter:((isset($this->filters["${module}_${action}"]["in"]["conditions"]))?$this->filters["${module}_${action}"]["in"]["conditions"]:false);
		$order = ($orderby)?$orderby:((isset($this->filters["${module}_${action}"]["in"]["order"]))?$this->filters["${module}_${action}"]["in"]["order"]:"");
		$sort = ($sort)?$sort:((isset($this->filters["${module}_${action}"]["in"]["sort"]))?$this->filters["${module}_${action}"]["in"]["sort"]:"");
		$limit = $this->filters["${module}_${action}"]["in"]["start"];

		$new = $this->GenerateFilter($filter, $order, $sort, $limit);

		$this->filters["${module}_${action}"] = array_merge($this->filters["${module}_${action}"], $new);

		return true;

	}

	function QuoteTables($tables) {

		return str_replace(array(",","."), array("`,`", "`.`"), $tables);

	}

	function GenerateFilter($filter=false, $orderby=false, $sort=false, $start=0) {

		$return = array("conditions"=>NULL, "order"=>NULL, "sort"=>NULL, "limit"=>NULL);

		if ($filter) {

			foreach ($filter as $key=>$value) {

				if (is_array($value)) {

					if (isset($value["multiple"])) $return["conditions"].=$this->SqlMultiple($value["multiple"], $key);
					elseif ($key == "dates") if ($where_dates = $this->SqlDateRange($value)) $return["conditions"].="(".$where_dates.") AND";

				} else $return["conditions"].=$this->SqlLike($value, $key);

			}

			$return["conditions"] = substr($return["conditions"], 0,-4);

			if (!$return["conditions"]) $return["conditions"] = " 1";

		} else $return["conditions"] = " 1";

		$return["order"] = ($orderby)?" ORDER BY `".$this->QuoteTables($orderby)."`":"";
		$return["sort"] = (preg_match("/^asc|desc$/",$sort) && $orderby)?" $sort":"";
		$return["limit"] = " LIMIT ".($start!==false?$start:0).",".__FILTER_PER_PAGE__;
		$return["where"] = $return["conditions"].$return["order"].$return["sort"].$return["limit"];

		return $return;

	}

	function SqlDateRange($dates) {

		if (!is_array($dates)) return "";

		$return = "";

		foreach($dates as $key=>$value) {

			if (isset($value[0])) {

				if ($value[0]) {

					$return.="(`".$this->QuoteTables($key)."` >= '".$value[0]."')";

				} else $return.="1";

			} else $return.="1";

			if(isset($value[1])) if($value[1]) {

				$return.=" AND (`".$this->QuoteTables($key)."` <= '".$value[1]."')";

			}

		}

		return $return;

	}

	function SqlMultiple($multiple, $table) {

		$multi = array();
		foreach($multiple as $mval) if ($mval) $multi[] = "(`".$this->QuoteTables($table)."` = '$mval')";

		$return = $multi ? "(".implode(" OR ", $multi).") AND " : "";

		return $return;

	}

	function SqlLike($value, $table) {

		if ($value=="NULL") $return = "(`".$this->QuoteTables($table)."` IS NULL) AND";
		else $return ="(`".$this->QuoteTables($table)."` LIKE '%$value%'".(!$value?(" OR `".$this->QuoteTables($table)."`  IS NULL"):"").") AND ";

		return $return;

	}


	function ResetFilter($module = __MODULE__, $action = __ACTION__) {

		unset($this->filters["${module}_${action}"]);
		return true;

	}

	function SetCondition($key, $value, $multiple = false, $module = __MODULE__, $action = __ACTION__) {

		if ($multiple) $this->filters["${module}_${action}"]["in"]["conditions"][$key]["multiple"][0] = $value;
			else $this->filters["${module}_${action}"]["in"]["conditions"][$key] = $value;

		$this->SetFilter(false, false, false, 0, $module, $action);

		return $this->filters["${module}_${action}"];

	}

	function GetCondition($key, $multiple = false, $module = __MODULE__, $action = __ACTION__) {

		if ($multiple) if (!isset($this->filters["${module}_${action}"]["in"]["conditions"][$key]["multiple"][0])) return false;

		if (!isset($this->filters["${module}_${action}"]["in"]["conditions"][$key])) return false;

		return $multiple ? $this->filters["${module}_${action}"]["in"]["conditions"][$key]["multiple"][0] : $this->filters["${module}_${action}"]["in"]["conditions"][$key];

	}

	function GetSort($module = __MODULE__, $action = __ACTION__) {

		if (!isset($this->filters["${module}_${action}"]["in"]["sort"])) return "desc";

		return ($this->filters["${module}_${action}"]["in"]["sort"]=="asc") ? "desc" : "asc";

	}

}

?>