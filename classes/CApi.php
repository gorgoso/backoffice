<?php

/*

	CApi.php
	------------------------------------------

	Version: 			2.00
	Last change: 	4.8.2010

*/

class CApi {

	function __construct() {

		$username = isset($_SERVER["PHP_AUTH_USER"])?$_SERVER["PHP_AUTH_USER"]:false;
		$password = isset($_SERVER["PHP_AUTH_PW"])?$_SERVER["PHP_AUTH_PW"]:false;

		if ($username != $GLOBALS["Library"]->Decrypt(__API_USERNAME__) || $password != $GLOBALS["Library"]->Decrypt(__API_PASSWORD__)) $this->Error("API_ACCESS_NOT_AUTHORIZED");

		return true;

	}

	private function Error($error_message) {

		throw new SoapFault("API", $error_message);

	}

	public function Auth($email, $password, $ip) {

		$client = $this->GetClient($email, "email");

		if (!$client) $this->Error("CLIENT_NOT_EXIST");
		if (!password_verify($password, $client["client"]["password"])) $this->Error("CLIENT_NOT_AUTHORIZED");

		if (!is_null($client["client"]["allowed_ip"])) {

			$ips = array_map("trim", explode(",", $client["client"]["allowed_ip"]));

			if (!in_array($ip, $ips)) $this->Error("CLIENT_IP_NOT_ALLOWED");

		}

		$GLOBALS["Sql"]->Query('UPDATE `clients` SET `log_date`=UNIX_TIMESTAMP() WHERE `client_id`=:client_id', array("client_id"=>$client["client"]["client_id"]));

		return $client["client"];

	}

	public function SendEmail($client_id, $document, $language, $specials, $attachement = false) {

		$_SESSION["Language"]->ChangeLanguage($language);

		$data = array(
			"CLIENT_ID_TXT" => $GLOBALS["Format"]->Id($client_id)
		);

		$client = $this->GetClient($client_id, "client_id");
		$tour = $this->GetTour($client["client"]["tour_id"], "tour_id");

		foreach($client as $prefix=>$values) foreach($values as $key=>$value) $data[$prefix."_".$key] = $value;
		foreach($tour as $prefix=>$values) foreach($values as $key=>$value) $data[$prefix."_".$key] = $value;

		$data = array_merge($data, $specials);

		$email_txt = $GLOBALS["Library"]->FormatEmail($document, $data, $language);

		if ($attachement) {

			require_once("classes/CGenerator.php");
			$Generator = new CGenerator($language);

			switch($attachement) {

				case "registration-leader":
					$attachement = array("type"=>"application/pdf", "name"=>$_SESSION["Language"]->Translate("TOUR-CONTRACT").".pdf", "data"=>$Generator->TourLeaderContract($client_id, true));
				break;

			}

		}

		return $GLOBALS["Library"]->SendMail($data["client_email"], $_SESSION["Language"]->Translate($GLOBALS["Format"]->Uppercase($document)), $email_txt, $attachement);

	}

	public function GetClient($id, $type) {

		if (!$results = $GLOBALS["Client"]->Get($id, $type)) return false;

		return $this->Update($results);

	}

	public function GetTour($id, $type) {

		if (!$results = $GLOBALS["Tour"]->Get($id, $type)) return false;

		return $this->Update($results);

	}

	public function GetDepartures() {

		if (!$results = $GLOBALS["Tour"]->GetDepartures()) return false;

		return $this->Update($results);

	}

	public function Register($tour, $client) {

		$GLOBALS["Sql"]->StartTransaction();

		if (isset($tour["tour_id"])) $tour_id = $tour["tour_id"];

		if (!$tour_id) if (!$tour_id = $GLOBALS["Tour"]->Add($tour["date"], $tour["departure_id"], $tour["tour"], $tour["price"])) return false;
		if (!$client_id = $GLOBALS["Client"]->Add($tour_id, $client)) return false;

		if (!$GLOBALS["Client"]->AddContact($client_id, "home_phone", $client["phone"])) return false;
		if (!$GLOBALS["Client"]->AddContact($client_id, "email", $client["email"])) return false;

		if ($client["organization"]) if (!$GLOBALS["Client"]->Organization($client_id, array("name"=>$client["organization"], "type"=>"church"))) return false;

		$GLOBALS["Sql"]->Commit();

		return $client_id;

	}

	public function UpdateClient($client_id, $update, $ip) {

		$updated = false;

		$GLOBALS["Sql"]->StartTransaction();

		if ($GLOBALS["Client"]->Update($client_id, $update["client"])) $updated = true;
		if ($GLOBALS["Client"]->Address($client_id, $update["address"])) $updated = true;
		if ($GLOBALS["Client"]->Contact($client_id, "home_phone", $update["contact"]["home_phone"])) $updated = true;
		if ($GLOBALS["Client"]->Contact($client_id, "cell_phone", $update["contact"]["cell_phone"])) $updated = true;
		if ($GLOBALS["Client"]->Contact($client_id, "emergency_phone", $update["contact"]["emergency_phone"], $update["contact"]["emergency_name"])) $updated = true;

		if ($GLOBALS["Client"]->Information($client_id, "passport_number", $update["information"]["passport_number"])) $updated = true;
		if ($GLOBALS["Client"]->Information($client_id, "passport_country", $update["information"]["passport_country"])) $updated = true;
		if ($GLOBALS["Client"]->Information($client_id, "passport_issued", $update["information"]["passport_issued"])) $updated = true;
		if ($GLOBALS["Client"]->Information($client_id, "passport_expired", $update["information"]["passport_expired"])) $updated = true;
		if ($GLOBALS["Client"]->Information($client_id, "travelling", $update["information"]["travelling"])) $updated = true;

		if ($update["information"]["travelling"] == "roommates") {

			if ($GLOBALS["Client"]->Information($client_id, "roommate1", $update["information"]["roommate1"])) $updated = true;
			if ($GLOBALS["Client"]->Information($client_id, "roommate2", $update["information"]["roommate2"])) $updated = true;

		}

		if (!$updated) return false;
		$GLOBALS["Sql"]->Commit();

		return true;

	}

	public function UpdateOrganization($client_id, $update, $ip) {

		$updated = false;

		$GLOBALS["Sql"]->StartTransaction();

		if ($GLOBALS["Client"]->Organization($client_id, $update)) $updated = true;

		if (!$updated) return false;
		$GLOBALS["Sql"]->Commit();

		return true;

	}

	public function Request($client_id, $request) {

		if (!$GLOBALS["Client"]->Request($client_id, $request)) return false;

		return true;

	}

	public function Brochure($client_id, $data, $language) {

		require_once("classes/CGenerator.php");
		$Generator = new CGenerator($language);

		return $Generator->Brochure($client_id, $data);

	}

	public function UpdatePassword($client_id, $password, $ip) {

		if (!$GLOBALS["Client"]->UpdatePassword($client_id, $password)) return false;

		return true;

	}

	private function Update($updated) {

		foreach($updated as $key=>$values) {

			if (is_array($values)) $updated[$key] = $this->Update($values);
			else {

				$updated[$key] = trim($values);
				$updated[$key] = htmlentities($values);

			}

		}

		return $updated;

	}

	public function Hash($string) {

		return $GLOBALS["Common"]->Hash($string);

	}

	public function GetOptions($group, $language) {

		if (!$results = $GLOBALS["Library"]->GetOptions($group, $language)) return false;

		return $results;

	}

	public function Validate($validates, $notest, $error) {

		$errors = array();

		foreach($validates as $key=>$value) {

			if ((in_array($key, $notest) && $value) || !in_array($key, $notest)) if ($condition = $GLOBALS["Sql"]->Fetch("SELECT `error_id`, `condition` FROM `validate` WHERE `validate_id`=:validate_id", array("validate_id"=>$key))) {

				if (!preg_match("/".$condition["condition"]."/i", trim($value))) {

					$errors[] = $error ? $error : $condition["error_id"];

				}

			}

			if ($key == "card_number") if (!$GLOBALS["Common"]->CheckCreditCard($value)) $errors[] = "BAD_CARD_NUMBER";

		}

		return $errors;

	}

	public function Payment($client_id, $payment) {

		$GLOBALS["Sql"]->StartTransaction();

		if (!$client = $GLOBALS["Client"]->Get($client_id, "client_id")) return false;

		if (!$payment_id = $GLOBALS["Client"]->AddPayment($client_id, $payment)) return false;

		//if (!$results = $GLOBALS["Client"]->ProcessPayment($payment_id)) return false;

		$GLOBALS["Sql"]->Commit();

		return $payment_id;

	}

	public function UpdatePayment($payment_id, $payment) {

		if (!$GLOBALS["Sql"]->Update("clients-payments", $payment, "payment_id", $payment_id)) return false;

		return true;

	}

	public function eCheck($payment_id, $echeck) {

		if (!$payment = $GLOBALS["Sql"]->Fetch("SELECT * FROM `clients-payments` WHERE `payment_id`=:payment_id", array("payment_id"=>$payment_id))) return false;

		$data = array(
			"Client_ID"=>$GLOBALS["Library"]->Decrypt(__ECHECK_CLIENTID__),
			"ApiPassword"=>$GLOBALS["Library"]->Decrypt(__ECHECK_PASSWORD__),
			"Name"=>$echeck["fullname"],
			"EmailAddress"=>$echeck["email"],
			"Phone"=>$GLOBALS["Format"]->Phone($echeck["phone"]),
			"PhoneExtension"=>"",
			"Address1"=>$echeck["address1"],
			"Address2"=>$echeck["address2"],
			"City"=>$echeck["city"],
			"State"=>$echeck["state"],
			"Zip"=>$echeck["zip"],
			"Country"=>$echeck["country"],
			"RoutingNumber"=>$echeck["routing_number"],
			"AccountNumber"=>$echeck["account_number"],
			"BankName"=>$echeck["bank_name"],
			"CheckMemo"=>"GST".$GLOBALS["Format"]->Id($payment_id),
			"CheckAmount"=>$payment["amount"],
			"CheckDate"=>$echeck["check_date"],
			"CheckNumber"=>$echeck["check_number"],
			"x_delim_data"=>false,
			"x_delim_char"=>false
		);

		if (!$results = $GLOBALS["Library"]->PostRequest("https://www.greenbyphone.com/eCheck.asmx/OneTimeDraftRTV", $data)) return false;

		$Xml = simplexml_load_string($results);

		if ((string)$Xml->Result != "0") return (string)$Xml->ResultDescription;

		$this->UpdatePayment($payment_id, array("ext_payment_id"=>(string)$Xml->Check_ID, "pay_date"=>time(), "method"=>"echeck", "status"=>"paid"));
		return true;

	}

	public function Unsubscribe($email) {

		if (!$GLOBALS["Sql"]->Insert("unsubscribed", array("email"=>$email))) return false;

		return true;

	}

}

?>