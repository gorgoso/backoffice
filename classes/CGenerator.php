<?php

/*

	CGenerator.php
	------------------------------------------

	Version: 			1.00
	Last change: 	6/14/2013 3:36:00 PM

*/

class CGenerator {

	private $informations;
	private $months, $today;
	private $client, $tour, $additional;
	private $language;

	function __construct($language = "en") {

		$this->language = $language;
		$this->informations = array();
		$this->months = $this->GetOptions("months");

		$this->today = $this->SplitDate();

		return true;

	}

	public function SplitDate($date = false, $isdate = false) {

		if (!$date) $date = time();

		list($return["year"], $return["month"], $return["day"]) = explode("-", $isdate ? $date : date("Y-n-j", $date));

		return $return;

	}

	private function GetOptions($group) {

		return $GLOBALS["Library"]->GetOptions($group, $this->language);

	}

	public function Informations($client_id) {

		if (!$this->client) if (!$this->client = $GLOBALS["Client"]->Get($client_id, "client_id")) return false;
		if (!$this->tour) if (!$this->tour = $GLOBALS["Tour"]->Get($this->client["client"]["tour_id"], "tour_id")) return false;

		$titles = $GLOBALS["Library"]->GetOptions("client_titles");
		$departures = $GLOBALS["Tour"]->GetDepartures();
		$tours = $this->GetOptions("tours");

		$departure_date = $this->SplitDate($this->tour["tour"]["departure_date"]);

		$contacts = array();
		foreach($this->client["contact"] as $contact) $contacts[$contact["type"]] = $contact["contact"];

		$address = $this->client["address"];

		$data = array(

			// DEFAULTS
			"TODAY_DAY"=>$GLOBALS["Format"]->Number($this->today["day"], 0),
			"TODAY_MONTH"=>$GLOBALS["Format"]->Number($this->today["month"], 0),
			"TODAY_MONTH_TEXT"=>ucfirst($this->months[$this->today["month"]]),
			"TODAY_YEAR"=>$this->today["year"],

			// TOUR DETAILS
			"TOUR_NAME"=>$tours[$this->tour["tour"]["tour"]],
			"TOUR_PRICE"=>$GLOBALS["Format"]->Number($this->tour["tour"]["price"], 2),
			"TOUR_CODE"=>$this->tour["tour"]["code"],
			"DEPARTURE_DAY"=>$GLOBALS["Format"]->Number($departure_date["day"], 0),
			"DEPARTURE_MONTH"=>$GLOBALS["Format"]->Number($departure_date["month"], 0),
			"DEPARTURE_MONTH_TEXT"=>ucfirst($this->months[$departure_date["month"]]),
			"DEPARTURE_YEAR"=>$departure_date["year"],
			"DEPARTURE_CITY"=>$departures[$this->tour["tour"]["departure_id"]]["city"].", ".$GLOBALS["Format"]->Uppercase($departures[$this->tour["tour"]["departure_id"]]["state"]),

			// CLIENT DETAILS
			"TITLE"=>$this->client["client"]["title"] ? $titles[$this->client["client"]["title"]] : "",
			"FIRSTNAME"=>$this->client["client"]["firstname"],
			"LASTNAME"=>$this->client["client"]["lastname"],
			"ADDRESS"=>$address["address1"].($address["address2"] ? ", ".$address["address2"] : "").($address["address3"] ? ", ".$address["address3"] : ""),
			"ADDRESS_FULL"=>$address["address1"].($address["address2"] ? ", ".$address["address2"] : "").($address["address3"] ? ", ".$address["address3"] : "").($address["city"] ? ", ".$address["city"] : "").($address["state"] ? ", ".$address["state"] : ""),
			"HOME_PHONE"=>isset($contacts["home_phone"]) ? $GLOBALS["Format"]->Phone($contacts["home_phone"]) : "",
			"WORK_PHONE"=>isset($contacts["work_phone"]) ? $GLOBALS["Format"]->Phone($contacts["work_phone"]) : "",
			"CELLULAR_PHONE"=>isset($contacts["cell_phone"]) ? $GLOBALS["Format"]->Phone($contacts["cell_phone"]) : "",
			"EMAIL"=>$contacts["email"],

		);

		$this->additional = $data;

		return $this->additional;

	}

	public function TourLeaderContract($client_id, $raw=false) {

		$this->Informations($client_id);

		$days = $this->tour["tour"]["tour"]=="israel-jesus" ? 10 : 12;

		$data = array_merge($this->additional, array(

			"DEPARTURE_DATE"=>$GLOBALS["Format"]->Date($this->tour["tour"]["departure_date"]),
			"ISRAEL_ARRIVAL_DATE"=>$GLOBALS["Format"]->Date(strtotime("+1 day", $this->tour["tour"]["departure_date"])),
			"ISRAEL_DEPARTURE_DATE"=>$GLOBALS["Format"]->Date(strtotime("+".($this->tour["tour"]["tour"]=="israel-jordan" ? 11 : 8)." day", $this->tour["tour"]["departure_date"])),
			"ARRIVAL_DATE"=>$GLOBALS["Format"]->Date(strtotime("+".($days-1)." day", $this->tour["tour"]["departure_date"])),
			"DAYS"=>$days

		));

		for ($d = 0; $d < 12; $d++) $data[($d+1)."DAY_DATE"] = $GLOBALS["Format"]->Date($this->tour["tour"]["departure_date"]+(86400*$d));

		$data["data_html"] = array(

			"IsIsraelJesus"=>$this->tour["tour"]["tour"]=="israel-jesus",
			"IsIsraelTurkey"=>$this->tour["tour"]["tour"]=="israel-turkey",
			"IsIsraelRome"=>$this->tour["tour"]["tour"]=="israel-rome",
			"IsIsraelAthens"=>$this->tour["tour"]["tour"]=="israel-athens",
			"IsIsraelJordan"=>$this->tour["tour"]["tour"]=="israel-jordan",
			"IsEuropeTour"=>preg_match("/turkey|rome|athens/", $this->tour["tour"]["tour"]),
			"NotJustIsrael"=>$days == 12,
			"NotJordan"=>$this->tour["tour"]["tour"]!="israel-jordan"

		);

		$document = $GLOBALS["Library"]->FormatDocument("tour-leader", $data, $this->language, false);

		$data = $GLOBALS["Library"]->HTML2PDF($document, "-s Letter");

		if ($raw) return $data;

		$GLOBALS["Common"]->FileOutput($data, "tour-leader.pdf");

	}

	public function Brochure($client_id, $data = array()) {

		$this->Informations($client_id);

		$days = $this->tour["tour"]["tour"]=="israel-jesus" ? 10 : 12;

		$photo = tempnam("/tmp", "photo");
		file_put_contents($photo, base64_decode($data["photo"]));

		$data = array_merge($this->additional, $data, array(

			"PRICE"=>$GLOBALS["Format"]->Number($this->tour["tour"]["price"]),
			"DEPARTURE_DATE"=>$GLOBALS["Format"]->Date($this->tour["tour"]["departure_date"], '\{\D\A\Y\}, jS \o\f \{\M\O\N\T\H\} Y'),
			"ARRIVAL_DATE"=>$GLOBALS["Format"]->Date(strtotime("+".($days-1)." day", $this->tour["tour"]["departure_date"]), '\{\D\A\Y\}, jS \o\f \{\M\O\N\T\H\} Y'),
			"DAYS"=>$days,
			"PHOTO_FILE"=>$photo

		));

		for ($d = 0; $d < $days+1; $d++) $data["DATE_DAY".($d+1)] = $GLOBALS["Format"]->Date($this->tour["tour"]["departure_date"]+(86400*$d), '\{\D\A\Y\}, jS \o\f \{\M\O\N\T\H\} Y');

		$data["data_html"] = array(

			"IsIsraelJesus"=>$this->tour["tour"]["tour"]=="israel-jesus",
			"IsIsraelTurkey"=>$this->tour["tour"]["tour"]=="israel-turkey",
			"IsIsraelRome"=>$this->tour["tour"]["tour"]=="israel-rome",
			"IsIsraelAthens"=>$this->tour["tour"]["tour"]=="israel-athens",
			"IsIsraelJordan"=>$this->tour["tour"]["tour"]=="israel-jordan",
			"NotJustIsrael"=>($days == 12),

		);

		$document = $GLOBALS["Library"]->FormatDocument("brochure", $data, $this->language, false);
		$results = base64_encode($GLOBALS["Library"]->HTML2PDF($document, "-s Legal -B 5mm -L 0mm -T 0mm -R 0mm"));

		unlink($photo);

		return $results;

	}

	public function Invoice($payment_id) {

		if (!$payment = $GLOBALS["Sql"]->Fetch("SELECT * FROM `clients-payments` WHERE `payment_id`=:payment_id", array("payment_id"=>$payment_id))) return false;

		$this->Informations($payment["client_id"]);

		$types = $GLOBALS["Library"]->GetOptions("payment_types");
		$titles = $GLOBALS["Library"]->GetOptions("client_titles");
		$days = $this->tour["tour"]["tour"]=="israel-jesus" ? 10 : 12;

		$data = array_merge($this->additional, array(

			"PAYMENT_ID_TXT"=>$GLOBALS["Format"]->Id($payment_id),
			"CURRENT_DATE"=>$GLOBALS["Format"]->Date(false, '\{\D\A\Y\}, jS \o\f \{\M\O\N\T\H\} Y'),
			"DEPARTURE_DATE"=>$GLOBALS["Format"]->Date($this->tour["tour"]["departure_date"], '\{\D\A\Y\}, jS \o\f \{\M\O\N\T\H\} Y'),
			"ARRIVAL_DATE"=>$GLOBALS["Format"]->Date(strtotime("+".($days-1)." day", $this->tour["tour"]["departure_date"]), '\{\D\A\Y\}, jS \o\f \{\M\O\N\T\H\} Y'),
			"TOUR_HOST_NAME"=>($this->tour["leader"]["title"] ? $titles[$this->tour["leader"]["title"]]." " : "").$this->tour["leader"]["firstname"]." ".$this->tour["leader"]["lastname"],
			"PAYMENT_TYPE"=>$types[$payment["type"]],
			"PRICE"=>$GLOBALS["Format"]->Number($payment["amount"]),
			"DISCOUNT"=>$GLOBALS["Format"]->Number($payment["discount"]),
			"TOTAL"=>$GLOBALS["Format"]->Number($payment["amount"]-$payment["discount"]),
			"FIRST_PAYMENT_DATE"=>$GLOBALS["Format"]->Date(strtotime("+3 days", $this->tour["tour"]["add_date"]), '\{\D\A\Y\}, jS \o\f \{\M\O\N\T\H\} Y'),
			"SECOND_PAYMENT_DATE"=>$GLOBALS["Format"]->Date(strtotime("+48 days", $this->tour["tour"]["add_date"]), '\{\D\A\Y\}, jS \o\f \{\M\O\N\T\H\} Y'),
			"THIRD_PAYMENT_DATE"=>$GLOBALS["Format"]->Date(strtotime("-45 days", $this->tour["tour"]["departure_date"]), '\{\D\A\Y\}, jS \o\f \{\M\O\N\T\H\} Y'),

		));

		$document = $GLOBALS["Library"]->FormatDocument("invoice", $data, $this->language, false);

		$data = $GLOBALS["Library"]->HTML2PDF($document, "-s Letter");

		$GLOBALS["Common"]->FileOutput($data, "Invoice-".$GLOBALS["Format"]->Id($payment_id).".pdf");

	}

}

?>