<?php

/*

	CCommon.php
	------------------------------------------

	Version: 			2.00
	Last change: 	9.7.2011

*/

class CCommon {

	public function AddFile($filename, $filetype, $filesize, $data, $note=NULL) {
	
		$save = array(
			"date"=>time(),
			"filename"=>strtr(utf8_decode(strtolower($filename)), utf8_decode(" éñóáíú"), "-enoaiu"),
			"filetype"=>$filetype,
			"filesize"=>$filesize,
			"data"=>$data,
			"note"=>$note
		);
					
		if (!$file_id = $GLOBALS["Sql"]->Insert("files", $save, "file_id")) return false;
					
		return $file_id;
	
	}

	public function DeleteFile($file_id) {

		$file = $GLOBALS["Sql"]->Fetch("SELECT `client_id`, `group`, `note` FROM `files` F LEFT JOIN `fil2cli` F2C USING(`file_id`) WHERE F.`file_id`=:file_id", array("file_id"=>$file_id));
		
		if (!$client = $GLOBALS["Client"]->Get($file["client_id"])) return false;
	
		$groups = $GLOBALS["Library"]->GetStatuses("file_groups");

		if (!$GLOBALS["Sql"]->Update("fil2cli", array("status"=>"deleted", "del_date"=>time()), "file_id", $file_id)) return false;

		$GLOBALS["Common"]->AddHistory("File deleted successfuly. (ID: $file_id, group: ".$groups[$file["group"]].", note: ".$file["note"].")", "files", $client["client"]["client_id"]);
		
		return true;
			
	}

	public function Hash($string) {

		return hash("sha256", __CRYPT_KEY__.$string);
		
	}

	public function Password($string) {

		return crypt($string, '$2a$12$'.$GLOBALS["Library"]->GenPass(22, 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'));
		
	}

	public function ResizeImage($file, $max_width = 2000, $max_height = 2000) {
	
    list($width, $height, $image_type) = getimagesize($file);

    switch ($image_type)
    {
        case 1: $src = imagecreatefromgif($file); break;
        case 2: $src = imagecreatefromjpeg($file); break;
        case 3: $src = imagecreatefrompng($file); break;
        default: return '';  break;
    }

    $x_ratio = $max_width / $width;
    $y_ratio = $max_height / $height;

    if(($width <= $max_width) && ($height <= $max_height)){

			$tn_width = $width;
			$tn_height = $height;

		} elseif (($x_ratio * $height) < $max_height) {
			
			$tn_height = ceil($x_ratio * $height);
			$tn_width = $max_width;
			
		} else {
			
			$tn_width = ceil($y_ratio * $width);
			$tn_height = $max_height;
			
		}
    
		$tmp = imagecreatetruecolor($tn_width, $tn_height);

		/* Check if this image is PNG or GIF to preserve its transparency */
		if ($image_type == 1 || $image_type==3) {
		
			imagealphablending($tmp, false);
			imagesavealpha($tmp,true);
			$transparent = imagecolorallocatealpha($tmp, 255, 255, 255, 127);
			imagefilledrectangle($tmp, 0, 0, $tn_width, $tn_height, $transparent);
		
		}
		
		imagecopyresampled($tmp,$src,0,0,0,0,$tn_width, $tn_height,$width,$height);
		
		ob_start();
		
		switch ($image_type) {
		
			case 1: imagegif($tmp); break;
			case 2: imagejpeg($tmp, NULL, 100);  break; // best quality
			case 3: imagepng($tmp, NULL, 0); break; // no compression
			default: echo ''; break;
			
		}
		
		$final_image = ob_get_contents();
		
		ob_end_clean();
		
		return $final_image;
		
	}

	public function AddHistory($notice, $group = NULL, $client_id = NULL, $user_id = 0) {
	
		$save = array(
			"date"=>time(),
			"group"=>$group,
			"client_id"=>$client_id,
			"user_id"=>($user_id ? $user_id : (isset($_SESSION["User"]) ? $_SESSION["User"]->user_id : "0")),
			"notice"=>$notice
		);
	
		if (!$GLOBALS["Sql"]->Insert("history", $save, "history_id")) return false;
	
		return true;
	
	}
	
	function FileOutput($data, $filename) {
		
		header("Content-Type: application/pdf");
		header("Content-Length: ".(string)(strlen($data)));
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		header("Content-Transfer-Encoding: binary\n");
	
		print $data;
		exit;

	}

	function filterData(&$str)
    {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }
	function ExportExcel($data, $filename){


		 $fileName = $filename . date('Ymd') . ".xls";
		 // headers for download
    	header("Content-Disposition: attachment; filename=\"$fileName\"");
    	header("Content-Type: application/vnd.ms-excel");
    	$flag = false;
    foreach($data as $row) {
        if(!$flag) {
            // display column names as first row
            echo implode("\t", array_keys($row)) . "\n";
            $flag = true;
        }
        // filter data
        @array_walk($row, 'filterData');
        echo implode("\t", array_values($row)) . "\n";

    }
    
    exit;

	}
	
	public function CheckCreditCard($number) {
		
		$number = preg_replace('/\D/', '', $number);
		
		$number_length = strlen($number);
		$parity = $number_length % 2;
		
		$total = 0;
		for ($i = 0; $i < $number_length; $i++) {
			
			$digit=$number[$i];
			
			if ($i % 2 == $parity) {
				
				$digit *= 2;
				if ($digit > 9) $digit-=9;
				
			}
		
			$total+=$digit;
			
		}
		
		return ($total % 10 == 0) ? true : false;
		
	}
	
}

?>