<?php

/*

	CTour.php
	------------------------------------------

	Version: 			2.00
	Last change: 	30.11.2014

*/

class CTour {
	function __construct() {

		

	}

	public function Get($id, $type = "tour_id") {

		if (!$data["tour"] = $GLOBALS["Sql"]->Fetch("SELECT * FROM `tours` WHERE `status` NOT IN ('deleted') AND `${type}`=:id", array("id"=>$id), $type)) return false;
		$data["leader"] = $GLOBALS["Sql"]->Fetch("SELECT * FROM `clients` WHERE `status` NOT IN ('deleted') AND `tour_id`=:tour_id AND `role`='leader'", array("tour_id"=>$data["tour"]["tour_id"]), "client_id");
		$data["passengers"] = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `clients` WHERE `status` NOT IN('disabled','deleted') AND `role`='passenger' AND `tour_id`=:tour_id", array("tour_id"=>$data["tour"]["tour_id"]), "client_id");

		/*if (!$data["clients"] = $this->GetClient($data["loan"]["client_id"])) return false;

		$data["contacts"] = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `loans-contacts` WHERE `status` IN('active') AND `loan_id`=:loan_id", $sqldata, "contact_id");
		$data["addresses"] = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `loans-addresses` WHERE `status` IN('active', 'verified') AND `loan_id`=:loan_id", $sqldata, "address_id");
		$data["informations"] = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `loans-informations` WHERE `status` IN('active') AND `loan_id`=:loan_id", $sqldata, "information_id");
		$data["jobs"] = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `loans-jobs` WHERE `status` IN('active', 'verified') AND `loan_id`=:loan_id", $sqldata, "job_id");
		$data["referencies"] = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `loans-referencies` WHERE `status` IN('active', 'verified') AND `loan_id`=:loan_id", $sqldata, "reference_id");
		$data["photo_id"] = ($photo = $GLOBALS["Sql"]->Fetch("SELECT `file_id` FROM `loans-files` WHERE `loan_id`=:loan_id AND `group`='photos' AND `status`='active' ORDER BY `add_date` DESC", $sqldata)) ? $photo["file_id"] : false;*/

		return $data;

	}

	public function GetDepartures() {

		$departures = $GLOBALS["Sql"]->SelectArray("SELECT *, 0 AS `distance` FROM `tours-departures` WHERE `status`='active'", array(), "departure_id");
		if (!$departures) return false;

		return $departures;

	}

	public function GetClient($id, $type = "client_id") {

		if (!$client = $GLOBALS["Sql"]->Fetch("SELECT * FROM `loans-clients` LEFT JOIN `levels` USING(`level_id`) WHERE `${type}`=:id", array("id"=>$id))) return false;

		return $client;

	}

	public function Add($date, $departure_id, $tour, $price) {

			
		do {$code = $GLOBALS["Library"]->GenPass(6);} while ($GLOBALS["Sql"]->Value("tours", "tour_id", "code", $code));

		$save = array(
			"add_date"=>time(),
			"user_id"=>(isset($_SESSION["User"]->user_id) ? $_SESSION["User"]->user_id : "0"),
			"code"=>$code,
			"departure_date"=>$date,
			"departure_id"=>$departure_id,
			"tour"=>$tour,
			"price"=>$price,
			"status"=>"pending"
		);

		if (!$tour_id = $GLOBALS["Sql"]->Insert("tours", $save, "tour_id")) return false;

		$GLOBALS["Common"]->AddHistory("Tour added successfuly. (ID: ${tour_id})", "tours", NULL, $tour_id);

		return $tour_id;

	}

	public function SetStatus($loan_id, $status, $specials = array()) {

		if (!$loan = $this->Get($loan_id)) return false;

		$types = $GLOBALS["Library"]->GetOptions("loans_statuses");
		if (!in_array($status, array_keys($types))) return false;

		if (!$GLOBALS["Sql"]->Update("loans", array_merge(array("status"=>$status), $specials), "loan_id", $loan_id)) return false;

		$GLOBALS["Common"]->AddHistory("Estado de préstamo cambiado exitosamente. (ID: ${loan_id}, estado: ".$types[$status].")", "loans", NULL, $loan_id);

		return true;

	}

	public function AddAddress($loan_id, $address1, $address2 = NULL, $address3 = NULL, $city, $zip = NULL, $state = NULL, $country) {

		$save = array(
			"loan_id"=>$loan_id,
			"add_date"=>time(),
			"address1"=>$address1,
			"address2"=>$address2,
			"address3"=>$address3,
			"city"=>$city,
			"zip"=>$zip,
			"state"=>$state,
			"country"=>$country,
			"status"=>"active"
		);

		if (!$address_id = $GLOBALS["Sql"]->Insert("loans-addresses", $save, "address_id")) return false;

		$GLOBALS["Common"]->AddHistory("Dirección agregada exitosamente. (ID: ${address_id}, dirección: ".$address1.", ".$city.")", "addresses", NULL, $loan_id);

		return true;

	}

	public function DeleteAddress($address_id) {

		if (!$GLOBALS["Sql"]->Update("loans-addresses", array("status"=>"deleted", "del_date"=>time()), "address_id", $address_id)) return false;

		$address = $GLOBALS["Sql"]->Fetch("SELECT * FROM `loans-addresses` WHERE `address_id`=:address_id", array("address_id"=>$address_id));

		$GLOBALS["Common"]->AddHistory("Dirección borrada exitosamente. (ID: ${address_id}, dirección: ".$address["address1"].", ".$address["city"].")", "addresses", NULL, $address["loan_id"]);

		return true;

	}

	public function AddInformation($loan_id, $type, $information, $notice = NULL) {

		$save = array(
			"loan_id"=>$loan_id,
			"add_date"=>time(),
			"type"=>$type,
			"information"=>$information,
			"notice"=>$notice ? $notice : NULL,
			"status"=>"active"
		);

		if (!$information_id = $GLOBALS["Sql"]->Insert("loans-informations", $save, "information_id")) return false;

		$types = $GLOBALS["Library"]->GetOptions("clients_informations_types");

		$GLOBALS["Common"]->AddHistory("Información agregada exitosamente. (ID: ${information_id}, tipo: ".$types[$type].")", "informations", NULL, $loan_id);

		return true;

	}

	public function DeleteInformation($information_id) {

		if (!$GLOBALS["Sql"]->Update("loans-informations", array("status"=>"deleted", "del_date"=>time()), "information_id", $information_id)) return false;

		$types = $GLOBALS["Library"]->GetOptions("clients_informations_types");
		$information = $GLOBALS["Sql"]->Fetch("SELECT * FROM `loans-informations` WHERE `information_id`=:information_id", array("information_id"=>$information_id));

		$GLOBALS["Common"]->AddHistory("Información borrada exitosamente. (ID: ${information_id}, tipo: ".$types[$information["type"]].")", "informations", NULL, $information["loan_id"]);

		return true;

	}

	public function DeleteJob($job_id) {

		if (!$GLOBALS["Sql"]->Update("loans-jobs", array("status"=>"deleted", "del_date"=>time()), "job_id", $job_id)) return false;

		$job = $GLOBALS["Sql"]->Fetch("SELECT * FROM `loans-jobs` WHERE `job_id`=:job_id", array("job_id"=>$job_id));

		$GLOBALS["Common"]->AddHistory("Informaciones laborales borrados exitosamente. (ID: ${job_id}, empresa: ".$job["company"].")", "jobs", NULL, $job["loan_id"]);

		return true;

	}

	public function AddReference($loan_id, $name, $notice, $phone, $cellular, $address1, $address2 = NULL, $address3 = NULL, $city, $zip, $state, $country) {

		if (preg_match("/^8[024]9/", $phone)) $phone = "1".$phone;
		if (preg_match("/^8[024]9/", $cellular)) $cellular = "1".$cellular;

		$save = array(
			"loan_id"=>$loan_id,
			"add_date"=>time(),
			"name"=>$name,
			"phone"=>$phone,
			"cellular"=>$cellular,
			"address1"=>$address1,
			"address2"=>$address2,
			"address3"=>$address3,
			"city"=>$city,
			"zip"=>$zip,
			"state"=>$state,
			"country"=>$country,
			"notice"=>$notice,
			"status"=>"active"
		);

		if (!$reference_id = $GLOBALS["Sql"]->Insert("loans-referencies", $save, "reference_id")) return false;

		$GLOBALS["Common"]->AddHistory("Referencia familiar agregada exitosamente. (ID: ${reference_id}, nombre: ${name})", "referencies", NULL, $loan_id);

		return true;

	}

	public function DeleteReference($reference_id) {

		if (!$GLOBALS["Sql"]->Update("loans-referencies", array("status"=>"deleted", "del_date"=>time()), "reference_id", $reference_id)) return false;

		$reference = $GLOBALS["Sql"]->Fetch("SELECT * FROM `loans-referencies` WHERE `reference_id`=:reference_id", array("reference_id"=>$reference_id));

		$GLOBALS["Common"]->AddHistory("Referencia familiar borrada exitosamente. (ID: ${reference_id}, nombre: ".$reference["name"].")", "referencies", NULL, $reference["loan_id"]);

		return true;

	}

	private function GetContact($loan_id, $type) {

		if (!$loan = $this->Get($loan_id)) return false;

		foreach($loan["contacts"] as $contact) if ($contact["type"]==$type) return $contact["contact"];

		return false;

	}

	public function GetEmail($loan_id) {

		return $this->GetContact($loan_id, "email");

	}

	public function GetCellular($loan_id) {

		return $this->GetContact($loan_id, "cellular");

	}

	public function GetEntities($type) {

		$sqldata = array("type"=>$type);
		return $GLOBALS["Sql"]->Values("loans-entities", "entity_id", "name", "`type`=:type AND `status`='active'", $sqldata);

	}

}

?>