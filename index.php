<?php

/*

	GOOD SHEPHERD TOURS - Backoffice
	------------------------------------------

	Version: 			2.05
	Last change: 	23.11.2014

*/

ob_start("ob_gzhandler");

/* Insert functions and classes */
require_once("classes/system/CInit.php");

$MiddleModule = new CPage(__MODULE__."/".__ACTION__);

$Header = new CPage("system/header");
$Header->Show();

if ($Error->Exists()) {

	$ErrorTemplate = new CPage("system/error");
	$ErrorTemplate->Show();

}

if (!$MiddleModule->Show()) {

	if ($Error->Exists()) {

		$Error->Add("MODULE_NOT_EXIST");
		$ErrorTemplate = new CPage("system/error");
		$ErrorTemplate->Show();

	}

}

$Footer = new CPage("system/footer");
$Footer->Show();

$_SESSION["Language"]->ResetDictionary();

/* close and end all unneeded */
ob_end_flush();

//echo $GLOBALS["Library"]->Crypt("hy0ft5bo9e");

?>