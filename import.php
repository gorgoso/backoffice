#!/usr/bin/php -f
<?php

/*

	GOOD SHEPHERD TOURS - Import
	------------------------------------------

	Version: 			2.05
	Last change: 	23.11.2014

*/

require_once("classes/system/CSql.php");

$Sql = new CSql();

$file_handle = fopen("data.csv", "r");

while (!feof($file_handle) ) {

	list($fb_id, $name, $address, $phone, $email, $www) = fgetcsv($file_handle, 2048);

	if (!$phone) continue;

	list($address, $city, $state, $country, $zip) = explode(", ", $address);
	if ($country != "United States") continue;

	var_dump($city);
	exit;

}

fclose($file_handle);


?>