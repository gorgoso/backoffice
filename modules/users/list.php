<?php

$filter = $_SESSION["Filter"]->GetFilter(false, "username", "asc");

$groups = $GLOBALS["Data"]->roles;
$blocked_groups = array(__GROUP_ID_VISITOR__);
foreach($blocked_groups as $g) unset($groups[$g]);

$blocked_groups_tmp = implode("','", $blocked_groups);

$sqldata = array("blocked_groups"=>$blocked_groups_tmp, "conditions"=>$filter["conditions"], "order"=>$filter["order"], "sort"=>$filter["sort"], "limit"=>$filter["limit"]);
$users = $GLOBALS["Sql"]->SelectArray("SELECT `user_id`, `username`, `group_id`, `regdate`, `lastlog`, `status` FROM `users` WHERE (`status` NOT IN('deleted')) AND `group_id` NOT IN(:blocked_groups) AND :conditions :order :sort :limit", $sqldata, "user_id");

$sqldata = array("blocked_groups"=>$blocked_groups_tmp, "conditions"=>$filter["conditions"]);
$users_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`user_id`) AS `count` FROM `users` WHERE (`status` NOT IN('deleted')) AND `group_id` NOT IN(:blocked_groups) AND :conditions", $sqldata);

$users_html = array();

$statuses = $GLOBALS["Library"]->GetStatuses("users");

$Users_tpl = new CTemplate(__TEMPLATES_DIR__."/users/list-row.tpl");
$Users_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

foreach($users as $user_id=>$user){

	$users[$user_id]["TXT_REGDATE"] = $GLOBALS["Format"]->Date($user["regdate"], __FORMAT_DATE_FULL__);
	$users[$user_id]["TXT_LASTLOG"] = ($user["lastlog"])?$GLOBALS["Format"]->Date($user["lastlog"], __FORMAT_DATE_FULL__):"never logged";
	$users[$user_id]["GROUP"] = $groups[$user["group_id"]];
	$users[$user_id]["SHOW_STATUS"] = $statuses[(!is_null($user["status"]))?$user["status"]:"NULL"];
	$users[$user_id]["CHANGE_STATUS"] = ($user["status"]=="disabled")?"enable":"disable";
	$users_html[$user_id]["IsBlocked"] = $user["status"]=="disabled";

}

$Page->Add("USERS", $Users_tpl->RepeatHtml($users, $users_html));
$Page->Add("LIST", $GLOBALS["Library"]->MakeList($users_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));

$Page->Add("USERNAME", isset($filter["in"]["conditions"]["username"])?$filter["in"]["conditions"]["username"]:"");

$Page->Add("GROUPS", $GLOBALS["Library"]->ShowSelect($groups, (isset($filter["in"]["conditions"]["group_id"]["multiple"][0])?$filter["in"]["conditions"]["group_id"]["multiple"][0]:"")));
$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, (isset($filter["in"]["conditions"]["status"])?$filter["in"]["conditions"]["status"]:"")));

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $users_count["count"]);

?>