<?php

$lead_id = $GLOBALS["Library"]->Register("id");
$results = $GLOBALS["Library"]->Register("task");

$now = time();

if ($lead_id) {

	if (!$lead = $GLOBALS["Sql"]->Fetch("SELECT * FROM `marketing-leads` WHERE `lead_id`=:lead_id AND (`user_id`=:user_id".($lead_id && $results ? " OR `user_id` IS NULL" : "").")", array("lead_id"=>$lead_id, "user_id"=>$_SESSION["User"]->user_id))) $GLOBALS["Library"]->Redirect(__PATH_ADMIN__."marketing/leads/");

} else {

	$limits = $GLOBALS["Sql"]->SelectArray("SELECT COUNT(`lead_id`) AS `count` FROM `marketing-leads` WHERE `user_id`=:user_id", array("user_id"=>$_SESSION["User"]->user_id));

	if ($limit["count"] >= __MAX_LEADS_LIMIT__) {

		$GLOBALS["Error"]->AddSession("USER_REACH_MAX_LEADS_LIMIT");
		$GLOBALS["Library"]->Redirect(__PATH_ADMIN__."marketing/list/");

	}

	$GLOBALS["Sql"]->Query("SELECT GET_LOCK('marketing-leads', 60)", array());

	if (!$lead = $GLOBALS["Sql"]->Fetch("SELECT * FROM `marketing-leads` WHERE `user_id` IS NULL AND (`block_date` IS NULL OR `block_date`+1800<UNIX_TIMESTAMP()) AND (`phone1` IS NOT NULL OR `phone2` IS NOT NULL) AND `type`=:type", array("type"=>$limit))) $GLOBALS["Library"]->Redirect(__PATH_ADMIN__."marketing/list/");
	$GLOBALS["Sql"]->Update("marketing-leads", array("block_date"=>$now), "lead_id", $lead["lead_id"]);

	$GLOBALS["Sql"]->Query("SELECT RELEASE_LOCK('marketing-leads')", array());

}

$lead_id = $lead["lead_id"];

if (strlen($lead["phone1"])==7) {

	$lead["phone1"] = "809".$lead["phone1"];
	$GLOBALS["Sql"]->Update("marketing-leads", array("phone1"=>$lead["phone1"]), "lead_id", $lead_id);

}

if (strlen($lead["phone2"])==7) {

	$lead["phone2"] = "809".$lead["phone2"];
	$GLOBALS["Sql"]->Update("marketing-leads", array("phone2"=>$lead["phone2"]), "lead_id", $lead_id);

}

$statuses = $GLOBALS["Library"]->GetOptions("marketing_leads_statuses");

$users = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `users` WHERE `status`='active' ORDER BY `username`", array(), "user_id");
foreach($users as $user_id=>$user) $users_tmp[$user_id] = $user["username"]." (".$user["name"].")";

if (is_array($results)) {

	if (!isset($results["result"])) $results["result"] = "";

	if (!array_key_exists($results["result"], $statuses)) $GLOBALS["Error"]->Add("BAD_CALL_RESULT");

	switch($results["result"]) {

		case "callback":

			if ($GLOBALS["Sql"]->Validate(array("date"=>$results["date"]), array(), "BAD_CALLBACK_DATE")) {

				$new_date = $GLOBALS["Library"]->GetTimestamp($results["date"]) + $results["time"];

				$date_tmp = explode("-", $results["date"]);

				if ($new_date < $now) $GLOBALS["Error"]->Add("BAD_CALLBACK_DATE");
				if (!checkdate($date_tmp[1], $date_tmp[2], $date_tmp[0])) $GLOBALS["Error"]->Add("BAD_CALLBACK_DATE");

			}

		break;

		case "no-answer":

			$new_date = $now + 7200;

		break;

		case "client":

			$new_date = $now;

		break;

	}

	if (!$GLOBALS["Error"]->Exists()) {

		$GLOBALS["Sql"]->StartTransaction();

		$save = array(
			"user_id"=>$_SESSION["User"]->user_id,
			"nextcall_date"=>$new_date,
			"lastcall_date"=>$now,
			"status"=>$results["result"]
		);

		if ($GLOBALS["Sql"]->Update("marketing-leads", $save, "lead_id", $lead_id)) {

			$save = array(
				"user_id"=>$_SESSION["User"]->user_id,
				"add_date"=>$now,
				"lead_id"=>$lead_id,
				"notice"=>$results["notice"],
				"status"=>$results["result"]
			);

			$GLOBALS["Sql"]->Insert("marketing-negotiations", $save);

			$GLOBALS["Error"]->AddSession("CALL_UPDATED_SUCCESSFULY", "notice");
			$GLOBALS["Sql"]->Commit();

			$GLOBALS["Library"]->Redirect(__PATH_ADMIN__."marketing/leads/");

		} else {

			$GLOBALS["Error"]->Add("CALL_NOT_UPDATED", "error");
			$GLOBALS["Sql"]->Rollback();

		}
	}
}

$Page->Add("LEAD_ID", $lead["lead_id"]);
$Page->Add("COMPANY_ID_TXT", $GLOBALS["Format"]->Id($lead["lead_id"]));
$Page->Add("NAME", $lead["name"]);

$Page->Add("TYPE_TXT", $types[$lead["type"]]);
$Page->Add("LASTCALL_DATE_TXT", $lead["lastcall_date"] ? $GLOBALS["Format"]->Date($lead["lastcall_date"], __FORMAT_DATE_FULL__) : "-");
$Page->Add("NEXTCALL_DATE_TXT", $lead["nextcall_date"] ? $GLOBALS["Format"]->Date($lead["nextcall_date"], __FORMAT_DATE_FULL__) : "-");
$Page->Add("STATUS_TXT", $statuses[$lead["status"]]);

$Page->Add("PHONE1", $lead["phone1"] ? $GLOBALS["Format"]->Phone($lead["phone1"]) : "-");
$Page->Add("PHONE2", $lead["phone2"] ? $GLOBALS["Format"]->Phone($lead["phone2"]) : "-");
$Page->Add("EMAIL", $lead["email"] ? $lead["email"] : "-");
$Page->Add("WWW", $lead["www"] ? $lead["www"] : "-");
$Page->Add("ADDRESS", $lead["address"] ? $lead["address"] : "-");
$Page->Add("CITY", $lead["city"] ? $lead["city"] : "-");
$Page->Add("STATE", $lead["state"] ? $GLOBALS["Format"]->Uppercase($lead["state"]) : "-");

$Page->Add("CONTACT_NAME", $lead["contact_name"] ? $lead["contact_name"] : "-");
$Page->Add("CONTACT_PHONE", $lead["contact_phone"] ? $GLOBALS["Format"]->Phone($lead["contact_phone"]) : "-");
$Page->Add("CONTACT_CELLULAR", $lead["contact_cellular"] ? $GLOBALS["Format"]->Phone($lead["contact_cellular"]) : "-");
$Page->Add("CONTACT_EMAIL", $lead["contact_email"] ? $lead["contact_email"] : "-");

$Page->AddHTML("IsExpired", $lead["nextcall_date"] ? $lead["nextcall_date"]<$now : false);

/* NEGOTATIONS */
$negotiations = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `marketing-negotiations` WHERE `lead_id`=:lead_id ORDER BY `add_date` DESC", array("lead_id"=>$lead_id));
$count = count($negotiations);
$c = 0;

if ($negotiations) foreach($negotiations as $negotiation_id=>$negotiation) {

	$negotiations[$negotiation_id]["DATE_TXT"] = $GLOBALS["Format"]->Date($negotiation["add_date"], __FORMAT_DATE_FULL__);
	$negotiations[$negotiation_id]["USER_TXT"] = $users[$negotiation["user_id"]]["username"];
	$negotiations[$negotiation_id]["STATUS_TXT"] = $statuses[$negotiation["status"]];

}

$Negotiations_tpl = new CTemplate(__TEMPLATES_DIR__."/marketing/leads-negotiations-row.tpl");
$Negotiations_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);
$Page->Add("NEGOTIATIONS", $Negotiations_tpl->RepeatHtml($negotiations));
$Page->Add("CALLS_COUNT", $count);

$Page->Add("DATE", $results["date"] ? $results["date"] : $GLOBALS["Format"]->Date());
$Page->Add("NOTICE", $results["notice"]);
$Page->Add("RESULT", isset($results["result"]) ? $results["result"] : "");

$times = array();
for ($h = 9; $h < 16; $h++) {

	for ($m = "00"; $m < 60; $m+=15) {

		$times[$h*3600+$m*60] = ($h>12 ? $h-12 : $h).":".$m." ".($h<12 ? "AM" : "PM");

	}

}

$Page->Add("TIMES", $GLOBALS["Library"]->ShowSelect($times, $results["time"]));

?>