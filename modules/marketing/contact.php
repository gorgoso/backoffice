<?php

$lead_id = $GLOBALS["Library"]->Register("id");
$add = $GLOBALS["Library"]->Register("add");

if (!$lead = $GLOBALS["Sql"]->Fetch("SELECT * FROM `marketing-leads` WHERE `lead_id`=:lead_id AND (`user_id`=:user_id OR `user_id` IS NULL)", array("lead_id"=>$lead_id, "user_id"=>$_SESSION["User"]->user_id))) $GLOBALS["Library"]->Redirect(__PATH_ADMIN__."marketing/leads/");

if (is_array($add)) {

	$add = $GLOBALS["Format"]->TextArray($add, array("contact_name", "contact_phone", "contact_phone", "contact_email"));

	$GLOBALS["Sql"]->Validate(array("string255"=>$add["contact_name"]), array(), "BAD_CONTACT_NAME");
	if ($add["contact_phone"]) $GLOBALS["Sql"]->Validate(array("string32"=>$add["contact_phone"]), array(), "BAD_CONTACT_PHONE");
	$GLOBALS["Sql"]->Validate(array("string32"=>$add["contact_cellular"]), array(), "BAD_CONTACT_CELLULAR");
	$GLOBALS["Sql"]->Validate(array("string255"=>$add["contact_email"]), array(), "BAD_CONTACT_EMAIL");

	if (!$GLOBALS["Error"]->Exists()) {

		$save = array(
			"user_id"=>$_SESSION["User"]->user_id,
			"contact_name"=>$add["contact_name"],
			"contact_phone"=>$add["contact_phone"],
			"contact_cellular"=>$add["contact_cellular"],
			"contact_email"=>$add["contact_email"]
		);

		if ($GLOBALS["Sql"]->Update("marketing-leads", $save, "lead_id", $lead_id)) {

			$GLOBALS["Error"]->AddSession("CONTACT_PERSON_ADDED_SUCCESSFULY", "notice");
			$GLOBALS["Library"]->Redirect(__PATH_ADMIN__."marketing/leads/${lead_id}/");

		} else $GLOBALS["Error"]->Add("CONTACT_PERSON_NOT_ADDED", "error");

	}
}

$Page->Add("LEAD_ID", $lead["lead_id"]);
$Page->Add("COMPANY_ID_TXT", $GLOBALS["Format"]->Id($lead["lead_id"]));
$Page->Add("NAME", $lead["name"]);
$Page->Add("CONTACT_NAME", $add["contact_name"] ? $add["contact_name"] : $lead["contact_name"]);
$Page->Add("CONTACT_PHONE", $add["contact_phone"] ? $add["contact_phone"] : $lead["contact_phone"]);
$Page->Add("CONTACT_CELLULAR", $add["contact_cellular"] ? $add["contact_cellular"] : $lead["contact_cellular"]);
$Page->Add("CONTACT_EMAIL", $add["contact_email"] ? $add["contact_email"] : $lead["contact_email"]);

?>