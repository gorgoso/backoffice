<?php

$filter = $_SESSION["Filter"]->GetFilter(array("status"=>array("multiple"=>array("no-answer", "callback"))), "nextcall_date", "desc");

$leads_html = array();
$sqldata = array("user_id"=>$_SESSION["User"]->user_id);
$leads = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `marketing-leads` WHERE `user_id`=:user_id AND ".$filter["where"], $sqldata);
$leads_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`lead_id`) AS `count` FROM `marketing-leads` WHERE `user_id`=:user_id AND ".$filter["conditions"], $sqldata);

$clients = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`lead_id`) AS `count` FROM `marketing-leads` WHERE `user_id`=:user_id AND `status` IN('client')", $sqldata);

$statuses = $GLOBALS["Library"]->GetOptions("marketing_leads_statuses");

$Leads_tpl = new CTemplate(__TEMPLATES_DIR__."/marketing/list-row.tpl");
$Leads_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

foreach($leads as $lead_id=>$lead) {

	$leads[$lead_id]["PHONE1_TXT"] = $lead["phone1"] ? $GLOBALS["Format"]->Phone($lead["phone1"]) : "-";
	$leads[$lead_id]["PHONE2_TXT"] = $lead["phone2"] ? $GLOBALS["Format"]->Phone($lead["phone2"]) : "-";
	$leads[$lead_id]["LASTCALL_DATE_TXT"] = $lead["lastcall_date"] ? $GLOBALS["Format"]->Date($lead["lastcall_date"], __FORMAT_DATE_FULL__) : "-";
	$leads[$lead_id]["NEXTCALL_DATE_TXT"] = $lead["nextcall_date"] ? $GLOBALS["Format"]->Date($lead["nextcall_date"], __FORMAT_DATE_FULL__) : "-";
	$leads[$lead_id]["STATUS_TXT"] = $statuses[$lead["status"]];

	$leads_html[$lead_id]["IsBlocked"] = ($lead["status"]=="canceled");

}

$Page->Add("LEADS", $Leads_tpl->RepeatHtml($leads, $leads_html));
$Page->Add("LIST", $GLOBALS["Library"]->MakeList($leads_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));
$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $leads_count["count"]);

$Page->Add("NAME", $_SESSION["Filter"]->GetCondition("name"));
$Page->Add("PHONE1", $_SESSION["Filter"]->GetCondition("phone1"));
$Page->Add("PHONE2", $_SESSION["Filter"]->GetCondition("phone2"));
$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, $_SESSION["Filter"]->GetCondition("status", true)));

$Page->Add("LEADS_COUNT", $clients["count"]);
$Page->Add("MAX_LEADS_LIMIT", __MAX_LEADS_LIMIT__);

?>