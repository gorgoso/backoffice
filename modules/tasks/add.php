<?php

$add = $GLOBALS["Library"]->Reg("add");
$client_id = $GLOBALS["Library"]->Reg("id");

if ($client_id) if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

$types = $GLOBALS["Library"]->GetStatuses("task-types");

$users = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `users` WHERE `status`='active' ORDER BY `username`", array(), "user_id");
foreach($users as $user_id=>$user) $users_tmp[$user_id] = $user["username"]." (".$user["name"].")";

if (is_array($add)) {

	$year = date("Y");
	$now = time();

	$add = $GLOBALS["Format"]->TextArray($add, array("subject", "description"));

	if (!array_key_exists($add["type"], $types)) $GLOBALS["Error"]->AddError("BAD_TASK_TYPE");
	
	if ($GLOBALS["Sql"]->Validate(array("date"=>$add["date"]), array(), "BAD_TASK_DATE")) {

		$new_date = $GLOBALS["Library"]->GetTimestamp($add["date"]) + $add["time"];
		
		$date_tmp = explode("-", $add["date"]);

		if ($new_date < $now) $GLOBALS["Error"]->AddError("BAD_TASK_DATE");		
		if (!checkdate($date_tmp[1], $date_tmp[2], $date_tmp[0])) $GLOBALS["Error"]->AddError("BAD_TASK_DATE");
		
	}

	if (!array_key_exists($add["user_id"], $users_tmp)) $GLOBALS["Error"]->AddError("BAD_TASK_USER_ID");
		
	if ($add["client_id"]) $GLOBALS["Sql"]->Validate(array("client_id"=>$add["client_id"]));

	if ($add["subject"]) $GLOBALS["Sql"]->Validate(array("notice"=>$add["subject"]), array(), "BAD_TASK_SUBJECT");

	if (!$add["description"]) $GLOBALS["Error"]->AddError("BAD_TASK_DESCRIPTION");
			
	if (!$GLOBALS["Error"]->ErrorExists()) {
		
		$GLOBALS["Sql"]->StartTransaction();
		
		$save = array(
			"add_user"=>$_SESSION["User"]->user_id,
			"add_date"=>$now,
			"execution_date"=>$new_date,
			"user_id"=>$add["user_id"],
			"client_id"=>$add["client_id"],
			"type"=>$add["type"],
			"subject"=>$add["subject"],
			"description"=>$add["description"]
		);
						
		if ($task_id = $GLOBALS["Sql"]->Insert("tasks", $save, "task_id")) {
	
			$GLOBALS["Sql"]->Commit();
			
			if ($add["client_id"]) $GLOBALS["Common"]->AddHistory("Task added to client. (ID: $task_id, type: ".$add["type"].", subject: ".$add["subject"].")", "tasks", $add["client_id"]);

			$GLOBALS["Error"]->AddSessionError("TASK_ADDED_SUCCESSFULY", "notice");
			$GLOBALS["Library"]->Go(__PATH_ADMIN__."tasks/list/");
		
		} else {
			
			$GLOBALS["Error"]->AddError("TASK_NOT_ADDED", "error");
			$GLOBALS["Sql"]->Rollback();

		}

	}

}

$times = array();
for ($h = 9; $h < 16; $h++) for ($m = "00"; $m < 60; $m+=15) $times[$h*3600+$m*60] = ($h>12 ? $h-12 : $h).":".$m." ".($h<12 ? "AM" : "PM");

$Page->Add("CLIENT_ID", $client_id ? $client_id : $add["client_id"]);
$Page->Add("LANGUAGE", __LANGUAGE__);

if ($client_id) {
	
	$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($client_id));
	$Page->Add("FIRSTNAME", $client["client"]["firstname"]);
	$Page->Add("LASTNAME", $client["client"]["lastname"]);
	
}

$Page->AddHTML("IsClient", $client_id);
$Page->AddHTML("IsNotClient", !$client_id);

$Page->Add("TYPES", $GLOBALS["Library"]->ShowSelect($types, $add["type"]));
$Page->Add("DATE", $add["date"] ? $add["date"] : $GLOBALS["Format"]->Date());
$Page->Add("TIMES", $GLOBALS["Library"]->ShowSelect($times, $add["time"]));
$Page->Add("USERS", $GLOBALS["Library"]->ShowSelect($users_tmp, $add["user_id"]));
$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($client_id ? $client_id : ($add["client_id"] ? $add["client_id"] : "")));
$Page->Add("SUBJECT", $add["subject"]);
$Page->Add("DESCRIPTION", $add["description"]);

?>