<?php

$filter = $_SESSION["Filter"]->GetFilter(false, "execution_date");

$calls = $GLOBALS["Sql"]->SelectArray("SELECT C.*, CL.`firstname`, CL.`lastname`, I.`amount`, I.`assessor_id`, I.`loan_status` FROM `tasks` C LEFT JOIN `clients` CL USING(`client_id`) LEFT JOIN (SELECT `assessor_id`, `client_id`, SUM(`amount`) AS `amount`, L.`status` AS `loan_status` FROM `loans` L LEFT JOIN (SELECT `loan_id`, SUM(`amount`+`arrear`-(`amount_payed`+`arrear_payed`)) AS `amount` FROM `installments` I LEFT JOIN (SELECT `installment_id`, IFNULL(SUM(D2I.`principal`),0)+IFNULL(SUM(D2I.`interest`),0) AS `amount_payed`, IFNULL(SUM(D2I.`arrear`),0) AS `arrear_payed` FROM `installments` I LEFT JOIN `dep2ins` D2I USING(`installment_id`) WHERE I.`status`='pending' AND `date`<UNIX_TIMESTAMP() GROUP BY `installment_id`) I2 USING(`installment_id`) GROUP BY `loan_id`) I USING(`loan_id`) GROUP BY `client_id`) I USING(`client_id`) WHERE C.`type`='payment' AND C.`status` NOT IN('deleted') AND CL.`status` NOT IN('deleted') AND I.`loan_status` NOT IN('legal') AND ".$filter["where"]); 

$calls_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`task_id`) AS `count` FROM `tasks` C LEFT JOIN `clients` CL USING(`client_id`) LEFT JOIN (SELECT `assessor_id`, `client_id`, SUM(`amount`) AS `amount`, L.`status` AS `loan_status` FROM `loans` L LEFT JOIN (SELECT `loan_id`, SUM(`amount`+`arrear`-(`amount_payed`+`arrear_payed`)) AS `amount` FROM `installments` I LEFT JOIN (SELECT `installment_id`, IFNULL(SUM(D2I.`principal`),0)+IFNULL(SUM(D2I.`interest`),0) AS `amount_payed`, IFNULL(SUM(D2I.`arrear`),0) AS `arrear_payed` FROM `installments` I LEFT JOIN `dep2ins` D2I USING(`installment_id`) WHERE I.`status`='pending' AND `date`<UNIX_TIMESTAMP() GROUP BY `installment_id`) I2 USING(`installment_id`) GROUP BY `loan_id`) I USING(`loan_id`) GROUP BY `client_id`) I USING(`client_id`) WHERE C.`type`='payment' AND C.`status` NOT IN('deleted') AND CL.`status` NOT IN('deleted') AND I.`loan_status` NOT IN('legal') AND ".$filter["conditions"]);

$calls_html=array();

$legal = $GLOBALS["Sql"]->Fetch("SELECT IFNULL(SUM(I.`amount`+I.`arrear`-(I.`amount_payed`+I.`arrear_payed`)), 0) AS `amount` FROM `loans` L LEFT JOIN (SELECT `loan_id`, I.`amount`, I.`arrear`, IFNULL(SUM(D2I.`principal`),0)+IFNULL(SUM(D2I.`interest`),0) AS `amount_payed`, IFNULL(SUM(D2I.`arrear`),0) AS `arrear_payed` FROM `installments` I LEFT JOIN `dep2ins` D2I USING(`installment_id`) WHERE I.`status`='pending' AND `date`<UNIX_TIMESTAMP() GROUP BY `installment_id`) I USING(`loan_id`) WHERE L.`status` IN('legal')");

$pending = $GLOBALS["Sql"]->Fetch("SELECT IFNULL(SUM(I.`amount`+I.`arrear`-(I.`amount_payed`+I.`arrear_payed`)), 0) AS `amount` FROM `loans` L LEFT JOIN (SELECT `loan_id`, I.`amount`, I.`arrear`, IFNULL(SUM(D2I.`principal`),0)+IFNULL(SUM(D2I.`interest`),0) AS `amount_payed`, IFNULL(SUM(D2I.`arrear`),0) AS `arrear_payed` FROM `installments` I LEFT JOIN `dep2ins` D2I USING(`installment_id`) WHERE I.`status`='pending' AND `date`<UNIX_TIMESTAMP() GROUP BY `installment_id`) I USING(`loan_id`) WHERE L.`status` NOT IN('legal')");

$assessores = $GLOBALS["Assessor"]->GetList();
foreach($assessores as $assessor_id=>$assessor) $assessores[$assessor_id] = $GLOBALS["Format"]->Id($assessor_id)." (".$assessor["firstname"]." ".$assessor["lastname"].")";

$statuses = $GLOBALS["Library"]->GetStatuses("calls");

$Calls_tpl = new CTemplate(__TEMPLATES_DIR__."/tasks/payments-row.tpl");
$Calls_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

$now = time();
$pending_count = 0;

foreach($calls as $client_id=>$call) {
	
	$calls[$client_id]["CLIENT_ID_TXT"] = $GLOBALS["Format"]->Id($call["client_id"]);
	$calls[$client_id]["ASSESSOR"] = $call["assessor_id"] ? $assessores[$call["assessor_id"]] : "-";
	$calls[$client_id]["AMOUNT_TXT"] = $call["amount"] ? $GLOBALS["Format"]->Number($call["amount"]) : "0.00";
	$calls[$client_id]["LAST_CALLDATE_TXT"] = ($call["last_calldate"] ? $GLOBALS["Format"]->Date($call["last_calldate"], __FORMAT_DATE_FULL__) : "-");
	$calls[$client_id]["CALLDATE_TXT"] = $GLOBALS["Format"]->Date($call["execution_date"], __FORMAT_DATE_FULL__);
	$calls[$client_id]["STATUS_TXT"] = $statuses[$call["call_status"]];

	$calls_html[$client_id]["IsBlocked"] = ($call["execution_date"]<$now);
	
	if ($call["execution_date"]<$now || $GLOBALS["Format"]->Date($call["execution_date"]) == $GLOBALS["Format"]->Date()) $pending_count++;
	
}

$Page->Add("CALLS", $Calls_tpl->RepeatHtml($calls, $calls_html));

$Page->Add("LIST", $GLOBALS["Library"]->MakeList($calls_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));

$Page->Add("CLIENT_ID", isset($filter["in"]["conditions"]["C.client_id"])?$filter["in"]["conditions"]["C.client_id"]:"");
$Page->Add("FIRSTNAME", isset($filter["in"]["conditions"]["CL.firstname"])?$filter["in"]["conditions"]["CL.firstname"]:"");
$Page->Add("LASTNAME", isset($filter["in"]["conditions"]["CL.lastname"])?$filter["in"]["conditions"]["CL.lastname"]:"");
$Page->Add("ASSESSORES", $GLOBALS["Library"]->ShowSelect($assessores, isset($filter["in"]["conditions"]["I.assessor_id"]["multiple"][0])?$filter["in"]["conditions"]["I.assessor_id"]["multiple"][0]:""));
$Page->Add("AMOUNT", isset($filter["in"]["conditions"]["I.amount"])?$filter["in"]["conditions"]["I.amount"]:"");
$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, isset($filter["in"]["conditions"]["C.call_status"]["multiple"][0])?$filter["in"]["conditions"]["C.call_status"]["multiple"][0]:""));

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $calls_count["count"]);

$Page->Add("PENDING_COUNT", $pending_count);
$Page->Add("PENDING_AMOUNT", $GLOBALS["Format"]->Number($pending["amount"]));
$Page->Add("LEGAL_AMOUNT", $GLOBALS["Format"]->Number($legal["amount"]));

?>