<?php

$filter = $_SESSION["Filter"]->GetFilter(array("T.status"=>array("multiple"=>array("active")), "user_id"=>array("multiple"=>array($_SESSION["User"]->user_id))), "execution_date");

$tasks = $GLOBALS["Sql"]->SelectArray("SELECT T.*, C.`fullname` FROM `tasks` T LEFT JOIN (SELECT `client_id`, CONCAT(`firstname`, ' ', `lastname`) AS `fullname` FROM `clients`) C ON(T.`client_id`=C.`client_id`) WHERE T.`type` NOT IN('payment') AND ".$filter["where"]);
$tasks_html=array();
$tasks_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`task_id`) AS `count`, C.`fullname` FROM `tasks` T LEFT JOIN (SELECT `client_id`, CONCAT(`firstname`, ' ', `lastname`) AS `fullname` FROM `clients`) C ON(T.`client_id`=C.`client_id`) WHERE T.`type` NOT IN('payment') AND ".$filter["conditions"]);

$pending = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`task_id`) AS `count` FROM `tasks` WHERE `type`='active' AND (`execution_date`<UNIX_TIMESTAMP() OR FROM_UNIXTIME(`execution_date`, '%Y-%m-%d') = CURDATE())");

$types = $GLOBALS["Library"]->GetStatuses("task-types");
$statuses = $GLOBALS["Library"]->GetStatuses("tasks");

$users = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `users` WHERE `status`='active' ORDER BY `username`", array(), "user_id");
foreach($users as $user_id=>$user) $users_tmp[$user_id] = $user["username"]." (".$user["name"].")";

$Tasks_tpl = new CTemplate(__TEMPLATES_DIR__."/tasks/list-row.tpl");
$Tasks_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

$now = time();

foreach($tasks as $task_id=>$task) {
	
	$tasks[$task_id]["USER_TXT"] = $task["user_id"] ? $users_tmp[$task["user_id"]] : "-";
	$tasks[$task_id]["TYPE_TXT"] = $types[$task["type"]];
	$tasks[$task_id]["ADD_DATE_TXT"] = $GLOBALS["Format"]->Date($task["add_date"], __FORMAT_DATE_FULL__);
	$tasks[$task_id]["EXECUTION_DATE_TXT"] = $GLOBALS["Format"]->Date($task["execution_date"], __FORMAT_DATE_FULL__);
	$tasks[$task_id]["STATUS_TXT"] = $statuses[$task["status"]];

	$tasks_html[$task_id]["IsBlocked"] = ($task["execution_date"]<$now);
	
}

$Page->Add("TASKS", $Tasks_tpl->RepeatHtml($tasks, $tasks_html));

$Page->Add("LIST", $GLOBALS["Library"]->MakeList($tasks_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));

$Page->Add("TYPES", $GLOBALS["Library"]->ShowSelect($types, isset($filter["in"]["conditions"]["type"]["multiple"][0])?$filter["in"]["conditions"]["type"]["multiple"][0]:""));
$Page->Add("USERS", $GLOBALS["Library"]->ShowSelect($users_tmp, isset($filter["in"]["conditions"]["user_id"]["multiple"][0])?$filter["in"]["conditions"]["user_id"]["multiple"][0]:""));
$Page->Add("FULLNAME", isset($filter["in"]["conditions"]["fullname"])?$filter["in"]["conditions"]["fullname"]:"");
$Page->Add("SUBJECT", isset($filter["in"]["conditions"]["subject"])?$filter["in"]["conditions"]["subject"]:"");
$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, isset($filter["in"]["conditions"]["T.status"]["multiple"][0])?$filter["in"]["conditions"]["T.status"]["multiple"][0]:""));

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $tasks_count["count"]);

$Page->Add("PENDING_COUNT", $pending["count"]);

?>