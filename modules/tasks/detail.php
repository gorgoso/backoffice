<?php

$task_id = $GLOBALS["Library"]->Reg("id");
$results = $GLOBALS["Library"]->Reg("task");
$from = $GLOBALS["Library"]->Reg("from");

if (!$task = $GLOBALS["Sql"]->Fetch("SELECT * FROM `tasks` WHERE `task_id`=:task_id", array("task_id"=>$task_id))) $GLOBALS["Library"]->Go(__PATH_ADMIN__."tasks/list/");
$client = $GLOBALS["Client"]->Get($task["client_id"]);

$statuses = $GLOBALS["Library"]->GetStatuses("calls");
$contact_types = $GLOBALS["Library"]->GetStatuses("contact_types");
$task_types = $GLOBALS["Library"]->GetStatuses("task-types");
$task_statuses = $GLOBALS["Library"]->GetStatuses("tasks");
$iscall = $task["type"]=="call" || $task["type"]=="payment";

$users = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `users` WHERE `status`='active' ORDER BY `username`", array(), "user_id");
foreach($users as $user_id=>$user) $users_tmp[$user_id] = $user["username"]." (".$user["name"].")";

$now = time();
$from = preg_match("/payments|list/", $from) ? $from : "list";

if (is_array($results)) {
		
	if (isset($results["result"]) && $iscall &&  $results["result"]!="deleted") {
			
		if (!array_key_exists($results["result"], $statuses)) $GLOBALS["Error"]->AddError("BAD_CALL_RESULT");
		
		switch($results["result"]) {
			
			case "callback":
	
				if ($GLOBALS["Sql"]->Validate(array("date"=>$results["date"]), array(), "BAD_CALLBACK_DATE")) {
		
					$new_date = $GLOBALS["Library"]->GetTimestamp($results["date"]) + $results["time"];
					
					$date_tmp = explode("-", $results["date"]);
			
					if ($new_date < $now) $GLOBALS["Error"]->AddError("BAD_CALLBACK_DATE");		
					if (!checkdate($date_tmp[1], $date_tmp[2], $date_tmp[0])) $GLOBALS["Error"]->AddError("BAD_CALLBACK_DATE");
					
				}
			
			break;
			
			case "no-answer":
			
				$new_date = $now + 7200;
			
			break;
			
		}
	} elseif (isset($results["result"]) && (!$iscall || $results["result"]=="deleted")) {
						
		if (!array_key_exists($results["result"], $task_statuses)) $GLOBALS["Error"]->AddError("BAD_CALL_RESULT");

		switch($results["result"]) {
			
			case "active":
				
				if ($GLOBALS["Sql"]->Validate(array("date"=>$results["date"]), array(), "BAD_TASK_DATE")) {
		
					$new_date = $GLOBALS["Library"]->GetTimestamp($results["date"]) + $results["time"];
					
					$date_tmp = explode("-", $results["date"]);
			
					if ($new_date < $now) $GLOBALS["Error"]->AddError("BAD_TASK_DATE");		
					if (!checkdate($date_tmp[1], $date_tmp[2], $date_tmp[0])) $GLOBALS["Error"]->AddError("BAD_TASK_DATE");
					
				}
				
			break;
			
			case "deleted":
			
				$new_date = $now;
			
			break;
			
		}
		
	} else $GLOBALS["Error"]->AddError("BAD_TASK_RESULT");

	if (!$GLOBALS["Error"]->ErrorExists()) {
		
		$GLOBALS["Sql"]->StartTransaction();
				
		$save = array(
			"execution_date"=>$new_date,
			"notice"=>$results["notice"],
		);

		if ($iscall && $results["result"]!="deleted") {
			
			$save["last_calldate"]=$now;
			$save["call_status"]=$results["result"];
			
		} else {
			
			$save["status"]=$results["result"];
			
		}
				
		if ($GLOBALS["Sql"]->Update("tasks", $save, "task_id", $task_id)) {
			
			$GLOBALS["Error"]->AddSessionError("TASK_UPDATED_SUCCESSFULY", "notice");
			$GLOBALS["Sql"]->Commit();
			
			$GLOBALS["Library"]->Go(__PATH_ADMIN__."tasks/$from/");
		
		} else {
			
			$GLOBALS["Error"]->AddError("TASK_NOT_UPDATED", "error");
			$GLOBALS["Sql"]->Rollback();

		}
	}
}

$Page->Add("TASK_ID", $task_id);
$Page->Add("CLIENT_ID", $task["client_id"]);
$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($task["client_id"]));
$Page->Add("FIRSTNAME", $client["client"]["firstname"]);
$Page->Add("LASTNAME", $client["client"]["lastname"]);

$Page->Add("FROM", $from);

if ($task["type"]!="payment") {

	$Page->Add("TASK_TYPE_TXT", $task_types[$task["type"]]);
	$Page->Add("ADD_USER_TXT", $task["add_user"] ? $users_tmp[$task["add_user"]] : "-");
	$Page->Add("USER_TXT", $task["user_id"] ? $users_tmp[$task["user_id"]] : "-");

}

$Page->Add("ADD_DATE_TXT", $GLOBALS["Format"]->Date($task["add_date"], __FORMAT_DATE_FULL__));

$Page->Add("EXECUTION_DATE_TXT", $GLOBALS["Format"]->Date($task["execution_date"], __FORMAT_DATE_FULL__));
$Page->Add("LAST_CALLDATE_TXT", $task["last_calldate"] ? $GLOBALS["Format"]->Date($task["last_calldate"], __FORMAT_DATE_FULL__) : "-");
$Page->Add("STATUS_TXT", $statuses[$task["call_status"]]);

$Page->AddHTML("IsPayment", $task["type"]=="payment");
$Page->AddHTML("IsNotPayment", $task["type"]!="payment");
$Page->AddHTML("IsClient", $task["client_id"]);

$Page->AddHTML("IsCall", $iscall);
$Page->AddHTML("IsNotCall", !$iscall);

$Page->AddHTML("IsDescription", $task["description"]);

$Page->AddHTML("IsNotContacts", !$client["contacts"]);

$Page->AddHTML("IsExpired", $task["execution_date"]<$now);

$Page->AddHTML("IsContacts", $client["contacts"]);
$Page->AddHTML("IsNotContacts", !$client["contacts"]);

$Page->AddHTML("IsReferences", $client["referencies"]);
$Page->AddHTML("IsNotReferences", !$client["referencies"]);

$Page->AddHTML("IsLoans", $client["loans"]);
$Page->AddHTML("IsNotLoans", !$client["loans"]);

$types_tmp = array();
$types_html = array();
$count = count($contact_types);
$c = 0;

foreach($contact_types as $type=>$type_txt) {

	if ($client["contacts"]) foreach($client["contacts"] as $contact) {
							
		if ($type == $contact["type"]) {

			$types_html[$type]["IsNotice"] = $contact["notice"];
			
			if ($contact["type"]=="email") continue;
			
			$types_tmp[$type]["TYPE_TXT"] = $type_txt;
			$types_tmp[$type]["CONTACT"] = $contact["code"].$contact["contact"];
			$types_tmp[$type]["PHONE_NOTICE"] = $contact["notice"];
			$types_tmp[$type]["CONTACT_TXT"] = $GLOBALS["Format"]->Phone($contact["code"].$contact["contact"]);
						
			continue;
			
		}
	}
	
	$types_html[$type]["IsLast"] = ($count == ++$c);

}

$Contacts_tpl = new CTemplate(__TEMPLATES_DIR__."/tasks/detail-contacts-row.tpl");
$Contacts_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);
$Contacts_tpl->Add("CLIENT_ID", $task["client_id"]);
$Page->Add("CONTACTS", $Contacts_tpl->RepeatHtml($types_tmp, $types_html));

$referencies = array();
$referencies_html = array();
$count = count($client["referencies"]);
$c = 0;

if ($client["referencies"]) foreach($client["referencies"] as $reference_id=>$reference) {
		
	$referencies[$reference_id]["NAME"] = $reference["name"];
	$referencies[$reference_id]["PHONE"] = $reference["phone_code"].$reference["phone"];
	$referencies[$reference_id]["CELLULAR"] = $reference["cellular_code"].$reference["cellular"];
	$referencies[$reference_id]["PHONE_TXT"] = "+".$reference["phone_code"]."-".$GLOBALS["Format"]->Phone($reference["phone"]);
	$referencies[$reference_id]["CELLULAR_TXT"] = "+".$reference["cellular_code"]."-".$GLOBALS["Format"]->Phone($reference["cellular"]);

	$referencies_html[$reference_id]["IsLast"] = ($count == ++$c);
	
}

$Referencies_tpl = new CTemplate(__TEMPLATES_DIR__."/tasks/detail-reference-row.tpl");
$Referencies_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);
$Referencies_tpl->Add("CLIENT_ID", $task["client_id"]);
$Page->Add("REFERENCIES", $Referencies_tpl->RepeatHtml($referencies, $referencies_html));

$assessores = $GLOBALS["Assessor"]->GetList();
foreach($assessores as $assessor_id=>$assessor) $assessores[$assessor_id] = $GLOBALS["Format"]->Id($assessor_id)." (".$assessor["firstname"]." ".$assessor["lastname"].")";

$loans = array();
$loans_html = array();
$count = count($client["loans"]);
$c = 0;

if ($client["loans"]) foreach($client["loans"] as $loan_id=>$loan) {
	
	if ($loan["status"]!="delivered" && $loan["status"]!="legal") continue;
	
	$installments = $GLOBALS["Sql"]->Fetch("SELECT IFNULL(SUM(`amount`)-SUM(`amount_payed`), 0) AS `amount`, IFNULL(SUM(`arrear`)-SUM(`arrear_payed`), 0) AS `arrear` FROM `installments` I LEFT JOIN (SELECT `installment_id`, IFNULL(SUM(D2I.`principal`),0)+IFNULL(SUM(D2I.`interest`),0) AS `amount_payed`, IFNULL(SUM(D2I.`arrear`), 0) AS `arrear_payed` FROM `installments` I LEFT JOIN `dep2ins` D2I USING(`installment_id`) GROUP BY `installment_id`) D2I USING(`installment_id`) WHERE `loan_id`=:loan_id AND `status`='pending' GROUP BY `loan_id`", array("loan_id"=>$loan_id));
		
	$loans[$loan_id]["LOAN_ID"] = $loan_id;
	$loans[$loan_id]["LOAN_ID_TXT"] = $GLOBALS["Format"]->Id($loan_id);
	$loans[$loan_id]["PAYDATE_TXT"] = $loan["last_paydate"] ? $GLOBALS["Format"]->Date($loan["last_paydate"]) : "-";
	$loans[$loan_id]["ASSESSOR_TXT"] = $loan["assessor_id"] ? $assessores[$loan["assessor_id"]] : "-";
	$loans[$loan_id]["AMOUNT_TXT"] = $GLOBALS["Format"]->Number($installments["amount"]);
	$loans[$loan_id]["ARREAR_TXT"] = $GLOBALS["Format"]->Number($installments["arrear"]);
	$loans[$loan_id]["TOTAL_TXT"] = $GLOBALS["Format"]->Number($installments["amount"]+$installments["arrear"]);

	$loans_html[$loan_id]["IsBlocked"] = ($loan["paydate"]<$now);
	$loans_html[$loan_id]["IsLast"] = ($count == ++$c);
	
}

$Loans_tpl = new CTemplate(__TEMPLATES_DIR__."/tasks/detail-loans-row.tpl");
$Loans_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);
$Page->Add("LOANS", $Loans_tpl->RepeatHtml($loans, $loans_html));

$Page->Add("LANGUAGE", __LANGUAGE__);
$Page->Add("DATE", $results["date"] ? $results["date"] : $GLOBALS["Format"]->Date());
$Page->Add("SUBJECT_TXT", $task["subject"]);
$Page->Add("DESCRIPTION_TXT", nl2br($task["description"]));
$Page->Add("NOTICE", $results["notice"] ? $results["notice"] : $task["notice"]);
$Page->Add("RESULT", isset($results["result"]) ? $results["result"] : "");

$times = array();
for ($h = 9; $h < 16; $h++) {
	
	for ($m = "00"; $m < 60; $m+=15) {
	
		$times[$h*3600+$m*60] = ($h>12 ? $h-12 : $h).":".$m." ".($h<12 ? "AM" : "PM");
	
	}
	
}

$Page->Add("TIMES", $GLOBALS["Library"]->ShowSelect($times, $results["time"]));

?>