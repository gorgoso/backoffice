<?php

$groups = $GLOBALS["Library"]->GetOptions("user_groups");

$Page->Add("NAME", $_SESSION["User"]->name);
$Page->Add("USERNAME", $_SESSION["User"]->username);
$Page->Add("USER_ROLE", $groups[$_SESSION["User"]->groups[$_SESSION["User"]->group_id]]);
$Page->Add("REGDATE_TXT", $GLOBALS["Format"]->Date($_SESSION["User"]->regdate, __FORMAT_DATE_FULL__));
$Page->Add("LASTLOG_TXT", $GLOBALS["Format"]->Date($_SESSION["User"]->lastlog, __FORMAT_DATE_FULL__));
$Page->Add("EMAIL", isset($_SESSION["User"]->email)?$_SESSION["User"]->email:"");
$Page->Add("PHONE", isset($_SESSION["User"]->phone)?$_SESSION["User"]->phone:"");

?>