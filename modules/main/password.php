<?php

$change = $GLOBALS["Library"]->Register("change");

if(is_array($change)) {

	if (!$change["password"] || !$change["password1"]) $GLOBALS["Error"]->Add("BAD_PASSWORD");
	if ($change["password"]!=$change["password1"]) $GLOBALS["Error"]->Add("PASSWORDS_NOT_MATCH");
	if (password_verify($change["password"], $_SESSION["User"]->password)) $GLOBALS["Error"]->Add("PASSWORDS_ARE_SAME", "warning");

	if (!$GLOBALS["Error"]->Exists()) {

		$save = array("password"=>$GLOBALS["Common"]->Password($change["password"]));

		if ($GLOBALS["Sql"]->Update("users", $save, "user_id", $_SESSION["User"]->user_id)) $GLOBALS["Error"]->Add("PASSWORD_CHANGED", "notice");

	}

}

$Page->Add("USERNAME", $_SESSION["User"]->username);

?>