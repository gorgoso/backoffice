<?php

if ($_SESSION["User"]->IsAuth()) $GLOBALS["Library"]->Redirect(__PATH_ADMIN__."login/");

$Page->Add("LANGUAGES", $GLOBALS["Library"]->ShowSelect($_SESSION["Language"]->GetLanguages(), __LANGUAGE__)); 

$redirect = $GLOBALS["Library"]->Register("redirect") ? $GLOBALS["Library"]->Register("redirect") : (isset($_SERVER["REDIRECT_SCRIPT_URI"]) ? $_SERVER["REDIRECT_SCRIPT_URI"] : "");

$Page->Add("REDIRECT", $redirect);

?>