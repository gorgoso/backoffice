<?php

$client_id = $GLOBALS["Library"]->Reg("id");

if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/"); 

$update = true;
$client_check = array(
	//"email" => false,
	//"home" => false,
	//"work" => false,
	//"cellular" => false,
	"physical_address" => false,
	"mailing_address" => false,
	"personal_document" => false
);

foreach($client["contacts"] as $contact) $client_check[$contact["type"]] = true;

if (isset($client["addresses"]["physical"]["country"])) $client_check["physical_address"] = true;
if (isset($client["addresses"]["mailing"]["country"])) $client_check["mailing_address"] = true;
if (isset($client["documents"])) if ($client["documents"]) $client_check["personal_document"] = true;

foreach($client_check as $key=>$cc) {
	
	if ($cc === false) {
			
		$update = false;
		$GLOBALS["Error"]->AddSessionError("MISSING_".strtoupper($key), "error");
		
	}

}

if ($update) if ($GLOBALS["Client"]->SetStatus($client_id, "verification")) {
	
	$GLOBALS["Error"]->AddSessionError("CLIENT_IN_VERIFICATION", "notice");
	
} else $GLOBALS["Error"]->AddSessionError("CLIENT_NOT_IN_VERIFICATION", "error");

$GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/detail/".$client_id."/");

?>