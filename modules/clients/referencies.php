<?php

$client_id = $GLOBALS["Library"]->Reg("id");
$add = $GLOBALS["Library"]->Reg("add");

if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

$statuses = $GLOBALS["Library"]->GetStatuses("referencies");
$countries = $GLOBALS["Data"]->countries;
$calling_codes = $GLOBALS["Library"]->GetStatuses("calling_codes");

if (is_array($add)) {

	$add = $GLOBALS["Format"]->TextArray($add, array("name", "notice", "phone", "cellular", "address1", "address2", "address3", "city", "zip", "state"));

	$GLOBALS["Sql"]->Validate(array("name"=>$add["name"]));
	if ($add["notice"]) $GLOBALS["Sql"]->Validate(array("notice"=>$add["notice"]));
	if (!array_key_exists($add["phone_code"], $calling_codes)) $GLOBALS["Error"]->AddError("BAD_PHONE_CALLING_CODE");
	$GLOBALS["Sql"]->Validate(array("phone"=>$add["phone"]), array(), "BAD_PHONE");
	if (!array_key_exists($add["cellular_code"], $calling_codes)) $GLOBALS["Error"]->AddError("BAD_CELLULAR_CALLING_CODE");
	$GLOBALS["Sql"]->Validate(array("cellular"=>$add["cellular"]), array(), "BAD_CELLULAR");
	$GLOBALS["Sql"]->Validate(array("address"=>$add["address1"]));
	if ($add["address2"]) $GLOBALS["Sql"]->Validate(array("address"=>$add["address2"]));
	if ($add["address3"]) $GLOBALS["Sql"]->Validate(array("address"=>$add["address3"]));
	$GLOBALS["Sql"]->Validate(array("city"=>$add["city"]));
	if ($add["zip"]) $GLOBALS["Sql"]->Validate(array("zip"=>$add["zip"]));
	$GLOBALS["Sql"]->Validate(array("state"=>$add["state"]));
	if (!array_key_exists($add["country"], $countries)) $GLOBALS["Error"]->AddError("BAD_COUNTRY");
	
	if (!$GLOBALS["Error"]->ErrorExists()) {

		$GLOBALS["Sql"]->StartTransaction();
		
		if ($GLOBALS["Client"]->AddReference($client_id, $add["name"], $add["notice"], $add["phone_code"], $add["phone"], $add["cellular_code"], $add["cellular"], $add["address1"], $add["address2"], $add["address3"], $add["city"], $add["zip"], $add["state"], $add["country"])) {
	
			$GLOBALS["Sql"]->Commit();
			
			$GLOBALS["Error"]->AddSessionError("REFERENCE_ADDED_SUCCESSFULY", "notice");
			$add = false;
		
		} else {
			
			$GLOBALS["Error"]->AddError("REFERENCE_NOT_ADDED", "error");
			$GLOBALS["Sql"]->Rollback();

		}
	}
}

$filter = $_SESSION["Filter"]->GetFilter(array("status"=>array("multiple"=>array("active"))), "add_date", "desc");

$sqldata = array("client_id"=>$client_id);
$referencies = $GLOBALS["Sql"]->SelectArray("SELECT *, C1.`code` AS `phone_code`, C1.`contact` AS `phone`, C2.`code` AS `cellular_code`, C2.`contact` AS `cellular`, R.`notice` FROM `referencies` R LEFT JOIN `ref2cli` R2C USING(`reference_id`) LEFT JOIN `addresses` A USING(`address_id`) LEFT JOIN `contacts` C1 ON(R.`phone_contact_id`=C1.`contact_id`) LEFT JOIN `contacts` C2 ON(R.`cellular_contact_id`=C2.`contact_id`) WHERE R2C.`client_id`=:client_id AND ".$filter["where"], $sqldata);
$referencies_html=array();
$referencies_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`reference_id`) AS `count`, C1.`code` AS `phone_code`, C1.`contact` AS `phone`, C2.`code` AS `cellular_code`, C2.`contact` AS `cellular` FROM `referencies` R LEFT JOIN `ref2cli` R2C USING(`reference_id`) LEFT JOIN `addresses` A USING(`address_id`) LEFT JOIN `contacts` C1 ON(R.`phone_contact_id`=C1.`contact_id`) LEFT JOIN `contacts` C2 ON(R.`cellular_contact_id`=C2.`contact_id`) WHERE R2C.`client_id`=:client_id AND ".$filter["conditions"], $sqldata);

$Referencies_tpl = new CTemplate(__TEMPLATES_DIR__."/clients/referencies-row.tpl");
$Referencies_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

foreach($referencies as $reference_id=>$reference) {

	$referencies[$reference_id]["ADD_DATE_TXT"] = $GLOBALS["Format"]->Date($reference["add_date"], __FORMAT_DATE_FULL__);
	$referencies[$reference_id]["DEL_DATE_TXT"] = $reference["del_date"] ? $GLOBALS["Format"]->Date($reference["del_date"], __FORMAT_DATE_FULL__) : "-";
	$referencies[$reference_id]["STATUS_TXT"] = $statuses[$reference["status"]];
	$referencies[$reference_id]["COUNTRY_TXT"] = $countries[$reference["country"]];
	$referencies[$reference_id]["PHONE_TXT"] = $GLOBALS["Format"]->Phone($reference["phone"]);
	$referencies[$reference_id]["CELLULAR_TXT"] = $GLOBALS["Format"]->Phone($reference["cellular"]);
	
	$referencies_html[$reference_id]["IsNotice"] = ($reference["notice"]);
	$referencies_html[$reference_id]["IsBlocked"] = ($reference["status"]=="deleted");
	$referencies_html[$reference_id]["IsAddress2"] = ($reference["address2"]);
	$referencies_html[$reference_id]["IsAddress3"] = ($reference["address3"]);
	$referencies_html[$reference_id]["IsZip"] = ($reference["zip"]);
	$referencies_html[$reference_id]["IsState"] = ($reference["state"]);

}

$Page->Add("CLIENT_ID", $client_id);
$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($client_id));
$Page->Add("FIRSTNAME", $client["client"]["firstname"]);
$Page->Add("LASTNAME", $client["client"]["lastname"]);

$Page->Add("NAME", $add["name"]);
$Page->Add("NOTICE", $add["notice"]);
$Page->Add("PHONE", $add["phone"]);
$Page->Add("CELLULAR", $add["cellular"]);

$Page->Add("ADDRESS1", $add["address1"]);
$Page->Add("ADDRESS2", $add["address2"]);
$Page->Add("ADDRESS3", $add["address3"]);
$Page->Add("CITY", $add["city"]);
$Page->Add("ZIP", $add["zip"]);
$Page->Add("STATE", $add["state"]);
$Page->Add("COUNTRIES", $GLOBALS["Library"]->ShowSelect($countries, (isset($add["country"])) ? $add["country"] : $client["client"]["nationality"]));

$Page->Add("CALLING_CODES", $GLOBALS["Library"]->ShowSelect($calling_codes, isset($add["code_phone"])?$add["code_phone"]:""));

$Page->Add("REFERENCIES", $Referencies_tpl->RepeatHtml($referencies, $referencies_html));

$Page->Add("LIST", $GLOBALS["Library"]->MakeList($referencies_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));

$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, isset($filter["in"]["conditions"]["status"]["multiple"][0])?$filter["in"]["conditions"]["status"]["multiple"][0]:""));
$Page->Add("NAME_FILTER", isset($filter["in"]["conditions"]["name"])?$filter["in"]["conditions"]["name"]:"");
$Page->Add("PHONE_FILTER", isset($filter["in"]["conditions"]["C1.contact"])?$filter["in"]["conditions"]["C1.contact"]:"");
$Page->Add("CELLULAR_FILTER", isset($filter["in"]["conditions"]["C2.contact"])?$filter["in"]["conditions"]["C2.contact"]:"");

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $referencies_count["count"]);

?>