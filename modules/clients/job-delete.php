<?php

$job_id = $GLOBALS["Library"]->Reg("id");
if (!$client_id = $GLOBALS["Sql"]->Value("job2cli", "client_id", "job_id", $job_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

if ($GLOBALS["Client"]->DeleteJob($job_id)) {
	
	$GLOBALS["Error"]->AddSessionError("JOB_DELETED", "notice");
	
} else $GLOBALS["Error"]->AddSessionError("JOB_NOT_DELETED", "error");

$GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/jobs/".$client_id."/");

?>