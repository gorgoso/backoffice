<?php

$reference_id = $GLOBALS["Library"]->Reg("id");
if (!$client_id = $GLOBALS["Sql"]->Value("ref2cli", "client_id", "reference_id", $reference_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

if ($GLOBALS["Client"]->DeleteReference($reference_id)) {
	
	$GLOBALS["Error"]->AddSessionError("REFERENCE_DELETED", "notice");
	
} else $GLOBALS["Error"]->AddSessionError("REFERENCE_NOT_DELETED", "error");

$GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/referencies/".$client_id."/");

?>