<?php

$address_id = $GLOBALS["Library"]->Reg("id");
if (!$client_id = $GLOBALS["Sql"]->Value("add2cli", "client_id", "address_id", $address_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

if ($GLOBALS["Client"]->DeleteAddress($address_id)) {
	
	$GLOBALS["Error"]->AddSessionError("ADDRESS_DELETED", "notice");
	
} else $GLOBALS["Error"]->AddSessionError("ADDRESS_NOT_DELETED", "error");

$GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/addresses/".$client_id."/");

?>