<?php

$contact_id = $GLOBALS["Library"]->Reg("id");
if (!$client_id = $GLOBALS["Sql"]->Value("con2cli", "client_id", "contact_id", $contact_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

if ($GLOBALS["Client"]->DeleteContact($contact_id)) {
	
	$GLOBALS["Error"]->AddSessionError("CONTACT_DELETED", "notice");
	
} else $GLOBALS["Error"]->AddSessionError("CONTACT_NOT_DELETED", "error");

$GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/contacts/".$client_id."/");

?>