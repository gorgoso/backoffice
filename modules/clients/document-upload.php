<?php

$document_id = $GLOBALS["Library"]->Reg("id");
$file = isset($_FILES["update"]) ? $_FILES["update"] : false;

if (!$document = $GLOBALS["Sql"]->Fetch("SELECT * FROM `doc2cli` LEFT JOIN `documents` USING(`document_id`) WHERE `document_id`=:document_id", array("document_id"=>$document_id))) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

if (!$client = $GLOBALS["Client"]->Get($document["client_id"])) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

$types = $GLOBALS["Library"]->GetStatuses("document_types");
$countries = $GLOBALS["Data"]->countries;

if (is_array($file)) {
	
	if ($file["error"]>0) $GLOBALS["Error"]->AddError("FILE_NOT_UPLOADED");
	if ($file["size"]<=0 || $file["size"]>8000000) $GLOBALS["Error"]->AddError("FILE_TOO_BIG");
	
	if (!$GLOBALS["Error"]->ErrorExists()) {
		
		$data = file_get_contents($file["tmp_name"]);
		unlink($file["tmp_name"]);
							
		if ($file_id = $GLOBALS["Common"]->AddFile($file["name"], $file["type"], $file["size"], $data, $_SESSION["Language"]->Translate("PERSONAL_ID"))) {
	
			$GLOBALS["Sql"]->Update("documents", array("file_id"=>$file_id), "document_id", $document_id);

			$GLOBALS["Sql"]->Query("UPDATE `fil2cli` SET `status`='deleted', `del_date`=UNIX_TIMESTAMP() WHERE `group`='personal_id' AND `client_id`=:client_id", array("client_id"=>$client["client_id"]));
	
			$save = array(
				"client_id"=>$client["client_id"],
				"file_id"=>$file_id,
				"add_date"=>time(),
				"group"=>"personal_id",
				"status"=>"active"
			);

			$GLOBALS["Sql"]->Insert("fil2cli", $save);

			$GLOBALS["Common"]->AddHistory("Personal ID added successfuly. (File ID: $file_id, document ID: $document_id)", "documents", $client["client_id"]);

			$GLOBALS["Error"]->AddSessionError("FILE_ADDED_SUCCESSFULY", "notice");
			$GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/document-upload/${document_id}/");
		
		} else $GLOBALS["Error"]->AddError("FILE_NOT_ADDED", "error");

	}

}

if ($document["country"] == "do" && $document["type"] == "personal-id") {
	
	$document["number"] = preg_replace("/([0-9]{3})([0-9]{7})([0-9]{1})/", "$1-$2-$3", $document["number"]);
	
}


$Page->Add("CLIENT_ID", $document["client_id"]);
$Page->Add("DOCUMENT_ID", $document_id);

$Page->Add("TYPE_TXT", $types[$document["type"]]);
$Page->Add("NUMBER", $document["number"]);
$Page->Add("COUNTRY_TXT", $countries[$document["country"]]);
$Page->Add("FIRSTNAME", $client["client"]["firstname"]);
$Page->Add("LASTNAME", $client["client"]["lastname"]);
$Page->Add("HASH", $GLOBALS["Library"]->GetHash($document["file_id"]));

$Page->AddHTML("IsUpload", !$document["file_id"]);
$Page->AddHTML("ShowDocument", $document["file_id"]);

?>