<?php

$contact_id = $GLOBALS["Library"]->Reg("id");
$sendmail = $GLOBALS["Library"]->Reg("sendmail");

if (!$contact = $GLOBALS["Sql"]->Fetch("SELECT `client_id`, `contact` FROM `con2cli` C2C LEFT JOIN `contacts` C USING(`contact_id`) WHERE C.`type`='email' AND C2C.`status`='active' AND C.`contact_id`=:contact_id", array("contact_id"=>$contact_id))) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

if (!$client = $GLOBALS["Client"]->Get($contact["client_id"])) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

$senders = array(
	__SENDMAIL_EMAIL__."|".__SENDMAIL_NAME__=>__SENDMAIL_NAME__." (".__SENDMAIL_EMAIL__.")",
	$_SESSION["User"]->email."|".$_SESSION["User"]->name=>$_SESSION["User"]->name." (".$_SESSION["User"]->email.")",
);

$templates = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `templates` WHERE `status`='active'", array(), "template_id");
$templates_tmp = array();
foreach($templates as $template_id=>$template) $templates_tmp[$template_id] = $template["name"];

if (is_array($sendmail)) {

	$sendmail = $GLOBALS["Format"]->TextArray($sendmail, array("sender", "subject"));

	if (!array_key_exists($sendmail["sender"], $senders)) $GLOBALS["Error"]->AddError("BAD_EMAIL_SENDER");
			
	$GLOBALS["Sql"]->Validate(array("notice"=>$sendmail["subject"]), array(), "BAD_EMAIL_TEXT");
	if (!$sendmail["text"]) $GLOBALS["Error"]->AddError("BAD_EMAIL_TEXT");
	
	if (!$GLOBALS["Error"]->ErrorExists()) {
		
		$header_txt = file_get_contents(__TEMPLATES_DIR__."/system/emails/header.tpl");
		$footer_txt = file_get_contents(__TEMPLATES_DIR__."/system/emails/footer.tpl");
		
		$email_txt = $header_txt.nl2br($sendmail["text"]).$footer_txt;
		$sender = explode("|", $sendmail["sender"]);
							
		if ($GLOBALS["Library"]->SendMail($contact["contact"], $sendmail["subject"], $email_txt, $sender[0], $sender[1])) {
			
			$GLOBALS["Common"]->AddHistory("E-mail send successfuly. (Subject: ".$sendmail["subject"].($sendmail["template_id"] ? ", template ID: ".$sendmail["template_id"] : "").")", "emails", $contact["client_id"]);
			
			if ($cellular = $GLOBALS["Client"]->GetCellular($contact["client_id"])) {
			
				$Sms = new CTemplate();
				$Sms->StringTemplate($_SESSION["Language"]->Translate("SMS_EMAIL_NOTIFICATION"));
				$Sms->Add("EMAIL", $contact["contact"]);
				$Sms->Translate();

				require_once("classes/CMaintenance.php");
				$M = new CMaintenance();
			
				if ($M->SendSMS($cellular, $Sms->result)) $GLOBALS["Common"]->AddHistory("SMS message sent (e-mail notification) to client cellular ${cellular}.", "emails", $contact["client_id"]);
				
			}
			
			$GLOBALS["Error"]->AddSessionError("EMAIL_SENT_SUCCESSFULY", "notice");
			$GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/detail/".$contact["client_id"]."/");
			
		} else $GLOBALS["Error"]->AddError("EMAIL_NOT_SEND", "error");

	}
	
}

$Page->Add("CONTACT_ID", $contact_id);

$Page->Add("CLIENT_ID", $client["client"]["client_id"]);
$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($client["client"]["client_id"]));
$Page->Add("FIRSTNAME", $client["client"]["firstname"]);
$Page->Add("LASTNAME", $client["client"]["lastname"]);

$Page->Add("SENDERS", $GLOBALS["Library"]->ShowSelect($senders, $sendmail["sender"]));
$Page->Add("TEMPLATES", $GLOBALS["Library"]->ShowSelect($templates_tmp, $sendmail["template_id"]));

$Page->Add("EMAIL_SUBJECT", $sendmail["subject"]);
$Page->Add("EMAIL_TEXT", $sendmail["text"]);

$Page->Add("TEMPLATES_JSON", json_encode($templates));

?>