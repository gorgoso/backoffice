<?php

$client_id = $GLOBALS["Library"]->Reg("id");
$add = $GLOBALS["Library"]->Reg("add");

if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

$statuses = $GLOBALS["Library"]->GetStatuses("information");
$types = $GLOBALS["Library"]->GetStatuses("info_types");
$civil_statuses = $GLOBALS["Library"]->GetStatuses("civil-status-".$client["client"]["sex"]);
$residence = $GLOBALS["Library"]->GetStatuses("residence");
$occupation = $GLOBALS["Library"]->GetStatuses("occupation");

if (is_array($add)) {

	$add = $GLOBALS["Format"]->TextArray($add, array("notice"));
	
	switch($add["type"]) {
		
		case "":
		break;
		
		case "civil-status":
			if (!array_key_exists($add["information"], $civil_statuses)) $GLOBALS["Error"]->AddError("BAD_CIVIL_STATUS");
		break;

		case "residence":
			if (!array_key_exists($add["information"], $residence)) $GLOBALS["Error"]->AddError("BAD_RESIDENCE");
		break;

		case "residence":
			if (!array_key_exists($add["information"], $occupation)) $GLOBALS["Error"]->AddError("BAD_OCCUPATION");
		break;
		
		default:
			$GLOBALS["Sql"]->Validate(array("notice"=>$add["information"]), array(), "BAD_INFORMATION");
		break;
		
	}

	if (!array_key_exists($add["type"], $types)) $GLOBALS["Error"]->AddError("BAD_CONTACT_TYPE");
	if ($add["notice"]) $GLOBALS["Sql"]->Validate(array("notice"=>$add["notice"]));

	if (!$GLOBALS["Error"]->ErrorExists()) {
		
		if ($GLOBALS["Client"]->AddInformation($client_id, $add["type"], $add["information"], $add["notice"])) {

			foreach($client["information"] as $information) {
				
				if ($information["type"] == $add["type"]) $GLOBALS["Client"]->DeleteInformation($information["information_id"]);
				
			}
	
			$GLOBALS["Error"]->AddSessionError("INFORMATION_ADDED_SUCCESSFULY", "notice");
			$add = array();
		
		} else $GLOBALS["Error"]->AddError("INFORMATION_NOT_ADDED", "error");

	}

}

$filter = $_SESSION["Filter"]->GetFilter(array("status"=>array("multiple"=>array("active"))), "add_date", "desc");

$sqldata = array("client_id"=>$client_id);
$information = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `information` I LEFT JOIN `inf2cli` I2C USING(`information_id`) WHERE I2C.`client_id`=:client_id AND ".$filter["where"], $sqldata);
$information_html=array();
$information_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`information_id`) AS `count` FROM `information` I LEFT JOIN `inf2cli` I2C USING(`information_id`) WHERE I2C.`client_id`=:client_id AND ".$filter["conditions"], $sqldata);

$Information_tpl = new CTemplate(__TEMPLATES_DIR__."/clients/information-row.tpl");

$Information_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

foreach($information as $information_id=>$info) {
	
	switch($info["type"]) {
			
		case "civil-status":
			$information[$information_id]["INFORMATION"] = $civil_statuses[$info["information"]];
		break;

		case "residence":
			$information[$information_id]["INFORMATION"] = $residence[$info["information"]];
		break;

		case "occupation":
			$information[$information_id]["INFORMATION"] = $occupation[$info["information"]];
		break;
				
	}
	
	$information[$information_id]["ADD_DATE_TXT"] = $GLOBALS["Format"]->Date($info["add_date"], __FORMAT_DATE_FULL__);
	$information[$information_id]["DEL_DATE_TXT"] = $info["del_date"] ? $GLOBALS["Format"]->Date($info["del_date"], __FORMAT_DATE_FULL__) : "-";
	$information[$information_id]["TYPE_TXT"] = $types[$info["type"]];
	$information[$information_id]["STATUS_TXT"] = $statuses[$info["status"]];
	$information[$information_id]["NOTICE"] = $info["notice"] ? addslashes(htmlspecialchars($info["notice"])) : "";
	
	$information_html[$information_id]["IsBlocked"] = ($info["status"]=="deleted");
	$information_html[$information_id]["IsNotDeleted"] = ($info["status"]!="deleted");
	$information_html[$information_id]["IsNotice"] = ($info["notice"]);

}

$Page->Add("CLIENT_ID", $client_id);
$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($client_id));
$Page->Add("FIRSTNAME", $client["client"]["firstname"]);
$Page->Add("LASTNAME", $client["client"]["lastname"]);
$Page->Add("INFORMATION", isset($add["information"])?$add["information"]:"");
$Page->Add("NOTICE", isset($add["notice"])?$add["notice"]:"");

$Page->Add("TYPES", $GLOBALS["Library"]->ShowSelect($types, isset($add["type"])?$add["type"]:""));
$Page->Add("CIVIL_STATUSES", json_encode($civil_statuses));
$Page->Add("RESIDENCE", json_encode($residence));
$Page->Add("OCCUPATION", json_encode($occupation));

$Page->Add("INFORMATION_LIST", $Information_tpl->RepeatHtml($information, $information_html));

$Page->Add("LIST", $GLOBALS["Library"]->MakeList($information_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));

$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, isset($filter["in"]["conditions"]["status"]["multiple"][0])?$filter["in"]["conditions"]["status"]["multiple"][0]:""));
$Page->Add("FILTER_TYPES", $GLOBALS["Library"]->ShowSelect($types, isset($filter["in"]["conditions"]["type"]["multiple"][0])?$filter["in"]["conditions"]["type"]["multiple"][0]:""));

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $information_count["count"]);

?>