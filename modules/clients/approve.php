<?php

$client_id = $GLOBALS["Library"]->Reg("id");

if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/"); 

if ($GLOBALS["Client"]->SetStatus($client_id, "active")) {
	
	$GLOBALS["Error"]->AddSessionError("CLIENT_APPROVED", "notice");

	if ($email = $GLOBALS["Client"]->GetEmail($client_id)) {
		
		$client["client"]["CLIENT_ID_TXT"] = $GLOBALS["Format"]->Id($client_id);
								
		$email_txt = $GLOBALS["Library"]->FormatEmail("client-approved", $client["client"], true, true);
					
		$GLOBALS["Library"]->SendMail($email, $_SESSION["Language"]->Translate("EMAIL_SUBJECT"), $email_txt);
						
	}
	
} else $GLOBALS["Error"]->AddSessionError("CLIENT_NOT_APPROVED", "error");

$GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/detail/".$client_id."/");

?>