<?php

$client_id = $GLOBALS["Library"]->Reg("id");
$add = $GLOBALS["Library"]->Reg("add");

if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

$statuses = $GLOBALS["Library"]->GetStatuses("documents");
$types = $GLOBALS["Library"]->GetStatuses("document_types");
$countries = $GLOBALS["Data"]->countries;

if (is_array($add)) {

	$add = $GLOBALS["Format"]->TextArray($add, array("number"));
	
	if (!array_key_exists($add["type"], $types)) $GLOBALS["Error"]->AddError("BAD_DOCUMENT_TYPE");
	
	$personal_id_tmp = "personal_id".($add["country"] == "do" && $add["type"] == "personal-id" ? "_do" : "");
	$add["number"] = preg_replace("/[^0-9]/", "", $add["number"]);
		
	$GLOBALS["Sql"]->Validate(array($personal_id_tmp=>$add["number"]));
	if (!array_key_exists($add["country"], $countries)) $GLOBALS["Error"]->AddError("BAD_COUNTRY");

	$year = date("Y");

	if ($add["date_issued"]) if ($GLOBALS["Sql"]->Validate(array("date"=>$add["date_issued"]), array(), "BAD_ISSUE_DATE")) {
		
		$date_tmp = explode("-", $add["date_issued"]);

		if (($date_tmp[0] < $year-100) || $date_tmp[0] > $year) $GLOBALS["Error"]->AddError("BAD_ISSUE_DATE");		
		if (!checkdate($date_tmp[1], $date_tmp[2], $date_tmp[0])) $GLOBALS["Error"]->AddError("BAD_ISSE_DATE");
		
	}

	if ($add["date_expiration"]) if ($GLOBALS["Sql"]->Validate(array("date"=>$add["date_expiration"]), array(), "BAD_EXPIRATION_DATE")) {
		
		$date_tmp = explode("-", $add["date_expiration"]);

		if (($date_tmp[0] < $year-100)) $GLOBALS["Error"]->AddError("BAD_EXPIRATION_DATE");		
		if (!checkdate($date_tmp[1], $date_tmp[2], $date_tmp[0])) $GLOBALS["Error"]->AddError("BAD_EXPIRATION_DATE");
		
	}
	
	$sqldata = array("type"=>$add["type"], "country"=>$add["country"], "number"=>$add["number"]);
	
	if ($GLOBALS["Sql"]->Fetch("SELECT `document_id` FROM `documents` WHERE `type`=:type AND `country`=:country AND `number`=:number", $sqldata)) $GLOBALS["Error"]->AddError("DOCUMENT_PERSONAL_EXISTS");
		
	if (!$GLOBALS["Error"]->ErrorExists()) {
		
		if ($GLOBALS["Client"]->AddDocument($client_id, $add["type"], $add["number"], $add["country"], $GLOBALS["Library"]->GetTimestamp($add["date_issued"]), $GLOBALS["Library"]->GetTimestamp($add["date_expiration"]))) {
	
			foreach($client["documents"] as $document) {
				
				if ($add["type"]==$document["type"] && $add["country"]==$document["country"]) $GLOBALS["Client"]->DeleteDocument($document["document_id"]);
				
			}
			
			$GLOBALS["Error"]->AddSessionError("ADDRESS_ADDED_SUCCESSFULY", "notice");
			$add = false;
		
		} else $GLOBALS["Error"]->AddError("ADDRESS_NOT_ADDED", "error");
		
	}

}

$filter = $_SESSION["Filter"]->GetFilter(array("status"=>array("multiple"=>array("active"))), "add_date", "desc");

$sqldata = array("client_id"=>$client_id);
$documents = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `documents` D LEFT JOIN `doc2cli` D2C USING(`document_id`) WHERE D2C.`client_id`=:client_id AND ".$filter["where"], $sqldata);
$documents_html=array();
$documents_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`document_id`) AS `count` FROM `documents` D LEFT JOIN `doc2cli` D2C USING(`document_id`) WHERE D2C.`client_id`=:client_id AND ".$filter["conditions"], $sqldata);

$Documents_tpl = new CTemplate(__TEMPLATES_DIR__."/clients/documents-row.tpl");

$Documents_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

foreach($documents as $document_id=>$document) {
	
	$documents[$document_id]["ADD_DATE_TXT"] = $GLOBALS["Format"]->Date($document["add_date"], __FORMAT_DATE_FULL__);
	$documents[$document_id]["DEL_DATE_TXT"] = $document["del_date"] ? $GLOBALS["Format"]->Date($document["del_date"], __FORMAT_DATE_FULL__) : "-";
	$documents[$document_id]["DATE_ISSUED_TXT"] = $document["date_issued"] ? $GLOBALS["Format"]->Date($document["date_issued"]) : "-";
	$documents[$document_id]["DATE_EXPIRATION_TXT"] = $document["date_expiration"] ? $GLOBALS["Format"]->Date($document["date_expiration"]) : "-";
	$documents[$document_id]["CONTACT_TYPE_TXT"] = $types[$document["type"]];
	$documents[$document_id]["STATUS_TXT"] = $statuses[$document["status"]];
	$documents[$document_id]["COUNTRY_TXT"] = $countries[$document["country"]];
	
	$documents[$document_id]["NUMBER_TXT"] = ($document["country"]=="do" && $document["type"]=="personal-id") ? preg_replace("/([0-9]{3})([0-9]{7})([0-9]{1})/", "$1-$2-$3", $document["number"]) : $document["number"];
	
	$documents_html[$document_id]["IsBlocked"] = ($document["status"]=="deleted");

}

$Page->Add("LANGUAGE", __LANGUAGE__);
$Page->Add("CLIENT_ID", $client_id);
$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($client_id));
$Page->Add("FIRSTNAME", $client["client"]["firstname"]);
$Page->Add("LASTNAME", $client["client"]["lastname"]);

$Page->Add("TYPES", $GLOBALS["Library"]->ShowSelect($types, $add["type"]));
$Page->Add("NUMBER", $add["number"]);
$Page->Add("COUNTRIES", $GLOBALS["Library"]->ShowSelect($countries, (isset($add["country"])) ? $add["country"] : $client["client"]["nationality"]));
$Page->Add("DATE_ISSUED", $add["date_issued"]);
$Page->Add("DATE_EXPIRATION", $add["date_expiration"]);

$Page->Add("DOCUMENTS", $Documents_tpl->RepeatHtml($documents, $documents_html));

$Page->Add("LIST", $GLOBALS["Library"]->MakeList($documents_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));

$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, isset($filter["in"]["conditions"]["status"]["multiple"][0])?$filter["in"]["conditions"]["status"]["multiple"][0]:""));
$Page->Add("FILTER_TYPES", $GLOBALS["Library"]->ShowSelect($types, isset($filter["in"]["conditions"]["type"]["multiple"][0])?$filter["in"]["conditions"]["type"]["multiple"][0]:""));

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $documents_count["count"]);

?>