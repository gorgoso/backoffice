<?php

$client_id = $GLOBALS["Library"]->Reg("id");
$add = $GLOBALS["Library"]->Reg("add");

if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

$statuses = $GLOBALS["Library"]->GetStatuses("addresses");
$types = $GLOBALS["Library"]->GetStatuses("address_types");
$countries = $GLOBALS["Data"]->countries;

if (is_array($add)) {

	$add = $GLOBALS["Format"]->TextArray($add, array("address1", "address2", "address3", "city", "zip", "state"));

	if (!array_key_exists($add["type"], $types)) $GLOBALS["Error"]->AddError("BAD_ADDRESS_TYPE");
			
	$GLOBALS["Sql"]->Validate(array("address"=>$add["address1"]));
	if ($add["address2"]) $GLOBALS["Sql"]->Validate(array("address"=>$add["address2"]));
	if ($add["address3"]) $GLOBALS["Sql"]->Validate(array("address"=>$add["address3"]));
	$GLOBALS["Sql"]->Validate(array("city"=>$add["city"]));
	if ($add["zip"]) $GLOBALS["Sql"]->Validate(array("zip"=>$add["zip"]));
	$GLOBALS["Sql"]->Validate(array("state"=>$add["state"]));
	if (!array_key_exists($add["country"], $countries)) $GLOBALS["Error"]->AddError("BAD_INPUT_COUNTRY");
	
	if (!$GLOBALS["Error"]->ErrorExists()) {
		
		if ($GLOBALS["Client"]->AddAddress($client_id, $add["type"], $add["address1"], $add["address2"], $add["address3"], $add["city"], $add["zip"], $add["state"], $add["country"])) {
				
			foreach($client["addresses"] as $address) {
				
				if ($add["type"]==$address["type"]) $GLOBALS["Client"]->DeleteAddress($address["address_id"]);
				
			}
						
			$GLOBALS["Error"]->AddSessionError("ADDRESS_ADDED_SUCCESSFULY", "notice");
			$add = false;
		
		} else $GLOBALS["Error"]->AddError("ADDRESS_NOT_ADDED", "error");
		
	}

}

$filter = $_SESSION["Filter"]->GetFilter(array("status"=>array("multiple"=>array("active"))), "add_date", "desc");

$sqldata = array("client_id"=>$client_id);
$addresses = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `addresses` A LEFT JOIN `add2cli` A2C USING(`address_id`) WHERE A2C.`client_id`=:client_id AND ".$filter["where"], $sqldata);
$addresses_html=array();
$addresses_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`address_id`) AS `count` FROM `addresses` A LEFT JOIN `add2cli` A2C USING(`address_id`) WHERE A2C.`client_id`=:client_id AND ".$filter["conditions"], $sqldata);

$Addresses_tpl = new CTemplate(__TEMPLATES_DIR__."/clients/addresses-row.tpl");

$Addresses_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

foreach($addresses as $address_id=>$address) {
	
	$addresses[$address_id]["ADD_DATE_TXT"] = $GLOBALS["Format"]->Date($address["add_date"], __FORMAT_DATE_FULL__);
	$addresses[$address_id]["DEL_DATE_TXT"] = $address["del_date"] ? $GLOBALS["Format"]->Date($address["del_date"], __FORMAT_DATE_FULL__) : "-";
	$addresses[$address_id]["CONTACT_TYPE_TXT"] = $types[$address["type"]];
	$addresses[$address_id]["STATUS_TXT"] = $statuses[$address["status"]];
	$addresses[$address_id]["COUNTRY_TXT"] = $countries[$address["country"]];
	
	$addresses_html[$address_id]["IsBlocked"] = ($address["status"]=="deleted");
	$addresses_html[$address_id]["IsAddress2"] = ($address["address2"]);
	$addresses_html[$address_id]["IsAddress3"] = ($address["address3"]);

}

$Page->Add("CLIENT_ID", $client_id);
$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($client_id));
$Page->Add("FIRSTNAME", $client["client"]["firstname"]);
$Page->Add("LASTNAME", $client["client"]["lastname"]);

$Page->Add("TYPES", $GLOBALS["Library"]->ShowSelect($types, isset($add["type"])?$add["type"]:$GLOBALS["Library"]->Reg("type")));

$Page->Add("ADDRESS1", $add["address1"]);
$Page->Add("ADDRESS2", $add["address2"]);
$Page->Add("ADDRESS3", $add["address3"]);
$Page->Add("CITY", $add["city"]);
$Page->Add("ZIP", $add["zip"]);
$Page->Add("STATE", $add["state"]);
$Page->Add("COUNTRIES", $GLOBALS["Library"]->ShowSelect($countries, (isset($add["country"])) ? $add["country"] : $client["client"]["nationality"]));

$Page->Add("ADDRESSES", $Addresses_tpl->RepeatHtml($addresses, $addresses_html));

$Page->Add("LIST", $GLOBALS["Library"]->MakeList($addresses_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));

$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, isset($filter["in"]["conditions"]["status"]["multiple"][0])?$filter["in"]["conditions"]["status"]["multiple"][0]:""));
$Page->Add("FILTER_TYPES", $GLOBALS["Library"]->ShowSelect($types, isset($filter["in"]["conditions"]["type"]["multiple"][0])?$filter["in"]["conditions"]["type"]["multiple"][0]:""));

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $addresses_count["count"]);

?>