<?php

$filter = $_SESSION["Filter"]->GetFilter(array("status"=>array("multiple"=>array("active", "pending"))), "add_date", "desc");
if ($client_id_tmp = $_SESSION["Filter"]->GetCondition("client_id", true)) $filter = $_SESSION["Filter"]->SetCondition("client_id", (int)$client_id_tmp, true);

$clients = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `clients` WHERE `status` NOT IN('deleted') AND ".$filter["where"]);

$clients_html=array();
$clients_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`client_id`) AS `count` FROM `clients` WHERE `status` NOT IN('deleted') AND ".$filter["conditions"]);

$statuses = $GLOBALS["Library"]->GetOptions("clients");
$roles = $GLOBALS["Library"]->GetOptions("client_roles");

$Clients_tpl = new CTemplate(__TEMPLATES_DIR__."/clients/list-row.tpl");
$Clients_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

foreach($clients as $client_id=>$client) {

	$clients[$client_id]["CLIENT_ID_TXT"] = $GLOBALS["Format"]->Id($client["client_id"]);
	$clients[$client_id]["DATE_TXT"] = $GLOBALS["Format"]->Date($client["add_date"], __FORMAT_DATE_FULL__);
	$clients[$client_id]["ROLE_TXT"] = $roles[$client["role"]];
	$clients[$client_id]["STATUS_TXT"] = $statuses[$client["status"]];

	$clients_html[$client_id]["IsBlocked"] = ($client["status"]=="disabled");

}

$Page->Add("CLIENTS", $Clients_tpl->RepeatHtml($clients, $clients_html));

$Page->Add("LIST", $GLOBALS["Library"]->MakeList($clients_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));

$Page->Add("CLIENT_ID", $GLOBALS["Format"]->Id($_SESSION["Filter"]->GetCondition("client_id", true)));
$Page->Add("ROLES", $GLOBALS["Library"]->ShowSelect($roles, $_SESSION["Filter"]->GetCondition("role", true)));
$Page->Add("FROM_DATE", isset($filter["in"]["conditions"]["dates"]["date"][0])?$GLOBALS["Format"]->Date($filter["in"]["conditions"]["dates"]["date"][0]):"");
$Page->Add("TO_DATE", isset($filter["in"]["conditions"]["dates"]["date"][1])?$GLOBALS["Format"]->Date($filter["in"]["conditions"]["dates"]["date"][1]):"");
$Page->Add("FIRSTNAME", $_SESSION["Filter"]->GetCondition("firstname"));
$Page->Add("LASTNAME", $_SESSION["Filter"]->GetCondition("lastname"));
$Page->Add("EMAIL", $_SESSION["Filter"]->GetCondition("email"));

$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, $_SESSION["Filter"]->GetCondition("status", true)));

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $clients_count["count"]);

?>