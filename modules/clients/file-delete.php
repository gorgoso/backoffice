<?php

$file_id = $GLOBALS["Library"]->Reg("id");
if (!$client_id = $GLOBALS["Sql"]->Value("fil2cli", "client_id", "file_id", $file_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

if ($result = $GLOBALS["Common"]->DeleteFile($file_id)) {
	
	$GLOBALS["Error"]->AddSessionError("FILE_DELETED", "notice");
	
} else $GLOBALS["Error"]->AddSessionError("FILE_NOT_DELETED", "error");

$GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/files/".$client_id."/");

?>