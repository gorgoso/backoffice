<?php

$client_id = $GLOBALS["Library"]->Reg("id");
$add = $GLOBALS["Library"]->Reg("add");

if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

$statuses = $GLOBALS["Library"]->GetStatuses("jobs");
$countries = $GLOBALS["Data"]->countries;

if (is_array($add)) {

	$add = $GLOBALS["Format"]->TextArray($add, array("company", "salary", "position", "address1", "address2", "address3", "city", "zip", "state"));

	$year = date("Y");
	$start_date = $GLOBALS["Library"]->GetTimestamp($add["start_date"]);
	$add["salary"] = $GLOBALS["Format"]->Number($add["salary"],2,".","");
	
	$GLOBALS["Sql"]->Validate(array("company"=>$add["company"]));
	if ($GLOBALS["Sql"]->Validate(array("date"=>$add["start_date"]), array(), "BAD_START_DATE")) {
		
		$date_tmp = explode("-", $add["start_date"]);

		if (($date_tmp[0] < $year-100) || $date_tmp[0] > $year) $GLOBALS["Error"]->AddError("BAD_START_DATE");		
		if (!checkdate($date_tmp[1], $date_tmp[2], $date_tmp[0])) $GLOBALS["Error"]->AddError("BAD_START_DATE");
		
	}

	if ($add["salary"]<=0 || $add["salary"]>99999999.99) $GLOBALS["Error"]->AddError("BAD_SALARY");
	$GLOBALS["Sql"]->Validate(array("amount"=>$add["salary"]));
	$GLOBALS["Sql"]->Validate(array("position"=>$add["position"]));

	$GLOBALS["Sql"]->Validate(array("address"=>$add["address1"]));
	if ($add["address2"]) $GLOBALS["Sql"]->Validate(array("address"=>$add["address2"]));
	if ($add["address3"]) $GLOBALS["Sql"]->Validate(array("address"=>$add["address3"]));
	$GLOBALS["Sql"]->Validate(array("city"=>$add["city"]));
	if ($add["zip"]) $GLOBALS["Sql"]->Validate(array("zip"=>$add["zip"]));
	$GLOBALS["Sql"]->Validate(array("state"=>$add["state"]));
	if (!array_key_exists($add["country"], $countries)) $GLOBALS["Error"]->AddError("BAD_COUNTRY");
	
	if (!$GLOBALS["Error"]->ErrorExists()) {

		$GLOBALS["Sql"]->StartTransaction();
	
		if ($GLOBALS["Client"]->AddJob($client_id, $add["company"], $start_date, $add["position"], $add["salary"], $add["address1"], $add["address2"], $add["address3"], $add["city"], $add["zip"], $add["state"], $add["country"])) {
	
			foreach($client["jobs"] as $job) {
				
				$GLOBALS["Client"]->DeleteJob($job["job_id"]);
				
			}

			$GLOBALS["Sql"]->Commit();
			
			$GLOBALS["Error"]->AddSessionError("JOB_ADDED_SUCCESSFULY", "notice");
			$add = false;
		
		} else {
			
			$GLOBALS["Error"]->AddError("JOB_NOT_ADDED", "error");
			$GLOBALS["Sql"]->Rollback();

		}
	}
}

$filter = $_SESSION["Filter"]->GetFilter(array("status"=>array("multiple"=>array("active"))), "add_date", "desc");

$sqldata = array("client_id"=>$client_id);
$jobs = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `jobs` J LEFT JOIN `job2cli` J2C USING(`job_id`) LEFT JOIN `addresses` A USING(`address_id`) WHERE J2C.`client_id`=:client_id AND ".$filter["where"], $sqldata);
$jobs_html=array();
$jobs_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`job_id`) AS `count` FROM `jobs` J LEFT JOIN `job2cli` J2C USING(`job_id`) LEFT JOIN `addresses` A USING(`address_id`) WHERE J2C.`client_id`=:client_id AND ".$filter["conditions"], $sqldata);

$Jobs_tpl = new CTemplate(__TEMPLATES_DIR__."/clients/jobs-row.tpl");

$Jobs_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

foreach($jobs as $job_id=>$job) {

	$jobs[$job_id]["START_DATE_TXT"] = $job["start_date"] ? $GLOBALS["Format"]->Date($job["start_date"]) : "-";
	
	$jobs[$job_id]["ADD_DATE_TXT"] = $GLOBALS["Format"]->Date($job["add_date"], __FORMAT_DATE_FULL__);
	$jobs[$job_id]["DEL_DATE_TXT"] = $job["del_date"] ? $GLOBALS["Format"]->Date($job["del_date"], __FORMAT_DATE_FULL__) : "-";
	$jobs[$job_id]["STATUS_TXT"] = $statuses[$job["status"]];
	$jobs[$job_id]["COUNTRY_TXT"] = $countries[$job["country"]];
	$jobs[$job_id]["SALARY_TXT"] = $GLOBALS["Format"]->Number($job["salary"]);
	
	$jobs_html[$job_id]["IsBlocked"] = ($job["status"]=="deleted");
	$jobs_html[$job_id]["IsAddress2"] = ($job["address2"]);
	$jobs_html[$job_id]["IsAddress3"] = ($job["address3"]);
	$jobs_html[$job_id]["IsZip"] = ($job["zip"]);
	$jobs_html[$job_id]["IsState"] = ($job["state"]);

}

$Page->Add("CLIENT_ID", $client_id);
$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($client_id));
$Page->Add("FIRSTNAME", $client["client"]["firstname"]);
$Page->Add("LASTNAME", $client["client"]["lastname"]);

$Page->Add("COMPANY", $add["company"]);
$Page->Add("START_DATE", $add["start_date"]);
$Page->Add("POSITION", $add["position"]);
$Page->Add("SALARY", $add["salary"] ? $add["salary"] : "0.00");

$Page->Add("ADDRESS1", $add["address1"]);
$Page->Add("ADDRESS2", $add["address2"]);
$Page->Add("ADDRESS3", $add["address3"]);
$Page->Add("CITY", $add["city"]);
$Page->Add("ZIP", $add["zip"]);
$Page->Add("STATE", $add["state"]);
$Page->Add("COUNTRIES", $GLOBALS["Library"]->ShowSelect($countries, (isset($add["country"])) ? $add["country"] : $client["client"]["nationality"]));

$Page->Add("JOBS", $Jobs_tpl->RepeatHtml($jobs, $jobs_html));

$Page->Add("LIST", $GLOBALS["Library"]->MakeList($jobs_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));

$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, isset($filter["in"]["conditions"]["status"]["multiple"][0])?$filter["in"]["conditions"]["status"]["multiple"][0]:""));
$Page->Add("COMPANY_FILTER", isset($filter["in"]["conditions"]["company"])?$filter["in"]["conditions"]["company"]:"");

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $jobs_count["count"]);

$Page->Add("LANGUAGE", __LANGUAGE__);

?>