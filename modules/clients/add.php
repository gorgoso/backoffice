<?php

$add = $GLOBALS["Library"]->Reg("add");

$provinces = $GLOBALS["Library"]->GetStatuses("provinces");
$calling_codes = $GLOBALS["Library"]->GetStatuses("calling_codes");

if (is_array($add)) {
	
	$add = $GLOBALS["Format"]->TextArray($add, array("username", "password", "name", "email", "phone"));
	
	if ($GLOBALS["Sql"]->Value("clients", "client_id", "username", $add["username"])) $GLOBALS["Error"]->AddSessionError("CLIENT_USERNAME_EXISTS");
	if ($GLOBALS["Sql"]->Value("clients", "client_id", "email", $add["email"])) $GLOBALS["Error"]->AddSessionError("CLIENT_EMAIL_EXISTS");
	if ($GLOBALS["Sql"]->Value("clients", "client_id", "phone", $add["phone"])) $GLOBALS["Error"]->AddSessionError("CLIENT_PHONE_EXISTS");

	$GLOBALS["Sql"]->Validate(array("username"=>$add["username"]));
	$GLOBALS["Sql"]->Validate(array("password"=>$add["password"]));
	$GLOBALS["Sql"]->Validate(array("name"=>$add["name"]));
	$GLOBALS["Sql"]->Validate(array("email"=>$add["email"]));
	$GLOBALS["Sql"]->Validate(array("phone"=>$add["phone"]));
	
	if (!array_key_exists($add["province"], $provinces)) $GLOBALS["Error"]->AddSessionError("BAD_PROVINCE");

	if (!$GLOBALS["Error"]->ErrorExists()) {
		
		$save = array(
			"username"=>$add["username"],
			"password"=>$add["password"],
			"name"=>$add["name"],
			"email"=>$add["email"],
			"phone"=>$add["phone"],
			"province"=>$add["province"]
		);
					
		if ($client_id = $GLOBALS["Client"]->Add($save)) {

			$GLOBALS["Error"]->AddSessionError("CLIENT_ADDED_SUCCESSFULY", "notice");
				
			$GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/detail/${client_id}/");
		
		} else $GLOBALS["Error"]->AddError("CLIENT_NOT_ADDED", "error");

	}

}

$Page->Add("USERNAME", $add["username"]);
$Page->Add("PASSWORD", $add["password"] ? $add["password"] : $GLOBALS["Library"]->GenPass());
$Page->Add("NAME", $add["name"]);
$Page->Add("EMAIL", $add["email"]);
$Page->Add("PHONE", $add["phone"]);
$Page->Add("PROVINCES", $GLOBALS["Library"]->ShowSelect($provinces, isset($add["province"])?$add["province"]:""));
$Page->Add("CALLING_CODES", $GLOBALS["Library"]->ShowSelect($calling_codes, isset($add["code"])?$add["code"]:""));

?>