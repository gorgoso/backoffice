<?php

$document_id = $GLOBALS["Library"]->Reg("id");
if (!$client_id = $GLOBALS["Sql"]->Value("doc2cli", "client_id", "document_id", $document_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

if ($result = $GLOBALS["Client"]->DeleteDocument($document_id)) {
	
	$GLOBALS["Error"]->AddSessionError("DOCUMENT_DELETED", "notice");
	
} else $GLOBALS["Error"]->AddSessionError("DOCUMENT_NOT_DELETED", "error");

$GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/documents/".$client_id."/");

?>