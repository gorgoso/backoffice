<?php

$client_id = $GLOBALS["Library"]->Register("id");
if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Redirect(__PATH_ADMIN__."clients/list/");

/* CLIENT GENERAL DETAILS */
$statuses = $GLOBALS["Library"]->GetOptions("clients");
$roles = $GLOBALS["Library"]->GetOptions("client_roles");
$titles = $GLOBALS["Library"]->GetOptions("client_titles");
//die($statuses[$client["client"]["status"]]);

//die($GLOBALS["Library"]->ShowSelect($titles,$client["client"]["title"] ? $titles[$client["client"]["title"]] : ""));
$Page->Add("CLIENT_ID", $client_id);
$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($client_id));
$Page->Add("CLIENT_NAME", ($client["client"]["title"] ? $titles[$client["client"]["title"]] : "")." ".$client["client"]["firstname"]." ".$client["client"]["lastname"]);
$Page->Add("ADD_DATE_TXT", $GLOBALS["Format"]->Date($client["client"]["add_date"], __FORMAT_DATE_FULL__));
$Page->Add("LOG_DATE_TXT", $GLOBALS["Format"]->Date($client["client"]["log_date"], __FORMAT_DATE_FULL__));
$Page->Add("TYPE_TXT", $roles[$client["client"]["role"]]);
$Page->Add("CLIENT_STATUS_TXT", $statuses[$client["client"]["status"]]);
$Page->AddHTML("IsPending", $client["client"]["status"]=="pending");
$Page->AddHTML("IsNotPending", $client["client"]["status"]!="pending");
$Page->Add("TITLES", $GLOBALS["Library"]->ShowSelect($titles,$client["client"]["title"]));
$Page->Add("ROLES", $GLOBALS["Library"]->ShowSelect($roles,$client["client"]["role"]));
$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses,$client["client"]["status"]));
$Page->Add("CLIENT_FIRST_NAME", $client["client"]["firstname"]);
$Page->Add("CLIENT_LAST_NAME", $client["client"]["lastname"]);
/* CLIENT CONTACTS */
$contact_types = $GLOBALS["Library"]->GetOptions("clients_contacts_types");

$Page->AddHTML("IsContacts", $client["contact"]);
$Page->AddHTML("IsNotContacts", !$client["contact"]);

$contacts_tmp = array();
$contacts_html = array();
$count = count($client["contact"]);
$c = 0;

foreach($contact_types as $type=>$type_txt) {

	foreach($client["contact"] as $contact) {

		if ($type == $contact["type"]) {

			$contacts_tmp[$type]["TYPE_TXT"]=$type_txt;
			$contacts_tmp[$type]["CLIENT_ID"]=$client_id;
			$contacts_tmp[$type]["CONTACT_ID"]=$contact["contact_id"];
			$contacts_tmp[$type]["CONTACT"]=$contact["contact"];
			$contacts_tmp[$type]["CONTACT_TXT"]=($contact["type"]!="email" ? "+".preg_replace("/([0-9]{3})([0-9]{3})([0-9]{3})/", "$1-$2-$3", $contact["contact"]) : $contact["contact"]);
			$contacts_tmp[$type]["PATH_ADMIN"]=__PATH_ADMIN__;
			$contacts_tmp[$type]["PATH_ADMIN"]=__PATH_ADMIN__;
			
			
			$contacts_html[$type]["IsEmail"] = ($contact["type"]=="email");
			$contacts_html[$type]["IsPhone"] = ($contact["type"]!="email");

			$c++;

			continue;

		}

	}

	$contacts_html[$type]["IsLast"] = ($count == $c);

}

$Contacts_tpl = new CTemplate(__TEMPLATES_DIR__."/clients/detail-contacts-row.tpl");
$Page->Add("CONTACTS", $Contacts_tpl->RepeatHtml($contacts_tmp, $contacts_html));

/* CLIENT INFORMATIONS */
$countries = $GLOBALS["Library"]->GetOptions("countries");
$clients_informations_types = $GLOBALS["Library"]->GetOptions("clients_informations_types");
$clients_informations_travelling = $GLOBALS["Library"]->GetOptions("clients_informations_travelling");

$Page->AddHTML("IsInformations", $client["information"]);
$Page->AddHTML("IsNotInformations", !$client["information"]);

$informations = $client["information"];

$c = 0;
$count = count($informations);
$informations_html = array();
$issuedCounry='';
foreach($informations as $information_id=>$information) {

	switch ($information["type"]) {
		case "passport_country":
			$information_tmp = $countries[$information["information"]];
			$issuedCounry = $information["information"];
		break;
		case "travelling":
			$information_tmp = $clients_informations_travelling[$information["information"]];
		break;
		default:
			$information_tmp = $information["information"];
		break;
	}

	$informations[$information_id]["TYPE_TXT"] = $clients_informations_types[$information["type"]];
	$informations[$information_id]["INFORMATION_ID"] = $information["information_id"];
	$informations[$information_id]["INFORMATION"] = $information_tmp;
	$informations_html[$information_id]["IsPassportNumber"] = ($information["type"]=="passport_number");
	$informations_html[$information_id]["IsPassportCountry"] = ($information["type"]=="passport_country");
	$informations_html[$information_id]["IsPassportExpired"] = ($information["type"]=="passport_expired");
	$informations_html[$information_id]["IsPassportIssued"] = ($information["type"]=="passport_issued");	
	$informations_html[$information_id]["IsLast"] = ($count == ++$c);

}

$Informations_tpl = new CTemplate(__TEMPLATES_DIR__."/clients/detail-information-row.tpl");
$Page->Add("INFORMATIONS", $Informations_tpl->RepeatHtml($informations, $informations_html));
$Page->Add("ISSUED_PASSPORT_COUNTRY", $GLOBALS["Library"]->ShowSelect($countries,$issuedCounry));

/* CLIENT ADDRESSES */
$Page->AddHTML("IsAddress", $client["address"]);
$Page->AddHTML("IsNotAddress", !$client["address"]);

if ($client["address"]) {

	
	

	$states =  $client["address"]["country"] =='us' ?$GLOBALS["Library"]->GetOptions("states_".$client["address"]["country"]): $GLOBALS["Library"]->GetOptions("provinces"); 
	
	
	$Page->Add("ADDRESS_ID", $client["address"]["address_id"]);
	$Page->Add("ADDRESS1", $client["address"]["address1"]);
	$Page->Add("ADDRESS2", $client["address"]["address2"]);
	$Page->Add("ADDRESS3", $client["address"]["address3"]);
	$Page->Add("ADDRESS_CITY", $client["address"]["city"]);
	$Page->Add("ADDRESS_STATE", $client["address"]["state"] ? $GLOBALS["Format"]->Uppercase($client["address"]["state"]) : "");
	$Page->Add("ADDRESS_STATE_TXT", $client["address"]["state"] ? $states[$client["address"]["state"]] : "");
	$Page->Add("ADDRESS_STATE_LIST", $GLOBALS["Library"]->ShowSelect($states,$client["address"]["state"]));
	$Page->Add("ADDRESS_ZIP", $client["address"]["zip"]);
	$Page->Add("SELECTED_COUNTRY", $client["address"]["country"]);
	$Page->Add("ADDRESS_COUNTRY", $GLOBALS["Library"]->ShowSelect($countries,$client["address"]["country"]));
	$Page->Add("ADDRESS_COUNTRY_TXT", $client["address"]["country"] ? $countries[$client["address"]["country"]] : "");
	$Page->Add("STATES_US", $GLOBALS["Library"]->ShowSelect($statuses,$client["client"]["status"]));
	$Page->AddHTML("IsAddress2", $client["address"]["address2"]);
	$Page->AddHTML("IsAddress3", $client["address"]["address3"]);
	$Page->AddHTML("IsZip", $client["address"]["zip"]);


}

/* CLIENT ORGANIZATION INFORMATIONS */
$organization_types = $GLOBALS["Library"]->GetOptions("organization_types");
$organization_denominations = $GLOBALS["Library"]->GetOptions("organization_denominations");


$organization_tmp = $client["organization"];
$Page->Add("ORGANIZATION_NAME", $organization_tmp["name"]);
$Page->Add("ORGANIZATION_TYPE_TXT", $organization_tmp["type"] ? $organization_types[$organization_tmp["type"]] : "");
$Page->Add("ORGANIZATION_TYPE_LIST", $GLOBALS["Library"]->ShowSelect($organization_types,$organization_tmp["type"]));
$Page->Add("DENOMINATION_TXT", $organization_tmp["denomination"] ? $organization_denominations[$organization_tmp["denomination"]] : "");
$Page->Add("DENOMINATION_TYPE_LIST", $GLOBALS["Library"]->ShowSelect($organization_denominations,$organization_tmp["denomination"]));
$Page->Add("ORGANIZATION_PHONE_TXT", $GLOBALS["Format"]->Phone($organization_tmp["phone"]));
$Page->Add("ORGANIZATION_EMAIL_TXT", $organization_tmp["email"]);
$Page->Add("ORGANIZATION_WWW_TXT", $organization_tmp["www"]);
$Page->Add("MEMBERS_TXT", $organization_tmp["members"] ? $organization_tmp["members"] : "-");
$Page->Add("SUPPORTERS_TXT", $organization_tmp["supporters"] ? $organization_tmp["supporters"] : "-");
$Page->Add("PARTICIPANTS_TXT", $organization_tmp["participants"] ? $organization_tmp["participants"] : "-");

$states = $GLOBALS["Library"]->GetOptions("states_".$organization_tmp["country"]);

$Page->Add("ORGANIZATION_ADDRESS1", $organization_tmp["address1"]);
$Page->Add("ORGANIZATION_ADDRESS2", $organization_tmp["address2"]);
$Page->Add("ORGANIZATION_ADDRESS3", $organization_tmp["address3"]);
$Page->Add("ORGANIZATION_CITY", $organization_tmp["city"]);
$Page->Add("ORGANIZATION_STATE", $organization_tmp["state"] ? $GLOBALS["Format"]->Uppercase($organization_tmp["state"]) : "");
$Page->Add("ORGANIZATION_STATE_TXT", $organization_tmp["state"] ? $states[$organization_tmp["state"]] : "");
$Page->Add("ORGANIZATION_ZIP", $organization_tmp["zip"]);
$Page->Add("ORGANIZATION_COUNTRY_TXT", $organization_tmp["country"] ? $countries[$organization_tmp["country"]] : "");
$Page->Add("SELECTED_COUNTRY_ORGANIZATION", $organization_tmp["country"]);

$Page->AddHTML("IsAddressOrganization2", $organization_tmp["address2"]);
$Page->AddHTML("IsAddressOrganization3", $organization_tmp["address3"]);
$Page->AddHTML("IsZipOrganization", $organization_tmp["zip"]);

$Page->AddHTML("IsOrganization", $client["organization"]);
$Page->AddHTML("IsNotOrganization", !$client["organization"]);

/* TOUR DETAILS */
$departures_tmp = $GLOBALS["Tour"]->GetDepartures();
$departures = array("do"=>array(), "us"=>array());
$departures_all = array();
foreach($departures_tmp as $departure_id=>$departure) {

	$departure_city = $departure["city"].", ".$GLOBALS["Format"]->Uppercase($departure["state"]);

	$departures[$departure["country"]][$departure_id] = $departure_city;
	$departures_all[$departure_id] = $departure_city;

}

$statuses = $GLOBALS["Library"]->GetOptions("tour_statuses");
$tour = $GLOBALS["Tour"]->Get($client["client"]["tour_id"]);

$Page->Add("DATE_TXT", $GLOBALS["Format"]->Date($tour["tour"]["add_date"], __FORMAT_DATE_FULL__));
$Page->Add("USER_TXT", "-"); //$client["client"]["user_txt"]);
$Page->Add("CODE", $tour["tour"]["code"]);
$Page->Add("DEPARTURE_DATE_TXT", $GLOBALS["Format"]->Date($tour["tour"]["departure_date"]));
$Page->Add("DEPARTURE_CITY_TXT", $tour["tour"]["departure_id"] ? $departures_all[$tour["tour"]["departure_id"]] : "");
$Page->Add("PRICE_TXT", $GLOBALS["Format"]->Number($tour["tour"]["price"]));
$Page->Add("STATUS_TXT", $statuses[$tour["tour"]["status"]]);
$Page->AddHTML("IsDisabled", $tour["tour"]["status"]=="deleted");

/* CLIENT PAYMENTS */
$payments_sql = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `clients-payments` WHERE `client_id`=:client_id ORDER BY `add_date` DESC", array("client_id"=>$client_id), "payment_id");
$statuses = $GLOBALS["Library"]->GetOptions("payment_statuses");
$types = $GLOBALS["Library"]->GetOptions("payment_types");
$methods = $GLOBALS["Library"]->GetOptions("payment_methods");

$payments = array();
$payments_html = array();
$count = count($payments_sql);
$c = 0;
$total_payments = 0;

foreach($payments_sql as $payment_id=>$payment) {

	$payments[$payment_id]["PAYMENT_ID"] = $payment_id;
	$payments[$payment_id]["PAYMENT_ID_TXT"] = $GLOBALS["Format"]->Id($payment_id);
	$payments[$payment_id]["ADD_DATE_TXT"] = $GLOBALS["Format"]->Date($payment["add_date"]);
	$payments[$payment_id]["PAY_DATE_TXT"] = $GLOBALS["Format"]->Date($payment["pay_date"]);
	$payments[$payment_id]["TYPE_TXT"] = $types[$payment["type"]];
	$payments[$payment_id]["METHOD_TXT"] = $payment["method"] ? $methods[$payment["method"]] : "-";
	$payments[$payment_id]["AMOUNT_TXT"] = $GLOBALS["Format"]->Number($payment["amount"]);
	$payments[$payment_id]["STATUS_TXT"] = $statuses[$payment["status"]];

	$payments_html[$payment_id]["IsBlocked"] = ($payment["status"]=="canceled");
	$payments_html[$payment_id]["IsLast"] = ($count == ++$c);

	$total_payments+=$payment["amount"];

}

$Payments_tpl = new CTemplate(__TEMPLATES_DIR__."/clients/detail-payment-row.tpl");
$Payments_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);
$Page->Add("PAYMENTS", $Payments_tpl->RepeatHtml($payments, $payments_html));
$Page->Add("TOTAL_PAYMENT_AMOUNT_TXT", $GLOBALS["Format"]->Number($total_payments));

$Page->AddHTML("IsNotPayed", $tour["tour"]["status"]=="deleted");

?>