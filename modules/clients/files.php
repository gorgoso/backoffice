<?php

$client_id = $GLOBALS["Library"]->Reg("id");
$loan_id = $GLOBALS["Library"]->Reg("loan");
$add = $GLOBALS["Library"]->Reg("add");
$file = isset($_FILES["update"]) ? $_FILES["update"] : false;

if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");
if ($loan_id) if (!array_key_exists($loan_id, $client["loans"])) $GLOBALS["Library"]->Go(__PATH_ADMIN__."loans/list/");

$statuses = $GLOBALS["Library"]->GetStatuses("files");
$groups = $GLOBALS["Library"]->GetStatuses("file_groups");

$loans = array();
foreach($client["loans"] as $loan_id_tmp=>$loan) $loans[$loan_id_tmp] = $GLOBALS["Format"]->Id($loan_id_tmp). " (".$GLOBALS["Format"]->Date($loan["evaluation_date"]).")";

if (is_array($add)) {

	$add = $GLOBALS["Format"]->TextArray($add, array("note"));

	if (!array_key_exists($add["group"], $groups)) $GLOBALS["Error"]->AddError("BAD_GROUP");
	if ($add["loan_id"]) if (!array_key_exists($add["loan_id"], $loans)) $GLOBALS["Error"]->AddError("BAD_LOAN");
	if ($add["note"]) $GLOBALS["Sql"]->Validate(array("notice"=>$add["note"]), array(), "BAD_NOTE");
	if (is_array($file)) {
		
		if ($file["error"]>0) $GLOBALS["Error"]->AddError("FILE_NOT_UPLOADED");
		if ($file["size"]<=0 || $file["size"]>8000000) $GLOBALS["Error"]->AddError("FILE_TOO_BIG");		
		
	} else $GLOBALS["Error"]->AddError("FILE_NOT_UPLOADED");

	if (!$GLOBALS["Error"]->ErrorExists()) {

		if (preg_match("/\.jpg|jpeg|png|gif$/i", $file["name"])) {
			
			$data = $GLOBALS["Common"]->ResizeImage($file["tmp_name"], ($add["group"]=="photos" ? 150 : 2000));
			
		} else $data = file_get_contents($file["tmp_name"]);
		
		unlink($file["tmp_name"]);

		if ($file_id = $GLOBALS["Common"]->AddFile($file["name"], $file["type"], strlen($data), $data, $add["note"])) {
					
			$save = array(
				"client_id"=>$client_id,
				"file_id"=>$file_id,
				"loan_id"=>$add["loan_id"],
				"add_date"=>time(),
				"group"=>$add["group"],
				"status"=>"active"
			);

			$GLOBALS["Sql"]->Insert("fil2cli", $save);

			$GLOBALS["Common"]->AddHistory("File added successfuly. (File ID: $file_id, group: ".$add["group"].", note: ".$add["note"].")", "files", $client_id);
	
			$GLOBALS["Error"]->AddSessionError("FILE_ADDED_SUCCESSFULY", "notice");
			$add = array();
		
		} else $GLOBALS["Error"]->AddError("FILE_NOT_ADDED", "error");

	}

}

$filter = $_SESSION["Filter"]->GetFilter(array("status"=>array("multiple"=>array("active"))), "add_date", "desc");
if ($loan_id) $filter = $_SESSION["Filter"]->SetCondition("loan_id", (int)$loan_id, true);

$files_html=array();

$sqldata = array("client_id"=>$client_id);
$files = $GLOBALS["Sql"]->SelectArray("SELECT F2C.*, F.`note` FROM `files` F LEFT JOIN `fil2cli` F2C USING(`file_id`) WHERE F2C.`client_id`=:client_id AND ".$filter["where"], $sqldata, "file_id");
$files_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`file_id`) AS `count` FROM `files` F LEFT JOIN `fil2cli` F2C USING(`file_id`) WHERE F2C.`client_id`=:client_id AND ".$filter["conditions"], $sqldata);

$Files_tpl = new CTemplate(__TEMPLATES_DIR__."/clients/files-row.tpl");
$Files_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

foreach($files as $file_id=>$file) {
		
	$files[$file_id]["LOAN_ID_TXT"] = $GLOBALS["Format"]->Id($file["loan_id"]);
	$files[$file_id]["FILE_HASH"] = $GLOBALS["Common"]->FileHash($file_id);
	$files[$file_id]["GROUP_TXT"] = $groups[$file["group"]];
	$files[$file_id]["NOTE"] = $file["note"] ? $file["note"] : "&nbsp;";
	$files[$file_id]["ADD_DATE_TXT"] = $GLOBALS["Format"]->Date($file["add_date"], __FORMAT_DATE_FULL__);
	$files[$file_id]["DEL_DATE_TXT"] = $file["del_date"] ? $GLOBALS["Format"]->Date($file["del_date"], __FORMAT_DATE_FULL__) : "-";
	$files[$file_id]["STATUS_TXT"] = $statuses[$file["status"]];
	
	$files_html[$file_id]["IsBlocked"] = ($file["status"]=="deleted");
	$files_html[$file_id]["IsNotDeleted"] = ($file["status"]!="deleted");

}

$Page->Add("CLIENT_ID", $client_id);
$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($client_id));
$Page->Add("FIRSTNAME", $client["client"]["firstname"]);
$Page->Add("LASTNAME", $client["client"]["lastname"]);

if ($loan_id) {
	
	$Page->Add("LOAN_ID", $loan_id);
	$Page->Add("LOAN_ID_TXT", $GLOBALS["Format"]->Id($loan_id));
	$Page->Add("LOAN_DATE_TXT", $GLOBALS["Format"]->Date($client["loans"][$loan_id]["evaluation_date"]));
	
}

$Page->Add("LOANS", $GLOBALS["Library"]->ShowSelect($loans, isset($add["loan_id"])?$add["loan_id"]:""));
$Page->Add("GROUPS", $GLOBALS["Library"]->ShowSelect($groups, isset($add["group"])?$add["group"]:""));
$Page->Add("NOTE", isset($add["note"])?$add["note"]:"");

$Page->Add("FILES", $Files_tpl->RepeatHtml($files, $files_html));

$Page->Add("LIST", $GLOBALS["Library"]->MakeList($files_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));

$Page->Add("GROUPS_FILTER", $GLOBALS["Library"]->ShowSelect($groups, $_SESSION["Filter"]->GetCondition("group", true)));
$Page->Add("NOTE_FILTER",$_SESSION["Filter"]->GetCondition("note"));
$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, $_SESSION["Filter"]->GetCondition("status", true)));
$Page->Add("LOANS_FILTER", $GLOBALS["Library"]->ShowSelect($loans, $_SESSION["Filter"]->GetCondition("loan_id", true)));

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $files_count["count"]);

$Page->AddHTML("IsLoanID", $loan_id);
$Page->AddHTML("IsNotLoanID", !$loan_id);

?>