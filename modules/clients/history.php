<?php

$client_id = $GLOBALS["Library"]->Reg("id");
$add = $GLOBALS["Library"]->Reg("add");

if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

$users = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `users` WHERE `status` NOT IN ('deleted') ORDER BY `username`", array(), "user_id");
foreach($users as $user_id=>$user) $users_tmp[$user_id] = $user["username"];

$groups = $GLOBALS["Library"]->GetStatuses("history_groups");

if (is_array($add)) {

	$add = $GLOBALS["Format"]->TextArray($add, array("notice"));

	$GLOBALS["Sql"]->Validate(array("description"=>$add["notice"]), array(), "BAD_NOTICE");

	if (!$GLOBALS["Error"]->ErrorExists()) {
		
		if ($GLOBALS["Common"]->AddHistory($add["notice"], "users", $client_id)) {
	
			$GLOBALS["Error"]->AddSessionError("NOTICE_ADDED_SUCCESSFULY", "notice");
			$add = array();
		
		} else $GLOBALS["Error"]->AddError("NOTICE_NOT_ADDED", "error");

	}

}

$filter = $_SESSION["Filter"]->GetFilter(array(), "date", "desc");

$sqldata = array("client_id"=>$client_id);
$history = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `history` WHERE `client_id`=:client_id AND ".$filter["where"], $sqldata);

$history_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`history_id`) AS `count` FROM `history` WHERE `client_id`=:client_id AND ".$filter["conditions"], $sqldata);

$History_tpl = new CTemplate(__TEMPLATES_DIR__."/clients/history-row.tpl");

foreach($history as $history_id=>$h) {
		
	$history[$history_id]["DATE_TXT"] = $GLOBALS["Format"]->Date($h["date"], __FORMAT_DATE_FULL__);
	$history[$history_id]["GROUP_TXT"] = $h["group"] ? $groups[$h["group"]] : "";
	$history[$history_id]["USERNAME_TXT"] = $users_tmp[$h["user_id"]];
	
}

$Page->Add("CLIENT_ID", $client_id);
$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($client_id));
$Page->Add("FIRSTNAME", $client["client"]["firstname"]);
$Page->Add("LASTNAME", $client["client"]["lastname"]);
$Page->Add("NOTICE", isset($add["notice"])?$add["notice"]:"");

$Page->Add("FROM_DATE", isset($filter["in"]["conditions"]["dates"]["date"][0])?$GLOBALS["Format"]->Date($filter["in"]["conditions"]["dates"]["date"][0]):"");
$Page->Add("TO_DATE", isset($filter["in"]["conditions"]["dates"]["date"][1])?$GLOBALS["Format"]->Date($filter["in"]["conditions"]["dates"]["date"][1]):"");
$Page->Add("GROUPS", $GLOBALS["Library"]->ShowSelect($groups, $_SESSION["Filter"]->GetCondition("group", true)));
$Page->Add("USERS", $GLOBALS["Library"]->ShowSelect($users_tmp, $_SESSION["Filter"]->GetCondition("user_id", true)));
$Page->Add("USER_ID", $_SESSION["User"]->user_id);
$Page->Add("USERNAME", $_SESSION["User"]->username);

$Page->Add("HISTORY", $History_tpl->RepeatHtml($history));

$Page->Add("LIST", $GLOBALS["Library"]->MakeList($history_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"], $client_id."/"));

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $history_count["count"]);

$Page->Add("LANGUAGE", __LANGUAGE__);

?>