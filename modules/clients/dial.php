<?php

$client_id = $GLOBALS["Library"]->Reg("id");
$phone = preg_replace("/[^0-9]/", "", $GLOBALS["Library"]->Reg("phone"));

if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

$is_cellular = false;
foreach($client["contacts"] as $contact) {
	
	if ($contact["code"].$contact["contact"] == $phone) {
		
		$is_cellular = ($contact["type"]=="cellular") ? "true" : "false";
		break;
		
	}

}

$extension = $_SESSION["User"]->extension;

$from = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : __PATH_ADMIN__."clients/detail/$client_id/";

if (!$extension) {

	$GLOBALS["Error"]->AddSessionError("NO_EXTENSION_DEFINED", "warning");
	
} else {
	
	if (preg_match("/^18[024]9[0-9]{7}$/", $phone) && $extension) {
		
		/* ASTERISK */
		/*$socket = fsockopen("192.168.2.100","5038", $errno, $errstr, 30);
		
		fputs($socket, "Action: Login\r\n");
		fputs($socket, "UserName: backoffice\r\n");
		fputs($socket, "Secret: RzB44VIH8DeB2YSuFuQrqvju\r\n\r\n");
				
		fputs($socket, "Action: Originate\r\n" );
		fputs($socket, "Channel: SIP/$extension\r\n" );
		fputs($socket, "Exten: $phone\r\n" );
		fputs($socket, "Timeout: 15000\r\n" );
		fputs($socket, "Context: from-internal\r\n" );
		fputs($socket, "Priority: 1\r\n" );
		fputs($socket, "Variable: CLIENT_ID=${client_id}\r\n" );
		fputs($socket, "Async: yes\r\n\r\n" );
				
		fputs($socket, "Action: Logoff\r\n\r\n");*/
		
		/* FREESWITCH */
		$socket = fsockopen("127.0.0.1","8021", $errno, $errstr, 5);
		socket_set_blocking($socket, false);
		
		if ($socket) {
		
			while (!feof($socket)) {
			
				$buffer =  fgets($socket, 1024);
				usleep(100);
				if (trim($buffer) == "Content-Type: auth/request") {
				
					fputs($socket, "auth ClueCon\n\n");
					break;
				
				}
			
			}
			
			$name = explode(" ", $client["client"]["firstname"]);
			$lastname = explode(" ", $client["client"]["lastname"]);
			$fullname = $name[0]." ".$lastname[0];
						
			fputs($socket, "api originate {origination_caller_id_name='${fullname}',origination_caller_id_number=${phone},client_id=${client_id}}user/${extension} ".($is_cellular ? $phone : substr($phone, 1))."\n\n");
			usleep(100);

			fclose($socket);

			$GLOBALS["Error"]->AddSessionError("CLIENTS_NUMBER_DIALED", "notice");
			
		}
	
	} else {
	
		$GLOBALS["Error"]->AddSessionError("NOT_VALID_PHONE_NUMBER", "warning");
	
	}

}

$GLOBALS["Library"]->Go($from);

?>