<?php

$client_id = $GLOBALS["Library"]->Reg("id");
$add = $GLOBALS["Library"]->Reg("add");

if (!$client = $GLOBALS["Client"]->Get($client_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

$statuses = $GLOBALS["Library"]->GetStatuses("contacts");
$types = $GLOBALS["Library"]->GetStatuses("contact_types");
$calling_codes = $GLOBALS["Library"]->GetStatuses("calling_codes");

if (is_array($add)) {

	$add = $GLOBALS["Format"]->TextArray($add, array("contact", "notice"));
	
	switch($add["type"]) {
		
		case "email":
			$add["contact"] = strtolower($add["contact"]);
			$GLOBALS["Sql"]->Validate(array("email"=>$add["contact"]));
		break;
		
		default:
			$add["contact"] = preg_replace("/[^0-9]/", "", $add["contact"]);
			$GLOBALS["Sql"]->Validate(array("phone"=>$add["contact"]));
			if (!array_key_exists($add["code"], $calling_codes)) $GLOBALS["Error"]->AddError("BAD_CALLING_CODE");
		break;
		
	}

	if (!array_key_exists($add["type"], $types)) $GLOBALS["Error"]->AddError("BAD_CONTACT_TYPE");
	if ($add["notice"]) $GLOBALS["Sql"]->Validate(array("notice"=>$add["notice"]));

	if (!$GLOBALS["Error"]->ErrorExists()) {
		
		if ($GLOBALS["Client"]->AddContact($client_id, $add["type"], $add["contact"], $add["code"], $add["notice"])) {
	
			$GLOBALS["Error"]->AddSessionError("CONTACT_ADDED_SUCCESSFULY", "notice");
			$add = array();
		
		} else $GLOBALS["Error"]->AddError("CLIENT_NOT_ADDED", "error");

	}

}

$filter = $_SESSION["Filter"]->GetFilter(array("status"=>array("multiple"=>array("active"))), "add_date", "desc");

$sqldata = array("client_id"=>$client_id);
$contacts = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `contacts` C LEFT JOIN `con2cli` C2C USING(`contact_id`) WHERE C2C.`client_id`=:client_id AND ".$filter["where"], $sqldata);
$contacts_html=array();
$contacts_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`contact_id`) AS `count` FROM `contacts` C LEFT JOIN `con2cli` C2C USING(`contact_id`) WHERE C2C.`client_id`=:client_id AND ".$filter["conditions"], $sqldata);

$Clients_tpl = new CTemplate(__TEMPLATES_DIR__."/clients/contacts-row.tpl");

$Clients_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

foreach($contacts as $contact_id=>$contact) {
	
	$tmp_contact = $contact["contact"];

	if ($contact["type"]!="email") {
		
		$tmp_contact = $GLOBALS["Format"]->Phone($tmp_contact);
				
	}
	
	$contacts[$contact_id]["ADD_DATE_TXT"] = $GLOBALS["Format"]->Date($contact["add_date"], __FORMAT_DATE_FULL__);
	$contacts[$contact_id]["DEL_DATE_TXT"] = $contact["del_date"] ? $GLOBALS["Format"]->Date($contact["del_date"], __FORMAT_DATE_FULL__) : "-";
	$contacts[$contact_id]["CONTACT_TYPE_TXT"] = $types[$contact["type"]];
	$contacts[$contact_id]["STATUS_TXT"] = $statuses[$contact["status"]];
	$contacts[$contact_id]["CONTACT"] = ($contact["type"]!="email" ? "+".$contact["code"]."-":"").$tmp_contact;
	$contacts[$contact_id]["NOTICE"] = $contact["notice"] ? addslashes(htmlspecialchars($contact["notice"])) : "";
	
	$contacts_html[$contact_id]["IsBlocked"] = ($contact["status"]=="deleted");
	$contacts_html[$contact_id]["IsEmail"] = ($contact["type"]=="email");
	$contacts_html[$contact_id]["IsPhone"] = ($contact["type"]!="email");
	$contacts_html[$contact_id]["IsNotice"] = ($contact["notice"]);
	$contacts_html[$contact_id]["IsNotDeleted"] = ($contact["status"]!="deleted");

}

$Page->Add("CLIENT_ID", $client_id);
$Page->Add("CLIENT_ID_TXT", $GLOBALS["Format"]->Id($client_id));
$Page->Add("FIRSTNAME", $client["client"]["firstname"]);
$Page->Add("LASTNAME", $client["client"]["lastname"]);
$Page->Add("CONTACT", isset($add["contact"])?$add["contact"]:"");
$Page->Add("NOTICE", isset($add["notice"])?$add["notice"]:"");

$Page->Add("TYPES", $GLOBALS["Library"]->ShowSelect($types, isset($add["type"])?$add["type"]:""));
$Page->Add("CALLING_CODES", $GLOBALS["Library"]->ShowSelect($calling_codes, isset($add["code"])?$add["code"]:""));

$Page->Add("CONTACTS", $Clients_tpl->RepeatHtml($contacts, $contacts_html));

$Page->Add("LIST", $GLOBALS["Library"]->MakeList($contacts_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));

$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, isset($filter["in"]["conditions"]["status"]["multiple"][0])?$filter["in"]["conditions"]["status"]["multiple"][0]:""));
$Page->Add("FILTER_TYPES", $GLOBALS["Library"]->ShowSelect($types, isset($filter["in"]["conditions"]["type"]["multiple"][0])?$filter["in"]["conditions"]["type"]["multiple"][0]:""));

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $contacts_count["count"]);

?>