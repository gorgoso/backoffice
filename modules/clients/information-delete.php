<?php

$information_id = $GLOBALS["Library"]->Reg("id");
if (!$client_id = $GLOBALS["Sql"]->Value("inf2cli", "client_id", "information_id", $information_id)) $GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/list/");

if ($GLOBALS["Client"]->DeleteInformation($information_id)) {
	
	$GLOBALS["Error"]->AddSessionError("INFORMATION_DELETED", "notice");
	
} else $GLOBALS["Error"]->AddSessionError("INFORMATION_NOT_DELETED", "error");

$GLOBALS["Library"]->Go(__PATH_ADMIN__."clients/information/".$client_id."/");

?>