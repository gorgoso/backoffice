<?php

if ($currency = $GLOBALS["Library"]->Reg("id")) $currency = $_SESSION["Filter"]->SetCondition("currency", $currency);
if (!$currency = $_SESSION["Filter"]->GetCondition("currency")) {
	
		$_SESSION["Filter"]->SetCondition("currency", "dop");
		$currency = "dop";
	
}

$currencies = array("dop"=>1, "eur"=>__EXCHANGE_RATE_EUR__);
$rate = $currencies[$currency];
$currency_txt = $_SESSION["Language"]->Translate(strtoupper("SHOW_CURRENCY_".$currency));

/* CLIENTS STATS */

$statuses = $GLOBALS["Library"]->GetStatuses("clients");
$clients = $GLOBALS["Sql"]->SelectArray("SELECT `status`, COUNT(`client_id`) AS `count` FROM `clients` GROUP BY `status`", array(), "status");
$clients_html = array();
$clients_count = 0;
$count = count($clients);
$c = 0;

foreach($clients as $status=>$client) $clients_count+=$client["count"];

foreach($clients as $status=>$client) {
	
	$clients[$status]["STATUS_TXT"] = $statuses[$client["status"]];
	$clients[$status]["PERCENTAGE_TXT"] = $GLOBALS["Format"]->Number(($client["count"]*100)/$clients_count);

	$clients_html[$status]["IsLast"] = ($count == ++$c);
	$clients_html[$status]["IsAmount"] = false;
	
}

$Clients_tpl = new CTemplate(__TEMPLATES_DIR__."/reports/summary-row.tpl");
$Page->Add("CLIENTS", $Clients_tpl->RepeatHtml($clients, $clients_html));
$Page->Add("CLIENTS_COUNT", $clients_count);

/* LOAN APPLICATIONS STATS */

$statuses = $GLOBALS["Library"]->GetStatuses("applications");
$applications = $GLOBALS["Sql"]->SelectArray("SELECT `status`, COUNT(`application_id`) AS `count` FROM `applications` GROUP BY `status`", array(), "status");
$applications_html = array();
$applications_count = 0;
$count = count($applications);
$c = 0;

foreach($applications as $status=>$application) $applications_count+=$application["count"];

foreach($applications as $status=>$application) {
	
	$applications[$status]["STATUS_TXT"] = $statuses[$application["status"]];
	$applications[$status]["PERCENTAGE_TXT"] = $GLOBALS["Format"]->Number(($application["count"]*100)/$applications_count);

	$applications_html[$status]["IsLast"] = ($count == ++$c);
	$applications_html[$status]["IsAmount"] = false;
	
}

$Applications_tpl = new CTemplate(__TEMPLATES_DIR__."/reports/summary-row.tpl");
$Page->Add("APPLICATIONS", $Applications_tpl->RepeatHtml($applications, $applications_html));
$Page->Add("APPLICATIONS_COUNT", $applications_count);

/* OTHER STATS */
$assessores = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`assessor_id`) AS `count` FROM `assessores` WHERE `status` IN('pending','active')");
$Page->Add("ASSESSORES_COUNT", $assessores["count"]);

$amounts = $GLOBALS["Sql"]->Fetch("SELECT SUM(`amount_principal`) AS `principal`, SUM(`amount_interest`) AS `interest` FROM `loans` WHERE `status` IN('pending', 'declined', 'canceled', 'payed', 'payed-renew')");
$Page->Add("AMOUNT_CAPITAL", $GLOBALS["Format"]->Number($amounts["principal"]/$rate));
$Page->Add("AMOUNT_INTEREST", $GLOBALS["Format"]->Number(($amounts["interest"]-$amounts["principal"])/$rate));
$Page->Add("AMOUNT_TOTAL", $GLOBALS["Format"]->Number($amounts["interest"]/$rate));

$deposits = $GLOBALS["Sql"]->Fetch("SELECT SUM(D2I.`principal`)+SUM(D2I.`interest`) AS `amount`, L.`status` FROM `dep2ins` D2I LEFT JOIN `installments` I ON (D2I.`installment_id` = I.`installment_id`) LEFT JOIN `loans` L ON(I.`loan_id`=L.`loan_id`) HAVING L.`status` NOT IN('pending', 'declined', 'canceled', 'payed', 'payed-renew')");

$Page->Add("AMOUNT_DEPOSITS", $GLOBALS["Format"]->Number($deposits["amount"]/$rate));
$Page->Add("AMOUNT_PENDING", $GLOBALS["Format"]->Number(($amounts["interest"]-$deposits["amount"])/$rate));

/* LOANS STATS */
$statuses = $GLOBALS["Library"]->GetStatuses("loans");
$loans = $GLOBALS["Sql"]->SelectArray("SELECT `status`, IFNULL(COUNT(`loan_id`),0) AS `count`, IFNULL(SUM(I.`principal`),0) AS `principal`, IFNULL(SUM(I.`interest`),0) AS `interest`, IFNULL(SUM(I.`arrear`),0) AS `arrear` FROM `loans` L LEFT JOIN (SELECT `loan_id`, SUM(`principal`) AS `principal`, SUM(`interest`) AS `interest`, SUM(`arrear`) AS `arrear` FROM `installments` WHERE `status`!='canceled' GROUP BY `loan_id`) I USING(`loan_id`) GROUP BY `status`", array(), "status");
$loans_html = array();
$loans_count = 0;
$count = count($loans);
$c = 0;

foreach($loans as $status=>$loan) $loans_count+=$loan["count"];

foreach($loans as $status=>$loan) {
	
	$loans[$status]["STATUS_TXT"] = $statuses[$loan["status"]];
	$loans[$status]["PERCENTAGE_TXT"] = $GLOBALS["Format"]->Number(($loan["count"]*100)/$loans_count);
	$loans[$status]["PRINCIPAL_TXT"] = $GLOBALS["Format"]->Number($loan["principal"]/$rate);
	$loans[$status]["INTEREST_TXT"] = $GLOBALS["Format"]->Number($loan["interest"]/$rate);
	$loans[$status]["ARREAR_TXT"] = $GLOBALS["Format"]->Number($loan["arrear"]/$rate);
	$loans[$status]["TOTAL_TXT"] = $GLOBALS["Format"]->Number(($loan["principal"]+$loan["interest"]+$loan["arrear"])/$rate);
	$loans[$status]["CURRENCY_TXT"] = $currency_txt;

	$loans_html[$status]["IsLast"] = ($count == ++$c);
	$loans_html[$status]["IsAmount"] = true;
	
}

$Loans_tpl = new CTemplate(__TEMPLATES_DIR__."/reports/summary-row.tpl");
$Page->Add("LOANS", $Loans_tpl->RepeatHtml($loans, $loans_html));
$Page->Add("LOANS_COUNT", $loans_count);
$Page->Add("BANK_ACCOUNT_BALANCE", $GLOBALS["Format"]->Number(__BANK_ACCOUNT_BALANCE__/$rate));

$Page->AddHTML("IsCurrencyDOP", $currency != "dop");
$Page->AddHTML("IsCurrencyEUR", $currency != "eur");

$Page->Add("CURRENCY_TXT", $currency_txt);
$Page->Add("EXCHANGE_RATE_EUR", __EXCHANGE_RATE_EUR__);

?>