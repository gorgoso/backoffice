<?php

$filter = $_SESSION["Filter"]->GetFilter(array("status"=>array("multiple"=>array("active", "pending"))), "add_date", "desc");

if ($tour_id_tmp = $_SESSION["Filter"]->GetCondition("tour_id", true)) $filter = $_SESSION["Filter"]->SetCondition("tour_id", (int)$tour_id_tmp, true);

$tours = $GLOBALS["Sql"]->SelectArray("SELECT * FROM `tours` WHERE `status` NOT IN('deleted') AND ".$filter["where"]);

$tours_html=array();
$tours_count = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`tour_id`) AS `count` FROM `tours` WHERE `status` NOT IN('deleted') AND ".$filter["conditions"]);

$statuses = $GLOBALS["Library"]->GetOptions("tour_statuses");

$types = $GLOBALS["Library"]->GetOptions("tours");

$departures_tmp = $GLOBALS["Tour"]->GetDepartures();
$departures = array("do"=>array(), "us"=>array());
$departures_all = array();
foreach($departures_tmp as $departure_id=>$departure) {

	$departure_city = $departure["city"].", ".$GLOBALS["Format"]->Uppercase($departure["state"]);

	$departures[$departure["country"]][$departure_id] = $departure_city;
	$departures_all[$departure_id] = $departure_city;

}

$Tours_tpl = new CTemplate(__TEMPLATES_DIR__."/reports/passengers-pay-row.tpl");
$Tours_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);

foreach($tours as $tour_id=>$tour) {

	$tours[$tour_id]["TOUR_ID_TXT"] = $GLOBALS["Format"]->Id($tour["tour_id"]);
	$tours[$tour_id]["ADD_DATE_TXT"] = $GLOBALS["Format"]->Date($tour["add_date"], __FORMAT_DATE_FULL__);
	$tours[$tour_id]["DEPARTURE_CITY"] = $tour["departure_id"] ? $departures_all[$tour["departure_id"]] : "";
	$tours[$tour_id]["DEPARTURE_DATE_TXT"] = $GLOBALS["Format"]->Date($tour["departure_date"]);
	$tours[$tour_id]["TOUR_TXT"] = $tour["tour"] ? $types[$tour["tour"]] : "";
	$tours[$tour_id]["PRICE_TXT"] = $GLOBALS["Format"]->Number($tour["price"]);
	$tours[$tour_id]["STATUS_TXT"] = $statuses[$tour["status"]];

	$tours_html[$tour_id]["IsBlocked"] = ($tour["status"]=="disabled");

}

$Page->Add("TOURS", $Tours_tpl->RepeatHtml($tours, $tours_html));

$Page->Add("LIST", $GLOBALS["Library"]->MakeList($tours_count["count"], __FILTER_PER_PAGE__, $filter["in"]["start"]));

$Page->Add("TOUR_ID", $GLOBALS["Format"]->Id($_SESSION["Filter"]->GetCondition("tour_id", true)));
$Page->Add("CODE", $_SESSION["Filter"]->GetCondition("code"));
$Page->Add("FROM_ADD_DATE", isset($filter["in"]["conditions"]["dates"]["add_date"][0])?$GLOBALS["Format"]->Date($filter["in"]["conditions"]["dates"]["add_date"][0]):"");
$Page->Add("TO_ADD_DATE", isset($filter["in"]["conditions"]["dates"]["add_date"][1])?$GLOBALS["Format"]->Date($filter["in"]["conditions"]["dates"]["add_date"][1]):"");
$Page->Add("FROM_DEPARTURE_DATE", isset($filter["in"]["conditions"]["dates"]["add_date"][0])?$GLOBALS["Format"]->Date($filter["in"]["conditions"]["dates"]["add_date"][0]):"");
$Page->Add("TO_DEPARTURE_DATE", isset($filter["in"]["conditions"]["dates"]["add_date"][1])?$GLOBALS["Format"]->Date($filter["in"]["conditions"]["dates"]["add_date"][1]):"");
$Page->Add("DEPARTURES_US", $GLOBALS["Library"]->ShowSelect($departures["us"], $_SESSION["Filter"]->GetCondition("departure_id", true)));
$Page->Add("DEPARTURES_DO", $GLOBALS["Library"]->ShowSelect($departures["do"], $_SESSION["Filter"]->GetCondition("departure_id", true)));
$Page->Add("TOURS_TYPES", $GLOBALS["Library"]->ShowSelect($types, $_SESSION["Filter"]->GetCondition("tour", true)));
$Page->Add("PRICE", $_SESSION["Filter"]->GetCondition("price"));
$Page->Add("STATUSES", $GLOBALS["Library"]->ShowSelect($statuses, $_SESSION["Filter"]->GetCondition("status", true)));

$Page->Add("SORT", $_SESSION["Filter"]->GetSort());
$Page->Add("COUNT", $tours_count["count"]);

?>