<?php
$tour_id = $GLOBALS["Library"]->Register("id");
$passengers = $GLOBALS["Sql"]->SelectArray("SELECT t.tour,c.firstname, c.lastname, c.email,cp.paypal_cc,cp.ext_payment_id,DATE_FORMAT(FROM_UNIXTIME(cp.add_date), '%e %b %Y') AS 'add_date',DATE_FORMAT(FROM_UNIXTIME(cp.pay_date), '%e %b %Y') AS 'pay_date',cp.method,cp.`type`,cp.amount,cp.discount,cp.`status`,t.price as 'tour_price' FROM clients c INNER JOIN tours t ON t.tour_id = c.tour_id INNER JOIN `clients-payments` cp ON cp.client_id = c.client_id WHERE t.tour_id=".$tour_id);

$GLOBALS["Common"]->ExportExcel($passengers, "passengers-payments");
