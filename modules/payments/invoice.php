<?php

$payment_id = $GLOBALS["Library"]->Register("id");

require_once("classes/CGenerator.php");
$Generator = new CGenerator("en");
if (!$Generator->Invoice($payment_id)) $GLOBALS["Error"]->AddSession("CANT_GENERATE_INVOICE");

$GLOBALS["Library"]->Redirect(__PATH_ADMIN__."payments/list/");

?>