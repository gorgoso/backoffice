<?php

$payment_id = $GLOBALS["Library"]->Register("id");
$add = $GLOBALS["Library"]->Register("add");

if (!$payment = $GLOBALS["Sql"]->Fetch("SELECT * FROM `clients-payments` CP LEFT JOIN `clients` USING(`client_id`) WHERE CP.`payment_id`=:payment_id AND CP.`status`='pending'", array("payment_id"=>$payment_id))) $GLOBALS["Library"]->Redirect(__PATH_ADMIN__."payments/list/");

$roles = $GLOBALS["Library"]->GetOptions("client_roles");
$titles = $GLOBALS["Library"]->GetOptions("client_titles");
$discounts = $GLOBALS["Library"]->GetOptions("payment_discounts");
ksort($discounts);

if (is_array($add)) {

	$add["amount"] = $GLOBALS["Format"]->Number($add["amount"],2,".","");
	if ($add["amount"]<0 || $add["amount"]>$payment["amount"]) $GLOBALS["Error"]->AddError("BAD_DISCOUNT_AMOUNT");
	$GLOBALS["Sql"]->Validate(array("amount"=>$add["amount"]));

	if (!$GLOBALS["Error"]->Exists()) {

		$save = array("discount"=>$add["amount"]);

		if ($GLOBALS["Sql"]->Update("clients-payments", $save, "payment_id", $payment_id)) {


			$GLOBALS["Error"]->AddSession("PAYMENT_DISCOUNT_UPDATED", "notice");

			$GLOBALS["Library"]->Redirect(__PATH_ADMIN__."payments/detail/${payment_id}/");

		} else {

			$GLOBALS["Error"]->AddError("CANT_UPDATE_PAYMENT_DISCOUNT", "error");

		}

	}
}

$Page->Add("PAYMENT_ID", $payment_id);
$Page->Add("PAYMENT_ID_TXT", $GLOBALS["Format"]->Id($payment_id));
$Page->Add("CLIENT_NAME", ($payment["title"] ? $titles[$payment["title"]] : "")." ".$payment["firstname"]." ".$payment["lastname"]);

$Page->Add("AMOUNT", $payment["amount"]);
$Page->Add("AMOUNT_TXT", $GLOBALS["Format"]->Number($payment["amount"]));
$Page->Add("DISCOUNTS", $GLOBALS["Library"]->ShowSelect($discounts, $add["discount"]));
$Page->Add("DISCOUNT", $add["discount"] ? $add["discount"] : $payment["discount"]);

?>