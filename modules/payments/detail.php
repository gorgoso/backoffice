<?php

$payment_id = $GLOBALS["Library"]->Register("id");
if (!$payment = $GLOBALS["Sql"]->Fetch("SELECT CP.*, `title`,`firstname`,`lastname` FROM `clients-payments` CP LEFT JOIN `clients` USING(`client_id`) WHERE `payment_id`=:payment_id", array("payment_id"=>$payment_id))) $GLOBALS["Library"]->Redirect(__PATH_ADMIN__."payments/list/");

$roles = $GLOBALS["Library"]->GetOptions("client_roles");
$titles = $GLOBALS["Library"]->GetOptions("client_titles");

$statuses = $GLOBALS["Library"]->GetOptions("payment_statuses");
$types = $GLOBALS["Library"]->GetOptions("payment_types");
$methods = $GLOBALS["Library"]->GetOptions("payment_methods");

$Page->Add("PAYMENT_ID", $payment_id);
$Page->Add("PAYMENT_ID_TXT", $GLOBALS["Format"]->Id($payment_id));
$Page->Add("CLIENT_NAME", ($payment["title"] ? $titles[$payment["title"]] : "")." ".$payment["firstname"]." ".$payment["lastname"]);
$Page->Add("ADD_DATE_TXT", $GLOBALS["Format"]->Date($payment["add_date"]));
$Page->Add("PAY_DATE_TXT", $GLOBALS["Format"]->Date($payment["pay_date"]));
$Page->Add("TYPE_TXT", $types[$payment["type"]]);
$Page->Add("METHOD_TXT", $payment["method"] ? $methods[$payment["method"]] : "-");
$Page->Add("AMOUNT_TXT", $GLOBALS["Format"]->Number($payment["amount"]));
$Page->Add("DISCOUNT_TXT", $GLOBALS["Format"]->Number($payment["discount"]));
$Page->Add("AMOUNT_TOTAL_TXT", $GLOBALS["Format"]->Number($payment["amount"]-$payment["discount"]));
$Page->Add("STATUS_TXT", $statuses[$payment["status"]]);

$Page->AddHTML("IsPending", $payment["status"]=="pending");

?>