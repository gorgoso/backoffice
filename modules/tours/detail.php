<?php

$tour_id = $GLOBALS["Library"]->Register("id");
if (!$tour = $GLOBALS["Tour"]->Get($tour_id)) $GLOBALS["Library"]->Redirect(__PATH_ADMIN__."tours/list/");

$tours = $GLOBALS["Library"]->GetOptions("tours");
$statuses = $GLOBALS["Library"]->GetOptions("tour_statuses");
$client_statuses = $GLOBALS["Library"]->GetOptions("clients");
$titles = $GLOBALS["Library"]->GetOptions("titles");

$departures_tmp = $GLOBALS["Tour"]->GetDepartures();

$departures = array("do"=>array(), "us"=>array());
$departures_all = array();
foreach($departures_tmp as $departure_id=>$departure) {

	$departure_city = $departure["city"].", ".$GLOBALS["Format"]->Uppercase($departure["state"]);
	$departureId = 	$departure["departure_id"];
	$departures[$departure["country"]][$departureId] = $departure_city;
	$departures_all[$departure_id] = $departure_city;

}

$Page->Add("TOUR_ID", $tour_id);
$Page->Add("TOUR_ID_TXT", $GLOBALS["Format"]->Id($tour_id));
$Page->Add("TOUR_NAME", $tours[$tour["tour"]["tour"]]);
$Page->Add("DATE_TXT", $GLOBALS["Format"]->Date($tour["tour"]["add_date"], __FORMAT_DATE_FULL__));
$Page->Add("USER_TXT", "-"); //$client["client"]["user_txt"]);
$Page->Add("CODE", $tour["tour"]["code"]);
$Page->Add("DEPARTURE_DATE_TXT", $GLOBALS["Format"]->Date($tour["tour"]["departure_date"]));
$Page->Add("DEPARTURES_US", $GLOBALS["Library"]->ShowSelect($departures["us"], $_SESSION["Filter"]->GetCondition("departure_id", true)));
$Page->Add("DEPARTURES_DO", $GLOBALS["Library"]->ShowSelect($departures["do"], $_SESSION["Filter"]->GetCondition("departure_id", true)));
$Page->Add("DEPARTURE_CITY_TXT", $tour["tour"]["departure_id"] ? $departures_all[$tour["tour"]["departure_id"]] : "");
$Page->Add("DEPARTURE_CITY_ID_TXT",$tour["tour"]["departure_id"]);
$Page->Add("PRICE_TXT", $GLOBALS["Format"]->Number($tour["tour"]["price"]));
$Page->Add("STATUS_TXT", $statuses[$tour["tour"]["status"]]);

$Page->Add("LEADER_CLIENT_ID", $tour["leader"]["client_id"]);
$Page->Add("LEADER_CLIENT_ID_TXT", $GLOBALS["Format"]->Id($tour["leader"]["client_id"]));
$Page->Add("LEADER_ADD_DATE_TXT", $GLOBALS["Format"]->Date($tour["leader"]["add_date"], __FORMAT_DATE_FULL__));
$Page->Add("LEADER_LOG_DATE_TXT", $GLOBALS["Format"]->Date($tour["leader"]["log_date"], __FORMAT_DATE_FULL__));
$Page->Add("LEADER_FULLNAME", ($tour["leader"]["title"] ? $titles[$tour["leader"]["title"]]." " : "").$tour["leader"]["firstname"]." ".$tour["leader"]["lastname"]);
$Page->Add("LEADER_EMAIL", $tour["leader"]["email"]);
$Page->Add("LEADER_STATUS_TXT", $tour["leader"]["status"] ? $client_statuses[$tour["leader"]["status"]] : "");

$Page->AddHTML("IsDisabled", $tour["tour"]["status"]=="deleted");

$Page->AddHTML("IsLeader", $tour["leader"]);
$Page->AddHTML("IsPassengers", $tour["passengers"]);
$Page->AddHTML("IsNotPassengers", !$tour["passengers"]);

$count = count($tour["passengers"]);
$c = 0;
$passengers = array();
$passengers_html = array();
$amount_paid_total = 0;

foreach($tour["passengers"] as $client_id=>$passenger) {

	$payments = $GLOBALS["Sql"]->Fetch("SELECT IFNULL(SUM(`amount`), 0) AS `paid` FROM `clients-payments` WHERE `client_id`=:client_id AND `status`='paid'", array("client_id"=>$client_id));
	$amount_paid_total += $payments["paid"];

	$passengers[$client_id]["CLIENT_ID"] = $client_id;
	$passengers[$client_id]["CLIENT_ID_TXT"] = $GLOBALS["Format"]->Id($client_id);
	$passengers[$client_id]["ADD_DATE_TXT"] = $GLOBALS["Format"]->Date($passenger["add_date"], __FORMAT_DATE_FULL__);
	$passengers[$client_id]["LOG_DATE_TXT"] = $GLOBALS["Format"]->Date($passenger["log_date"], __FORMAT_DATE_FULL__);
	$passengers[$client_id]["FULLNAME"] = ($passenger["title"] ? $titles[$passenger["title"]]." " : "").$passenger["firstname"]." ".$passenger["lastname"];
	$passengers[$client_id]["EMAIL"] = $passenger["email"];
	$passengers[$client_id]["PAID_TXT"] = $GLOBALS["Format"]->Number($payments["paid"]);
	$passengers[$client_id]["PENDING_TXT"] = $GLOBALS["Format"]->Number($tour["tour"]["price"]-$payments["paid"]);
	$passengers[$client_id]["STATUS_TXT"] = $client_statuses[$passenger["status"]];

	$passengers_html[$client_id]["IsLast"] = ($count == ++$c);

}

$Contacts_tpl = new CTemplate(__TEMPLATES_DIR__."/tours/detail-passengers-row.tpl");
$Contacts_tpl->Add("PATH_ADMIN", __PATH_ADMIN__);
$Page->Add("PASSENGERS", $Contacts_tpl->RepeatHtml($passengers, $passengers_html));

$Page->Add("AMOUNT_PAID_TOTAL", $GLOBALS["Format"]->Number($amount_paid_total));
$Page->Add("AMOUNT_PENDING_TOTAL", $GLOBALS["Format"]->Number(($tour["tour"]["price"] * $count) - $amount_paid_total));

$Page->Add("COUNT", $count);

?>