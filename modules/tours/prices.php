<?php

$tours = $GLOBALS["Library"]->GetOptions("tours");
$departures_tmp = $GLOBALS["Tour"]->GetDepartures();
$departures = array("do"=>array(), "us"=>array());
foreach($departures_tmp as $departure_id=>$departure) {

	$departure_city = $departure["city"].", ".$GLOBALS["Format"]->Uppercase($departure["state"]);
	$departureId = 	$departure["departure_id"];
	$departures[$departure["country"]][$departureId] = $departure_city;
	$departures_all[$departure_id] = $departure_city;

}


$Page->Add("DEPARTURES_US", $GLOBALS["Library"]->ShowSelect($departures["us"], $_SESSION["Filter"]->GetCondition("departure_id", true)));
$Page->Add("DEPARTURES_DO", $GLOBALS["Library"]->ShowSelect($departures["do"], $_SESSION["Filter"]->GetCondition("departure_id", true)));
$Page->Add("TOURS", $GLOBALS["Library"]->ShowSelect($tours));
?>