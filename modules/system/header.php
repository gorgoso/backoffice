<?php

if ($_SESSION["User"]->IsAuth()) {

	$groups = $GLOBALS["Library"]->GetOptions("user_groups");

	//$tasks = $GLOBALS["Sql"]->Fetch("SELECT COUNT(`task_id`) AS `count` FROM `tasks` WHERE `user_id`=:user_id AND `type` NOT IN('payment') AND `status`='active' AND `execution_date`<UNIX_TIMESTAMP()", array("user_id"=>$_SESSION["User"]->user_id));
	$tasks["count"] = 0;

	$Page->Add("USERNAME", ($_SESSION["User"]->IsAuth())?$_SESSION["User"]->username:"-");

	$Page->AddHTML("IsClients", __MODULE__=="clients");
	$Page->AddHTML("IsTours", __MODULE__=="tours");
	$Page->AddHTML("IsReports", __MODULE__=="reports");
	$Page->AddHTML("IsTasks", __MODULE__=="tasks" && __ACTION__!="payments");
	$Page->AddHTML("IsAccounting", __MODULE__=="accounting");
	$Page->AddHTML("IsMarketing", __MODULE__=="marketing");
	$Page->AddHTML("IsSystem", __MODULE__=="system");
	
	$Page->Add("USER_ROLE", $groups[$_SESSION["User"]->groups[$_SESSION["User"]->group_id]]);

	$Page->Add("TASKS_PENDING", $tasks["count"]);
	$Page->AddHTML("TasksPending", $tasks["count"]);

}

?>