<?php

//ob_end_clean();

$file_hash = $GLOBALS["Library"]->Reg("id");

if (strlen($file_hash) == 40) {
		
	$file = $GLOBALS["Sql"]->Fetch("SELECT `file_id` FROM `files` WHERE SHA1(CONCAT(:crypt_key, `file_id`))=:file_hash", array("file_hash"=>$file_hash, "crypt_key"=>__CRYPT_KEY__));
	
	if (!$file) die("BAD_FILE_HASH");
	
	$file = $GLOBALS["Sql"]->Fetch("SELECT `filetype`, `filename`, `data` FROM `files` WHERE `file_id`=:file_id", array("file_id"=>$file["file_id"]));
			
	header("Cache-Control: public, must-revalidate");
	header("Pragma: hack");
	header("Content-Type: ".$file["filetype"]);
	header("Content-Length: ".(string)(strlen($file["data"])) );
	header('Content-Disposition: attachment; filename="'.$file["filename"].'"');
	header("Content-Transfer-Encoding: binary\n");
		
	print $file["data"];
	
	exit();
		
	}

?>