<?php

$postdata = array_merge(array("cmd"=>"_notify-validate"), $_POST);

if (!$postdata) die();

$response = $GLOBALS["Library"]->PostRequest("https://www.paypal.com/cgi-bin/webscr", $postdata);

if ($response == "VERIFIED") {

	$client = $GLOBALS["Client"]->Get($postdata["custom"], "client_id");
	$payment = $client["payments"][(int)$postdata["invoice"]];

	if (!$payment) die();

	if ($payment["status"] != "pending") die();
	if ($postdata["payment_status"] != "Completed") die();
	if ($postdata["receiver_email"] != "paypal@goodshepherdtour.com") die();
	//if ($postdata["payment_gross"] != $payment["amount"]) die();

	if ($payment["paypal_id"]==$postdata["txn_id"]) die();

	$save = array(
		"ext_payment_id"=>$postdata["txn_id"],
		"method"=>"paypal",
		"pay_date"=>time(),
		"status"=>"paid"
	);

	if (!$GLOBALS["Sql"]->Update("clients-payments", $save, "payment_id", $payment["payment_id"])) {

		$GLOBALS["Library"]->SendMail("info@goodshepherdtour.com", "ERROR / GOODSHEPHERDTOUR", nl2br(var_export($postdata, true)));

	}

} else {

	$GLOBALS["Error"]->Log("PayPal ".($response == "INVALID" ? "invalid" : "no")." response: ".var_export($postdata, true));

}

die();

?>