<?php

$GLOBALS["Error"]->Log("Daily script triggered.", "info");

$last_run_date = strtotime(__LAST_DAILY_DATE__);
$next_run_date = $last_run_date+86400;
$now = time();

if ($next_run_date > $now) die("STILL_NOT_NEXT_DAY");

require_once("classes/CMaintenance.php");

$Go = new CMaintenance();

/* Re-calculate arrears for installments */
$Go->RecalculateArrears();

/* Plan calls to clients to pay */
$Go->PlanCalls($next_run_date);

$Go->ExpiredInstallments();

$Go->LegalWarnings();

$Go->LoanToLegal();

$Go->LoanFromLegal();

$Go->ExchangeRate();

$GLOBALS["Sql"]->Update("config", array("value"=>date("Y-m-d", $next_run_date)), "config_id", "LAST_DAILY_DATE");

?>