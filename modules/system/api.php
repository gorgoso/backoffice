<?php

require_once("classes/CApi.php");

ob_clean();

if (!isset($GLOBALS["HTTP_RAW_POST_DATA"])) $GLOBALS["HTTP_RAW_POST_DATA"] = file_get_contents('php://input');

$server = new SoapServer(null, array('uri' => __PATH_API__));

$server->setClass("CApi");
$server->handle();

exit();

?>