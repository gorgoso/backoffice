<?php

$path = __CAPTCHA_BG__;
$tcode = $GLOBALS["Library"]->GenPass(4,"ABCDEFGHIJKLMNPQRSTUVWXYZ");

$bg = imagecreatefromjpeg($path);
list($width, $height) = getimagesize($path);
$grey = imagecolorallocate($bg, 128, 128, 128);
$white = imagecolorallocate($bg, 255, 255, 255);

$left = rand(0,$width-70);
$up = rand(0,$height-16);

$small = imagecreatetruecolor(70, 16);

imagecopy($small, $bg, 0, 0, $left, $up, $width, $height);

imagettftext($small, 13, 0, 11, 15, $grey, __CAPTCHA_FONT__, $tcode);
imagettftext($small, 13, 0, 10, 14, $white, __CAPTCHA_FONT__, $tcode);

imagepng($small);

imagedestroy($small);
imagedestroy($bg);

$_SESSION["Captcha"] = $tcode;

?>