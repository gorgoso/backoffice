<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_SUMMARY}</b></a>
	</div>
	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_EXCHANGE_RATE}:</b> {EXCHANGE_RATE_EUR} DOP (Pesos)</a>
	</div>
	{IsCurrencyDOP}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}reports/summary/dop/">{LANG_TXT_CURRENCY_DOP}</a>
	</div>{-IsCurrencyDOP}
	{IsCurrencyEUR}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}reports/summary/eur/">{LANG_TXT_CURRENCY_EUR}</a>
	</div>{-IsCurrencyEUR}
</div>

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_CLIENTS_COUNT}</td>
		<td>{CLIENTS_COUNT}</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_LOANS_COUNT}</td>
		<td><b>{LOANS_COUNT}</b></td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_APPLICATIONS_COUNT}</td>
		<td>{APPLICATIONS_COUNT}</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_ASSESSORES_COUNT}</td>
		<td>{ASSESSORES_COUNT}</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_AMOUNT_ACCOUNT}</td>
		<td><br /><b>{BANK_ACCOUNT_BALANCE} {CURRENCY_TXT}</b></td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_AMOUNT_CAPITAL}</td>
		<td><br />{AMOUNT_CAPITAL} {CURRENCY_TXT}</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_AMOUNT_INTEREST}</td>
		<td>{AMOUNT_INTEREST} {CURRENCY_TXT}</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_AMOUNT_TOTAL}</td>
		<td><b>{AMOUNT_TOTAL} {CURRENCY_TXT}</b></td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_AMOUNT_DEPOSITS}</td>
		<td><br />{AMOUNT_DEPOSITS} {CURRENCY_TXT}</td>
	</tr>
	<tr class="last-bd">
		<td class="title"><br />{LANG_TXT_AMOUNT_PENDING}</td>
		<td><br /><b>{AMOUNT_PENDING} {CURRENCY_TXT}</b></td>
	</tr>
</table>

<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CLIENTS}</b></a>
	</div>
</div>

<table class="classic">
	<tr>
		<td class="title" width="30%">{LANG_TXT_CLIENT_STATUS}</td>
		<td class="title" width="23%">{LANG_TXT_COUNT}</td>
		<td class="title" width="47%">{LANG_TXT_PERCENTAGE}</td>
	</tr>
	{CLIENTS}
</table>

<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_APPLICATIONS}</b></a>
	</div>
</div>

<table class="classic">
	<tr>
		<td class="title" width="30%">{LANG_TXT_APPLICATION_STATUS}</td>
		<td class="title" width="23%">{LANG_TXT_COUNT}</td>
		<td class="title" width="47%">{LANG_TXT_PERCENTAGE}</td>
	</tr>
	{APPLICATIONS}
</table>

<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_LOANS}</b></a>
	</div>
</div>

<table class="classic">
	<tr>
		<td class="title" width="30%">{LANG_TXT_LOAN_STATUS}</td>
		<td class="title" width="10%">{LANG_TXT_COUNT}</td>
		<td class="title" width="10%">{LANG_TXT_PERCENTAGE}</td>
		<td class="title">{LANG_TXT_PRINCIPAL}</td>
		<td class="title">{LANG_TXT_INTEREST}</td>
		<td class="title">{LANG_TXT_ARREAR}</td>
		<td class="title">{LANG_TXT_TOTAL}</td>
	</tr>
	{LOANS}
</table>