<p>
	Estimado {CLIENT_TITLE} {CLIENT_FIRSTNAME}.
</p>

<p>
	<b>Muchas gracias por su interés y decisión de formar parte de mi grupo 
	en nuestro viaje {TOUR_NAME}. Esto promete ser una experiencia única, 
	y tenerlo a usted en mi grupo la convierte en mucho mas especial.</b>
</p>

<p>
	Good Shepherd Tours me ha creado una página web, y yo he creado en 
	ella una cuenta para usted, en la cual puede completar el proceso de 
	registro de su viaje. Una vez terminado este paso, el sistema le irá 
	solicitando a través de señales, introducir números de pasaporte, 
	realizar pagos, seleccionar asiento en los diferentes vuelos, elegir 
	su compañero de cuarto, elegir extensiones del viaje (en caso de 
	desearlo).
</p>

<p>
	Estas son las credenciales de acceso a su cuenta del pasajero:
</p>

<p>
	<b>Usuario:</b> <b style="color:#5a7712;">{CLIENT_EMAIL}</b><br />
	<b>Contraseña:</b> <b style="color:#5a7712;">{PASSWORD}</b>
</p>


<p>
	Para completar el registro de su viaje, por favor siga los siguientes pasos:
</p>

<ul>
	<li>Visita nuestra página web <a href="http://www.goodshepherdtour.com/es/acceso/">www.goodshepherdtour.com</a></li>
	<li>Entre el usuario y la clave que le fueron asignados previamente:</li>
	<li>Introduzca toda la información requerida para quedar así registrado como pasajero de este viaje</li>
</ul>

<p>
	Lo primero que se registra es e vuelo, por lo que debemos tener de 
	inicio su nombre completo, tal cual aparece en su pasaporte para fines 
	del boleto aéreo. En la página de Internet de Good Shepherd Tours también 
	encontrará informaciones que le serán útil a la hora de planear su 
	viaje, tales como: ideas de cómo empacar, que artículos puede llevar, 
	entre otras. Por lo tanto, es muy importante que usted conozca toda 
	la información que provee la página de Internet de Good Shepherd Tours, 
	para de esa manera empiece a familiarizarse con el programa de viaje 
	antes de llegar la fecha de partida.
</p>

<p>
	Si necesita alguna información adicional por favor déjemelo saber. Yo 
	estaré en continuo contacto con el Representante de Good Shepherd Tours 
	para nuestro viaje, por lo que podré suministrarle respuestas rápidas 
	y oportunamente.
</p>

<p>
	Reitero mi gratitud por haber decidido emprender este viaje tan especial 
	como parte de mi grupo!
</p>

<p>
	Dios le bendiga,<br />
	<b>{LEADER_TITLE} {LEADER_FIRSTNAME} {LEADER_LASTNAME}</b>
</p>