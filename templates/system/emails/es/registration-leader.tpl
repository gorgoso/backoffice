<p>
	Estimado {CLIENT_TITLE} {CLIENT_FIRSTNAME}.
</p>

<p>
	<b>Gracias por registrarse con nosotros en nuestro viaje {TOUR_NAME} cuya 
		salida es en {TOUR_DEPARTURE}.</b>
</p>
	
<p>
	Sin duda ha tomado una decisión muy importante, y ha dado el primer paso 
	para hacer realidad lo que puede ser un sueño para usted: Organizar un 
	viaje a La Tierra de Las Sagradas Escrituras!
</p>

<p>
	Durante {COMPANY_YEARS} años, Good Shepherd Tours ha estado orgullosamente 
	brindando a la Comunidad Cristiana paquetes de viajes hacia La Tierra Santa 
	de excelente calidad, y a la vez ofreciendo los MEJORES beneficios posibles 
	para sus lideres de Grupo y esto siempre con los precios MAS BAJOS del 
	Mercado! 
</p>

<p>
	<b>Imagine que con tan sólo traer 6 pasajeros, usted y sus esposa 
	pueden viajar sin costo alguno!</b>
</p>

<p>
	Ahora que ha creado una cuenta en nuestro sitio web, se le ha dado
	acceso que le dará acceso ilimitado a la información de su viaje en grupo.
</p>

<p>
	<b>
		Usuario: <b style="color:#5a7712;">{CLIENT_EMAIL}</b><br />
		Contraseña: <b style="color:#5a7712;">{PASSWORD}</b><br /><br />
		Código de viaje: <b style="color:#5a7712;">{TOUR_CODE}</b>
	</b>
</p>

<p>
	Puede acceder con el mismo cuantas veces guste, y efectuar las siguientes acciones:
</p>

<ul>
	<li>Tener acceso a la información del Programa de Beneficios para Lideres de Grupo</li>
	<li>
		Crear brochures en linea los cuales pueden ser enviados por e-mail a los 
		potenciales pasajeros, Y/O también pueden ser impresos y distribuidos 
		físicamente
	</li>
	<li>Crear su propia pagina web para promocionar sus viajes (detalles adelante)</li>
	<li>Agregar pasajeros a su grupo de viaje</li>
	<li>Actualizar información personal sobre el viaje: números de pasaportes, entre otras</li>
</ul>

<p>
	Sus participantes serán capaces de unirse a su viaje con el código de viaje
	mencionado anteriormente y obtendrán propia cuenta del pasajero.
</p>

<p>
	Una vez que estén registrados, la información de ellos sera actualizada 
	en los datos de su viaje, permitiéndole acceso completo a su grupo, ya 
	sea aquellos que usted haya registrado directamente o bien sea hayan 
	incorporado ellos mismos para ser parte de grupo.
</p>
	
<p>
	Una vez registrado, cada pasajero podrá tener acceso a su cuenta y manejar 
	la información en ella, ya sea para agregar, o modificar la misma, así como 
	podrá realizar pagos en-linea. También les será enviado vía e-mail alertas 
	de pago, basado en las fechas límites pre-establecidas.
</p>

<p>
	<b>Detalles de la página Web para promoción del Viaje</b><br /><br />
	Active nuestro Programa sobre Promocion de Viaje, e cual le ayudará a captar 
	pasajeros fuera de la organización a la cual usted pertenece. La promoción 
	puede ser individual o puede incluir una invitación a otros pastores para 
	que se incorporen a su grupo - para aquellos pastores que usted traiga y 
	los mismos reciban los beneficios de lideres de grupo, usted percibirá 
	beneficios por pasajeros de estos.
	<b>Estos beneficios ser percibirán en comisiones que pueden ir desde $50 a 
	$100 por pasajero pagando precio regular, y los mismos están sujetos al 
	precio del viaje y la ciudad de partida.</b>
</p>

<p>
	<b>¿Para que esperar? ¡Empecemos a promocionar su Viaje!</b>
</p>

<ul>
	<li>Primero, complete la información requerida para crear un brochure o panfleto de su viaje destino</li>
	<li>Luego, suba al sistema los correos electrónicos (e-mails) a los cuales les serán enviados los brochures o panfletos</li>
	<li>Finalmente, converse con nuestro equipo de ventas, sobre como encaminar el plan de promoción de su viaje</li>
</ul>

<p>
	Queremos recordarle que, a pesar de haber diseñado un programa en-línea 
	práctico y de fácil manejo, seguimos estando a su disposición para 
	asegurarnos de que todo el procedimiento marche de la manera más clara 
	y fácil para el planeamiento de su viaje. Si tiene alguna solicitud en 
	particular, inconveniente o duda de alguna índole, por favor no dude 
	en contactarnos.
</p>

<p>
	Reciba un sincero agradecimiento de parte de todo el equipo de Good 
	Shepherd Tours, por habernos permitido trabajar conjuntamente con 
	usted y sus pasajeros. Esperamos y contamos con que este viaje seria 
	una experiencia maravillosa, la cual deseará repetir año tras año!
</p>