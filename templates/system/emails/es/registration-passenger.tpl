<p>
	Estimado {CLIENT_TITLE} {CLIENT_FIRSTNAME}.
</p>

<p>
	<b>Muchas gracias por su interés y decisión de acompañarnos en nuestro 
		viaje {TOUR_NAME}. Esto promete ser una experiencia única, y tenerlo 
		a usted con nosotros la convierte en mucho más especial.</b>
</p>

<p>
	Good Shepherd Tours ha creado una cuenta para usted, en la cual puede 
	completar el proceso de registro de su viaje. Una vez terminado este 
	paso, el sistema le irá solicitando a través de señales, introducir 
	números de pasaporte, realizar pagos, seleccionar asiento en los 
	diferentes vuelos, elegir su compañero de cuarto, elegir extensiones 
	del viaje (en caso de desearlo).
</p>

<p>
	Estas son las credenciales de acceso a su cuenta del pasajero:
</p>

<p>
	<b>Usuario:</b> <b style="color:#5a7712;">{CLIENT_EMAIL}</b><br />
	<b>Contraseña:</b> <b style="color:#5a7712;">{PASSWORD}</b>
</p>

<p>
	Para completar el registro de su viaje, por favor siga los siguientes pasos:
</p>

<ul>
	<li>Visita nuestra página web <a href="http://www.goodshepherdtour.com/es/acceso/">www.goodshepherdtour.com</a></li>
	<li>Entre el usuario y la clave que le fueron asignados previamente:</li>
	<li>Introduzca toda la información requerida para quedar así registrado como pasajero de este viaje</li>
</ul>

<p>
	Lo primero que se registra es el vuelo, por lo que debemos tener de 
	inicio su nombre completo, tal cual aparece en su pasaporte para fines 
	del boleto aéreo. En nuestra página de Internet también encontrará 
	informaciones que le serán útil a la hora de planear su viaje, tales 
	como: ideas de cómo empacar, que artículos puede llevar, entre otras. Por 
	lo tanto, es muy importante que usted empiece a familiarizarse con 
	toda la información que proveemos a través de ella, antes de llegar 
	la fecha de partida.
</p>

<p>
	Si tiene alguna pregunta o duda, por favor no dude en contactarnos, 
	siempre haremos todo lo posible para proveerle la mejor solución.
</p>

<p>
	Reitero mi agradecimiento por acompañarnos en este viaje maravilloso 
	y tan especial!
</p>

<p>
	Que Dios le bendiga,<br />
	<b>Daniel Malka</b><br />
	Presidente, fundador<br />
	Good Shepherd Tours
</p>