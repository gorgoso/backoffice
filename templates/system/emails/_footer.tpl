							</td>
						</tr>
						<tr>
							<td colspan="2" width="700" style="padding: 0px 30px 30px 30px; text-align: justify;">
								<hr>
								<font size="1">
									Due to the nature of Internet communications, GOOD SHEPHERD TOURS INC. can not
									guarantee the security of this message. All such information is transmitted at
									your own risk. The use of email by the which is considered as recognition of
									these risks.<br><br>
									This email and any files attachment may contain confidential information.
									If you have received this e-mail or any attachment in error, please delete the
									message / attachment, and notify the sender. Please do not copy, disclose or
									use email, any attachments, or any information contained therein.
							</td>
						</tr>
						<tr style="background-color: #cccbc7; text-align: center; color: #000000;">
							<td colspan="2" style="padding: 10px;" width="700">
								2010-{CURRENT_YEAR} &copy; GOOD SHEPHERD TOURS INC. All rights reserved.
								<BR>
								<FONT SIZE="1">2960 West Liberty, Pittsburgh, PA 15216, USA<BR>
								Toll-free phone 844-500-TOUR (8687) ~ International phone +1-412-513-5022<BR>
								E-mail <a href="mailto:info@goodshepherdtour.com" style="color:#5a7712; text-decoration: none;">
									<span style="color:#5a7712;">info@goodshepherdtour.com</span></a> ~
								Web: <a href="http://www.goodshepherdtour.com/" style="color:#5a7712; text-decoration: none;">
									<span style="color:#5a7712;">www.goodshepherdtour.com</span></a>
								</FONT>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

	</body>
</html>