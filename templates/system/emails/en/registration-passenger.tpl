<p>
	Dear {CLIENT_TITLE} {CLIENT_FIRSTNAME}.
</p>

<p>
	<b>Thank you for your interest in traveling with us on our {TOUR_NAME}. 
	This promises to be the trip of a lifetime, and to have you join us for 
	this experience will make the tour even more special.</b>
</p>

<p>
	Good Shepherd Tours has created an account for you so you can complete 
	your registration process. Once you’ve input your information, the 
	computer program will prompt you regularly to make payments, input 
	passport information, select seats on the various flights, select your 
	roommate and take any tour extensions if you choose to do so.
</p>

<p>
	Here are the credentials to access your passenger account:
</p>

<p>
	<b>Username:</b> <b style="color:#5a7712;">{CLIENT_EMAIL}</b><br />
	<b>Password:</b> <b style="color:#5a7712;">{PASSWORD}</b>
</p>

<p>
	To complete your tour registration, please do the following:
</p>

<ul>
	<li>Go to <a href="http://www.goodshepherdtour.com/en/login/">www.goodshepherdtour.com</a></li>
	<li>Login with credentials mentioned before</li>
	<li>Add the required information so you are logged in as a tour passenger</li>
</ul>

<p>
	The flights are booked first, so we have to have your 
	full name (as listed on your passport) for the airline tickets. 
	The website also offers site information, packing tips and other 
	travel information that will be helpful to all passengers, so I 
	urge you to review the information and become familiar with the 
	tour prior to departing on our first flight.
</p>

<p>
	If you have any questions, please let us know, so we will get your 
	questions and/or concerns taken care of quickly.
</p>

<p>
	Again, thanks for joining on this trip of a lifetime!
</p>

<p>
	Blessings,<br />
	<b>Daniel Malka</b><br />
	President<br />
	Good Shepherd Tours
</p>