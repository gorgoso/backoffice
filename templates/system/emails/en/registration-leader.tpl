<p>
	Dear {CLIENT_TITLE} {CLIENT_FIRSTNAME}.
</p>

<p>
	<b>Thank You for registering for a {TOUR_NAME} departing {TOUR_DATE} from 
	{TOUR_DEPARTURE} with Good Shepherd Tours.</b>
</p>
	
<p>
	You have made an important decision, and have taken the first step to 
	fulfilling your dream of leading a tour group to the Holy Lands!
</p>

<p>
	For {COMPANY_YEARS} years, Good Shepherd Tours has proudly provided the 
	Christian community with top quality tour programs to the Holy Lands 
	that offered the BEST Tourhost Benefits available at the lowest price 
	in the market!
</p>

<p>
	<b>Imagine! By signing up only six other travelers, you and your spouse 
	qualify to join the trip for free.</b>
</p>

<p>
	Now that you have created an account on our website, you have been given 
	access that will give you unlimited access to your group tour information.
</p>

<p>
	<b>
		Your access username: <b style="color:#5a7712;">{CLIENT_EMAIL}</b><br />
		Your access password: <b style="color:#5a7712;">{PASSWORD}</b><br /><br />
		Your tour code is: <b style="color:#5a7712;">{TOUR_CODE}</b>
	</b>
</p>

<p>
	You can log in anytime to do the following:
</p>

<ul>
	<li>Access our Tour Host benefit program</li>
	<li>
		Create a brochure online that can be printed and distributed or mailed 
		to potential passengers OR can be emailed to potential passengers
	</li>
	<li>Create your own Tour promotion website (details below)</li>
	<li>Add participants to your tour</li>
	<li>Update your personal tour information and passport numbers</li>
</ul>

<p>
	Your tour participants will be able to join your tour with the tour code
	provided upper and they will get own passenger account.
</p>

<p>
	Once registered, their information will be updated into your Tour data, 
	giving you full access to your tour group whether pre-registered by you, 
	or independently signing in to join your tour group!
</p>
	
<p>
	Each passenger, after registration, will be able to access their account 
	and add/edit any information. The passengers will also be able to make 
	their tour payments online, and will be prompted via e-mail to make 
	their payments based on our pre-set deadlines.
</p>

<p>
	<b>Tour Promotion Website Details</b><br /><br />
	Activate our Tour Promotion program, which will enable you to reach 
	potential passengers that are not part of your church. This can be 
	an individual promotion, or can include an invitation to other 
	Pastors who will join your group - with the Pastor receiving the Tour 
	<b>Host benefit, and YOU receiving a per-person commission for each tour 
	member accompanying the tourhost. This commission will vary 
	between $50 to $100 per paying passenger, and will be based on your 
	tour price and departure city.</b>
</p>

<p>
	<b>So, lets get busy and start promoting your tour!</b>
</p>

<ul>
	<li>First, fill in the data to create your Tour brochure or flyer</li>
	<li>Second, upload e-mail addresses to send the tour brochure / flyer to your e-mail list</li>
	<li>Third, speak with our Sales Team to get your Tour Promotion plan together</li>
</ul>

<p>
	Remember, we have made the tour program easily accessible online, however, 
	we are available to work with you personally to ensure that everything 
	flows smoothly for your tour. If you have any special requests or need 
	clarification on any issue, please feel free to contact us.
</p>

<p>
	Thank you from the staff of Good Shepherd Tours for giving us this opportunity 
	to work with you and your tour passengers. We look forward to making this 
	a wonderful journey and one that you will want to take year after year!
</p>