<style type="text/css">
	legend {padding: 0px 10px 0px 10px; text-align: left; font-weight: bold; background: #ffffff;}
	table {width:100%; text-align: left;}
	td, th {padding: 6px 10px 6px 10px;}
	th {background: #000000; color: #ffffff;}
	table.r tr:nth-child(even) {background: #e9e9e9;}
	table.r tr:nth-child(odd) {background: #dadada;}
	div.txt {float: left;}
	div.fill {border-bottom: 1px solid; padding-left: 10px; overflow: hidden;}
	span.check {display: inline-block; padding: 0px; width: 20px; border: 1px solid; text-align: center;}
	table tr {page-break-inside: avoid;}
</style>

<h1>FORMULARIO DE REGISTRO</h1>
<center>
	<b>GOOD SHEPHERD TOURS INC.</b><br />
	2960 West Liberty, Pittsburgh, PA 15216, EE.UU.<br />
	Tel�fono sin cargos: 844-500-TOUR (8687) ~ Tel�fono internacional: +1-412-440-3292 ~ E-mail: info@goodshepherdtour.com
</center>

<br />

<table>
	<tr>
		<td width="50%">

			<fieldset>
				<legend>INFORMACI�N SOBRE PROGRAMA DE VIAJE</legend>
				
				<table>
					<tr>
						<td>
							<div class="txt">Fecha de salida:&nbsp;</div>
							<div class="fill">{DEPARTURE_DATE}</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="txt">Ciudad de salida:&nbsp;</div>
							<div class="fill">{DEPARTURE_CITY}</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="txt">Programa de viaje:&nbsp;</div>
							<div class="fill">{TOUR_NAME}</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="txt">Precio del viaje:&nbsp;</div>
							<div class="fill">US$ {TOUR_PRICE}</div>
						</td>
					</tr>
				</table>
				
			</fieldset>
			
		</td>
		<td width="50%">

			<fieldset>
				<legend>INFORMACI�N SOBRE EL L�DER DE GRUPO DE VIAJE</legend>
				
				<table>
					<tr>
						<td>
							<div class="txt">Nombre completo:&nbsp;</div>
							<div class="fill">{TITLE} {FIRSTNAME} {LASTNAME}</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="txt">Tel�fono:&nbsp;</div>
							<div class="fill">{HOME_PHONE}</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="txt">Correo electr�nico:&nbsp;</div>
							<div class="fill">{EMAIL}</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="txt">C�digo de viaje:&nbsp;</div>
							<div class="fill">{TOUR_CODE}</div>
						</td>
					</tr>
				</table>
								
			</fieldset>
			
		</td>
	</tr>
</table>

<br />

<table>
	<tr>
		<td>

			<fieldset>
				<legend>INFORMACI�N SOBRE TRANSPORTE A�REO</legend>
				
				<table class="r" style="border: 1px solid; text-align: center;">
					<tr class="bl">
						<th>Salida / Llegada</th>
						<th width="100">Fecha</th>
					</tr>
					<tr>
						<td>Salida: <b>{DEPARTURE_CITY}</b></td>
						<td>{DEPARTURE_DATE}</td>
					</tr>
					<tr>
						<td>Llegada: <b>Tel-Aviv, Israel</b></td>
						<td>{ISRAEL_ARRIVAL_DATE}</td>
					</tr>
					<tr>
						<td>Salida: <b>Tel-Aviv, Israel</b></td>
						<td>{ISRAEL_DEPARTURE_DATE}</td>
					</tr>{IsEuropeTour}
					<tr>
						<td>Llegada: <b>{IsIsraelTurkey}Estambul, Turqu�a{-IsIsraelTurkey}{IsIsraelRome}Roma, Italia{-IsIsraelRome}{IsIsraelAthens}Atenas, Grecia{-IsIsraelAthens}</b></td>
						<td>{ISRAEL_DEPARTURE_DATE}</td>
					</tr>
					<tr>
						<td>Salida: <b>{IsIsraelTurkey}Estambul, Turqu�a{-IsIsraelTurkey}{IsIsraelRome}Roma, Italia{-IsIsraelRome}{IsIsraelAthens}Atenas, Grecia{-IsIsraelAthens}</b></td>
						<td>{ARRIVAL_DATE}</td>
					</tr>{-IsEuropeTour}
					<tr>
						<td>Llegada: <b>{DEPARTURE_CITY}</b></td>
						<td>{ARRIVAL_DATE}</td>
					</tr>
				</table>
				
			</fieldset>

		</td>
	</tr>
</table>

<br />

<table>
	<tr>
		<td>

			<fieldset>
				<legend>INFORMACI�N SOBRE TRANSPORTE TERRESTRE</legend>
				
				<table class="r" style="border: 1px solid; text-align: center;">
					<tr class="bl">
						<th>D�a</th>
						<th width="100">Fecha</th>
						<th>Transporte/Traslados</th>
					</tr>
					<tr>
						<td>1.</td>
						<td>{DEPARTURE_DATE}</td>
						<td>Salida desde su ciudad de origen en vuelo hacia Israel</td>
					</tr>
					<tr>
						<td>2.</td>
						<td>{2DAY_DATE}</td>
						<td><b>Traslado desde el aeropuerto de Tel-Aviv, Israel hacia el hotel</b></td>
					</tr>
					<tr>
						<td>3.</td>
						<td>{3DAY_DATE}</td>
						<td><b>Desde las 8:00AM hasta las 6:00PM visitas tur�sticas</b></td>
					</tr>
					<tr>
						<td>4.</td>
						<td>{4DAY_DATE}</td>
						<td><b>Desde las 8:00AM hasta las 6:00PM visitas tur�sticas</b></td>
					</tr>
					<tr>
						<td>5.</td>
						<td>{5DAY_DATE}</td>
						<td><b>Desde las 8:00AM hasta las 6:00PM visitas tur�sticas</b></td>
					</tr>
					<tr>
						<td>6.</td>
						<td>{6DAY_DATE}</td>
						<td><b>Desde las 8:00AM hasta las 6:00PM visitas tur�sticas</b></td>
					</tr>
					<tr>
						<td>7.</td>
						<td>{7DAY_DATE}</td>
						<td><b>Desde las 8:00AM hasta las 6:00PM visitas tur�sticas</b></td>
					</tr>
					<tr>
						<td>8.</td>
						<td>{8DAY_DATE}</td>
						<td><b>D�a libre</b></td>
					</tr>
					<tr>
						<td>9.</td>
						<td>{9DAY_DATE}</td>
						<td><b>Mitad del d�a para vista panor�mica y traslado desde el hotel hasta el aeropuerto de Tel-Aviv, Israel</b></td>
					</tr>{IsEuropeTour}
					<tr>
						<td>9.</td>
						<td>{9DAY_DATE}</td>
						<td>Traslado desde el aeropuerto {IsIsraelTurkey}Estambul, Turqu�a{-IsIsraelTurkey}{IsIsraelRome}Roma, Italia{-IsIsraelRome}{IsIsraelAthens}Atenas, Grecia{-IsIsraelAthens}, hasta el hotel</td>
					</tr>
				</table>

			</fieldset>

		</td>
	</tr>
</table>

<table>
	<tr>
		<td>

			<fieldset>
				<legend>INFORMACI�N SOBRE TRANSPORTE TERRESTRE</legend>
				
				<table class="r" style="border: 1px solid; text-align: center;">
					<tr class="bl">
						<th>D�a</th>
						<th width="100">Fecha</th>
						<th>Transporte/Traslados</th>
					</tr>
					<tr>
						<td>10.</td>
						<td>{10DAY_DATE}</td>
						<td><b>Desde las 8:00AM hasta las 6:00PM visitas tur�sticas</b></td>
					</tr>
					<tr>
						<td>11.</td>
						<td>{11DAY_DATE}</td>
						<td><b>Traslado desde el hotel hasta el aeropuerto de {IsIsraelTurkey}Estambul, Turqu�a{-IsIsraelTurkey}{IsIsraelRome}Roma, Italia{-IsIsraelRome}{IsIsraelAthens}Atenas, Grecia{-IsIsraelAthens}</b></td>
					</tr>
					{-IsEuropeTour}<tr>
						<td>{DAYS}.</td>
						<td>{ARRIVAL_DATE}</td>
						<td>Vuelo retorno a casa</td>
					</tr>
				</table>
				
			</fieldset>

		</td>
	</tr>
</table>

<br />

<table>
	<tr>
		<td>

			<fieldset>
				<legend>INFORMACI�N SOBRE ACTIVIDADES Y VISITAS TUR�STICAS</legend>
				
				<table class="r" style="border: 1px solid; text-align: center;">
					<tr class="bl">
						<th>D�a</th>
						<th width="100">Fecha</th>
						<th>Actividades y visitas tur�sticas</th>
					</tr>
					<tr>
						<td>1.</td>
						<td>{DEPARTURE_DATE}</td>
						<td>Salida desde su ciudad de origen en vuelo hacia Israel</td>
					</tr>
					<tr>
						<td>2.</td>
						<td>{2DAY_DATE}</td>
						<td>Traslado desde el aeropuerto de Tel-Aviv, Israel hasta el hotel</td>
					</tr>
					<tr>
						<td>3.</td>
						<td>{3DAY_DATE}</td>
						<td><b>Visitamos Cesarea, Haifa, Meguido, Tiberias</b></td>
					</tr>
					<tr>
						<td>4.</td>
						<td>{4DAY_DATE}</td>
						<td><b>Visitamos Tiberias, Cafarna�n, Banias</b></td>
					</tr>
					<tr>
						<td>5.</td>
						<td>{5DAY_DATE}</td>
						<td><b>Visitamos Tiberias, Beit Shean, Belen, Jerusal�n</b></td>
					</tr>
					<tr>
						<td>6.</td>
						<td>{6DAY_DATE}</td>
						<td>
							<b>Visitamos la antigua ciudad de Jerusal�n, La Puerta de San Esteban, La Piscina de Betesda, El Muro de Los Lamentos, La Iglesia del Santo Sepulcro, Torre de David, La Tumba del Jard�n, Los Jardines de Getseman�</b>
						</td>
					</tr>
					<tr>
						<td>7.</td>
						<td>{7DAY_DATE}</td>
						<td><b>Visitamos El Antiguo Jeric�, El Buen Samaritano, El Mar Muerto, Las Cuevas de Qumr�n, Masada</b></td>
					</tr>
					<tr>
						<td>8.</td>
						<td>{8DAY_DATE}</td>
						<td>D�a libre</td>
					</tr>
					<tr>
						<td>9.</td>
						<td>{9DAY_DATE}</td>
						<td>{NotJordan}Traslado desde el hotel a aeropuerto de Tel-Aviv, Israel {IsEuropeTour}, vuelo a {IsIsraelTurkey}Estanbul, Turq�a{-IsIsraelTurkey}{IsIsraelRome}Roma, Italia{-IsIsraelRome}{IsIsraelAthens}Atenas, Grecia{-IsIsraelAthens}{-IsEuropeTour}{-NotJordan}{IsIsraelJordan}Transfer from the hotel to Jordan.<br /><b>Visit Jerash, Amman</b>{-IsIsraelJordan}</td>
					</tr>{NotJustIsrael}{NotJordan}
					<tr>
						<td>9.</td>
						<td>{9DAY_DATE}</td>
						<td>Traslado desde el aeropuerto hasta el hotel, en {IsIsraelTurkey}Estanbul, Turq�a{-IsIsraelTurkey}{IsIsraelRome}Roma, Italia{-IsIsraelRome}{IsIsraelAthens}Atenas, Grecia{-IsIsraelAthens}</td>
					</tr>{-NotJordan}{-IsEuropeTour}
					<tr>
						<td>10.</td>
						<td>{10DAY_DATE}</td>
						<td><b>Visitamos 
							{IsIsraelTurkey}la Iglesia Hagia Sof�a, La Cisterna Bas�lica, Las Ruinas del Hip�dromo{-IsIsraelTurkey}
							{IsIsraelRome}Vatican city, Castel Sant'Angelo, the Tiber, Piazza Navona, Bernini�s fountain, The Pantheon{-IsIsraelRome}
							{IsIsraelAthens}the Parthenon, Temple of Athena, The temple of Olympian Zeus, Royal palace, Lycabettus hill, The Flea market{-IsIsraelAthens}
							{IsIsraelJordan}Wadi Moussa, Petra, El Khazneh{-IsIsraelJordan}</b>
						</td>
					</tr>
					<tr>
						<td>11.</td>
						<td>{11DAY_DATE}</td>
						<td><b>Visitamos 
							{IsIsraelTurkey}El Palacio de Topkapi, Kasikci diamond, Spice market, Crucero a lo largo del Estrecho de Bosporus{-IsIsraelTurkey}
							{IsIsraelRome}the Roman Colosseum, Barberini palace, Piazza Venezia, the Roman Forum, the citadel of Ancient Rome, Piazza del Campidoglio{-IsIsraelRome}
							{IsIsraelAthens}three Greek islands{-IsIsraelAthens}
							{IsIsraelJordan}Madaba, Mount Nebo, Byzantine church{-IsIsraelJordan}</b>
						</td>
					</tr>
					<tr>
						<td>{DAYS}.</td>
						<td>{ARRIVAL_DATE}</td>
						<td>Traslado desde el hotel hasta el aeropuerto en {IsIsraelTurkey}Estanbul, Turq�a{-IsIsraelTurkey}{IsIsraelRome}Roma, Italia{-IsIsraelRome}{IsIsraelAthens}Atenas, Grecia{-IsIsraelAthens}{IsIsraelJordan}Tel-Aviv, Israel{-IsIsraelJordan}</td>
					</tr>{-NotJustIsrael}
					<tr>
						<td>{DAYS}.</td>
						<td>{ARRIVAL_DATE}</td>
						<td>Vuelo retorno a casa</td>
					</tr>
				</table>
				
			</fieldset>

		</td>
	</tr>
</table>

<br />

<table>
	<tr>
		<td width="50%" style="vertical-align: top; text-align: justify;">

			<fieldset style="padding-right: 40px; height: 300px;">
				<legend>INCLU�DO EN EL PRECIO DEL PROGRAMA DE VIAJE</legend>
				
				<ul>
					<li>Transporte a�reo ida y vuelta a Tel Aviv desde ciertos aeropuertos internacionales de su ciudad</li>
					<li>Servicios de reuniones y traslados entre aeropuertos y hoteles de ultramar, incluyendo transporte de cargas</li>
					<li>Ocho (8) noches en hoteles de cuatro estrellas (ocupaci�n doble) con habitaciones de dos camas y ba�o privado</li>
					<li>Paseos en coche de lujo con aire acondicionado</li>
					<li>Desayunos bufete y cenas diarios en el Hotel</li>
					<li>Servicios completos de escoltas de gu�as con licencia y experiencia gubernamental</li>
					<li>Visitas tur�sticas con entradas a los lugares en el Itinerario</li>
				</ul>
				
			</fieldset>
			
		</td>
		<td width="50%" style="vertical-align: top; text-align: justify;">

			<fieldset style="padding-right: 40px; height: 300px;">
				<legend>NO INCLU�DO EN EL PRECIO DEL PROGRAMA DE VIAJE</legend>
				
				<ul>
					<li>Propinas a gu�as tur�sticos</li>
					<li>Propinas a choferes transportes terrestres</li>
					<li>Servicios del hotel</li>
					<li>Pago de visado (Si aplica)</li>
				</ul>
			</fieldset>
			
		</td>
	</tr>
</table>

<table>
	<tr>
		<td style="text-align: justify; height: 100px; vertical-align: top;">

			Yo acepto los t�rminos y condiciones del programa de viaje seleccionado 
			con Good Shepherd Tours, en lo expuesto en los materiales publicitarios 
			entregados, impresos (brochures) y medios virtuales (p�gina web). 
			Reafirmo entender y aceptar las pol�ticas de pagos, y que el no 
			cumplimiento de las mismas conlleva a costos adicionales a los promocionados 
			en el material de Good Shepherd Tours.
			
		</td>
	</tr>
</table>

<table>
	<tr>
		<td width="50%"><div class="txt">Fecha:&nbsp;</div><div class="fill">{TODAY_YEAR}-{TODAY_MONTH}-{TODAY_DAY}</div></td>
		<td width="50%"><div class="txt">Firma:&nbsp;</div><div class="fill">&nbsp;</div></td>
	</tr>
</table>