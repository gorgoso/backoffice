<style type="text/css">
	legend {padding: 0px 10px 0px 10px; text-align: left; font-weight: bold; background: #ffffff;}
	table {width:100%; text-align: left;}
	td, th {padding: 6px 10px 6px 10px;}
	th {background: #000000; color: #ffffff;}
	table.r tr:nth-child(even) {background: #e9e9e9;}
	table.r tr:nth-child(odd) {background: #dadada;}
	div.txt {float: left;}
	div.fill {border-bottom: 1px solid; padding-left: 10px; overflow: hidden;}
	span.check {display: inline-block; padding: 0px; width: 20px; border: 1px solid; text-align: center;}
	table tr {page-break-inside: avoid;}
</style>

<div style="text-align: left">
	<img src="http://www.goodshepherdtour.com/graphics/logo.png" style="margin-right: 30px; float: left">
	<h1 style="text-align: left; text-decoration: none;">GOOD SHEPHERD TOURS INC.</h1>
		2960 West Liberty, Pittsburgh, PA 15216, USA<br />
		Toll-free phone: 844-500-TOUR (8687) ~ International phone: +1-412-513-5022<br />
		E-mail: info@goodshepherdtour.com
</div>

<br />

<p style="text-align: justify">
	Thank you for your payment of <b>{PAYMENT_TYPE} of US${TOTAL}</b> to reserve
	your space on our upcoming tour. As per our policy, you will make a total of
	three payments prior to the tour departure, at which time, your account will
	be paid in full.
</p>

<fieldset>
	<legend>INVOICE INFORMATION</legend>
	<table>
		<tr>
			<td width="30%">Invoice number:</td>
			<td style="text-transform: uppercase; font-size: 20px"><b>{PAYMENT_ID_TXT}</b></td>
		</tr>
		<tr>
			<td>Invoice date:</td>
			<td style="text-transform: uppercase">{CURRENT_DATE}</td>
		</tr>
	</table>
</fieldset>

<br />

<fieldset>
	<legend>PASSENGER INFORMATION</legend>
	<table>
		<tr>
			<td width="30%">Name:</td>
			<td style="text-transform: uppercase; font-size: 20px"><b>{TITLE} {FIRSTNAME} {LASTNAME}</b></td>
		</tr>
		<tr>
			<td>Address:</td>
			<td style="text-transform: uppercase">{ADDRESS_FULL}</td>
		</tr>
		<tr>
			<td>Phone:</td>
			<td style="text-transform: uppercase">{HOME_PHONE}</td>
		</tr>
		<tr>
			<td>E-mail:</td>
			<td style="text-transform: uppercase">{EMAIL}</td>
		</tr>
	</table>
</fieldset>

<br />

<fieldset>
	<legend>TOUR INFORMATION</legend>
	<table>
		<tr>
			<td width="30%">Tour:</td>
			<td style="text-transform: uppercase; font-size: 20px"><b>{TOUR_NAME}</b></td>
		</tr>
		<tr>
			<td>Departure city:</td>
			<td style="text-transform: uppercase">{DEPARTURE_CITY}</td>
		</tr>
		<tr>
			<td>Departure date:</td>
			<td style="text-transform: uppercase">{DEPARTURE_DATE}</td>
		</tr>
		<tr>
			<td>Arrival date:</td>
			<td style="text-transform: uppercase">{ARRIVAL_DATE}</td>
		</tr>
		<tr>
			<td>Code:</td>
			<td style="text-transform: uppercase">{TOUR_CODE}</td>
		</tr>
		<tr>
			<td>Tour host name:</td>
			<td style="text-transform: uppercase">{TOUR_HOST_NAME}</td>
		</tr>
	</table>
</fieldset>

<br />

<fieldset>
	<legend>PAYMENT DETAIL</legend>
	<table>
		<tr>
			<td width="30%">Payment:</td>
			<td style="text-transform: uppercase; font-size: 20px"><b>{PAYMENT_TYPE}</b></td>
		</tr>
		<tr>
			<td>Price:</td>
			<td style="text-transform: uppercase">US$ {PRICE}</td>
		</tr>
		<tr>
			<td>Dicount:</td>
			<td style="text-transform: uppercase">US$ {DISCOUNT}</td>
		</tr>
		<tr>
			<td>Sale price:</td>
			<td style="text-transform: uppercase; font-size: 20px"><b>US$ {TOTAL}</b></td>
		</tr>
	</table>
</fieldset>

<ul style="text-align: left">
	<li><b>AIRLINE TICKET DEPOSIT:</b> pay before {FIRST_PAYMENT_DATE}</li>
	<li><b>AIRLINE TICKET FINAL PAYMENT:</b> pay before {SECOND_PAYMENT_DATE}</li>
	<li><b>TOUR PAYMENT:</b> pay before {THIRD_PAYMENT_DATE}</li>
</ul>

<p style="text-align: justify">
	Thank you again for your payment and if you have any questions, don't hesitate
	to contact us at toll-free phone 844-500-TOUR (8687), international phone
	+1-412-513-5022 or by e-mail at info@goodshepherdtour.com.
</p>
