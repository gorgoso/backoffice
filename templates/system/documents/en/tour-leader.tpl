<style type="text/css">
	legend {padding: 0px 10px 0px 10px; text-align: left; font-weight: bold; background: #ffffff;}
	table {width:100%; text-align: left;}
	td, th {padding: 6px 10px 6px 10px;}
	th {background: #000000; color: #ffffff;}
	table.r tr:nth-child(even) {background: #e9e9e9;}
	table.r tr:nth-child(odd) {background: #dadada;}
	div.txt {float: left;}
	div.fill {border-bottom: 1px solid; padding-left: 10px; overflow: hidden;}
	span.check {display: inline-block; padding: 0px; width: 20px; border: 1px solid; text-align: center;}
	table tr {page-break-inside: avoid;}
</style>

<h1>TOUR REGISTRATION FORM</h1>
<center>
	<b>GOOD SHEPHERD TOURS INC.</b><br />
	2960 West Liberty, Pittsburgh, PA 15216, USA<br />
	Toll-free phone: 844-500-TOUR (8687) ~ International phone: +1-412-513-5022 ~ E-mail: info@goodshepherdtour.com
</center>

<br />

<table>
	<tr>
		<td width="50%">

			<fieldset>
				<legend>TOUR PROGRAM INFORMATION</legend>

				<table>
					<tr>
						<td>
							<div class="txt">Departure date:&nbsp;</div>
							<div class="fill">{DEPARTURE_DATE}</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="txt">Departure city:&nbsp;</div>
							<div class="fill">{DEPARTURE_CITY}</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="txt">Tour program:&nbsp;</div>
							<div class="fill">{TOUR_NAME}</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="txt">Tour price:&nbsp;</div>
							<div class="fill">US$ {TOUR_PRICE}</div>
						</td>
					</tr>
				</table>

			</fieldset>

		</td>
		<td width="50%">

			<fieldset>
				<legend>TOUR HOST INFORMATION</legend>

				<table>
					<tr>
						<td>
							<div class="txt">Full name:&nbsp;</div>
							<div class="fill">{TITLE} {FIRSTNAME} {LASTNAME}</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="txt">Phone number:&nbsp;</div>
							<div class="fill">{HOME_PHONE}</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="txt">E-mail address:&nbsp;</div>
							<div class="fill">{EMAIL}</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="txt">Tour code:&nbsp;</div>
							<div class="fill">{TOUR_CODE}</div>
						</td>
					</tr>
				</table>

			</fieldset>

		</td>
	</tr>
</table>

<br />

<table>
	<tr>
		<td>

			<fieldset>
				<legend>AIR TRANSPORTATION INFORMATION</legend>

				<table class="r" style="border: 1px solid; text-align: center;">
					<tr class="bl">
						<th>Departure / Arrival</th>
						<th width="100">Date</th>
					</tr>
					<tr>
						<td>Departure: <b>{DEPARTURE_CITY}</b></td>
						<td>{DEPARTURE_DATE}</td>
					</tr>
					<tr>
						<td>Arrival: <b>Tel-Aviv, Israel</b></td>
						<td>{ISRAEL_ARRIVAL_DATE}</td>
					</tr>
					<tr>
						<td>Departure: <b>Tel-Aviv, Israel</b></td>
						<td>{ISRAEL_DEPARTURE_DATE}</td>
					</tr>{IsEuropeTour}
					<tr>
						<td>Arrival: <b>{IsIsraelTurkey}Istanbul, Turkey{-IsIsraelTurkey}{IsIsraelRome}Rome, Italy{-IsIsraelRome}{IsIsraelAthens}Athens, Greece{-IsIsraelAthens}</b></td>
						<td>{ISRAEL_DEPARTURE_DATE}</td>
					</tr>
					<tr>
						<td>Departure: <b>{IsIsraelTurkey}Istanbul, Turkey{-IsIsraelTurkey}{IsIsraelRome}Rome, Italy{-IsIsraelRome}{IsIsraelAthens}Athens, Greece{-IsIsraelAthens}</b></td>
						<td>{ARRIVAL_DATE}</td>
					</tr>{-IsEuropeTour}
					<tr>
						<td>Arrival: <b>{DEPARTURE_CITY}</b></td>
						<td>{ARRIVAL_DATE}</td>
					</tr>
				</table>

			</fieldset>

		</td>
	</tr>
</table>

<br />

<table>
	<tr>
		<td>

			<fieldset>
				<legend>LAND TRANSPORTATION INFORMATION</legend>

				<table class="r" style="border: 1px solid; text-align: center;">
					<tr class="bl">
						<th>Day</th>
						<th width="100">Date</th>
						<th>Transportation</th>
					</tr>
					<tr>
						<td>1.</td>
						<td>{DEPARTURE_DATE}</td>
						<td>Depart from your home town and fly to Israel</td>
					</tr>
					<tr>
						<td>2.</td>
						<td>{2DAY_DATE}</td>
						<td><b>Transfer from Tel-Aviv, Israel airport to the hotel</b></td>
					</tr>
					<tr>
						<td>3.</td>
						<td>{3DAY_DATE}</td>
						<td><b>From 8:00AM to 6:00PM full day sightseeing</b></td>
					</tr>
					<tr>
						<td>4.</td>
						<td>{4DAY_DATE}</td>
						<td><b>From 8:00AM to 6:00PM full day sightseeing</b></td>
					</tr>
					<tr>
						<td>5.</td>
						<td>{5DAY_DATE}</td>
						<td><b>From 8:00AM to 6:00PM full day sightseeing</b></td>
					</tr>
					<tr>
						<td>6.</td>
						<td>{6DAY_DATE}</td>
						<td><b>From 8:00AM to 6:00PM full day sightseeing</b></td>
					</tr>
					<tr>
						<td>7.</td>
						<td>{7DAY_DATE}</td>
						<td><b>From 8:00AM to 6:00PM full day sightseeing</b></td>
					</tr>
					<tr>
						<td>8.</td>
						<td>{8DAY_DATE}</td>
						<td><b>Day of leisure</b></td>
					</tr>
					<tr>
						<td>9.</td>
						<td>{9DAY_DATE}</td>
						<td><b>Half day of sightseeing and transfer from the hotel to Tel-Aviv, Israel airport</b></td>
					</tr>{IsEuropeTour}
					<tr>
						<td>9.</td>
						<td>{9DAY_DATE}</td>
						<td>Transfer from {IsIsraelTurkey}Istanbul, Turkey{-IsIsraelTurkey}{IsIsraelRome}Rome, Italy{-IsIsraelRome}{IsIsraelAthens}Athens, Greece{-IsIsraelAthens} airport to the hotel</td>
					</tr>
				</table>

			</fieldset>

		</td>
	</tr>
</table>

<table>
	<tr>
		<td>

			<fieldset>
				<legend>LAND TRANSPORTATION INFORMATION</legend>

				<table class="r" style="border: 1px solid; text-align: center;">
					<tr class="bl">
						<th>Day</th>
						<th width="100">Date</th>
						<th>Transportation</th>
					</tr>
					<tr>
						<td>10.</td>
						<td>{10DAY_DATE}</td>
						<td><b>From 8:00AM to 6:00PM full day sightseeing</b></td>
					</tr>
					<tr>
						<td>11.</td>
						<td>{11DAY_DATE}</td>
						<td><b>Transfer from the hotel to {IsIsraelTurkey}Istanbul, Turkey{-IsIsraelTurkey}{IsIsraelRome}Rome, Italy{-IsIsraelRome}{IsIsraelAthens}Athens, Greece{-IsIsraelAthens} airport</b></td>
					</tr>
					{-IsEuropeTour}<tr>
						<td>{DAYS}.</td>
						<td>{ARRIVAL_DATE}</td>
						<td>Flight home</td>
					</tr>
				</table>

			</fieldset>

		</td>
	</tr>
</table>

<br />

<table>
	<tr>
		<td>

			<fieldset>
				<legend>ACTIVITIES & SIGHTSEEING INFORMATION</legend>

				<table class="r" style="border: 1px solid; text-align: center;">
					<tr class="bl">
						<th>Day</th>
						<th width="100">Date</th>
						<th>Activities & Sightseeing</th>
					</tr>
					<tr>
						<td>1.</td>
						<td>{DEPARTURE_DATE}</td>
						<td>Depart from your home town and fly to Israel</td>
					</tr>
					<tr>
						<td>2.</td>
						<td>{2DAY_DATE}</td>
						<td>Transfer from Tel-Aviv, Israel airport to the hotel</td>
					</tr>
					<tr>
						<td>3.</td>
						<td>{3DAY_DATE}</td>
						<td><b>Visit Caesarea, Haifa, Megiddo, Tiberias</b></td>
					</tr>
					<tr>
						<td>4.</td>
						<td>{4DAY_DATE}</td>
						<td><b>Visit Tiberias, Capernaum, Banias</b></td>
					</tr>
					<tr>
						<td>5.</td>
						<td>{5DAY_DATE}</td>
						<td><b>Visit Tiberias, Beit Shean, Bait Lehem, Jerusalem</b></td>
					</tr>
					<tr>
						<td>6.</td>
						<td>{6DAY_DATE}</td>
						<td>
							<b>Visit Jerusalem old city, St. Stephens gate, Pool of Bethesda, Western wall,
							Church of the Holy Sepulchre, Tower of David, Garden Tomb, Garden of Gethsemane</b>
						</td>
					</tr>
					<tr>
						<td>7.</td>
						<td>{7DAY_DATE}</td>
						<td><b>Visit Ancient Jericho, The good Samaritan, the Dead sea, Qumran caves, Masada</b></td>
					</tr>
					<tr>
						<td>8.</td>
						<td>{8DAY_DATE}</td>
						<td>Leisure day</td>
					</tr>
					<tr>
						<td>9.</td>
						<td>{9DAY_DATE}</td>
						<td>{NotJordan}Transfer from the hotel to Tel-Aviv, Israel airport{IsEuropeTour}, fly to {IsIsraelTurkey}Istanbul, Turkey{-IsIsraelTurkey}{IsIsraelRome}Rome, Italy{-IsIsraelRome}{IsIsraelAthens}Athens, Greece{-IsIsraelAthens}{-IsEuropeTour}{-NotJordan}{IsIsraelJordan}Transfer from the hotel to Jordan.<br /><b>Visit Jerash, Amman</b>{-IsIsraelJordan}</td>
					</tr>{NotJustIsrael}{NotJordan}
					<tr>
						<td>9.</td>
						<td>{9DAY_DATE}</td>
						<td>Transfer from {IsIsraelTurkey}Istanbul, Turkey{-IsIsraelTurkey}{IsIsraelRome}Rome, Italy{-IsIsraelRome}{IsIsraelAthens}Athens, Greece{-IsIsraelAthens} airport to the hotel</td>
					</tr>{-NotJordan}{-IsEuropeTour}
					<tr>
						<td>10.</td>
						<td>{10DAY_DATE}</td>
						<td><b>Visit
							{IsIsraelTurkey}Hagia Sophia church, Basilica Cistern, The ancient Hippodrome{-IsIsraelTurkey}
							{IsIsraelRome}Vatican city, Castel Sant'Angelo, the Tiber, Piazza Navona, Berniniís fountain, The Pantheon{-IsIsraelRome}
							{IsIsraelAthens}the Parthenon, Temple of Athena, The temple of Olympian Zeus, Royal palace, Lycabettus hill, The Flea market{-IsIsraelAthens}
							{IsIsraelJordan}Wadi Moussa, Petra, El Khazneh{-IsIsraelJordan}</b>
						</td>
					</tr>
					<tr>
						<td>11.</td>
						<td>{11DAY_DATE}</td>
						<td><b>Visit
							{IsIsraelTurkey}Topkapi palace, Kasikci diamond, Spice market, cruise along the Bosphorus{-IsIsraelTurkey}
							{IsIsraelRome}the Roman Colosseum, Barberini palace, Piazza Venezia, the Roman Forum, the citadel of Ancient Rome, Piazza del Campidoglio{-IsIsraelRome}
							{IsIsraelAthens}three Greek islands{-IsIsraelAthens}
							{IsIsraelJordan}Madaba, Mount Nebo, Byzantine church{-IsIsraelJordan}</b>
						</td>
					</tr>
					<tr>
						<td>{DAYS}.</td>
						<td>{ARRIVAL_DATE}</td>
						<td>Transfer from the hotel to {IsIsraelTurkey}Istanbul, Turkey{-IsIsraelTurkey}{IsIsraelRome}Rome, Italy{-IsIsraelRome}{IsIsraelAthens}Athens, Greece{-IsIsraelAthens}{IsIsraelJordan}Tel-Aviv, Israel{-IsIsraelJordan} airport</td>
					</tr>{-NotJustIsrael}
					<tr>
						<td>{DAYS}.</td>
						<td>{ARRIVAL_DATE}</td>
						<td>Flight home</td>
					</tr>
				</table>

			</fieldset>

		</td>
	</tr>
</table>

<br />

<table>
	<tr>
		<td width="50%" style="vertical-align: top; text-align: justify;">

			<fieldset style="padding-right: 40px; height: 300px;">
				<legend>INCLUDED IN TOUR PRICE</legend>

				<ul>
					<li>Round trip air transportation to Tel-Aviv from select international airports in your area.</li>
					<li>Meeting services and transfers between airports and hotels overseas, including porterage.</li>
					<li>Eight or ten nights in four-star hotels (double occupancy) with twin-bedded rooms and private facilities.</li>
					<li>Touring by deluxe air-conditioned / heated motor-coaches.</li>
					<li>Full breakfast and table d'hote dinners daily.</li>
					<li>Full services of experienced government licensed escort/guide throughout.</li>
					<li>Complete sightseeing with entrance fees to places visited per itinerary.</li>
				</ul>

			</fieldset>

		</td>
		<td width="50%" style="vertical-align: top; text-align: justify;">

			<fieldset style="padding-right: 40px; height: 300px;">
				<legend>NOT INCLUDED IN TOUR PRICE</legend>

				<ul>
					<li>Tips to tour guides.</li>
					<li>Tips to bus drivers.</li>
					<li>Hotel additional services.</li>
					<li>Visa fee (if applicable).</li>
				</ul>

			</fieldset>

		</td>
	</tr>
</table>

<table>
	<tr>
		<td style="text-align: justify; height: 100px; vertical-align: top;">

			I accept the rate and terms of this tour program as set forth this
			tour registration form. I have read the brochure tour terms and
			conditions, I do accept and understand them. I understated that
			the passengers on this tour must adhere to our payments policy in
			order to guarantee the tour price, late payment can result in
			paying higher air fare.

		</td>
	</tr>
</table>

<table>
	<tr>
		<td width="50%"><div class="txt">Date:&nbsp;</div><div class="fill">{TODAY_YEAR}-{TODAY_MONTH}-{TODAY_DAY}</div></td>
		<td width="50%"><div class="txt">Signature:&nbsp;</div><div class="fill">&nbsp;</div></td>
	</tr>
</table>