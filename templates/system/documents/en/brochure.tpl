<style>
	#bg {position: relative; background: url("/home/www/g/goodshepherdtour.com/backoffice/graphics/brochure/bg.png") no-repeat; background-size: 100% auto; height: 1000px;}
	#title {position: absolute; left: 90px; top: 15px; font-size: 45px;}
	#foto {position: absolute; right: 0px; top: 80px; width: 255px; height: 260px; z-index: -1;}
	#foto img {width: 100%; height: 100%;}
	#tdate {position: absolute; right: 20px; top: 350px; font-size: 23px;}
	#price {position: absolute; right: 20px; top: 380px; font-size: 28px;}

	#places {position: absolute; left: 55px; top: 465px; font-size: 20px;}
	#placesl, #placesr, #includeslist, #contact {font-size: 16px; text-align: left;}
	#placesl {position: absolute; left: 60px; top: 490px;}
	#placesr {position: absolute; left: 210px; top: 490px;}

	#programincluded {position: absolute; left: 450px; top: 465px; font-size: 20px;}
	#programincludedlist {position: absolute; left: 440px; top: 490px; font-size: 12px; width: 310px;}
	#programincludedlist ul {margin: 0px; padding: 0px 0px 0px 20px; text-align: left;}

	#contact {position: absolute; left: 450px; top: 705px; font-size: 15px;}
	#names td {width: 25%; color: #c0c0c0; font-size: 18px;}

	#main {text-align: justify; font-size: 11px;}

	.sh {text-shadow: 3px 3px 5px rgba(0, 0, 0, 1);}
	.wh {color: #ffffff;}
	.fw {width: 100%;}
	.day {color: #cfa824; font-size: 20px; font-weight: bold; text-transform: uppercase;}
	.inbox {padding: 30px;}
</style>

<div id="bg" class="fw">

	<div id="title" class="sh wh">
		<b><i>Join {FULLNAME} for<br />{DAYS} life-changing days in the</i></b>
	</div>

	<div id="foto">
		<!-- MAX WIDTH: 290px, MAX HEIGHT: 320px -->
		<img src="{PHOTO_FILE}" border="0" />
	</div>

	<div id="tcode">
		<b class="wh"></b>
	</div>

	<div id="tdate">
		<b>FROM <span class="wh">{DEPARTURE_DATE}</span> TO <span class="wh">{ARRIVAL_DATE}</span></b>
	</div>

	<div id="price">
		<b>US$ {PRICE} FROM <span class="wh">{DEPARTURE_CITY}</span>&nbsp;&nbsp;&nbsp;TOUR CODE <span class="wh">{TOUR_CODE}</span></b>
	</div>

	<div id="places" class="wh">
		<b>Sites to be visited in Israel include:</b>
	</div>

	<div id="placesl">
		Joppa<br />
		Caesarea<br />
		Meggido<br />
		Mt. Carmel<br />
		Nazareth<br />
		Sea of Galilee<br />
		Capernaum<br />
		Tiberias<br />
		Jordan River<br />
		Qumran caves<br />
		Dead Sea<br />
		Bethabara<br />
		Ein Gedi<br />
		Masada<br />
		Haifa
	</div>

	<div id="placesr">
		Jericho<br />
		Jerusalem<br />
		Mount of Olives<br />
		Temple Mount<br />
		Pool of Bethesda<br />
		Western Wall<br />
		Bethlehem<br />
		Upper Room<br />
		Pilate's Palace<br />
		Gethsemane<br />
		House of Caiaphas<br />
		Pool of Siloam<br />
		Calvary<br />
		Garden Tomb<br />
		Mount of Beatitudes
	</div>

	<div id="programincluded" class="wh">
		<b>The tour includes:</b>
	</div>

	<div id="programincludedlist">
		<ul>
			<li>Round trip air transportation to Tel-Aviv from select international airports in your area.</li>
			<li>Meeting services and transfers between airports and hotels overseas, including porterage.</li>
			<li>Eight or ten nights in four-star hotels with twin-bedded rooms and private facilities.</li>
			<li>Touring by deluxe air-conditioned / heated motor-coaches.</li>
			<li>Full breakfast and table d'hote dinners daily.</li>
			<li>Full services of experienced government licensed escort / guide throughout.</li>
			<li>Complete sightseeing with entrance fees to places visited per itinerary.</li>
		</ul>
	</div>

	<div id="contact">
		<b style="color: #cfa824; font-size: 23px">GOOD SHEPHERD TOURS INC.</b><br />
		Toll-free phone: 844-500-TOUR (8687)<br />
		International phone: +1-412-440-3292<br />
		E-mail: <a href="mailto:info@goodshepherdtour.com">info@goodshepherdtour.com</a>
	</div>

</div>

<table id="names" class="fw">
	<tr>
		<td>Caesarea Amphitheater</td>
		<td>Mount of Olives</td>
		<td>Temple Mount</td>
		<td>Jordan River</td>
	</tr>
</table>

<div id="main" class="fw">
	<div class="inbox">

		<b style="font-size: 45px">TOUR ITINERARY DAY BY DAY</b>

		<p>
			<b class="day">{DATE_DAY1} - Departure</b><br />
			We depart from the international airport nearest to your city, on overnight
			flight to Tel Aviv. Dinner and breakfast will be served.
		</p>

		<p style="page-break-after: always;">
			<b class="day">{DATE_DAY2} - Arrival</b><br />
			Afternoon arrival to Tel Aviv. Upon arrival, we will be met and transferred
			to our hotel. Check-in and accommodation in the hotel. Dinner at the hotel.
		</p>

		<p style="padding-top: 30px;">
			<b class="day">{DATE_DAY3} - Caesarea - Haifa - Megiddo - Tiberias</b><br />
			Breakfast at the hotel. Check-out of the hotel. We begin our journey northward
			by motor-coach along the Mediterranean Sea to Caesarea. This ancient city was
			built in honor of Caesar by Herod the great, and was the capital of the Romans
			in the Holy Land for nearly 500 years.
			You will see the Crusader City, and the fantastic aquaduct before continuing
			to Haifa, one of the most beautiful cities in the world.
			View the Ba'hai Gardens, one of the holiest sites of the Ba'hai religion.
			We will drive to the crest of Mt. Carmel, through the Jezreel Valley and arrive
			at the site of Megiddo, one of King Solomon's "chariot cities" and according
			to scripture, the location for the last battle in history.
			Lunch on your own, en route.
			See Gideon's brook, the "Well of Herod" and continue on to Nazareth, Jesus'
			boyhood home, where you will visit the Church of Annunciation.
			Then to Cana where he made his first miracle of turning water into wine,
			continue to Tiberias and to our hotel. Check into the Tiberias hotel. Dinner
			at the Tiberias hotel.
		</p>

		<p>
			<b class="day">{DATE_DAY4} - Tiberias - Capernaum - Banias</b><br />
			Breakfast at the hotel. There is nothing quite like witnessing the morning
			sun on the Sea of Galilee. Although there are many places which we must admit
			are traditional, you can be sure that the Lord Jesus walked along these shores.
			After breakfast (and a Sunrise service, if you choose) we return to Tiberias
			for a full day of sightseeing. Visit the Mt. of Beatitudes, traditional site
			of the Sermon on the Mount. Visit Tabgha, the Chapel dedicated to the
			multiplication of the loaves and fishes. Continue on to the ruins at Capernaum,
			then the Synagogue and the house of Simon Peter. Cross the sea of Galilee by
			a boat similar to those used in Jesus days. We end the day with a stop at the
			Jordan River, where a baptismal service is possible. Come back to our Tiberias
			hotel. Dinner at the Tiberias hotel.
		</p>

		<p>
			<b class="day">{DATE_DAY5} - Tiberias - Beit shean - Bait lehem - Jerusalem</b><br />
			Breakfast at the hotel. Check out of the hotel. This morning will begin with
			a stop at Gergesa. Then to south through Beth-Shan, the Jordan Valley, past
			Shiloh and Bethel. Our trip takes us "up into Jerusalem": continuing on into
			Bethlehem. Visit the Church of Nativity, built over the stable where Jesus
			was born and the Church of Visitation. Take some time for shopping. Check into
			our Jerusalem hotel. Dinner at the Jerusalem hotel.
		</p>

		<p>
			<b class="day">{DATE_DAY6} - Jerusalem old City - St. Stephens Gate<br />
			Pool of Bethesda - Western Wall - Church of the Holy Sepulchre<br />
			Tower of David - Garden Tomb - Garden of Gethsemane</b><br />
			Breakfast at the hotel. The high point of our entire tour. Enter the Old City
			through the Damascus Gate and begin a walking tour which takes us through St.
			Stephens Gate to the Pool of Bethesda and Saint Anne’s Church. Continue to the
			Temple area of Mt. Moriah. Visit the Dome of the Rock area (entry to the Mosque
			not included) and the Western Wall, also know as the "Wailing Wall" which is
			the only remaining portion of the original temple built by Solomon. Walk through
			the Old City Bazaars to the church of the Holy Sepulchre. View the Tower of
			David, the Citadel and King David's Tomb. Visit the Jewish Quarter before we
			leave the Old City through Jaffa Gate. We visit Mt. Zion and the Upper Room, then
			onto the Garden Tomb and Golgotha, place of the skull, where a worship service
			is possible. Our evening ends with a visit to the Garden of Gethsemane and with
			a trip to the Mt. of Olives for a spectacular view of the city. Come back to
			our Jerusalem hotel. Dinner at the Jerusalem hotel.
		</p>

		<p>
			<b class="day">{DATE_DAY7} - Ancient Jericho - The Good Samaritan<br />
			The Dead Sea - Qumran caves - Masada</b><br />
			Breakfast at the hotel. We begin with an early morning departure today as we
			head south from Jerusalem. See the ruins of ancient Jericho, stop at the Inn
			of the Good Samaritan, drive through the modern city of Jericho and continue
			to the Dead Sea, 1300 feets below sea level, the lowest spot on Earth. See the
			Qumran caves where the Dead Sea scrolls were found. Then continue to Masada.
			Ascend the mountain by cable car to the remarkable ruins of the fortress built
			by Herod the Great. From 70-73 AD, Jewish Zealots made their last stand
			against the Romans. Come back to our Jerusalem hotel. Dinner at the Jerusalem
			hotel.
		</p>

		<p>
			<b class="day">{DATE_DAY8} - Jerusalem</b><br />
			Breakfast at the hotel. Today we will have a full day of leisure to revisit
			favorite spots in Jerusalem. Explore some of the recent excavations or try
			your hand at bartering at the famous bazaars of the Old City. Our guide will
			have several options available for those who prefer to visit sites not on
			the "typical" tour, so prepare for this exciting day and make the most of it!
			Come back to our Jerusalem hotel. Dinner at the Jerusalem hotel.
		</p>

		<p>
			<b class="day">{DATE_DAY9} - Jerusalem / {IsIsraelJesus}Europe - USA{-IsIsraelJesus}{IsIsraelTurkey} Istanbul{-IsIsraelTurkey}{IsIsraelRome}Rome{-IsIsraelRome}{IsIsraelAthens}Athens{-IsIsraelAthens}{IsIsraelJordan}Jordan - Jerash{-IsIsraelJordan}</b><br />
			Breakfast at the hotel. {IsIsraelJesus}Today is your departure day from
			Israel. Most groups will enjoy a leisurely morning in Jerusalem and transfer
			to the airport mid-afternoon for an early evening flight. These groups will
			overnight in their European gateway city. If you are on a very late night
			flight from Israel, you will enjoy a full day of leisure to explore Israel,
			relax in this fascinating country. After dinner at the hotel, we have a
			late check out and transfer to the airport for our flight home via your
			European gateway (with no overnight). Check your flight itinerary for
			details regarding your group itinerary. Check-out of Jerusalem hotel.
			Dinner at the Jerusalem hotel.{-IsIsraelJesus}
			{IsIsraelTurkey}Check-out of Jerusalem hotel. Enjoy a
			leisurely morning in Jerusalem before transferring to the airport where we
			will bid "Shalom" to Israel, and board our flight to Istanbul. Arrive at
			Istanbul’s Atatürk International Airport. After customs, we will be met
			by our local representative and transferred to the hotel, enjoying a
			panaromic city tour en route. Check to Istanbul hotel. Dinner at the
			Istanbul hotel.{-IsIsraelTurkey}
			{IsIsraelRome}Check-out of Jerusalem hotel. Enjoy a
			leisurely morning in Jerusalem before transferring to the airport where we
			will bid "Shalom" to Israel, and board our flight to Rome. Arrive at Rome's
			Fumincino Airport. After customs, we will be met by our local representative
			and transferred to the hotel, enjoying a panaromic city tour en route. Check
			to Rome hotel. Dinner at the Rome hotel.{-IsIsraelRome}
			{IsIsraelAthens}Check-out of Jerusalem hotel. Enjoy a leisurely morning
			in Jerusalem before transferring to the airport where we will bid "Shalom"
			to Israel, and board our flight to Athens. Arrive at Athen's International
			Airport. After customs, we will be met by our local representative and
			transferred to the hotel, enjoying a panaromic city tour en route. Come back
			to our Athens hotel. Dinner at the Athens hotel.{-IsIsraelAthens}
			{IsIsraelJordan}Check-out of Jerusalem hotel. Drive from Tel Aviv / Jerusalem
			to the Israel / Jordanian border at the Allenby or Sheik Hussein Bridge to
			enter Jordan. Proceed to Jerash, located about 45 kilometers north of Amman,
			in the fertile heights of the Gilad. Visit the ancient Roman city with its
			colonnades and baths. Drive to Amman for a city tour of the Jordanian capital.
			Check to Amman hotel. Dinner at the Amman hotel.{-IsIsraelJordan}
		</p>

		<p{NotJustIsrael}{IsIsraelRome} style="page-break-after: always;"{-IsIsraelRome}{IsIsraelAthens} style="page-break-after: always;"{-IsIsraelAthens}{-NotJustIsrael}>
			<b class="day">{DATE_DAY10} - {IsIsraelJesus}Arrive USA{-IsIsraelJesus}
			{IsIsraelTurkey}ISTANBUL - HAGIA SOPHIA CHURCH<br />
			BASILICA CISTERN - THE ANCIENT HIPPODROME{-IsIsraelTurkey}
			{IsIsraelRome}ROME - VATICAN CITY - CASTEL SANT' ANGELO<br />
			THE TIBER - PIAZZA NAVONA - BERNINI’S FOUNTAIN - THE PANTHEON{-IsIsraelRome}
			{IsIsraelAthens}ATHENS – THE PARTHENON<br />TEMPLE OF ATHENA - THE TEMPLE OF OLYMPIAN ZEUS - ROYAL PALACE
			<br />LYCABETTUS HILL - THE FLEA MARKET{-IsIsraelAthens}
			{IsIsraelJordan}Jerash - Wadi Moussa - Petra<br />mount nebo{-IsIsraelJordan}</b><br />
			{IsIsraelJesus}Breakfast at the flight. For those who had an overnight in
			an European city, you will be transferred to the airport for your flight
			back home. Groups with late night flights from Tel Aviv will arrive USA
			early afternoon. SHALOM!{-IsIsraelJesus}
			{IsIsraelTurkey}Breakfast at the hotel. Today we begin with a visit to the
			world famous Hagia Sophia Church which is now a museum. Constructed by Emperor
			Justinianos as a church at the beginning of the 6th Century, Hagia Sophia
			remained the largest cathedral in the world for almost a thousand years and is
			considered the 8th Wonder of the World. The Church was converted into a mosque
			in 1453 and subsequently into a museum in 1935. Continue to the cascading domes
			and six slender minarets of the Sultan Ahmet Mosque (better known as the "Blue
			Mosque"), which dominates the skyline of Istanbul. In the 17th century, Sultan
			Ahmet I. wished to build an Islamic place of worship that would be even better
			than the Hagia Sophia, and the mosque named for him is the result. The two great
			architectural achievements now stand next to each other in Istanbul's main square,
			and it is up to visitors to decide which is more impressive. The Blue Mosque was
			commissioned by Sultan Ahmet when he was only 19 years old. It was built near the
			Hagia Sophia, over the site of the ancient hippodrome and Byzantine imperial palace
			(whose mosaics can be seen in the nearby Mosaic Museum). Construction work began
			in 1609 and took seven years. The mosque was designed by architect Mehmet Aga.
			Sultan Ahmet was so anxious for his magnificent creation to be completed that he
			often assisted in the work. Sadly, he died just a year after the completion of
			his masterpiece, at the age of 27. He is buried outside the mosque with his wife
			and three sons. The original mosque complex included a madrasa, a hospital, a han,
			a primary school, a market, an imaret and the tomb of the founder. Most of these
			buildings were torn down in the 19th century. Walking through the ancient
			Hippodrome where the chariot races took place in antiquity, we will get to the
			Basilica Cistern, a wonderfully serie underground chamber built by Justinian in
			537 and comprised of 336 columns. Come back to our Istanbul hotel. Dinner at the
			Istanbul hotel.{-IsIsraelTurkey}
			{IsIsraelRome}Breakfast at the hotel. A full day of touring today begins at the
			center of Catholicism – Vatican City. We arrive at the Piazza San Pietro, and
			enter the Basilica of St. Peter, the symbol of Christianity and the largest church
			in the world covering 22,000 square meters, equivalent to 5-1/2 acres of land.
			Michelangelo’s dome is 137 ft, wide, and the entire building rises to a height of
			446 ft. Countless masterpieces are housed here, including Bernini’s bronze
			Baldacchino and Michelangelo’s Pietà. The Vatican Museum offers innumerable
			paintings, highlighted by one of the cornerstones of Italian art, Michelangelo’s
			Sistine Chapel. From Vatican City, we enjoy a short walk to Castel Sant'Angelo,
			one of the town's most famous landmarks. Originally built as a mausoleum, Castel
			Sant'Angelo was transformed into an fortress in the Middle Ages that defended the
			northern entrance of the city. The popes commissioned the construction of a fortified
			corridor connected to the Vatican Palaces, to be used as an escape route. The
			castle guarded the riches of the Popes, and was used to store enormous reserves of
			food in the event of an attack, housing water tanks, granaries and even a mill.
			Castel Sant' Angelo was notorious as the scene of executions by decapitation, where
			the heads of the condemned were hung for days along the bridge as a warning. From
			here, we cross the Tiber, and arrive at Piazza Navona, built on top of the remains
			of Domitian’s Stadium. Here we see three famous fountains, the most important of
			which is Bernini’s Fountain of the Four Rivers (the Nile, Ganges, Danube and Rio
			de la Plata). The Stadium, built before 86 A.D., was the favorite spot to hold
			games, tournaments and processions, and etween the seventeenth and nineteen
			centuries, the piazza was often flooded for aquatic games and to stage naval battles.
			Continue to the Pantheon, the finest example of architecture of ancient Rome. Here
			lies the tombs of renaissance painter and architect Raphael, baroque painter
			Annibale Carracci and kings Vittorio Emanuele II. and Umberto I. Today’s tour ends
			at the Piazza di Spagna, renowned for Bernini’s fountain and the celebrated Spanish
			Steps. Creating a refined, eighteenth century atmosphere, it is no coincidence that
			high-fashion brands, such as Gucci, Bulgari and Valentino, all have show-piece shops
			just off the piazza. Home to English poets Keats and Shelley, the world-famous piazza,
			located at the foot of the Pincio Hill, has always been considered both a priceless
			cultural gem as well as popular tourist attraction. Come back to our Rome hotel. Dinner
			at the Rome hotel.{-IsIsraelRome}
			{IsIsraelAthens}Breakfast at the hotel. This morning, our full day of touring begins
			with a visit to the “sacred rock”, the Acropolis which is the most photographed site
			in all of Greece. Here you will see the Parthenon that was dedicated to the goddess
			Athena, Propylea, the monumental entrance to the sacred area, the temple
			of Athena Nike which was dedicated to Victory, the Erechtheion with its famous female
			statues, the Maidens dedicated to both goddess Athena and god Poseidon, the Odeum of
			Herodus Atticus and the ruins of the first theatre in the whole world - the Theatre
			of Dionysus, and finally the Areopagus (Mars Hill). We will proceed to the Temple of
			Olympian Zeus and then to Panathenaic or Kallimarmaro Stadium which hosted the first
			modern Olympic Games in 1896. We will drive by the Prime Minister’s Residence, the
			former Royal Palace, today’s Presidential Mansion and the House of Parliament with
			the Tomb of the Unknown Soldier for a brief visit and to watch the changing of the
			Guard. We continue our tour by driving up to the highest point of Athens, Lycabettus
			Hill, which offers the best 360 degree panoramic view of the city. From here we proceed
			to the oldest and most picturesque neighborhood of Athens, Plaka with it’s stone-paved
			narrow streets, traditional Greek taverns - best gyros - and antique and souvenir stores.
			After lunch in Plaka we’ll drive through the city centre to the Flea Market, where we’ll
			visit the Ancient Agora (the political, financial and commercial centre of ancient Athens).
			There we’ll see the temple of Hephaistos, known also as Theseum (best preserved ancient
			temple in Greece) and the Stoa of Attalos (an ancient shopping centre) which was restored
			in the fifties by the American Archaeological School. We end the day with a visit to the
			National Archaeological Museum, which has one of the most extensive and important
			archaeological collections in the world. It houses, among other important exhibits, the
			frescoes of the island of Santorini and the prehistoric golden collection of Mycenae
			with the famous mask of Agamemnon. Come back to our Athens hotel. Dinner at the Athens
			hotel.{-IsIsraelAthens}
			{IsIsraelJordan}Breakfast at the hotel. After an early breakfast, we leave the hotel
			and travel south on the road to Wadi Moussa. Then on horseback we travel to the "Siq"
			(canyon), before traveling by foot to Petra, known as "Sela Edom" or Red Rock City,
			the ancient capital of the Nabateans from the 3rd Century BC through the 2nd Century AD.
			Visit the most interesting monuments, such as the Treasury, El Khazneh (a tomb of a
			Nabatean king), the field of tombs. Al Madhbah (the altar) where you can view the entire
			city. Reutrn to our Amman hotel. Dinner at the Amman hotel.{-IsIsraelJordan}
		</p>

		{NotJustIsrael}<p{IsIsraelRome} style="padding-top: 30px;"{-IsIsraelRome}{IsIsraelAthens} style="padding-top: 30px;"{-IsIsraelAthens}>
			<b class="day">{DATE_DAY11} - {IsIsraelTurkey} Istanbul - Topkapi Palace<br />
			Kasikci Diamond - Spice Market - Cruise along the Bosphorus{-IsIsraelTurkey}
			{IsIsraelRome}The Roman Colosseum - Barberini palace<br />Piazza Venezia - The Roman Forum
			- The Citadel of Ancient Rome<br />Piazza del Campidoglio{-IsIsraelRome}{IsIsraelAthens}ATHENS - VISIT THREE GREEK ISLANDS{-IsIsraelAthens}{IsIsraelJordan}MOUNT NEBO{-IsIsraelJordan}</b><br />
			{IsIsraelTurkey}Breakfast at the hotel. Today’s touring begins with a visit to the
			magnificent Topkapi Palace, the residence of the Ottoman Sultans for 400 years.
			Enjoy the exquisite collection of Chinese porcelain, the stunning Imperial Treasury
			including the "Spoonmaker's Diamond" aka the Kasikci Diamond, which is the third
			largest diamond in the world and the Topkapi Dagger. We leave the Palace and view
			the Egyptian Obelisks, which are the oldest historical works in Istanbul. These
			stones, which were made of pink granite, were constructed in year 390 in the era
			of Theodosius. Visit the Spice Market, followed by a cruise along the Bosphorus,
			which separates Asia from Europe. Our final stop is the Grand Covered Bazaar with
			4000 tiny shops. Come back to our Istanbul hotel. Dinner at the Istanbul hotel.{-IsIsraelTurkey}
			{IsIsraelRome}Breakfast at the hotel. Today's touring begins at the Roman Colosseum,
			one of the most imposing of ancient structures completely covered intravertine stone
			slabs. The amphitheater could hold up to seventy thousand spectators and the seats
			were inclined in such a way as to enable a perfect view from wherever one sat. Entry
			was free for all Roman citizens, but places were divided according to social status.
			Like modern sports stadiums, the Colosseum gave spectators efficient protection from
			the sun thanks to its ingenious roof covering, an enormous linen tarpaulin hung by
			a system of ropes and wooden poles that took one hundred sailors from the Imperial
			fleet to move it in perfect synchrony to the beating of a drum. Today, we see the
			skeleton of what was the greatest arena in the ancient world. Three-fifths of the
			outer surrounding brick walls are missing, used in the Middle Ages, as an enormous
			quarry to build Barberini Palace, Piazza Venezia and even St. Peter's. On entering,
			we see the arena straight ahead of us. The stage floor has now disappeared and in
			its place you can see the cellars. Two underground floors housed the lifts and hoists
			with counter weights, creating special effects used to hoist up animals and
			gladiators who burst into the arena through trapdoors in a burst of white dust,
			giving the audience great surprise effects. After the VI. century, with the Empire's
			decline, the Colosseum fell into disusre, and housed jails, hospitals, hermits and
			even a cemetery. Threatened with demolition by Sixtus V., it was declared a sacred
			monument dedicated to the Passion of Christ by Benedict XIV. Since then, it has
			become an object of worship and was protected from further destruction and ruin.
			Situated between Piazza Venezia and the Colosseum, we arrive at the Roman Forum,
			one of the most important archaeological sites in the world. Three thousand years
			ago, this valley between Campidoglio and the Quirinal was submerged in marshland.
			By an incredible invention of engineering, a canal that is still in function to
			this very day allowed for the drainage of the land. The area soon began to develop
			and by the end of the 7th century BC, it was the heart and soul of city life. It was
			in Caesar's time, when Rome had become the capital of a vast empire, that the
			Forum became a place for celebrations. The Citadel of ancient Rome, is a must for
			every visitor. A broad flight of steps (the Cordonata) leads up to Michelangelo's
			spectacular Piazza del Campidoglio. The most incredible panoramic view of the entire
			Forum complex can be seen from the magnificent Terraces of Campidoglio (Capital Hill).
			Here you can observe the imposing ruins of Basilica Emilia, the only remaining
			Republican basilica, or the Curia, which was once the seat of the Senate. This is
			flanked by the Palazzo Nuovo and Palazzo dei Conservatori, housing the Capitoline
			Museum with their fine collections of sculptures and paintings. We will take some
			time to view more than 200 paintings from the fourteenth to eighteenth centuries
			that are on display here, including those by Titian, Pietro da Cortona, Caravaggio,
			Guercino, Rubens and others. Taking centre stage in the piazza itself is a replica
			of the bronze statue of the Roman Emperor Marcus Aurelius. After a fascinating
			day in Rome come back to our Rome hotel. Dinner at the Rome hotel.{-IsIsraelRome}
			{IsIsraelAthens}Breakfast at the hotel. Today, enjoy a full day of leisure exploring
			Athens on your own, visiting the Port of Piraeus or walking along the Mediterranean
			Sea. An optional cruise of three Greek islands is available, and is very worthwhile
			for the first-time traveler to Greece. Visit three Greek Islands in one day on an
			island-hopping cruise from Athens! This day of touring includes visits to the islands
			of Hydra, Poros and Egina, and includes a delicious Greek lunch and hydrofoil boat
			transfers. Sightsee, shop, swim or join exciting excursions at additional expense at
			each island. Meet your guide in central Athens and head to Piraeus Port by luxury,
			air-conditioned coach. You’ll set sail from here for the island of Poros, roughly two
			hours away, during which the cruise hostess will provide information about the 3 islands.
			Arrive in Poros, the smallest of the three islands, and enjoy 1-hour of free time to
			wander around this endearing island. Visit Trinzia, Poros’ Russian dockyard, and learn
			about Greece’s cooperation with the Russian army in the 18th and 19th centuries or
			take a stroll in the Lemon Forest and find pretty windmills and waterfalls as you walk
			through the densely wooded area of lemon and orange trees. Back on ship, enjoy lunch
			(first sitting) while sitting comfortably on the lounge and admiring the views of the
			Mediterranean Sea during our 1 hour trip to the second island, Hydra. We enter the
			port of Hydra, whose amphitheater shape once served as a safe shelter for Pirates.
			Hydra is best known for its colorful capital, full of quaint, narrow alleys, charming
			red-tiled houses and mansions silently standing witness to a long and turbulent history.
			Your guide will point out the best shops and cafes, and then leave you to explore
			independently for approximately 2 hours. When you return back on board, relax on the
			ship's deck and enjoy the warm breeze as you sail to your last stop, the larger island
			of Egina. Upon arrival, you can either join a sightseeing tour to the Temple of Aphaia
			or stroll around the main town at leisure. Also known as Port of Aegina, the island is
			a bustling one, with ferries, catamarans and flying dolphins dotting the harbor. Soak
			up the sights or perhaps relax in one of the town’s popular cafes. The journey back
			to Piraeus takes approximately one hour. A traditional Greek folk show with singers
			and dancers in original costumes will entertain and animate you up to the final moment
			as the ship docks in Piraeus. On arrival, you’ll be transferred the short distance
			back to central Athens where your tour ends. Return to Athens hotel. Dinner at the
			Athens hotel.{-IsIsraelAthens}
			{IsIsraelJordan}Breakfast at the hotel. Check out of our hotel. After breakfast, we
			drive to Madaba to see the ancient mosaic map of the Holy Land and ruins of historical
			sites. Continue to Mount Nebo, from where Moses viewed the Promised Land. From there
			you will see the Jordan Vally, Jericho, the Dead Sea, and more... Visit the remains
			of a Byzantine church with a mosaic floor, and then drive to the border and to
			Jerusalem or Tel Aviv. Check in to our Israel hotel. Dinner at the Israel hotel.
			{-IsIsraelJordan}
		</p>

		<p>
			<b class="day">{DATE_DAY12} - {IsIsraelTurkey}Istanbul{-IsIsraelTurkey}{IsIsraelRome}ROME{-IsIsraelRome}{IsIsraelAthens}ATHENS{-IsIsraelAthens}{IsIsraelJordan}JORDAN{-IsIsraelJordan} - USA</b><br />
			Breakfast at the hotel. Check out of our hotel.
			{IsIsraelTurkey}An early departure takes us from our hotel to the airport for our
			continuing flight home to the USA. We return home exhausted but exhilarated by 12
			days exploring Israel and Istanbul.{-IsIsraelTurkey}
			{IsIsraelRome}Today, we bid “Arrivederci” to Rome, transferring to Fumincino
			Airport for our flight home. We arrive in the USA in the early afternoon, exhausted
			but exhilarated from twelve days exploring the life of Christ, and the life of
			Ancient Rome.{-IsIsraelRome}
			{IsIsraelAthens}An early morning transfer takes us back to Athens International
			Airport for our flight home. We bid a fond “Yassas” to Greece, and board our flight.
			Arriving in the USA early afternoon – exhausted but exhilarated by 12 wonderful
			days retracing the footsteps of Jesus and learning about early Greek
			culture.{-IsIsraelAthens}
			{IsIsraelJordan}For those who had an overnight in an European city, you will be
			transferred to the airport for your flight back home. Groups with late night
			flights from Tel Aviv will arrive USA early afternoon.{-IsIsraelJordan}

		</p>{-NotJustIsrael}

	</div>
</div>