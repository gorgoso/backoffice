<!DOCTYPE html>
<html lang="{LANGUAGE}" ng-app="myApp">

<head>
	<title>BACKOFFICE - {LANG_TXT_PAGE_TITLE}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="author" content="ROOTSHELL E.I.R.L. (info@rootshell.do)">
	<link rel="icon" href="/favicon.ico">
	<link rel="stylesheet" href="{PATH_ADMIN}environment/screen.css" media="screen">
	<link rel="stylesheet" href="{PATH_ADMIN}environment/print.css" media="print">
	<link rel="stylesheet" href="{PATH_ADMIN}environment/jquery-ui.css">
	<link rel="stylesheet" href="{PATH_ADMIN}environment/css/font-awesome.min.css">
	<script src="{PATH_ADMIN}environment/jquery.js"></script>
	<script src="{PATH_ADMIN}environment/jquery-ui.js"></script>
	<script src="{PATH_ADMIN}environment/library.js"></script>
	<script src="{PATH_ADMIN}environment/angular.min.js"></script>
	<script src="{PATH_ADMIN}environment/app.js"></script>
</head>

<body>

	<div id="content">
		<div id="head">

			{IsAuth}
			<ul id="menu">
				{ShowClients}<li>
					<a {IsClients}class="menu_item_on"{-IsClients} href="{PATH_ADMIN}clients/list/" title="{LANG_TXT_CLIENTS}">
						<span title="{LANG_TXT_CLIENTS}" class="clients"></span> {LANG_TXT_CLIENTS}
					</a>
					<ul class="submenu">
						<li><a href="{PATH_ADMIN}clients/list/"><span title="{LANG_TXT_CLIENTS_LIST}"></span> {LANG_TXT_CLIENTS_LIST}</a></li>
						<li><a href="{PATH_ADMIN}clients/add/"><span title="{LANG_TXT_CLIENTS_ADD}"></span> {LANG_TXT_CLIENTS_ADD}</a></li>
					</ul>
				</li>{-ShowClients}
				{ShowTours}<li>
					<a {IsTours}class="menu_item_on"{-IsTours} href="{PATH_ADMIN}tours/list/" title="{LANG_TXT_TOURS}">
						<span title="{LANG_TXT_TOURS}" class="tours"></span> {LANG_TXT_TOURS}
					</a>
					<ul class="submenu">
						<li><a href="{PATH_ADMIN}tours/list/"><span title="{LANG_TXT_TOURS_LIST}"></span> {LANG_TXT_TOURS_LIST}</a>
						</li>
						<li><a href="{PATH_ADMIN}tours/add/"><span title="{LANG_TXT_TOURS_ADD}"></span> {LANG_TXT_TOURS_ADD}</a>
						</li>
						<li><a href="{PATH_ADMIN}tours/prices/"><span title="{LANG_TXT_TOURS_PRICES}"></span> {LANG_TXT_TOURS_PRICES}</a>
						</li>
					</ul>
				</li>{-ShowTours}
				{ShowMarketing}<li>
					<a {IsMarketing}class="menu_item_on"{-IsMarketing}href="{PATH_ADMIN}marketing/list/" title="{LANG_TXT_MARKETING}">
						<span title="{LANG_TXT_MARKETING}" class="marketing"></span> {LANG_TXT_MARKETING}
					</a>
					<ul class="submenu">
						<li><a href="{PATH_ADMIN}marketing/leads/"><span></span> {LANG_TXT_MARKETING_LEADS}</a></li>
						<li><a href="{PATH_ADMIN}marketing/list/"><span></span> {LANG_TXT_MARKETING_LIST}</a></li>
					</ul>
				</li>{-ShowMarketing}
				{ShowTasks}<li>
					<a {IsTasks}class="menu_item_on"{-IsTasks}href="{PATH_ADMIN}tasks/list/" title="{LANG_TXT_TASKS}">
						<span title="{LANG_TXT_TASKS}" class="tasks{TasksPending}-pending{-TasksPending}"></span> {LANG_TXT_TASKS} {TasksPending}({TASKS_PENDING}){-TasksPending}
					</a>
					<ul class="submenu">
						<li><a href="{PATH_ADMIN}tasks/list/"><span title="{LANG_TXT_TASKS_LIST}"></span> {LANG_TXT_TASKS_LIST}</a></li>
						<li><a href="{PATH_ADMIN}tasks/add/"><span title="{LANG_TXT_TASKS_ADD}"></span> {LANG_TXT_TASKS_ADD}</a></li>
					</ul>
				</li>{-ShowTasks}
				{ShowReports}<li>
					<a {IsReports}class="menu_item_on"{-IsReports}href="{PATH_ADMIN}reports/summary/" title="{LANG_TXT_REPORTS}">
						<span title="{LANG_TXT_REPORTS}" class="reports"></span> {LANG_TXT_REPORTS}
					</a>
					<ul class="submenu">
						<li>
						<a href="{PATH_ADMIN}reports/summary/"><span title="{LANG_TXT_SUMMARY_REPORT}"></span> {LANG_TXT_SUMMARY_REPORT}</a>
						</li>
						<li>
						<a href="{PATH_ADMIN}reports/tours-passengers/"><span title="{LANG_TXT_TOURS_PASSENGER_REPORT}"></span> {LANG_TXT_TOURS_PASSENGER_REPORT}</a>
						</li>
						<li>
						<a href="{PATH_ADMIN}reports/passengers-pay/"><span title="{LANG_TXT_TOURS_PASSENGERS_PAYMENTS}"></span> {LANG_TXT_TOURS_PASSENGERS_PAYMENTS}</a>
						</li>
						<li>
						<a href="{PATH_ADMIN}reports/rooming/"><span title="{LANG_TXT_TOURS_PASSENGERS_ROOMING}"></span> {LANG_TXT_TOURS_PASSENGERS_ROOMING}</a>
						</li>

					</ul>
				</li>{-ShowReports}
				{ShowSystem}<li>
					<a {IsSystem}class="menu_item_on"{-IsSystem}href="{PATH_ADMIN}users/list/" title="{LANG_TXT_SYSTEM}">
						<span title="{LANG_TXT_SYSTEM}" class="system"></span> {LANG_TXT_SYSTEM}
					</a>
					<ul class="submenu">
						<li><a href="{PATH_ADMIN}system/users/"><span title="{LANG_TXT_USERS}"></span> {LANG_TXT_USERS}</a></li>
						<li><a href="{PATH_ADMIN}system/groups/"><span title="{LANG_TXT_GROUPS}"></span> {LANG_TXT_GROUPS}</a></li>
						<li><a href="{PATH_ADMIN}system/privileges/"><span title="{LANG_TXT_PRIVILEGES}"></span> {LANG_TXT_PRIVILEGES}</a></li>
					</ul>
				</li>{-ShowSystem}

				<li style="float:right; width: 150px">
					<a href="{PATH_ADMIN}logout/" title="{LANG_TXT_LOGOUT}">
						<span title="{LANG_TXT_LOGOUT}" class="logout"></span> {USER_ROLE}: {USERNAME}
					</a>
					<ul class="submenu">
						<li><a href="{PATH_ADMIN}password/"><span title="{LANG_TXT_PASSWORD}"></span> {LANG_TXT_PASSWORD}</a></li>
						<li><a href="{PATH_ADMIN}logout/"><span title="{LANG_TXT_LOGOUT}"></span> {LANG_TXT_LOGOUT}</a></li>
					</ul>
				</li>

			</ul>
			{-IsAuth}

		</div>

		<div id="inner_content">
			<div id="inner_padding">
			<h1>{LANG_TXT_PAGE_HEADER}</h1>