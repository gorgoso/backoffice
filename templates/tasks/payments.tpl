<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	{ShowLoanSearch}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}calls/search/">{LANG_TXT_LOAN_SEARCH}</a>
	</div>{-ShowLoanSearch}

	<div class="right">
		<div class="tab">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a>{LANG_TXT_PENDING_CALLS}: <b>{PENDING_COUNT}</b>, {LANG_TXT_PENDING_AMOUNT}: <b>{PENDING_AMOUNT} DOP (Pesos)</b><!--, {LANG_TXT_LEGAL_AMOUNT}: <b>{LEGAL_AMOUNT} DOP (Pesos)</b>--></a>
		</div>
	</div>

</div>

<form action="{PATH_ADMIN}tasks/payments/" method="post">
<table cellpadding="0" cellspacing="0" class="classic">
	<tr class="title">
		<td>&nbsp;</td>
		<td><a href="{PATH_ADMIN}tasks/payments/?order=C.client_id&amp;sort={SORT}">{LANG_TXT_CLIENT_ID}</a></td>
		<td><a href="{PATH_ADMIN}tasks/payments/?order=CL.firstname&amp;sort={SORT}">{LANG_TXT_FIRSTNAME}</a></td>
		<td><a href="{PATH_ADMIN}tasks/payments/?order=CL.lastname&amp;sort={SORT}">{LANG_TXT_LASTNAME}</a></td>
		<td><a href="{PATH_ADMIN}tasks/payments/?order=I.assessor_id&amp;sort={SORT}">{LANG_TXT_ASSESSOR}</a></td>
		<td><a href="{PATH_ADMIN}tasks/payments/?order=I.amount&amp;sort={SORT}">{LANG_TXT_TOTAL}</a></td>
		<td><a href="{PATH_ADMIN}tasks/payments/?order=C.execution_date&amp;sort={SORT}">{LANG_TXT_NEXT_CALLDATE}</a></td>
		<td><a href="{PATH_ADMIN}tasks/payments/?order=C.last_calldate&amp;sort={SORT}">{LANG_TXT_LAST_CALLDATE}</a></td>
		<td><a href="{PATH_ADMIN}tasks/payments/?order=C.call_status&amp;sort={SORT}">{LANG_TXT_STATUS}</a></td>
	</tr>
	<tr class="title">
		<td>&nbsp;</td>
		<td><input type="text" onclick="" style="width:100px" name="filter[C.client_id]" value="{CLIENT_ID}" /></td>
		<td><input type="text" style="width:150px" name="filter[CL.firstname]" value="{FIRSTNAME}" /></td>
		<td><input type="text" style="width:150px" name="filter[CL.lastname]" value="{LASTNAME}" /></td>
		<td><select name="filter[I.assessor_id][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{ASSESSORES}</select></td>
		<td><input type="text" style="width:150px" name="filter[I.amount]" value="{AMOUNT}" /></td>
		<td colspan="2">&nbsp;</td>
		<td><select name="filter[C.call_status][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{STATUSES}</select></td>
	</tr>
		{CALLS}
	<tr class="list last"><td colspan="9"><div class="count">{COUNT}</div>{LIST}</td></tr>
</table>

<div id="btnfilter">
	<input type="submit" value="{LANG_TXT_SET_FILTER}" class="submit" />
	<input type="submit" value="{LANG_TXT_RESET_FILTER}" class="reset" name="reset" />
</div>

</form>