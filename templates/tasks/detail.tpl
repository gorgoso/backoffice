<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a>{IsNotPayment}<b>{LANG_TXT_TASK_TYPE}:</b> {TASK_TYPE_TXT}{-IsNotPayment}{IsClient}{IsNotPayment}, {-IsNotPayment}<b>{LANG_TXT_CLIENT_ID}:</b> {CLIENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {FIRSTNAME} {LASTNAME}{-IsClient}</a>
	</div>
	{IsClient}{ShowClient}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}clients/detail/{CLIENT_ID}/">{LANG_TXT_CLIENT_DETAIL}</a>
	</div>{-ShowClient}
	{ShowHistory}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}clients/history/{CLIENT_ID}/">{LANG_TXT_CLIENT_HISTORY}</a>
	</div>{-ShowHistory}{-IsClient}
	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}tasks/{FROM}/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}tasks/detail/{TASK_ID}/?from={FROM}" method="post">

<table class="classic">
	{IsNotPayment}<tr class="first">
		<td class="title" width="30%">{LANG_TXT_ADD_USER}</td>
		<td>{ADD_USER_TXT}</td>
	</tr>{-IsNotPayment}
	<tr{IsPayment} class="first"{-IsPayment}>
		<td class="title"{IsPayment} width="30%"{-IsPayment}>{LANG_TXT_ADD_DATE}</td>
		<td>{ADD_DATE_TXT}</td>
	</tr>
	{IsNotPayment}<tr>
		<td class="title"><br />{LANG_TXT_USER}</td>
		<td><br /><b>{USER_TXT}</b></td>
	</tr>{-IsNotPayment}
	<tr>
		<td class="title">{LANG_TXT_EXECUTION_DATE}<br /><br /></td>
		<td><b>{IsExpired}<span class="red">{-IsExpired}{EXECUTION_DATE_TXT}{IsExpired}</span>{-IsExpired}</b><br /><br /></td>
	</tr>
	{IsCall}<tr>
		<td class="title">{LANG_TXT_LAST_CALLDATE}</td>
		<td>{LAST_CALLDATE_TXT}</td>
	</tr>
	<tr class="last-bd">
		<td class="title">{LANG_TXT_STATUS}</td>
		<td><b>{STATUS_TXT}</b></td>
	</tr>{-IsCall}
</table>

{IsDescription}<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_TASK_INFO}</b></a>
	</div>
</div>

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_SUBJECT}</td>
		<td><b>{SUBJECT_TXT}</b></td>
	</tr>
	<tr class="last">
		<td class="title"><br />{LANG_TXT_DESCRIPTION}<br /><br /></td>
		<td><br />{DESCRIPTION_TXT}<br /><br /></td>
	</tr>
</table>{-IsDescription}

<br />

{IsCall}<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CONTACTS}</b></a>
	</div>
</div>

<table class="classic">
	{IsNotContacts}<tr class="last">
		<td class="title" width="30%">&nbsp;</td>
		<td><br />{LANG_TXT_NO_CONTACTS}<br /><br /></td>
	</tr>{-IsNotContacts}
	{IsContacts}{CONTACTS}{-IsContacts}
</table>

<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_REFERENCES}</b></a>
	</div>
</div>

<table class="classic">
	{IsNotReferences}<tr class="last">
		<td class="title" width="30%">&nbsp;</td>
		<td colspan="2"><br />{LANG_TXT_NO_REFERENCES}<br /><br /></td>
	</tr>{-IsNotReferences}
	{IsReferences}<tr>
		<td class="title">{LANG_TXT_NAME}</td>
		<td class="title">{LANG_TXT_PHONE}</td>
		<td class="title">{LANG_TXT_CELLULAR}</td>
	</tr>
	{REFERENCIES}{-IsReferences}
</table>

<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_LOANS}</b></a>
	</div>

</div>

<table class="classic">
	<tr>
		<td class="title">&nbsp;</td>
		<td class="title">{LANG_TXT_LOAN_ID}</td>
		<td class="title">{LANG_TXT_PAYDATE}</td>
		<td class="title">{LANG_TXT_ASSESSOR}</td>
		<td class="title">{LANG_TXT_AMOUNT}</td>
		<td class="title">{LANG_TXT_ARREAR}</td>
		<td class="title">{LANG_TXT_TOTAL}</td>
	</tr>
	{IsNotLoans}
	<tr class="last">
		<td colspan="7"><br />{LANG_TXT_NO_LOAN}<br /><br /></td>
	</tr>
	{-IsNotLoans}
	{IsLoans}{LOANS}{-IsLoans}
</table>{-IsCall}

<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_RESULTS}</b></a>
	</div>
</div>

<table class="classic">
	{IsCall}<tr class="first">
		<td class="title" width="30%">{LANG_TXT_CALL_RESULTS}</td>
		<td><input type="radio" name="task[result]" value="no-answer" id="call-no-answer" class="nb" /><label for="call-no-answer">{LANG_TXT_NO_ANSWER}</label></td>
	</tr>
	<tr>
		<td class="title">&nbsp;</td>
		<td><input type="radio" name="task[result]" value="callback" id="callback" class="nb" /><label for="callback">{LANG_TXT_CALLBACK}</label></td>
	</tr>
	<tr id="iscallback">
		<td class="title">{LANG_TXT_CALLBACK_DATE} <span class="red">*</span></td>
		<td>
			<input type="text" style="width:75px" name="task[date]" value="{DATE}" class="pick-date" />
			<select name="task[time]">
				{TIMES}
			</select>
		</td>
	</tr>{-IsCall}
	{IsNotPayment}<tr{IsNotCall} class="first"{-IsNotCall}>
		<td class="title" width="30%">{LANG_TXT_TASK_RESULTS}</td>
		<td><input type="radio" name="task[result]" value="deleted" id="task-done" class="nb" /><label for="task-done">{LANG_TXT_DONE}</label></td>
	</tr>{-IsNotPayment}
	{IsNotCall}<tr>
		<td class="title" width="30%">&nbsp;</td>
		<td><input type="radio" name="task[result]" value="active" id="task-postpone" class="nb" /><label for="task-postpone">{LANG_TXT_POSTPONE}</label></td>
	</tr>
	<tr id="iscallback">
		<td class="title">{LANG_TXT_POSTPONE_DATE}</td>
		<td>
			<input type="text" style="width:75px" name="task[date]" value="{DATE}" class="pick-date" />
			<select name="task[time]">
				{TIMES}
			</select>
		</td>
	</tr>{-IsNotCall}
	<tr>
		<td class="title"><br />{LANG_TXT_NOTICE}</td>
		<td><br /><textarea name="task[notice]" style="width: 350px; height: 100px">{NOTICE}</textarea></td>
	</tr>
	<tr class="last"><td class="title">&nbsp;</td><td><br /><input class="submit" type="submit" value="{IsNotCall}{LANG_TXT_SAVE_TASK}{-IsNotCall}{IsCall}{LANG_TXT_SAVE_CALL}{-IsCall}" /><br /><br /></td></tr>
</table>
</form>

<script type="text/javascript" language="JavaScript">

$(document).ready(function() {

	$(".pick-date").datepicker({minDate: "+0d"});
	
	ShowDate('{RESULT}');

	$('input[name="task[result]"]').change(function() {
			
		ShowDate($('input[name="task[result]"]:checked').val());
	
	});

});

function ShowDate(iscallback) {
					
	if (iscallback == "callback") {
		
		$('input:radio[name="task[result]"]').filter('[value="callback"]').attr('checked', true);
		$("tr#iscallback").show();
	
	} else if (iscallback == "active") {
		
		$('input:radio[name="task[result]"]').filter('[value="active"]').attr('checked', true);
		$("tr#iscallback").show();
	
	} else {
		
		$("tr#iscallback").hide();
		
	}
		
	return true;
	
}

</script>