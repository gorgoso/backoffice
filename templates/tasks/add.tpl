<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_TASK_INFO}</b></a>
	</div>

	{IsClient}<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CLIENT_ID}:</b> {CLIENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {FIRSTNAME} {LASTNAME}</a>
	</div>{-IsClient}

	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}tasks/list/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}tasks/add/" method="post">
{IsClient}<input type="hidden" name="add[client_id]" value="{CLIENT_ID_TXT}" />{-IsClient}
<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_TYPE} <span class="red">*</span></td>
		<td>
			<select name="add[type]" style="width: 160px" />
				<option value="">- {LANG_TXT_SELECT} -</option>
				<option value=""></option>
				{TYPES}
			</select>
		</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_EXECUTION_DATE} <span class="red">*</span></td>
		<td>
			<input type="text" style="width:75px" name="add[date]" value="{DATE}" class="pick-date" />
			<select name="add[time]">
				{TIMES}
			</select>
		</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_EXECUTION_USER} <span class="red">*</span></td>
		<td>
			<select name="add[user_id]" style="width: 200px" />
				<option value="">- {LANG_TXT_SELECT} -</option>
				<option value=""></option>
				{USERS}
			</select>
		</td>
	</tr>
	{IsNotClient}<tr>
		<td class="title"><br />{LANG_TXT_SELECT_CLIENT}<br /><br /></td>
		<td><br /><input type="text" name="add[client_id]" value="{CLIENT_ID_TXT}" style="width: 150px" maxlength="8" /><br /><br /></td>
	</tr>{-IsNotClient}
	<tr>
		<td class="title">{LANG_TXT_SUBJECT} <span class="red">*</span></td>
		<td>
			<input type="text" name="add[subject]" value="{SUBJECT}" style="width: 350px" maxlength="128" />
		</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_DESCRIPTION} <span class="red">*</span></td>
		<td><br /><textarea name="add[description]" style="width: 350px; height: 150px">{DESCRIPTION}</textarea></td>
	</tr>
	<tr class="first last-bd"><td class="title">&nbsp;</td><td><input class="submit" type="submit" value="{LANG_TXT_ADD_TASK}" /></td></tr>
</table>

</form>

<script type="text/javascript" language="JavaScript">

$(document).ready(function() {
	
	$(".pick-date").datepicker({minDate: "+0d"});
	
});

</script>