<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	{ShowAddTask}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}tasks/add/">{LANG_TXT_ADD_TASK}</a>
	</div>{-ShowAddTask}

	{ShowTaskSearch}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}tasks/search/">{LANG_TXT_TASK_SEARCH}</a>
	</div>{-ShowTaskSearch}

	<div class="right">
		<div class="tab">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a>{LANG_TXT_PENDING_TASKS}: <b>{PENDING_COUNT}</b></a>
		</div>
	</div>

</div>

<form action="{PATH_ADMIN}tasks/list/" method="post">
<table cellpadding="0" cellspacing="0" class="classic">
	<tr class="title">
		<td>&nbsp;</td>
		<td><a href="{PATH_ADMIN}tasks/list/?order=user_id&amp;sort={SORT}">{LANG_TXT_USER}</a></td>
		<td><a href="{PATH_ADMIN}tasks/list/?order=type&amp;sort={SORT}">{LANG_TXT_TYPE}</a></td>
		<td><a href="{PATH_ADMIN}tasks/list/?order=fullname&amp;sort={SORT}">{LANG_TXT_FULLNAME}</a></td>
		<td><a href="{PATH_ADMIN}tasks/list/?order=subject&amp;sort={SORT}">{LANG_TXT_SUBJECT}</a></td>
		<td><a href="{PATH_ADMIN}tasks/list/?order=add_date&amp;sort={SORT}">{LANG_TXT_ADD_DATE}</a></td>
		<td><a href="{PATH_ADMIN}tasks/list/?order=execution_date&amp;sort={SORT}">{LANG_TXT_EXECUTION_DATE}</a></td>
		<td><a href="{PATH_ADMIN}tasks/list/?order=T.status&amp;sort={SORT}">{LANG_TXT_STATUS}</a></td>
	</tr>
	<tr class="title">
		<td>&nbsp;</td>
		<td><select name="filter[user_id][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{USERS}</select></td>
		<td><select name="filter[type][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{TYPES}</select></td>
		<td><input type="text" style="width:150px" name="filter[fullname]" value="{FULLNAME}" /></td>
		<td><input type="text" style="width:200px" name="filter[subject]" value="{SUBJECT}" /></td>
		<td colspan="2">&nbsp;</td>
		<td><select name="filter[T.status][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{STATUSES}</select></td>
	</tr>
		{TASKS}
	<tr class="list last"><td colspan="8"><div class="count">{COUNT}</div>{LIST}</td></tr>
</table>

<div id="btnfilter">
	<input type="submit" value="{LANG_TXT_SET_FILTER}" class="submit" />
	<input type="submit" value="{LANG_TXT_RESET_FILTER}" class="reset" name="reset" />
</div>

</form>