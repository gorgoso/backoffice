<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_COMPANY_ID}:</b> {COMPANY_ID_TXT}, <b>{LANG_TXT_COMPANY_NAME}:</b> {NAME}</a>
	</div>
	<div class="right">
		<div class="tab tabchange">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}marketing/list/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}marketing/leads/{LEAD_ID}/" method="post">

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_LASTCALL_DATE}</td>
		<td>{LASTCALL_DATE_TXT}</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_NEXTCALL_DATE}</td>
		<td><b>{IsExpired}<span class="red">{-IsExpired}{NEXTCALL_DATE_TXT}{IsExpired}</span>{-IsExpired}</b></td>
	</tr>
	<tr class="last-bd">
		<td class="title"><br />{LANG_TXT_STATUS}</td>
		<td><br /><b>{STATUS_TXT}</b></td>
	</tr>
</table>

<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_INFORMATION}</b></a>
	</div>
</div>

<table class="classic">
	<tr class="first">
		<td class="title" width="30%"><br />{LANG_TXT_PHONE} #1</td>
		<td><br /><b>{PHONE1}</b></td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_PHONE} #2</td>
		<td><b>{PHONE2}</b></td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_EMAIL}</td>
		<td><br /><a href="mailto:{EMAIL}"><b>{EMAIL}</b></a></td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_WWW}</td>
		<td><a href="http://{WWW}/" target="_blank">{WWW}</a></td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_ADDRESS}</td>
		<td><br />{ADDRESS}</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_CITY}</td>
		<td>{CITY}</td>
	</tr>
	<tr class="last-bd">
		<td class="title">{LANG_TXT_STATE}</td>
		<td>{STATE}</td>
	</tr>
</table>

<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CONTACT}</b></a>
	</div>

	<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}marketing/contact/{LEAD_ID}/">{LANG_TXT_EDIT_CONTACT}</a>
	</div>

</div>

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_CONTACT_NAME}</td>
		<td><b>{CONTACT_NAME}</b></td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_PHONE}</td>
		<td><br />{CONTACT_PHONE}</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_CELLULAR}</td>
		<td>{CONTACT_CELLULAR}</td>
	</tr>
	<tr class="last-bd">
		<td class="title">{LANG_TXT_EMAIL}</td>
		<td><a href="mailto:{CONTACT_EMAIL}"><b>{CONTACT_EMAIL}</b></a></td>
	</tr>
</table>

<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_RESULTS}</b></a>
	</div>
</div>

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_CALL_RESULTS}</td>
		<td><input type="radio" name="task[result]" value="no-answer" id="call-no-answer" class="nb" /><label for="call-no-answer">{LANG_TXT_NO_ANSWER}</label></td>
	</tr>
	<tr>
		<td class="title">&nbsp;</td>
		<td><input type="radio" name="task[result]" value="callback" id="callback" class="nb" /><label for="callback">{LANG_TXT_CALLBACK}</label></td>
	</tr>
	<tr id="iscallback">
		<td class="title">{LANG_TXT_CALLBACK_DATE} <span class="red">*</span></td>
		<td>
			<input type="text" style="width:120px" name="task[date]" value="{DATE}" class="pick-date" />
			<select name="task[time]">
				{TIMES}
			</select>
		</td>
	</tr>
	<tr>
		<td class="title">&nbsp;</td>
		<td><input type="radio" name="task[result]" value="client" id="call-client" class="nb" /><label for="call-client">{LANG_TXT_LEAD_CLIENT}</label></td>
	</tr>
	<tr>
		<td class="title">&nbsp;</td>
		<td><input type="radio" name="task[result]" value="dnd" id="dnd-client" class="nb" /><label for="dnd-client">{LANG_TXT_DND_CLIENT}</label></td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_NOTICE}</td>
		<td><br /><input type="text" name="task[notice]" style="width: 450px" maxlength="255" value="{NOTICE}" /></td>
	</tr>
	<tr class="last-bd"><td class="title">&nbsp;</td><td><br /><input class="submit" type="submit" value="{LANG_TXT_SAVE_CALL}" /></td></tr>
</table>

<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_NEGOTIATIONS}</b></a>
	</div>

</div>

<table class="classic">
	<tr>
		<td class="title">{LANG_TXT_CALL_DATE}</td>
		<td class="title">{LANG_TXT_USER}</td>
		<td class="title">{LANG_TXT_NOTICE}</td>
		<td class="title">{LANG_TXT_STATUS}</td>
	</tr>
	{NEGOTIATIONS}
	<tr class="last">
		<td class="title" colspan="3">{LANG_TXT_CALLS_COUNT}</td>
		<td class="title">{CALLS_COUNT}</td>
	</tr>
</table>

</form>

<script type="text/javascript" language="JavaScript">

$(document).ready(function() {

	$(".pick-date").datepicker({minDate: "+0d"});

	ShowDate('{RESULT}');

	$('input[name="task[result]"]').change(function() {

		ShowDate($('input[name="task[result]"]:checked').val());

	});

});

function ShowDate(iscallback) {

	if (iscallback == "callback") {

		$('input:radio[name="task[result]"]').filter('[value="callback"]').attr('checked', true);
		$("tr#iscallback").show();

	} else if (iscallback == "active") {

		$('input:radio[name="task[result]"]').filter('[value="active"]').attr('checked', true);
		$("tr#iscallback").show();

	} else {

		$("tr#iscallback").hide();

	}

	return true;

}

</script>