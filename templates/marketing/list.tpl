<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	{ShowCallNewLeads}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}marketing/leads/">{LANG_TXT_NEW_LEADS}</a>
	</div>{-ShowCallNewLeads}

	<div class="right">
		<div class="tab">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a>{LANG_TXT_LEADS_COUNT}: <b>{LEADS_COUNT} / {MAX_LEADS_LIMIT}</b></a>
		</div>
	</div>

</div>

<form action="{PATH_ADMIN}marketing/list/" method="post">
<table class="classic">
	<tr class="title">
		<td>&nbsp;</td>
		<td><a href="{PATH_ADMIN}marketing/list/?order=name&amp;sort={SORT}">{LANG_TXT_NAME}</a></td>
		<td><a href="{PATH_ADMIN}marketing/list/?order=phone1&amp;sort={SORT}">{LANG_TXT_PHONE} #1</a></td>
		<td><a href="{PATH_ADMIN}marketing/list/?order=phone2&amp;sort={SORT}">{LANG_TXT_PHONE} #2</a></td>
		<td><a href="{PATH_ADMIN}marketing/list/?order=lastcall_date&amp;sort={SORT}">{LANG_TXT_LASTCALL_DATE}</a></td>
		<td><a href="{PATH_ADMIN}marketing/list/?order=nextcall_date&amp;sort={SORT}">{LANG_TXT_NEXTCALL_DATE}</a></td>
		<td><a href="{PATH_ADMIN}marketing/list/?order=status&amp;sort={SORT}">{LANG_TXT_STATUS}</a></td>
	</tr>
	<tr class="title">
		<td>&nbsp;</td>
		<td><input type="text" style="width:200px" name="filter[name]" value="{NAME}" /></td>
		<td><input type="text" style="width:150px" name="filter[phone1]" value="{PHONE1}" /></td>
		<td><input type="text" style="width:150px" name="filter[phone2]" value="{PHONE2}" /></td>
		<td colspan="2">&nbsp;</td>
		<td><select name="filter[status][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{STATUSES}</select></td>
	</tr>
	{LEADS}
	<tr class="list last"><td colspan="7"><div class="count">{COUNT}</div>{LIST}</td></tr>
</table>

<div id="btnfilter">
	<input type="submit" value="{LANG_TXT_SET_FILTER}" class="submit" />
	<input type="submit" value="{LANG_TXT_RESET_FILTER}" class="reset" name="reset" />
</div>

</form>