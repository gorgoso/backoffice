<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_COMPANY_ID}:</b> {COMPANY_ID_TXT}, <b>{LANG_TXT_COMPANY_NAME}:</b> {NAME}</a>
	</div>
	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}marketing/leads/{LEAD_ID}/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}marketing/contact/{LEAD_ID}/" method="post">

	<table class="classic">
		<tr class="first">
			<td class="title" width="30%">{LANG_TXT_CONTACT_NAME} <span class="red">*</span></td>
			<td><input type="text" name="add[contact_name]" value="{CONTACT_NAME}" style="width: 250px" maxlength="255" /></td>
		</tr>
		<tr>
			<td class="title"><br />{LANG_TXT_CONTACT_PHONE}</td>
			<td><br /><input type="text" name="add[contact_phone]" value="{CONTACT_PHONE}" style="width: 200px" maxlength="32" /></td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_CONTACT_CELLULAR} <span class="red">*</span></td>
			<td><input type="text" name="add[contact_cellular]" value="{CONTACT_CELLULAR}" style="width: 200px" maxlength="32" /></td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_CONTACT_EMAIL} <span class="red">*</span></td>
			<td><input type="text" name="add[contact_email]" value="{CONTACT_EMAIL}" style="width: 250px" maxlength="255" /></td>
		</tr>
		<tr class="last-bd"><td class="title">&nbsp;</td><td><br /><input class="submit" type="submit" value="{LANG_TXT_ADD_CONTACT}" /></td></tr>
	</table>

</form>