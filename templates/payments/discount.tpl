<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_PAYMENT_ID}:</b> {PAYMENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {CLIENT_NAME}</a>
	</div>
	<div class="right">
		<div class="tab tabchange">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}payments/details/{PAYMENT_ID}/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}payments/discount/{PAYMENT_ID}/" method="post">

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_AMOUNT}</td>
		<td><b>{AMOUNT_TXT} USD</b></td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_DISCOUNT_PERCENTAGE}</td>
		<td>
			<br />
			<select name="add[discount]" style="width: 160px">
				<option value="">- {LANG_TXT_SELECT} -</option>
				<option value=""></option>
				{DISCOUNTS}
			</select>
		</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_DISCOUNT} <span class="red">*</span></td>
		<td><input type="text" style="width:100px" name="add[amount]" value="{DISCOUNT}" /> USD</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_DIFFERENCE}</td>
		<td><br /><b><span id="difference">0.00</span> USD</b></td>
	</tr>
	<tr class="first last-bd"><td class="title">&nbsp;</td><td><input class="submit" type="submit" value="{LANG_TXT_CHANGE_DISCOUNT}" /></td></tr>
</table>
</form>

<script type="text/javascript" language="JavaScript">

$(document).ready(function() {

	$('input[name="add[amount]"]').bind('change', function() {
		CalculateDifference();
	});

	$('select[name="add[discount]"]').bind('change', function() {
		CalculateDiscount();
	});

	CalculateDiscount();

	function CalculateDiscount() {

		var tamount = Number({AMOUNT});
		var discount = Number($("select[name='add[discount]']").val());

		if (discount > 0 && tamount) {

			damount = PreciseRound(Number(tamount/100*discount), 2);

		} else {

			damount = tamount;

		}

		$('input[name="add[amount]"]').val(PreciseRound(Number(tamount - damount), 2));

		CalculateDifference();

		return true;

	}

	function CalculateDifference() {

		var amount = Number({AMOUNT});
		var newamount = Number($('input[name="add[amount]"]').val());

		var difference = (amount-newamount < 0) ? 0 : amount - newamount;

		$('span#difference').html($().NumberFormat(difference));

		return true;

	}
});

</script>