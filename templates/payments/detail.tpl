<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_PAYMENT_ID}:</b> {PAYMENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {CLIENT_NAME}</a>
	</div>
	{ShowChangeDiscount}{IsPending}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}payments/discount/{PAYMENT_ID}/">{LANG_TXT_CHANGE_DISCOUNT}</a>
	</div>{-IsPending}{-ShowChangeDiscount}
	{ShowGenerateInvoice}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}payments/invoice/{PAYMENT_ID}/">{LANG_TXT_GENERATE_INVOICE}</a>
	</div>{-ShowGenerateInvoice}
	<div class="right">
		<div class="tab tabchange">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}payments/list/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_ADD_DATE}</td>
		<td>{ADD_DATE_TXT}</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_PAY_DATE}</td>
		<td>{PAY_DATE_TXT}</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_TYPE}<br /><br /></td>
		<td><br /><b>{TYPE_TXT}</b><br /><br /></td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_METHOD}</td>
		<td>{METHOD_TXT}</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_AMOUNT}</td>
		<td><br />{AMOUNT_TXT} USD</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_DISCOUNT}</td>
		<td>{DISCOUNT_TXT} USD</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_AMOUNT_TOTAL}</td>
		<td><b>{AMOUNT_TOTAL_TXT} USD</b></td>
	</tr>
	<tr class="last-bd">
		<td class="title">{LANG_TXT_STATUS}</td>
		<td>{IsPending}<span class="red">{-IsPending}{STATUS_TXT}{IsPending}</span>{-IsPending}</td>
	</tr>
</table>