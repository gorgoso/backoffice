<tr class="change{IsBlocked} blocked{-IsBlocked}">
	<td><a href="{PATH_ADMIN}system/download/{FILE_HASH}/"><span title="{LANG_TXT_DOWNLOAD_FILE}" class="list"></span></a></td>
	<td>{LOAN_ID_TXT}</td>
	<td><b>{GROUP_TXT}</b></td>
	<td>{ADD_DATE_TXT}</td>
	<td>{DEL_DATE_TXT}</td>
	<td>{NOTE}</td>
	<td>{STATUS_TXT}</td>
	<td>{IsNotDeleted}<a href="{PATH_ADMIN}clients/file-delete/{FILE_ID}/"><span title="{LANG_TXT_DELETE_FILE}" class="delete"></span></a>{-IsNotDeleted}</td>
</tr>