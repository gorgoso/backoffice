<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/list/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}clients/add/" method="post">

	<table class="classic">
		<tr class="first">
			<td class="title" width="30%">{LANG_TXT_USERNAME} <span class="red">*</span></td>
			<td><input type="text" name="add[username]" value="{USERNAME}" style="width: 200px" maxlength="32" /></td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_PASSWORD} <span class="red">*</span></td>
			<td><input type="text" name="add[password]" value="{PASSWORD}" style="width: 200px" maxlength="32" /></td>
		</tr>
		<tr>
			<td class="title"><br />{LANG_TXT_NAME} <span class="red">*</span></td>
			<td><br /><input type="text" name="add[name]" value="{NAME}" style="width: 250px" maxlength="128" /></td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_EMAIL} <span class="red">*</span></td>
			<td><input type="text" name="add[email]" value="{EMAIL}" style="width: 250px" maxlength="128" /></td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_PHONE} <span class="red">*</span></td>
			<td>
				<select name="add[code]">
					<option value="">- {LANG_TXT_SELECT} -</option>
					<option value=""></option>
					{CALLING_CODES}
				</select>
				<input type="text" name="add[phone]" value="{PHONE}" style="width: 150px" maxlength="7" />
			</td>
		</tr>
		<tr>
			<td class="title"><br />{LANG_TXT_PROVINCE} <span class="red">*</span></td>
			<td>
				<br />
				<select name="add[province]" style="width: 200px">
					<option value="">- {LANG_TXT_SELECT} -</option>
					<option value=""></option>
					{PROVINCES}
				</select>
			</td>
		</tr>
		<tr class="last-bd"><td class="title">&nbsp;</td><td><br /><input class="submit" type="submit" value="{LANG_TXT_ADD_CLIENT}" /></td></tr>
	</table>

</form>