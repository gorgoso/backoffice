<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CLIENT_ID}:</b> {CLIENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {FIRSTNAME} {LASTNAME}</a>
	</div>

	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/detail/{CLIENT_ID}/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}clients/documents/{CLIENT_ID}/" method="post">

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_ADD_DOCUMENT} <span class="red">*</span></td>
		<td>
			<select name="add[type]" style="width: 150px" />
				<option value="">- {LANG_TXT_SELECT} -</option>
				<option value=""></option>
				{TYPES}
			</select>
		</td>
	</tr>
	<tr>
		<td class="title" width="30%"><br />{LANG_TXT_NUMBER} {LANG_TXT_DOCUMENT} <span class="red">*</span></td>
		<td>
			<br /><input type="text" name="add[number]" value="{NUMBER}" style="width: 200px" maxlength="32" />
		</td>
	</tr>
	<tr>
		<td class="title" width="30%">{LANG_TXT_COUNTRY} {LANG_TXT_DOCUMENT} <span class="red">*</span></td>
		<td>
			<select name="add[country]" />
				<option value="">- {LANG_TXT_SELECT} -</option>
				<option value=""></option>
				<option value="do">{LANG_TXT_DOMINICAN_REPUBLIC}</option>
				<option value=""></option>
				{COUNTRIES}
			</select>
		</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_ISSUE_DATE} {LANG_TXT_DOCUMENT}</td>
		<td>
			<br /><input type="text" style="width:75px" name="add[date_issued]" value="{DATE_ISSUED}" class="date-from" />
		</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_EXPIRATION_DATE} {LANG_TXT_DOCUMENT}</td>
		<td>
			<input type="text" style="width:75px" name="add[date_expiration]" value="{DATE_EXPIRATION}" class="date-to" />
		</td>
	</tr>
	<tr class="last"><td class="title">&nbsp;</td><td><br /><input class="submit" type="submit" value="{LANG_TXT_ADD_DOCUMENT}" /><br /><br /></td></tr>
</table>

</form>

<br />

<form action="{PATH_ADMIN}clients/documents/{CLIENT_ID}/" method="post">

<table cellpadding="0" cellspacing="0" class="classic">
	<tr class="title">
		<td>&nbsp;</td>
		<td><a href="{PATH_ADMIN}clients/documents/{CLIENT_ID}/?order=type&amp;sort={SORT}">{LANG_TXT_TYPE} {LANG_TXT_DOCUMENT}</a></td>
		<td><a href="{PATH_ADMIN}clients/documents/{CLIENT_ID}/?order=number&amp;sort={SORT}">{LANG_TXT_NUMBER} {LANG_TXT_DOCUMENT}</a></td>
		<td><a href="{PATH_ADMIN}clients/documents/{CLIENT_ID}/?order=country&amp;sort={SORT}">{LANG_TXT_COUNTRY}</a></td>
		<td><a href="{PATH_ADMIN}clients/documents/{CLIENT_ID}/?order=date_issued&amp;sort={SORT}">{LANG_TXT_ISSUE_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/documents/{CLIENT_ID}/?order=date_expiration&amp;sort={SORT}">{LANG_TXT_EXPIRATION_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/documents/{CLIENT_ID}/?order=add_date&amp;sort={SORT}">{LANG_TXT_ADD_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/documents/{CLIENT_ID}/?order=del_date&amp;sort={SORT}">{LANG_TXT_DEL_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/documents/{CLIENT_ID}/?order=status&amp;sort={SORT}">{LANG_TXT_STATUS}</a></td>
		<td>{LANG_TXT_MANAGE}</td>
	</tr>
	<tr class="title">
		<td>&nbsp;</td>
		<td><select name="filter[type][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{FILTER_TYPES}</select></td>
		<td colspan="6">&nbsp;</td>
		<td><select name="filter[status][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{STATUSES}</select></td>
		<td>&nbsp;</td>
	</tr>
		{DOCUMENTS}
	<tr class="list last"><td colspan="10"><div class="count">{COUNT}</div>{LIST}</td></tr>
</table>

<div id="btnfilter">
	<input type="submit" value="{LANG_TXT_SET_FILTER}" class="submit" />
	<input type="submit" value="{LANG_TXT_RESET_FILTER}" class="reset" name="reset" />
</div>

</form>

<script type="text/javascript" language="JavaScript">

$(document).ready(function() {

	$(".date-from").datepicker();
	$(".date-to").datepicker();
	
});

</script>