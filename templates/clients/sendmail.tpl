<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CLIENT_ID}:</b> {CLIENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {FIRSTNAME} {LASTNAME}</a>
	</div>

	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/detail/{CLIENT_ID}/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}clients/sendmail/{CONTACT_ID}/" method="post">

	<table class="classic">
		<tr class="first">
			<td class="title" width="30%">{LANG_TXT_TEMPLATE}<br /><br /></td>
			<td>
				<select name="sendmail[template_id]">
					<option value="">- {LANG_TXT_SELECT} -</option>
					<option value=""></option>
					{TEMPLATES}
				</select><br /><br />
			</td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_SENDER} <span class="red">*</span></td>
			<td>
				<select name="sendmail[sender]">
					<option value="">- {LANG_TXT_SELECT} -</option>
					<option value=""></option>
					{SENDERS}
				</select>
			</td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_SUBJECT} <span class="red">*</span></td>
			<td><br /><input type="text" name="sendmail[subject]" value="{EMAIL_SUBJECT}" style="width: 400px" maxlength="128" /></td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_TEXT} <span class="red">*</span></td>
			<td><textarea name="sendmail[text]" rows="20" cols="80" style="width: 500px; height: 250px" >{EMAIL_TEXT}</textarea></td>
		</tr>
		<tr class="last-bd"><td class="title">&nbsp;</td><td><br /><input class="submit" type="submit" value="{LANG_TXT_SENDMAIL}" /></td></tr>
	</table>

</form>

<script language="JavaScript" type="text/javascript">
	
$(document).ready(function() {
	
	$("select[name='sendmail[template_id]']").bind('change', function() {
		
		var template_id = $(this).val();
		
		if(!template_id) {
			
  		$("select[name='sendmail[sender]']").val('');
  		$("input[name='sendmail[subject]']").val('');
  		$("textarea[name='sendmail[text]']").val('');
			
			return false;
			
		}
		
  	var templates = {TEMPLATES_JSON};
  	  	
  	$("select[name='sendmail[sender]']").val(templates[template_id]["sender"]);
  	$("input[name='sendmail[subject]']").val(templates[template_id]["subject"]);
  	$("textarea[name='sendmail[text]']").val(templates[template_id]["text"]);
  	
	});
});

</script>