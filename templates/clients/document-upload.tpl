<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{TYPE_TXT}:</b> {NUMBER}, <b>{LANG_TXT_COUNTRY}:</b> {COUNTRY_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {FIRSTNAME} {LASTNAME}</a>
	</div>
	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/documents/{CLIENT_ID}/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}clients/document-upload/{DOCUMENT_ID}/" method="post" enctype="multipart/form-data">

	<table class="classic">
		{IsUpload}<tr class="change">
			<td class="title" width="30%"><br />{LANG_TXT_FILE} <span class="red">*</span><br /><br /></td>
			<td><br /><input type="file" name="update" style="width: 350px" /><br /><br /></td>
		</tr>
		<tr class="last"><td class="title">&nbsp;</td><td><br /><input class="submit" type="submit" value="{LANG_TXT_UPLOAD_DOCUMENT}" /><br /><br /></td></tr>{-IsUpload}
		{ShowDocument}<tr class="last">
			<td class="title" width="30%">{TYPE_TXT}</td>
			<td><br /><img src="{PATH_ADMIN}system/download/{HASH}/" alt="{LANG_TXT_CLIENT_NAME}: {FIRSTNAME} {LASTNAME} ({NUMBER})" /><br /><br /></td>
		</tr>{-ShowDocument}
	</table>

</form>