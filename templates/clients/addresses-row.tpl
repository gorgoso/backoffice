<tr class="change{IsBlocked} blocked{-IsBlocked}">
	<td>{CONTACT_TYPE_TXT}</td>
	<td>
		{ADDRESS1}
		{IsAddress2}<br />{ADDRESS2}<br />{-IsAddress2}
		{IsAddress3}{ADDRESS3}{-IsAddress3}
	</td>
	<td>{CITY}</td>
	<td>{ZIP}</td>
	<td>{STATE}</td>
	<td>{COUNTRY_TXT}</td>
	<td>{ADD_DATE_TXT}</td>
	<td>{DEL_DATE_TXT}</td>
	<td>{STATUS_TXT}</td>
	<td><a class="address" data-value="{TYPE}|{ADDRESS1}|{ADDRESS2}|{ADDRESS3}|{CITY}|{ZIP}|{STATE}|{COUNTRY}"><span title="{LANG_TXT_UPDATE_ADDRESS}" class="edit"></span></a></td>
	<td><a href="{PATH_ADMIN}clients/address-delete/{ADDRESS_ID}/"><span title="{LANG_TXT_DELETE_ADDRESS}" class="delete"></span></a></td>
</tr>