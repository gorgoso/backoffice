<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CLIENT_ID}:</b> {CLIENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {FIRSTNAME} {LASTNAME}</a>
	</div>

	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/detail/{CLIENT_ID}/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}clients/referencies/{CLIENT_ID}/" method="post">

<table class="classic">
	<tr>
		<td class="title" width="30%">{LANG_TXT_NAME} <span class="red">*</span></td>
		<td>
			<input type="text" name="add[name]" value="{NAME}" style="width: 350px" maxlength="128" />
		</td>
	</tr>
	<tr>
		<td class="title" width="30%">{LANG_TXT_NOTICE}</td>
		<td>
			<input type="text" name="add[notice]" value="{NOTICE}" style="width: 250px" maxlength="128" />
		</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_CALLING_CODE} / {LANG_TXT_PHONE} <span class="red">*</span></td>
		<td>
			<br />
			<select name="add[phone_code]">
				<option value="">- {LANG_TXT_SELECT} -</option>
				<option value=""></option>
				<option value="1809">{LANG_TXT_DOMINICAN_REPUBLIC} (+1-809)</option>
				<option value="1829">{LANG_TXT_DOMINICAN_REPUBLIC} (+1-829)</option>
				<option value="1849">{LANG_TXT_DOMINICAN_REPUBLIC} (+1-849)</option>
				<option value=""></option>
				{CALLING_CODES}
			</select>
			<input type="text" name="add[phone]" value="{PHONE}" style="width: 150px" maxlength="16" />
		</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_CALLING_CODE} / {LANG_TXT_CELLULAR} <span class="red">*</span></td>
		<td>
			<br />
			<select name="add[cellular_code]">
				<option value="">- {LANG_TXT_SELECT} -</option>
				<option value=""></option>
				<option value="1809">{LANG_TXT_DOMINICAN_REPUBLIC} (+1-809)</option>
				<option value="1829">{LANG_TXT_DOMINICAN_REPUBLIC} (+1-829)</option>
				<option value="1849">{LANG_TXT_DOMINICAN_REPUBLIC} (+1-849)</option>
				<option value=""></option>
				{CALLING_CODES}
			</select>
			<input type="text" name="add[cellular]" value="{CELLULAR}" style="width: 150px" maxlength="16" />
		</td>
	</tr>
	<tr>
		<td class="title" width="30%"><br />{LANG_TXT_ADDRESS} <span class="red">*</span><br /><br /><br /><br /><br /></td>
		<td><br />
			<input type="text" name="add[address1]" value="{ADDRESS1}" style="width: 350px" maxlength="128" /><br />
			<input type="text" name="add[address2]" value="{ADDRESS2}" style="width: 350px" maxlength="128" /><br />
			<input type="text" name="add[address3]" value="{ADDRESS3}" style="width: 350px" maxlength="128" /><br /><br />
		</td>
	</tr>
	<tr>
		<td class="title" width="30%">{LANG_TXT_CITY} <span class="red">*</span></td>
		<td>
			<input type="text" name="add[city]" value="{CITY}" style="width: 250px" maxlength="64" />
		</td>
	</tr>
	<tr>
		<td class="title" width="30%">{LANG_TXT_ZIP}</td>
		<td>
			<input type="text" name="add[zip]" value="{ZIP}" style="width: 200px" maxlength="8" />
		</td>
	</tr>
	<tr>
		<td class="title" width="30%">{LANG_TXT_STATE} <span class="red">*</span></td>
		<td>
			<input type="text" name="add[state]" value="{STATE}" style="width: 250px" maxlength="64" />
		</td>
	</tr>
		<tr>
			<td class="title"><br />{LANG_TXT_COUNTRY} <span class="red">*</span></td>
			<td>
				<br />
				<select name="add[country]" style="width: 250px">
					<option value="">- {LANG_TXT_SELECT} -</option>
					<option value=""></option>
					<option value="do">{LANG_TXT_DOMINICAN_REPUBLIC}</option>
					<option value=""></option>
					{COUNTRIES}
				</select>
			</td>
		</tr>
	<tr class="last"><td class="title">&nbsp;</td><td><br /><input class="submit" type="submit" value="{LANG_TXT_ADD_REFERENCE}" /><br /><br /></td></tr>
</table>

</form>

<br />

<form action="{PATH_ADMIN}clients/referencies/{CLIENT_ID}/" method="post">

<table cellpadding="0" cellspacing="0" class="classic">
	<tr class="title">
		<td>&nbsp;</td>
		<td><a href="{PATH_ADMIN}clients/referencies/{CLIENT_ID}/?order=name&amp;sort={SORT}">{LANG_TXT_NAME}</a></td>
		<td><a href="{PATH_ADMIN}clients/referencies/{CLIENT_ID}/?order=C1.contact&amp;sort={SORT}">{LANG_TXT_PHONE}</a></td>
		<td><a href="{PATH_ADMIN}clients/referencies/{CLIENT_ID}/?order=C2.contact&amp;sort={SORT}">{LANG_TXT_CELLULAR}</a></td>
		<td><a href="{PATH_ADMIN}clients/referencies/{CLIENT_ID}/?order=add_date&amp;sort={SORT}">{LANG_TXT_ADD_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/referencies/{CLIENT_ID}/?order=del_date&amp;sort={SORT}">{LANG_TXT_DEL_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/referencies/{CLIENT_ID}/?order=status&amp;sort={SORT}">{LANG_TXT_STATUS}</a></td>
		<td colspan="2">{LANG_TXT_MANAGE}</td>
	</tr>
	<tr class="title">
		<td>&nbsp;</td>
		<td><input type="text" style="width:200px" name="filter[name]" value="{NAME_FILTER}" /></td>
		<td><input type="text" style="width:150px" name="filter[C1.contact]" value="{PHONE_FILTER}" /></td>
		<td><input type="text" style="width:150px" name="filter[C2.contact]" value="{CELLULAR_FILTER}" /></td>
		<td colspan="2">&nbsp;</td>
		<td><select name="filter[status][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{STATUSES}</select></td>
		<td colspan="2">&nbsp;</td>
	</tr>
		{REFERENCIES}
	<tr class="list last"><td colspan="9"><div class="count">{COUNT}</div>{LIST}</td></tr>
</table>

<div id="btnfilter">
	<input type="submit" value="{LANG_TXT_SET_FILTER}" class="submit" />
	<input type="submit" value="{LANG_TXT_RESET_FILTER}" class="reset" name="reset" />
</div>

</form>

<script type="text/javascript" language="JavaScript">
$(document).ready(function() {

	$('a.reference').click(function() {
		
		var elements = $(this).data("value").split('|');
				
		$(':input[name="add[name]"]').val(elements[0]);
		$(':input[name="add[phone_code]"]').val(elements[1]);
		$(':input[name="add[phone]"]').val(elements[2]);
		$(':input[name="add[cellular_code]"]').val(elements[3]);
		$(':input[name="add[cellular]"]').val(elements[4]);
		$(':input[name="add[address1]"]').val(elements[5]);
		$(':input[name="add[address2]"]').val(elements[6]);
		$(':input[name="add[address3]"]').val(elements[7]);
		$(':input[name="add[city]"]').val(elements[8]);
		$(':input[name="add[zip]"]').val(elements[9]);
		$(':input[name="add[state]"]').val(elements[10]);
		$(':input[name="add[country]"]').val(elements[11]);
		
	});
	
});

</script>