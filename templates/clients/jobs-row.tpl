<tr class="change{IsBlocked} blocked{-IsBlocked}">
	<td>
		<b>{COMPANY}</b><br />
		{ADDRESS1}
		{IsAddress2}<br />{ADDRESS2}<br />{-IsAddress2}
		{IsAddress3}{ADDRESS3}{-IsAddress3}<br />
		{CITY}{IsZip}, {ZIP}{-IsZip}{IsState}, {STATE}{-IsState}<br />
		{COUNTRY_TXT}
	</td>
	<td>{START_DATE_TXT}</td>
	<td>{POSITION}</td>
	<td><b>{SALARY_TXT} DOP (Pesos)</b></td>
	<td>{ADD_DATE_TXT}</td>
	<td>{DEL_DATE_TXT}</td>
	<td>{STATUS_TXT}</td>
	<td><a class="job" data-value="{COMPANY}|{START_DATE_TXT}|{POSITION}|{SALARY}|{ADDRESS1}|{ADDRESS2}|{ADDRESS3}|{CITY}|{ZIP}|{STATE}|{COUNTRY}"><span title="{LANG_TXT_UPDATE_JOB}" class="edit"></span></a></td>
	<td><a href="{PATH_ADMIN}clients/job-delete/{JOB_ID}/"><span title="{LANG_TXT_DELETE_JOB}" class="delete"></span></a></td>
</tr>