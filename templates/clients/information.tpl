<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CLIENT_ID}:</b> {CLIENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {FIRSTNAME} {LASTNAME}</a>
	</div>

	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/detail/{CLIENT_ID}/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}clients/information/{CLIENT_ID}/" method="post">

<table cellpadding="0" cellspacing="0" class="classic">
	<tr>
		<td class="title" width="30%">{LANG_TXT_TYPE} <span class="red">*</span></td>
		<td>
			<select name="add[type]" style="width: 150px" />
				<option value=""></option>
				{TYPES}
			</select>
		</td>
	</tr>
	<tr id="information">
		<td class="title" width="30%"><span id="information-description"></span>&nbsp;<span class="red">*</span></td>
		<td>
			<input type="text" name="add[information]" value="{INFORMATION}" style="width: 250px" maxlength="128" id="info-input" />
			<select name="add[information]" id="info-select">
			</select>
		</td>
	</tr>
	<tr id="notice">
		<td class="title" width="30%">{LANG_TXT_NOTICE}</td>
		<td>
			<input type="text" name="add[notice]" value="{NOTICE}" style="width: 250px" maxlength="128" />
		</td>
	</tr>
	<tr class="last"><td class="title">&nbsp;</td><td><br /><input class="submit" type="submit" value="{LANG_TXT_ADD_INFORMATION}" /><br /><br /></td></tr>
</table>

</form>

<br />

<form action="{PATH_ADMIN}clients/information/{CLIENT_ID}/" method="post">

<table cellpadding="0" cellspacing="0" class="classic">
	<tr class="title">
		<td>&nbsp;</td>
		<td><a href="{PATH_ADMIN}clients/information/{CLIENT_ID}/?order=type&amp;sort={SORT}">{LANG_TXT_TYPE}</a></td>
		<td><a href="{PATH_ADMIN}clients/information/{CLIENT_ID}/?order=information&amp;sort={SORT}">{LANG_TXT_INFORMATION}</a></td>
		<td><a href="{PATH_ADMIN}clients/information/{CLIENT_ID}/?order=add_date&amp;sort={SORT}">{LANG_TXT_ADD_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/information/{CLIENT_ID}/?order=del_date&amp;sort={SORT}">{LANG_TXT_DEL_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/information/{CLIENT_ID}/?order=status&amp;sort={SORT}">{LANG_TXT_STATUS}</a></td>
		<td>{LANG_TXT_MANAGE}</td>
	</tr>
	<tr class="title">
		<td>&nbsp;</td>
		<td><select name="filter[type][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{FILTER_TYPES}</select></td>
		<td>&nbsp;</td>
		<td colspan="2">&nbsp;</td>
		<td><select name="filter[status][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{STATUSES}</select></td>
		<td>&nbsp;</td>
	</tr>
		{INFORMATION_LIST}
	<tr class="list last"><td colspan="7"><div class="count">{COUNT}</div>{LIST}</td></tr>
</table>

<div id="btnfilter">
	<input type="submit" value="{LANG_TXT_SET_FILTER}" class="submit" />
	<input type="submit" value="{LANG_TXT_RESET_FILTER}" class="reset" name="reset" />
</div>

</form>

<script type="text/javascript" language="JavaScript">

$(document).ready(function() {

	$("select[name='add[type]']").bind('change', function(){
		ChangeTypes();
	});
	
	ChangeTypes();
	
});

function ChangeTypes() {
	
	var contacttype = $("select[name='add[type]']");

	switch(contacttype.val()) {

		case "":
			$("tr#information").hide();
			$("tr#notice").hide();
		break;
		
		case "civil-status":
			ShowSelect({CIVIL_STATUSES});
		break;

		case "residence":
			ShowSelect({RESIDENCE});
		break;

		case "occupation":
			ShowSelect({OCCUPATION});
		break;

		default:
			$("tr#information").show();
		break;	

	}	
		
	$("span#information-description").text($("select[name='add[type]'] option:selected").text());

}

function ShowSelect(values) {

	defaults = [ "- {LANG_TXT_SELECT} -", "" ];

	$("select#info-select").empty();
	
	var dropdown = $("select#info-select")[0];

	$.each(defaults, function(key, value) {
    dropdown.add(new Option(value, ''));
	});

	$.each(values, function(key, value) {
    dropdown.add(new Option(value, key));
	});

	$("input#info-input").hide();
	$("select#info-select").show();
	$("tr#notice").show();
	$("tr#information").show();

}

</script>