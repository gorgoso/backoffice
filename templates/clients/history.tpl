<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CLIENT_ID}:</b> {CLIENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {FIRSTNAME} {LASTNAME}</a>
	</div>

	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/detail/{CLIENT_ID}/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}clients/history/{CLIENT_ID}/" method="post">

<table class="classic">
	<tr>
		<td class="title" width="30%">{LANG_TXT_NOTICE} <span class="red">*</span></td>
		<td>
			<input type="text" name="add[notice]" value="{NOTICE}" style="width: 300px" maxlength="255" />
		</td>
	</tr>
	<tr class="first last-bd"><td class="title">&nbsp;</td><td><input class="submit" type="submit" value="{LANG_TXT_ADD_NOTICE}" /></td></tr>
</table>

</form>

<br />

<form action="{PATH_ADMIN}clients/history/{CLIENT_ID}/" method="post" id="filterform">

<table class="classic">
	<tr class="title">
		<td><a href="{PATH_ADMIN}clients/history/{CLIENT_ID}/?order=group&amp;sort={SORT}">{LANG_TXT_GROUP}</a></td>
		<td><a href="{PATH_ADMIN}clients/history/{CLIENT_ID}/?order=user_id&amp;sort={SORT}">{LANG_TXT_USERNAME}</a></td>
		<td><a href="{PATH_ADMIN}clients/history/{CLIENT_ID}/?order=date&amp;sort={SORT}">{LANG_TXT_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/history/{CLIENT_ID}/?order=notice&amp;sort={SORT}">{LANG_TXT_NOTICE}</a></td>
	</tr>
	<tr class="title">
		<td><select name="filter[group][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{GROUPS}</select></td>
		<td><select name="filter[user_id][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option><option value="{USER_ID}">{USERNAME}</option>{USERS}</select></td>
		<td><input type="text" style="width:140px" name="filter[dates][date][0]" value="{FROM_DATE}" class="date-from" /><br /><input type="text" style="width:140px" name="filter[dates][date][1]" value="{TO_DATE}" class="date-to" /></td>
		<td>&nbsp;</td>
	</tr>
		{HISTORY}
	<tr class="list last"><td colspan="4"><div class="count">{COUNT}</div>{LIST}</td></tr>
</table>

<div id="btnfilter">
	<input type="submit" value="{LANG_TXT_SET_FILTER}" class="submit" />
	<input type="submit" value="{LANG_TXT_RESET_FILTER}" class="reset" name="reset" />
</div>

</form>

<script type="text/javascript" language="JavaScript">

$(document).ready(function() {

	$(".date-from").datepicker({maxDate: "{TO_DATE}"});
	$(".date-to").datepicker({minDate: "{FROM_DATE}"});

	$("input[name='filter[dates][date][0]']").bind('change', function(){
		currvalue = $("input[name='filter[dates][date][0]']").val();
		$("input[name='filter[dates][date][0]']").val(currvalue+" 12:00AM");
		$(".date-to").datepicker("option", "minDate", currvalue);
	});

	$("input[name='filter[dates][date][1]']").bind('change', function(){
		currvalue = $("input[name='filter[dates][date][1]']").val();
		$("input[name='filter[dates][date][1]']").val(currvalue+" 12:00PM");
		$("#filterform").submit();
	});
		
});

</script>