<tr class="change{IsBlocked} blocked{-IsBlocked}">
	<td>{IsNotice}<a href="#" onclick="alert('{NOTICE}'); return false;"><span title="{LANG_TXT_NOTICE}" class="list"></span></a>{-IsNotice}</td>
	<td>
		<b>{NAME}</b><br />
		{ADDRESS1}
		{IsAddress2}<br />{ADDRESS2}<br />{-IsAddress2}
		{IsAddress3}{ADDRESS3}{-IsAddress3}<br />
		{CITY}{IsZip}, {ZIP}{-IsZip}{IsState}, {STATE}{-IsState}<br />
		{COUNTRY_TXT}
	</td>
	<td><b>+{PHONE_CODE}-{PHONE_TXT}</b></td>
	<td>+{CELLULAR_CODE}-{CELLULAR_TXT}</td>
	<td>{ADD_DATE_TXT}</td>
	<td>{DEL_DATE_TXT}</td>
	<td>{STATUS_TXT}</td>
	<td><a class="reference" data-value="{NAME}|{PHONE_CODE}|{PHONE}|{CELLULAR_CODE}|{CELLULAR}|{ADDRESS1}|{ADDRESS2}|{ADDRESS3}|{CITY}|{ZIP}|{STATE}|{COUNTRY}"><span title="{LANG_TXT_UPDATE_REFERENCE}" class="edit"></span></a></td>
	<td><a href="{PATH_ADMIN}clients/reference-delete/{REFERENCE_ID}/"><span title="{LANG_TXT_DELETE_REFERENCE}" class="delete"></span></a></td>
</tr>