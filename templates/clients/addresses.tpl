<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CLIENT_ID}:</b> {CLIENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {FIRSTNAME} {LASTNAME}</a>
	</div>

	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/detail/{CLIENT_ID}/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}clients/addresses/{CLIENT_ID}/" method="post">

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_ADD_ADDRESS_TYPE} <span class="red">*</span></td>
		<td>
			<select name="add[type]" style="width: 200px" />
				<option value="">- {LANG_TXT_SELECT} -</option>
				<option value=""></option>
				{TYPES}
			</select>
		</td>
	</tr>
	<tr>
		<td class="title" width="30%"><br />{LANG_TXT_ADDRESS} <span class="red">*</span><br /><br /><br /><br /><br /></td>
		<td><br />
			<input type="text" name="add[address1]" value="{ADDRESS1}" style="width: 350px" maxlength="128" /><br />
			<input type="text" name="add[address2]" value="{ADDRESS2}" style="width: 350px" maxlength="128" /><br />
			<input type="text" name="add[address3]" value="{ADDRESS3}" style="width: 350px" maxlength="128" /><br /><br />
		</td>
	</tr>
	<tr>
		<td class="title" width="30%">{LANG_TXT_CITY} <span class="red">*</span></td>
		<td>
			<input type="text" name="add[city]" value="{CITY}" style="width: 250px" maxlength="64" />
		</td>
	</tr>
	<tr>
		<td class="title" width="30%">{LANG_TXT_ZIP}</td>
		<td>
			<input type="text" name="add[zip]" value="{ZIP}" style="width: 200px" maxlength="8" />
		</td>
	</tr>
	<tr>
		<td class="title" width="30%">{LANG_TXT_STATE} <span class="red">*</span></td>
		<td>
			<input type="text" name="add[state]" value="{STATE}" style="width: 250px" maxlength="64" />
		</td>
	</tr>
		<tr>
			<td class="title"><br />{LANG_TXT_COUNTRY} <span class="red">*</span></td>
			<td>
				<br />
				<select name="add[country]" style="width: 250px" />
					<option value="">- {LANG_TXT_SELECT} -</option>
					<option value=""></option>
					<option value="do">{LANG_TXT_DOMINICAN_REPUBLIC}</option>
					<option value=""></option>
					{COUNTRIES}
				</select>
			</td>
		</tr>
	<tr class="last-bd"><td class="title">&nbsp;</td><td><br /><input class="submit" type="submit" value="{LANG_TXT_ADD_ADDRESS}" /></td></tr>
</table>

</form>

<br />

<form action="{PATH_ADMIN}clients/addresses/{CLIENT_ID}/" method="post">

<table cellpadding="0" cellspacing="0" class="classic">
	<tr class="title">
		<td><a href="{PATH_ADMIN}clients/addresses/{CLIENT_ID}/?order=type&amp;sort={SORT}">{LANG_TXT_TYPE}</a></td>
		<td>{LANG_TXT_ADDRESS}</td>
		<td>{LANG_TXT_CITY}</td>
		<td>{LANG_TXT_ZIP}</td>
		<td>{LANG_TXT_STATE}</td>
		<td>{LANG_TXT_COUNTRY}</td>
		<td><a href="{PATH_ADMIN}clients/addresses/{CLIENT_ID}/?order=add_date&amp;sort={SORT}">{LANG_TXT_ADD_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/addresses/{CLIENT_ID}/?order=del_date&amp;sort={SORT}">{LANG_TXT_DEL_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/addresses/{CLIENT_ID}/?order=status&amp;sort={SORT}">{LANG_TXT_STATUS}</a></td>
		<td colspan="2">{LANG_TXT_MANAGE}</td>
	</tr>
	<tr class="title">
		<td><select name="filter[type][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{FILTER_TYPES}</select></td>
		<td colspan="7">&nbsp;</td>
		<td><select name="filter[status][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{STATUSES}</select></td>
		<td colspan="2">&nbsp;</td>
	</tr>
		{ADDRESSES}
	<tr class="list last"><td colspan="11"><div class="count">{COUNT}</div>{LIST}</td></tr>
</table>

<div id="btnfilter">
	<input type="submit" value="{LANG_TXT_SET_FILTER}" class="submit" />
	<input type="submit" value="{LANG_TXT_RESET_FILTER}" class="reset" name="reset" />
</div>

</form>

<script type="text/javascript" language="JavaScript">
$(document).ready(function() {

	$('a.address').click(function() {
		
		var elements = $(this).data("value").split('|');
				
		$(':input[name="add[type]"]').val(elements[0]);
		$(':input[name="add[address1]"]').val(elements[1]);
		$(':input[name="add[address2]"]').val(elements[2]);
		$(':input[name="add[address3]"]').val(elements[3]);
		$(':input[name="add[city]"]').val(elements[4]);
		$(':input[name="add[zip]"]').val(elements[5]);
		$(':input[name="add[state]"]').val(elements[6]);
		$(':input[name="add[country]"]').val(elements[7]);
		
	});
	
});

</script>