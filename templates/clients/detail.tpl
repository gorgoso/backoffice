<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CLIENT_ID}:</b> {CLIENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {CLIENT_NAME}</a>
	</div>
	{ShowHistory}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}clients/history/{CLIENT_ID}/">{LANG_TXT_CLIENT_HISTORY}</a>
	</div>{-ShowHistory}
	{ShowFiles}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}clients/files/{CLIENT_ID}/">{LANG_TXT_CLIENT_FILES}</a>
	</div>{-ShowFiles}
	{ShowSettings}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}clients/settings/{CLIENT_ID}/">{LANG_TXT_CLIENT_SETTINGS}</a>
	</div>{-ShowSettings}
	<div class="right">
		<div class="tab tabchange">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/list/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<div ng-controller="passengerController">
<form name="passengerForm" ng-submit="submit(passengerForm.$valid,'{CLIENT_ID_TXT}','{PATH_ADMIN}')">
<table class="classic" >
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_CLIENT_NAME}</td>
		<td>{CLIENT_NAME}</td>
		<td>
			<label>Title</label>
			 <select id="selectTitles">
				{TITLES}
			</select>
		</td>
		<td>
			<label>FirstName</label>
			<input id="inputFirstName" type="text" value="{CLIENT_FIRST_NAME}" required />
		</td>
		<td>
		<label>LastName</label>
		<input id="inputLastName" type="text" value="{CLIENT_LAST_NAME}" required />
		</td>
	</tr>
	<tr >
		<td class="title" >{LANG_TXT_ADD_DATE}</td>
		<td>{ADD_DATE_TXT}</td>		
	</tr>
	<tr>
		<td class="title">{LANG_TXT_LOG_DATE}</td>
		<td>{LOG_DATE_TXT}</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_TYPE}<br /><br /></td>
		<td><br /><b>{TYPE_TXT}</b><br /><br /></td>
		<td>
			<label>Role</label>
			 <select id="selectRole">
				{ROLES}
			</select>
		</td>
	</tr>
	<tr class="last-bd">
		<td class="title">{LANG_TXT_STATUS}</td>
		<td>
		{IsPending}<span class="red">{CLIENT_STATUS_TXT}</span>{-IsPending}
		{IsNotPending}<span >{CLIENT_STATUS_TXT}</span>{-IsNotPending}
		</td>
		<td>
			<label>Status</label>
			 <select id="selectStatus">
				{STATUSES}
			</select>
		</td></td><td></td></td><td></td></td></td><td></td>
	</tr>
	
</table>

<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	{ShowClientDetail}
	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a class="click" name="client-detail">{LANG_TXT_CLIENT_DETAIL}</a>
	</div>{-ShowClientDetail}
	{ShowTourDetail}<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a class="click" name="tour-detail">{LANG_TXT_TOUR_DETAIL}</a>
	</div>{-ShowTourDetail}
	{ShowEmails}<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a class="click" name="tour-emails">{LANG_TXT_EMAILS}</a>
	</div>{-ShowEmails}
</div>

<table class="classic">
	<tr class="last">
		<td class="title"></td>
	</tr>
</table>

<br /><br />

{ShowClientDetail}
<div id="client-detail" class="toggle">

	<div class="navigation">
		<div class="limg"></div>
		<div class="rimg"></div>

		<div class="tab">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a><b>{LANG_TXT_CONTACTS}</b></a>
		</div>
		{ShowContactsUpdate}<div class="tab tabchange">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/contacts/{CLIENT_ID}/">{LANG_TXT_MANAGE_CONTACTS}</a>
		</div>{-ShowContactsUpdate}
	</div>

	<table class="classic">
		{IsNotContacts}
		<tr class="last">
			<td class="title" width="30%">&nbsp;</td>
			<td><br />{LANG_TXT_NO_CONTACTS}<br /><br /></td>
		</tr>{-IsNotContacts}
		{IsContacts}
		<div id="sectionContact" class="secstion">
		{CONTACTS}	
		</div>	
		{-IsContacts}
	</table>

	<br />

	<div class="navigation">
		<div class="limg"></div>
		<div class="rimg"></div>

		<div class="tab">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a><b>{LANG_TXT_INFORMATION}</b></a>
		</div>
		{ShowInformationUpdate}
		<div class="tab tabchange">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/informations/{CLIENT_ID}/">{LANG_TXT_MANAGE_INFORMATION}</a>
		</div>
		{-ShowInformationUpdate}

	</div>

	<table class="classic">
		{IsNotInformations}<tr class="last">
			<td class="title" width="30%">&nbsp;</td>
			<td><br />{LANG_TXT_NO_INFORMATION}<br /><br /></td>
		</tr>{-IsNotInformations}
		{IsInformations}{INFORMATIONS}{-IsInformations}
	</table>

	<br />

	<div class="navigation">
		<div class="limg"></div>
		<div class="rimg"></div>
		<div class="tab">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a><b>{LANG_TXT_ADDRESSES}</b></a>
		</div>
		{ShowAddressesUpdate}<div class="tab tabchange">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/addresses/{CLIENT_ID}/">{LANG_TXT_MANAGE_ADDRESSES}</a>
		</div>{-ShowAddressesUpdate}
	</div>

	<table class="classic">
		{IsNotAddress}
		<tr class="last">
			<td class="title" width="30%">&nbsp;</td>
			<td><br />{LANG_TXT_NO_ADDRESS}<br /><br /></td>
		</tr>
		{-IsNotAddress}
		{IsAddress}
		<tr class="first">
			
			<td class="title">{LANG_TXT_COUNTRY}</td>
			<td>{ADDRESS_COUNTRY_TXT}</td>
			<td>
				<select class="address" ng-init="country='{SELECTED_COUNTRY}'" ng-model="country" id="country" ng-change="updStates(country,'{PATH_ADMIN}')">
				
					{ADDRESS_COUNTRY}
				</select>
			</td>
			
		</tr>
		<tr>
			<td class="title">{LANG_TXT_STATE}</td>
			<td>{ADDRESS_STATE_TXT} ({ADDRESS_STATE})</td>
			<td>
				<select  class="address" id="state">
					{ADDRESS_STATE_LIST}
				</select>
			</td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_CITY}</td>
			<td>{ADDRESS_CITY}</td>
			<td><input type="text" value="{ADDRESS_CITY}" class="address" id="city" required/></td>
		</tr>
		
		{IsZip}<tr>
			<td class="title">{LANG_TXT_ZIP}</td>
			<td>{ADDRESS_ZIP}</td>
			<td><input type="number" value="{ADDRESS_ZIP}" class="address" id="zip" required /></td>
		</tr>{-IsZip}
		<tr class="last-bd">
			<td class="title" width="30%">{LANG_TXT_ADDRESS}</td>
			<td>
				{ADDRESS1}<br />
				{IsAddress2}{ADDRESS2}<br />{-IsAddress2}
				{IsAddress3}{ADDRESS3}<br />{-IsAddress3}
			</td>
			<td>
				<input type="text" value="{ADDRESS1}" class="address" required id="address1" /><br/>
				{IsAddress2}
				<input type="text" value="{ADDRESS2}" class="address" required id="address2" /><br />
				{-IsAddress2}
				{IsAddress3}
				<input type="text" value="{ADDRESS3}" required class="address" id="address3" /><br />
				{-IsAddress3}


			</td>
		</tr>{-IsAddress}
	</table>

	<br />

	{IsOrganization}<table width="100%" class="nb">
		<tr>
			<td width="50%" valign="top" class="nb">{-IsOrganization}
				<div class="navigation">
					<div class="limg"></div>
					<div class="rimg"></div>

					<div class="tab">
						<div class="limg"></div>
						<div class="rimg"></div>
						<a><b>{LANG_TXT_ORGANIZATION}</b></a>
					</div>
					{ShowOrganizationUpdate}<div class="tab tabchange">
						<div class="limg"></div>
						<div class="rimg"></div>
						<a href="{PATH_ADMIN}clients/organization/{CLIENT_ID}/">{LANG_TXT_MANAGE_ORGANIZATION}</a>
					</div>{-ShowOrganizationUpdate}
				</div>

				<table class="classic">
					{IsNotOrganization}
					<tr class="last">
						<td class="title" width="30%">&nbsp;</td>
						<td><br />{LANG_TXT_NO_ORGANIZATION}<br /><br /></td>
					</tr>
					{-IsNotOrganization}
					{IsOrganization}
					<tr class="first">
						<td class="title" width="30%">{LANG_TXT_ORGANIZATION_NAME}</td>
						<td><b>{ORGANIZATION_NAME}</b></td>
						<td><input class="organization" required type="text" value="{ORGANIZATION_NAME}" /></td>
					</tr>
					<tr>
						<td class="title"><br />{LANG_TXT_ORGANIZATION_TYPE}</td>
						<td><br />{ORGANIZATION_TYPE_TXT}</td>
						<td><select class="organization">{ORGANIZATION_TYPE_LIST}</select></td>
					</tr>
					<tr>
						<td class="title">{LANG_TXT_DENOMINATION}</td>
						<td>{DENOMINATION_TXT}</td>
						<td><select class="organization">{DENOMINATION_TYPE_LIST}</select></td>
					</tr>
					<tr>
						<td class="title"><br />{LANG_TXT_PHONE}</td>
						<td><br />{ORGANIZATION_PHONE_TXT}</td>
						<td> <input type="text" class="organization" required value="{ORGANIZATION_PHONE_TXT}" /></td>
					</tr>
					<tr>
						<td class="title">{LANG_TXT_EMAIL}</td>
						<td>{ORGANIZATION_EMAIL_TXT}</td>
						<td> <input type="email" class="organization" required value="{ORGANIZATION_EMAIL_TXT}" /></td>
					</tr>
					<tr>
						<td class="title">{LANG_TXT_WWW}</td>
						<td>{ORGANIZATION_WWW_TXT}</td>
						<td> <input type="url" class="organization"  value="{ORGANIZATION_WWW_TXT}" /></td>
					</tr>
					<tr>
						<td class="title"><br />{LANG_TXT_MEMBERS}</td>
						<td><br />{MEMBERS_TXT}</td>
						<td> <input type="number" class="organization"  value="{MEMBERS_TXT}" /></td>
					</tr>
					<tr>
						<td class="title">{LANG_TXT_SUPPORTERS}</td>
						<td>{SUPPORTERS_TXT}</td>
						<td> <input type="number" class="organization"  value="{SUPPORTERS_TXT}" /></td>
					</tr>
					<tr class="last-bd">
						<td class="title">{LANG_TXT_PARTICIPANTS}</td>
						<td>{PARTICIPANTS_TXT}</td>
						<td> <input type="number" class="organization"  value="{PARTICIPANTS_TXT}" /></td>
					</tr>{-IsOrganization}
				</table>
			{IsOrganization}</td>
			<td width="1%" class="nb">&nbsp;</td>
			<td width="49%" valign="top" class="nb">
				<div class="navigation">
					<div class="limg"></div>
					<div class="rimg"></div>

					<div class="tab">
						<div class="limg"></div>
						<div class="rimg"></div>
						<a><b>{LANG_TXT_ORGANIZATION_ADDRESS}</b></a>
					</div>
				</div>

				<table class="classic">
					<tr class="first">
						<td class="title" width="30%">{LANG_TXT_COUNTRY}</td>						
						<td>{ORGANIZATION_COUNTRY_TXT}</td>
						<td><select class="organizationAddress" ng-init="countryOrganization='{SELECTED_COUNTRY_ORGANIZATION}'" ng-model="countryOrganization" id="countryOrganization" ng-change="updStatesOrganization(countryOrganization,'{PATH_ADMIN}')">				
					{ADDRESS_COUNTRY}
				</select></td>
						
					</tr>
					<tr>
						
						<td class="title">{LANG_TXT_STATE}</td>
						<td>{ORGANIZATION_STATE_TXT} ({ORGANIZATION_STATE})</td>
						<td>
						<select  class="organizationAddress" id="state">
							{ADDRESS_STATE_LIST}
						</select>
						</td>
					</tr>
					<tr>
						<td class="title">{LANG_TXT_CITY}</td>
						<td>{ORGANIZATION_CITY}</td>
						<td><input class="organizationAddress" type="text" value="{ORGANIZATION_CITY}" /></td>
					</tr>
					{IsZipOrganization}<tr>
						<td class="title">{LANG_TXT_ZIP}</td>
						<td>{ORGANIZATION_ZIP}</td>
						<td><input class="organizationAddress" type="number" value="{ORGANIZATION_ZIP}" /></td>
					</tr>{-IsZipOrganization}
					<tr class="last-bd">
						<td class="title">{LANG_TXT_ADDRESS}</td>
						<td>
							{ORGANIZATION_ADDRESS1}<br />
							{IsAddressOrganization2}{ORGANIZATION_ADDRESS2}<br />{-IsAddressOrganization2}
							{IsAddressOrganization3}{ORGANIZATION_ADDRESS3}<br />{-IsAddressOrganization3}
						</td>
						<td>
							<input type="text" value="{ORGANIZATION_ADDRESS1}" class="organizationAddress"  id="address1" /><br/>
				{IsAddress2}
				<input type="text" value="{ORGANIZATION_ADDRESS2}" class="organizationAddress"  id="address2" /><br />
				{-IsAddress2}
				{IsAddress3}
				<input type="text" value="{ORGANIZATION_ADDRESS3}"  class="organizationAddress" id="address3" /><br />
				{-IsAddress3}
						</td>
					</tr>
				</table>

			</td>
		</tr>
	</table>{-IsOrganization}
	<!-- UPDATE CLIENT DETAIL BUTTON(Victor Marin) -->
<table class="classic">
	<tr>
	<td></td><td></td><td style="width:100px;"></td><td style="width:100px;"></td><td></td><td></td>
	<td style="text-align:right;" > <br/><button type="submit" style="width:75px; margin-left:10px;"  ><i class="fa fa-floppy-o fa-3 "></i></button></td>
	
	</tr>
</table>
</form>
</div>
<!-- UPDATE CLIENT DETAIL BUTTON -->

	<!--{CanVerifyLoan}{IsDeliver}<div id="btnfilter">
		<form action="{PATH_ADMIN}loans/deliver/{LOAN_ID}/" method="post">
			<input class="submit" type="submit" name="order[submit]" value="{LANG_TXT_DELIVER_LOAN}" />
		</form>
	</div>{-IsDeliver}

	{IsNotPayed}{IsNotApproved}{IsVerified}<div id="btnfilter">
		<form action="{PATH_ADMIN}loans/approve/{LOAN_ID}/" method="post">
			<input class="submit" type="submit" name="order[submit]" value="{LANG_TXT_APPROVE_LOAN}" />
		</form>
	</div>{-IsVerified}

	{CanBeDeclined}<div id="btnfilter_right">
		<form action="{PATH_ADMIN}loans/decline/{LOAN_ID}/" method="post">
			<input type="submit" name="order[submit]" value="{LANG_TXT_DECLINE_LOAN}" />
		</form>
	</div>{-CanBeDeclined}{-IsNotApproved}{-IsNotPayed}{-CanVerifyLoan}-->

</div>

{-ShowClientDetail}

{ShowTourDetail}<div id="tour-detail" class="toggle">

	<div class="navigation">
		<div class="limg"></div>
		<div class="rimg"></div>

		<div class="tab">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a><b>{LANG_TXT_TOUR_PARAMETERS}</b></a>
		</div>
	</div>

	<table class="classic">
		<tr class="first">
			<td class="title" width="30%">{LANG_TXT_REGDATE}</td>
			<td>{DATE_TXT}</td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_USER}</td>
			<td>{USER_TXT}</td>
		</tr>
		<tr>
			<td class="title"><br />{LANG_TXT_CODE}<br /><br /></td>
			<td><br /><b>{CODE}</b><br /><br /></td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_DEPARTURE_DATE}</td>
			<td>{DEPARTURE_DATE_TXT}</td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_DEPARTURE_CITY}</td>
			<td>{DEPARTURE_CITY_TXT}</td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_PRICE}</td>
			<td>{PRICE_TXT} USD</td>
		</tr>
		<tr class="last-bd">
			<td class="title">{LANG_TXT_TOUR_STATUS}</td>
			<td>{IsDisabled}<span class="red">{-IsDisabled}{STATUS_TXT}{IsDisabled}</span>{-IsDisabled}</td>
		</tr>
	</table>

	{ShowPayments}<br />

	<div class="navigation">
		<div class="limg"></div>
		<div class="rimg"></div>

		<div class="tab">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a><b>{LANG_TXT_PAYMENTS}</b></a>
		</div>
		{ShowAddNewPayment}{IsNotPayed}<div class="tab tabchange">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}payments/add/{CLIENT_ID}/">{LANG_TXT_ADD_PAYMENT}</a>
		</div>{-IsNotPayed}{-ShowAddNewPayment}

	</div>

	<table class="classic">
		<tr>
			<td class="title">&nbsp;</td>
			<td class="title">{LANG_TXT_PAYMENT_ID}</td>
			<td class="title">{LANG_TXT_PAYMENT_ADD_DATE}</td>
			<td class="title">{LANG_TXT_PAYMENT_PAY_DATE}</td>
			<td class="title">{LANG_TXT_PAYMENT_TYPE}</td>
			<td class="title">{LANG_TXT_PAYMENT_METHOD}</td>
			<td class="title">{LANG_TXT_PAYMENT_AMOUNT}</td>
			<td class="title">{LANG_TXT_PAYMENT_STATUS}</td>
		</tr>
		{PAYMENTS}
		<tr class="list last">
			<td colspan="6">{LANG_TXT_TOTAL_AMOUNTS}</td>
			<td>{TOTAL_PAYMENT_AMOUNT_TXT} USD</td>
			<td>&nbsp;</td>
		</tr>
	</table>{-ShowPayments}

</div>{-ShowTourDetail}

<script>
	$(document).ready(function() {
		function createCookie(name,value) {
			document.cookie = name+"="+value+"; path=/";
		}
		function readCookie(name) {
			var nameEQ = name + "=";
			var ca = document.cookie.split(';');
			for(var i=0;i < ca.length;i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1,c.length);
				if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
			}
			return null;
		}
		function ShowContent(content) {
			$("div.toggle").hide();
			$("#"+content).show();
			$("a.click").css('font-weight', 'normal');
			$("a.click[name='"+content+"']").css('font-weight', 'bold');
			createCookie("show-content", content);
			return true;
		}
		$("a.click").click(function() {ShowContent($(this).attr('name'));});
		var content = readCookie("show-content");
		if (!content) content = "client-detail";
		ShowContent(content);
	});
</script>