<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CLIENT_ID}:</b> {CLIENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {FIRSTNAME} {LASTNAME}</a>
	</div>

	{IsLoanID}<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_LOAN_ID}:</b> {LOAN_ID_TXT}, <b>{LANG_TXT_LOAN_DATE}:</b> {LOAN_DATE_TXT}</a>
	</div>{-IsLoanID}

	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			{IsNotLoanID}<a href="{PATH_ADMIN}clients/detail/{CLIENT_ID}/">{LANG_TXT_BACK_CLIENT}</a>{-IsNotLoanID}
			{IsLoanID}<a href="{PATH_ADMIN}loans/detail/{LOAN_ID}/">{LANG_TXT_BACK_LOAN}</a>{-IsLoanID}
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}clients/files/{CLIENT_ID}/" method="post" enctype="multipart/form-data">

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_GROUP} <span class="red">*</span></td>
		<td>
			<select name="add[group]" style="width: 200px">
				<option value="">- {LANG_TXT_SELECT} -</option>
				<option value=""></option>
				{GROUPS}
			</select>
		</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_LOAN}</td>
		<td>
			<select name="add[loan_id]">
				<option value="">- {LANG_TXT_SELECT} -</option>
				<option value=""></option>
				{LOANS}
			</select>
		</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_NOTE}</td>
		<td>
			<input type="text" name="add[note]" value="{NOTE}" style="width: 350px" maxlength="128" />
		</td>
	</tr>
	<tr>
		<td class="title" width="30%"><br />{LANG_TXT_FILE} <span class="red">*</span><br /><br /></td>
		<td><br /><input type="file" name="update" style="width: 350px" /><br /><br /></td>
	</tr>
	<tr class="last-bd"><td class="title">&nbsp;</td><td><input class="submit" type="submit" value="{LANG_TXT_ADD_FILE}" /></td></tr>
</table>

</form>

<br />

<form action="{PATH_ADMIN}clients/files/{CLIENT_ID}/" method="post">

<table class="classic">
	<tr class="title">
		<td>&nbsp;</td>
		<td><a href="{PATH_ADMIN}clients/files/{CLIENT_ID}/?order=loan_id&amp;sort={SORT}">{LANG_TXT_LOAN}</a></td>
		<td><a href="{PATH_ADMIN}clients/files/{CLIENT_ID}/?order=group&amp;sort={SORT}">{LANG_TXT_GROUP}</a></td>
		<td><a href="{PATH_ADMIN}clients/files/{CLIENT_ID}/?order=add_date&amp;sort={SORT}">{LANG_TXT_ADD_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/files/{CLIENT_ID}/?order=del_date&amp;sort={SORT}">{LANG_TXT_DEL_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/files/{CLIENT_ID}/?order=note&amp;sort={SORT}">{LANG_TXT_NOTE}</a></td>
		<td><a href="{PATH_ADMIN}clients/files/{CLIENT_ID}/?order=status&amp;sort={SORT}">{LANG_TXT_STATUS}</a></td>
		<td>{LANG_TXT_MANAGE}</td>
	</tr>
	<tr class="title">
		<td>&nbsp;</td>
		<td><select name="filter[loan_id][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{LOANS_FILTER}</select></td>
		<td><select name="filter[group][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{GROUPS_FILTER}</select></td>
		<td colspan="2">&nbsp;</td>
		<td><input type="text" style="width:150px" name="filter[note]" value="{NOTE_FILTER}" /></td>
		<td><select name="filter[status][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{STATUSES}</select></td>
		<td>&nbsp;</td>
	</tr>
		{FILES}
	<tr class="list last"><td colspan="8"><div class="count">{COUNT}</div>{LIST}</td></tr>
</table>

<div id="btnfilter">
	<input type="submit" value="{LANG_TXT_SET_FILTER}" class="submit" />
	<input type="submit" value="{LANG_TXT_RESET_FILTER}" class="reset" name="reset" />
</div>

</form>