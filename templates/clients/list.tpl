<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	{ShowAddNewClient}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}clients/add/">{LANG_TXT_ADD_CLIENT}</a>
	</div>{-ShowAddNewClient}
</div>

<form action="{PATH_ADMIN}clients/list/" method="post" name="clients">
<table class="classic">
	<tr class="title">
		<td>&nbsp;</td>
		<td><a href="{PATH_ADMIN}clients/list/?order=client_id&amp;sort={SORT}">{LANG_TXT_CLIENT_ID}</a></td>
		<td><a href="{PATH_ADMIN}clients/list/?order=role&amp;sort={SORT}">{LANG_TXT_ROLE}</a></td>
		<td><a href="{PATH_ADMIN}clients/list/?order=add_date&amp;sort={SORT}">{LANG_TXT_ADD_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/list/?order=firstname&amp;sort={SORT}">{LANG_TXT_FIRSTNAME}</a></td>
		<td><a href="{PATH_ADMIN}clients/list/?order=lastname&amp;sort={SORT}">{LANG_TXT_LASTNAME}</a></td>
		<td><a href="{PATH_ADMIN}clients/list/?order=email&amp;sort={SORT}">{LANG_TXT_EMAIL}</a></td>
		<td><a href="{PATH_ADMIN}clients/list/?order=status&amp;sort={SORT}">{LANG_TXT_STATUS}</a></td>
	</tr>
	<tr class="title">
		<td>&nbsp;</td>
		<td><input type="text" style="width:100px" name="filter[client_id][multiple][]" value="{CLIENT_ID}" /></td>
		<td><select name="filter[role][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{ROLES}</select></td>
		<td><input type="text" style="width:140px" name="filter[dates][date][0]" value="{FROM_DATE}" class="date-from" /><br /><input type="text" style="width:140px" name="filter[dates][date][1]" value="{TO_DATE}" class="date-to" /></td>
		<td><input type="text" style="width:150px" name="filter[firstname]" value="{FIRSTNAME}" /></td>
		<td><input type="text" style="width:200px" name="filter[lastname]" value="{LASTNAME}" /></td>
		<td><input type="text" style="width:140px" name="filter[email]" value="{EMAIL}" /></td>
		<td><select name="filter[status][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{STATUSES}</select></td>
	</tr>
		{CLIENTS}
	<tr class="list last"><td colspan="8"><div class="count">{COUNT}</div>{LIST}</td></tr>
</table>

<div id="btnfilter">
	<input type="submit" value="{LANG_TXT_SET_FILTER}" class="submit" />
	<input type="submit" value="{LANG_TXT_RESET_FILTER}" class="reset" name="reset" />
</div>

</form>

<script type="text/javascript" language="JavaScript">

$(document).ready(function() {

	$(".date-from").datepicker({maxDate: "{TO_DATE}"});
	$(".date-to").datepicker({minDate: "{FROM_DATE}"});

	$("input[name='filter[dates][date][0]']").bind('change', function(){
		currvalue = $("input[name='filter[dates][date][0]']").val();
		$("input[name='filter[dates][date][0]']").val(currvalue+" 12:00AM");
		$(".date-to").datepicker("option", "minDate", currvalue);
	});

	$("input[name='filter[dates][date][1]']").bind('change', function(){
		currvalue = $("input[name='filter[dates][date][1]']").val();
		$("input[name='filter[dates][date][1]']").val(currvalue+" 12:00PM");
		$("form").submit();
	});

});

</script>