<tr class="change{IsBlocked} blocked{-IsBlocked}">
	<td>{IsNotice}<a href="#" onclick="alert('{NOTICE}'); return false;"><span title="{LANG_TXT_NOTICE}" class="list"></span></a>{-IsNotice}</td>
	<td>{CONTACT_TYPE_TXT}</td>
	<td>
		<b>{IsEmail}<a href="mailto:{CONTACT}">{CONTACT}</a>{-IsEmail}
		{IsPhone}{CONTACT}{-IsPhone}</b>
	</td>
	<td>{ADD_DATE_TXT}</td>
	<td>{DEL_DATE_TXT}</td>
	<td>{STATUS_TXT}</td>
	<td>{IsNotDeleted}<a href="{PATH_ADMIN}clients/contact-delete/{CONTACT_ID}/"><span title="{LANG_TXT_DELETE_CONTACT}" class="delete"></span></a>{-IsNotDeleted}</td>
</tr>