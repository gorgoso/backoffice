<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_CLIENT_ID}:</b> {CLIENT_ID_TXT}, <b>{LANG_TXT_CLIENT_NAME}:</b> {FIRSTNAME} {LASTNAME}</a>
	</div>

	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}clients/detail/{CLIENT_ID}/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<form action="{PATH_ADMIN}clients/contacts/{CLIENT_ID}/" method="post">

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_ADD_CONTACT} <span class="red">*</span></td>
		<td>
			<select name="add[type]" style="width: 150px" />
				<option value=""></option>
				{TYPES}
			</select>
		</td>
	</tr>
	<tr id="calling-code">
		<td class="title" width="30%">{LANG_TXT_CALLING_CODE} <span class="red">*</span></td>
		<td>
			<select name="add[code]" />
				<option value="">- {LANG_TXT_SELECT} -</option>
				<option value=""></option>
				<option value="1809">{LANG_TXT_DOMINICAN_REPUBLIC} (+1-809)</option>
				<option value="1829">{LANG_TXT_DOMINICAN_REPUBLIC} (+1-829)</option>
				<option value="1849">{LANG_TXT_DOMINICAN_REPUBLIC} (+1-849)</option>
				<option value=""></option>
				{CALLING_CODES}
			</select>
		</td>
	</tr>
	<tr id="contact">
		<td class="title" width="30%"><span id="contact-description"></span>&nbsp;<span class="red">*</span></td>
		<td>
			<input type="text" name="add[contact]" value="{CONTACT}" style="width: 250px" maxlength="128" />
		</td>
	</tr>
	<tr id="notice">
		<td class="title" width="30%">{LANG_TXT_NOTICE}</td>
		<td>
			<input type="text" name="add[notice]" value="{NOTICE}" style="width: 250px" maxlength="128" />
		</td>
	</tr>
	<tr class="last-bd"><td class="title">&nbsp;</td><td><br /><input class="submit" type="submit" value="{LANG_TXT_ADD_CONTACT}" /></td></tr>
</table>

</form>

<br />

<form action="{PATH_ADMIN}clients/contacts/{CLIENT_ID}/" method="post">

<table cellpadding="0" cellspacing="0" class="classic">
	<tr class="title">
		<td>&nbsp;</td>
		<td><a href="{PATH_ADMIN}clients/contacts/{CLIENT_ID}/?order=type&amp;sort={SORT}">{LANG_TXT_TYPE}</a></td>
		<td><a href="{PATH_ADMIN}clients/contacts/{CLIENT_ID}/?order=contact&amp;sort={SORT}">{LANG_TXT_CONTACT}</a></td>
		<td><a href="{PATH_ADMIN}clients/contacts/{CLIENT_ID}/?order=add_date&amp;sort={SORT}">{LANG_TXT_ADD_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/contacts/{CLIENT_ID}/?order=del_date&amp;sort={SORT}">{LANG_TXT_DEL_DATE}</a></td>
		<td><a href="{PATH_ADMIN}clients/contacts/{CLIENT_ID}/?order=status&amp;sort={SORT}">{LANG_TXT_STATUS}</a></td>
		<td>{LANG_TXT_MANAGE}</td>
	</tr>
	<tr class="title">
		<td>&nbsp;</td>
		<td><select name="filter[type][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{FILTER_TYPES}</select></td>
		<td>&nbsp;</td>
		<td colspan="2">&nbsp;</td>
		<td><select name="filter[status][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{STATUSES}</select></td>
		<td>&nbsp;</td>
	</tr>
		{CONTACTS}
	<tr class="list last"><td colspan="7"><div class="count">{COUNT}</div>{LIST}</td></tr>
</table>

<div id="btnfilter">
	<input type="submit" value="{LANG_TXT_SET_FILTER}" class="submit" />
	<input type="submit" value="{LANG_TXT_RESET_FILTER}" class="reset" name="reset" />
</div>

</form>

<script type="text/javascript" language="JavaScript">

$(document).ready(function() {

	$("select[name='add[type]']").bind('change', function(){
		ShowCallingCode();
	});
	
	ShowCallingCode();
	
});

function ShowCallingCode() {
	
	var contacttype = $("select[name='add[type]']");
	
	if (!contacttype.val()) {
		
		$("tr#contact").hide();
		$("tr#calling-code").hide();
		$("tr#notice").hide();
			
	} else {
	
		if (contacttype.val() == "email") $("tr#calling-code").hide(); else $("tr#calling-code").show();
		$("tr#contact").show();
		$("tr#notice").show();
			
	}
		
	$("#contact-description").text($("select[name='add[type]'] option:selected").text());

}
</script>