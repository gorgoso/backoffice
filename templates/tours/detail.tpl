<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_TOUR_ID}:</b> {TOUR_ID_TXT}, <b>{LANG_TXT_TOUR_NAME}:</b> {TOUR_NAME}</a>
	</div>
	<!--{ShowHistory}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}clients/history/{CLIENT_ID}/">{LANG_TXT_CLIENT_HISTORY}</a>
	</div>{-ShowHistory}
	{ShowFiles}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}clients/files/{CLIENT_ID}/">{LANG_TXT_CLIENT_FILES}</a>
	</div>{-ShowFiles}
	{ShowSettings}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}clients/settings/{CLIENT_ID}/">{LANG_TXT_CLIENT_SETTINGS}</a>
	</div>{-ShowSettings}-->
	<div class="right">
		<div class="tab tabchange">
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}tours/list/">{LANG_TXT_BACK}</a>
		</div>
	</div>
</div>

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_REGDATE}</td>
		<td>{DATE_TXT}</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_USER}</td>
		<td>{USER_TXT}</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_CODE}<br /><br /></td>
		<td><br /><b>{CODE}</b><br /><br /></td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_DEPARTURE_DATE}</td>
		<td>{DEPARTURE_DATE_TXT}</td>
	</tr>
		<tr >
			<td class="title">{LANG_TXT_DEPARTURE_CITY}</td>
			<td><label id="lbDepCity">{DEPARTURE_CITY_TXT}</label></td>
			<td >
				<div ng-controller="departureCntroller">
					<select name="" ng-model="city" ng-change="updDepartureCity(city,'{PATH_ADMIN}','{TOUR_ID}')">
						<option value="">- all -</option>						
						<optgroup label="United States">{DEPARTURES_US}</optgroup>
						<optgroup label="Dominican_Republic">{DEPARTURES_DO}</optgroup>
					</select>
				</div>
			</td>
		</tr>
		
	<tr>
		<td class="title">{LANG_TXT_PRICE}</td>
		<td style="width:358px">{PRICE_TXT} USD</td>		
		<td style="width:158px">
			<div ng-controller="priceController" ng-init="init('{PRICE_TXT}')" >
				<input type="number" name"tourPrice" id="tourPriceId"  value="{{price}}"/>
			</div>
		 
		 </td>
		<td> 
		<div ng-controller="priceController">
			<button ng-click="updateTourPrice('{TOUR_ID}','{PATH_ADMIN}')"><i class="fa fa-refresh"></i>
		</div>
		
		</button></td>
		
		
	</tr>
	<tr >
		<td class="title">{LANG_TXT_TOUR_STATUS}</td>
		<td>{IsDisabled}<span class="red">{-IsDisabled}{STATUS_TXT}{IsDisabled}</span>{-IsDisabled}</td>
	</tr>
	<tr class="last-bd" ng-controller="tourController">
		<td class="title"></td>
		<td> <button ng-click="deleteTour('{TOUR_ID}','{PATH_ADMIN}')">Delete Tour</button></td>
	</tr>
</table>

{IsLeader}<br />

<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>
	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a><b>{LANG_TXT_TOUR_LEADER}</b></a>
	</div>
</div>

<table class="classic">
	<tr>
		<td class="title">&nbsp;</td>
		<td class="title">{LANG_TXT_CLIENT_ID}</td>
		<td class="title">{LANG_TXT_ADD_DATE}</td>
		<td class="title">{LANG_TXT_LASTLOG}</td>
		<td class="title">{LANG_TXT_FULLNAME}</td>
		<td class="title">{LANG_TXT_EMAIL}</td>
		<td class="title">{LANG_TXT_STATUS}</td>
	</tr>
	<tr class="change last">
		<td width="5%"><a href="{PATH_ADMIN}clients/detail/{LEADER_CLIENT_ID}/"><span title="{LANG_TXT_CLIENT_DETAIL}" class="list"></span></a></td>
		<td width="10%">{LEADER_CLIENT_ID_TXT}</td>
		<td width="15%">{LEADER_ADD_DATE_TXT}</td>
		<td width="15%">{LEADER_LOG_DATE_TXT}</td>
		<td><b>{LEADER_FULLNAME}</b></td>
		<td width="30%">{LEADER_EMAIL}</td>
		<td width="10%">{LEADER_STATUS_TXT}</td>
	</tr>
</table>

{-IsLeader}<br />

<div ng-controller="passengerController" ng-init="info=true; add=false">
<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab" ng-click="add=false; info= true" >
		<div class="limg"></div>
		<div class="rimg"></div>
		<a ng-class="{bold: info}">{LANG_TXT_PASSENGERES}</a>
	</div>
	{ShowAddPassenger}<div class="tab" ng-click="add=true; info=false" >
		<div class="limg"></div>
		<div class="rimg"></div>
		<a ng-class="{bold: add}">{LANG_TXT_ADD_PASSENGER}</a>
	</div>{-ShowAddPassenger}

</div>

<table class="classic" ng-show="info">
	{IsNotPassengers}<tr class="last">
		<td class="title" width="30%">&nbsp;</td>
		<td><br />{LANG_TXT_NO_PASSENGERS}<br /><br /></td>
	</tr>{-IsNotPassengers}
	{IsPassengers}<tr>
		<td class="title" width="5%">&nbsp;</td>
		<td class="title" width="10%">{LANG_TXT_CLIENT_ID}</td>
		<td class="title" width="10%">{LANG_TXT_ADD_DATE}</td>
		<td class="title" width="10%">{LANG_TXT_LASTLOG}</td>
		<td class="title">{LANG_TXT_FULLNAME}</td>
		<td class="title" width="20%">{LANG_TXT_EMAIL}</td>
		<td class="title" width="10%">{LANG_TXT_AMOUNT_PAID}</td>
		<td class="title" width="10%">{LANG_TXT_AMOUNT_PENDING}</td>
		<td class="title" width="10%">{LANG_TXT_STATUS}</td>
	</tr>
	{PASSENGERS}
	<tr class="last">
		<td class="title" colspan="6">&nbsp;</td>
		<td class="title">{AMOUNT_PAID_TOTAL} USD</td>
		<td class="title">{AMOUNT_PENDING_TOTAL} USD</td>
		<td class="title"><div class="count">{COUNT}</div></td>
	</tr>{-IsPassengers}
</table>


<table class="classic" ng-show="add">
	<tr>
		<td class="title" width="30%">{LANG_TXT_FULLNAME}</td>
		<td><input id="firstName" ng-model="firstName" type="text" placeholder="First" /> <input id="lastName" ng-model="lastName" type="text" placeholder="Last" /></td>
	</tr>
	<tr>
		<td class="title" width="30%">{LANG_TXT_EMAIL}</td>
		<td><input id="email" ng-model="email" type="text"/></td>
	</tr>
	<tr>
		<td class="title" width="30%">{LANG_TXT_PHONE}</td>
		<td><input id="phone" ng-model="phone" type="text"/></td>
	</tr>

	<tr>
		<td width="30%"></td>
		<td><button 
			ng-click="addPassenger('{TOUR_ID}','{PATH_ADMIN}','{TOUR_NAME}','{LEADER_FULLNAME}')" style="width:75px;">
			<i class="fa fa-floppy-o fa-3 "></i>
		</button></td>
	</tr>

</table>

</div>