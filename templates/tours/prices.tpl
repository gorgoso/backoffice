<div ng-controller="tourPriceController">
	
<form ng-submit="submit(passengerForm.$valid,'{PATH_ADMIN}')">
	
<table class="classic" >
<tr class="first">
<td>
<label>{LANG_TXT_TOURS_NAMES}</label><br/>
	<select id="selectTourName" required>
		{TOURS}
	</select>
</td>
<td>
<label>{LANG_TXT_DEPARTURE_CITY}</label><br/>
	<select id="selectDepartureCity" required>
		{DEPARTURES_US}
		<option disabled><b>--Dominican Republic--</b></option>
		{DEPARTURES_DO}
	</select>

</td>
<td>
	<label>{LANG_TXT_START_DATE}</label><br/>
	<input type="text" style="width:140px"   class="date-from" id="departureDate" required/>
</td>
<td>
<br/>
	<button id="searchButton" type="submit">Search</button>
</td>
</tr>

</table>
<br/><br/>
</form>

	
<table class="classic" id="tourPriceResult" >
<thead>
	<th>{LANG_TXT_TOURS_NAMES}</th>
	<th>{LANG_TXT_DEPARTURE_CITY}</th>
	<th>{LANG_TXT_START_DATE}</th>	
	<th>{LANG_TXT_PRICE}</th>
	<th>Update</th>


</thead>
<tbody>
	
</tbody>
	
</table>




</div>


<script type="text/javascript">
	$(document).ready(function() 
	{
		$(".date-from").datepicker();
	});

	function updatePrice(id){

		var price = $("#tourPriceId"+id).val();
		var url = "{PATH_ADMIN}/services/tourPriceService.php?action=updatePrice&tourPriceId="+id+"&price="+price;
		
		$.get(url,function(data){

			$("#searchButton").click();

		});
	}

</script>