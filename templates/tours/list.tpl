<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	{ShowAddNewTour}<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}tours/add/">{LANG_TXT_ADD_TOUR}</a>
	</div>{-ShowAddNewTour}
</div>

<form action="{PATH_ADMIN}tours/list/" method="post" name="tours">
<table class="classic">
	<tr class="title">
		<td>&nbsp;</td>
		<td><a href="{PATH_ADMIN}tours/list/?order=tour_id&amp;sort={SORT}">{LANG_TXT_TOUR_ID}</a></td>
		<td><a href="{PATH_ADMIN}tours/list/?order=code&amp;sort={SORT}">{LANG_TXT_CODE}</a></td>
		<td><a href="{PATH_ADMIN}tours/list/?order=add_date&amp;sort={SORT}">{LANG_TXT_ADD_DATE}</a></td>
		<td><a href="{PATH_ADMIN}tours/list/?order=departure_date&amp;sort={SORT}">{LANG_TXT_DEPARTURE_DATE}</a></td>
		<td><a href="{PATH_ADMIN}tours/list/?order=departure_id&amp;sort={SORT}">{LANG_TXT_DEPARTURE_CITY}</a></td>
		<td><a href="{PATH_ADMIN}tours/list/?order=tour&amp;sort={SORT}">{LANG_TXT_TOUR}</a></td>
		<td><a href="{PATH_ADMIN}tours/list/?order=price&amp;sort={SORT}">{LANG_TXT_PRICE}</a></td>
		<td><a href="{PATH_ADMIN}tours/list/?order=status&amp;sort={SORT}">{LANG_TXT_STATUS}</a></td>
	</tr>
	<tr class="title">
		<td>&nbsp;</td>
		<td><input type="text" style="width:100px" name="filter[tour_id][multiple][]" value="{TOUR_ID}" /></td>
		<td><input type="text" style="width:100px" name="filter[code]" value="{CODE}" /></td>
		<td><input type="text" style="width:140px" name="filter[dates][add_date][0]" value="{FROM_ADD_DATE}" class="date-from" /><br /><input type="text" style="width:140px" name="filter[dates][add_date][1]" value="{TO_ADD_DATE}" class="date-to" /></td>
		<td><input type="text" style="width:140px" name="filter[dates][departure_date][0]" value="{FROM_DEPARTURE_DATE}" class="date-from" /><br /><input type="text" style="width:140px" name="filter[dates][departure_date][1]" value="{TO_DEPARTURE_DATE}" class="date-to" /></td>
		<td>
			<select name="filter[departure_id][multiple][]" onchange="this.form.submit();">
				<option value="">- {LANG_TXT_ALL} -</option>
				<option value=""></option>
				<optgroup label="{LANG_TXT_USA}">{DEPARTURES_US}</optgroup>
				<optgroup label="{LANG_TXT_DOMINICAN_REPUBLIC}">{DEPARTURES_DO}</optgroup>
			</select>
		</td>
		<td><select name="filter[tour][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{TOURS_TYPES}</select></td>
		<td><input type="text" style="width:150px" name="filter[price]" value="{PRICE}" /></td>
		<td><select name="filter[status][multiple][]" onchange="this.form.submit();"><option value="">- {LANG_TXT_ALL} -</option><option value=""></option>{STATUSES}</select></td>
	</tr>
		{TOURS}
	<tr class="list last"><td colspan="9"><div class="count">{COUNT}</div>{LIST}</td></tr>
</table>

<div id="btnfilter">
	<input type="submit" value="{LANG_TXT_SET_FILTER}" class="submit" />
	<input type="submit" value="{LANG_TXT_RESET_FILTER}" class="reset" name="reset" />
</div>

</form>

<script>

	$(document).ready(function() {

		$(".date-from").datepicker({maxDate: "{TO_DATE}"});
		$(".date-to").datepicker({minDate: "{FROM_DATE}"});

		$("input[name='filter[dates][add_date][0]']").bind('change', function(){
			currvalue = $("input[name='filter[dates][add_date][0]']").val();
			$("input[name='filter[dates][add_date][0]']").val(currvalue+" 12:00AM");
			$(".date-to").datepicker("option", "minDate", currvalue);
		});

		$("input[name='filter[dates][add_date][1]']").bind('change', function(){
			currvalue = $("input[name='filter[dates][add_date][1]']").val();
			$("input[name='filter[dates][add_date][1]']").val(currvalue+" 12:00PM");
			$("form").submit();
		});

		$("input[name='filter[dates][departure_date][0]']").bind('change', function(){
			currvalue = $("input[name='filter[dates][departure_date][0]']").val();
			$("input[name='filter[dates][departure_date][0]']").val(currvalue+" 12:00AM");
			$(".date-to").datepicker("option", "minDate", currvalue);
		});

		$("input[name='filter[dates][departure_date][1]']").bind('change', function(){
			currvalue = $("input[name='filter[dates][departure_date][1]']").val();
			$("input[name='filter[dates][departure_date][1]']").val(currvalue+" 12:00PM");
			$("form").submit();
		});

	});

</script>