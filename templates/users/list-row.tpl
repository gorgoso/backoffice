<tr class="change{IsBlocked} blocked{-IsBlocked}">
	<td><a href="{PATH_ADMIN}users/privileges/{USER_ID}/"><span title="{LANG_TXT_PRIVILEGES}" class="list"></span></a></td>
	<td><b>{USERNAME}</b></td>
	<td>{GROUP}</td>
	<td>{TXT_REGDATE}</td>
	<td>{TXT_LASTLOG}</td>
	<td><a href="{PATH_ADMIN}users/{CHANGE_STATUS}/{USER_ID}/" onclick="if(!confirm('You want to {CHANGE_STATUS} user {USERNAME}?')) return false;"><span title="Change status of user {USERNAME} to: {CHANGE_STATUS}" class="edit"></span></a></td>
	<td>{SHOW_STATUS}</td>
	<td><a href="{PATH_ADMIN}users/edit/{USER_ID}/"><span title="{LANG_TXT_EDIT_USER}" class="edit"></span></a></td>
	<td><a href="{PATH_ADMIN}users/delete/{USER_ID}/" onclick="if (!confirm('Delete user: {USERNAME}?')) return false"><span title="{LANG_TXT_DELETE_USER}" class="delete"></span></a></td>
</tr>