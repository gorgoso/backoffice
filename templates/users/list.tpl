<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab tabchange">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a href="{PATH_ADMIN}users/add/">Add new user</a>
	</div>
	
	<div class="right">
		<div class="tab tabchange">		
			<div class="limg"></div>
			<div class="rimg"></div>
			<a href="{PATH_ADMIN}users/roles/">Roles</a>
		</div>
	</div>
	
</div>

<form action="{PATH_ADMIN}users/list/" method="post" id="filter">

	<table cellpadding="0" cellspacing="0" class="classic">
		<tr class="title">
			<td>&nbsp;</td>
			<td><a href="{PATH_ADMIN}users/list/?order=username&amp;sort={SORT}">Username</a></td>
			<td><a href="{PATH_ADMIN}users/list/?order=group_id&amp;sort={SORT}">User role</a></td>
			<td><a href="{PATH_ADMIN}users/list/?order=regdate&amp;sort={SORT}">Registration date</a></td>
			<td><a href="{PATH_ADMIN}users/list/?order=lastlog&amp;sort={SORT}">Last login date</a></td>
			<td colspan="2"><a href="{PATH_ADMIN}users/list/?order=status&amp;sort={SORT}">Status</a></td>
			<td colspan="2">Manage</td>
		</tr>
		<tr class="title">
			<td>&nbsp;</td>
			<td><input type="text" style="width:120px" name="filter[username]" value="{USERNAME}" /></td>
			<td><select name="filter[group_id][multiple][]"><option value="">- all -</option>{GROUPS}</select></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="2"><select name="filter[status]"><option value="">- all -</option>{STATUSES}</select></td>
			<td colspan="2">&nbsp;</td>
		</tr>
			{USERS}
		<tr class="list last"><td colspan="9"><div class="count">{COUNT}</div>{LIST}</td></tr>
	</table>

	<div id="btnfilter">
		<input type="submit" value="Set filter" class="submit" />
		<input type="submit" value="Reset filter" class="reset" name="reset" />
	</div>

</form>

<script type="text/javascript" language="JavaScript">
$(function() {
    $('select[name^="filter"]').change(function() {
          $('#filter').submit();
    });
});
</script>