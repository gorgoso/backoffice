<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a>{LANG_TXT_USERNAME}: <b>{USERNAME}</b></a>
	</div>

</div>

<form action="{PATH_ADMIN}password/" method="post">

	<table cellpadding="0" cellspacing="0" class="classic">
		<tr class="first">
			<td class="title" width="30%">{LANG_TXT_PASSWORD}</td>
			<td><input type="password" name="change[password]" /></td>
		</tr>
		<tr>
			<td class="title">{LANG_TXT_PASSWORD2}</td>
			<td><input type="password" name="change[password1]" /></td>
		</tr>
		<tr class="first last-bd">
			<td class="title">&nbsp;</td>
			<td><input class="submit" type="submit" value="{LANG_TXT_CHANGE}" /></td>
		</tr>
	</table>

</form>
