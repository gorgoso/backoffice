<div class="navigation">
	<div class="limg"></div>
	<div class="rimg"></div>

	<div class="tab">
		<div class="limg"></div>
		<div class="rimg"></div>
		<a>{LANG_TXT_NAME}: <b>{NAME}</b></a>
	</div>
</div>

<table class="classic">
	<tr class="first">
		<td class="title" width="30%">{LANG_TXT_USERNAME}</td>
		<td><b>{USERNAME}</b></td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_ROLE}</td>
		<td>{USER_ROLE}</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_REGDATE}</td>
		<td><br />{REGDATE_TXT}</td>
	</tr>
	<tr>
		<td class="title">{LANG_TXT_LASTLOG}</td>
		<td>{LASTLOG_TXT}</td>
	</tr>
	<tr>
		<td class="title"><br />{LANG_TXT_EMAIL}</td>
		<td><br />{EMAIL}</td>
	</tr>
	<tr class="last-bd">
		<td class="title">{LANG_TXT_PHONE}</td>
		<td>{PHONE}</td>
	</tr>
</table>