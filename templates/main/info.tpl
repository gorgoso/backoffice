<form action="{PATH_ADMIN}login/" method="post">
	<input type="hidden" name="redirect" value="{REDIRECT}" />

	<div style="text-align: center; padding: 50px">
		
	<div style="width: 400px; margin:auto; text-align: left">

	<div class="navigation">
		<div class="limg"></div>
		<div class="rimg"></div>
	</div>
		
	<table class="classic">
		<tr class="first">
			<td colspan="2" style="text-align: center">
				<img src="{PATH_ADMIN}graphics/logo.png" alt="Good Shepherd Tours" />
				<br /><br />{LANG_TXT_LOGIN_TEXT}<br /><br />
			</td>
		</tr>
		<tr class="first">
			<td class="title" width="40%"><b>{LANG_TXT_LANGUAGE}</b></td>
			<td>
				<select name="language" style="width: 135px" id="change-language">
					{LANGUAGES}
				</select>
			</td>
		</tr>
		<tr>
			<td class="title" width="40%"><b>{LANG_TXT_USERNAME}</b></td>
			<td><input type="text" name="username" maxlength="16" /></td>
		</tr>
		<tr>
			<td class="title"><b>{LANG_TXT_PASSWORD}</b></td>
			<td><input type="password" name="password" /></td>
		</tr>
		<tr>
			<td class="title"><b>{LANG_TXT_CAPTCHA}</b></td>
			<td>
				<input type="text" name="captcha" style="width: 68px; text-transform:uppercase" maxlength="4" />
				<img src="{PATH_ADMIN}system/captcha/" width="70" height="16" alt="{LANG_TXT_CAPTCHA}" border="1" style="position: relative; top: 3px; left: 7px" maxlength="4" />
			</td>
		</tr>
		<tr class="last-bd">
			<td class="title">&nbsp;</td>
			<td><br /><input type="submit" value="{LANG_TXT_LOGIN}" /></td>
		</tr>
	</table>

	</div>
	</div>
	
</form>

<script type="text/javascript" language="JavaScript">
$(function() {
    $('#change-language').change(function() {
          window.location.href = '{PATH_ADMIN}?language=' + $(this).val();
    });
});
</script>